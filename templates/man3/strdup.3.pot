# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2023-08-27 17:24+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "strdup"
msgstr ""

#. type: TH
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid "2023-07-20"
msgstr ""

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "strdup, strndup, strdupa, strndupa - duplicate a string"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>string.hE<gt>>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<char *strdup(const char *>I<s>B<);>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"B<char *strndup(const char >I<s>B<[.>I<n>B<], size_t >I<n>B<);>\n"
"B<char *strdupa(const char *>I<s>B<);>\n"
"B<char *strndupa(const char >I<s>B<[.>I<n>B<], size_t >I<n>B<);>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<strdup>():"
msgstr ""

#.     || _XOPEN_SOURCE && _XOPEN_SOURCE_EXTENDED
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"    _XOPEN_SOURCE E<gt>= 500\n"
"        || /* Since glibc 2.12: */ _POSIX_C_SOURCE E<gt>= 200809L\n"
"        || /* glibc E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<strndup>():"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"    Since glibc 2.10:\n"
"        _POSIX_C_SOURCE E<gt>= 200809L\n"
"    Before glibc 2.10:\n"
"        _GNU_SOURCE\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "B<strdupa>(), B<strndupa>():"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "    _GNU_SOURCE\n"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<strdup>()  function returns a pointer to a new string which is a "
"duplicate of the string I<s>.  Memory for the new string is obtained with "
"B<malloc>(3), and can be freed with B<free>(3)."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The B<strndup>()  function is similar, but copies at most I<n> bytes.  If "
"I<s> is longer than I<n>, only I<n> bytes are copied, and a terminating null "
"byte (\\[aq]\\e0\\[aq]) is added."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"B<strdupa>()  and B<strndupa>()  are similar, but use B<alloca>(3)  to "
"allocate the buffer."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"On success, the B<strdup>()  function returns a pointer to the duplicated "
"string.  It returns NULL if insufficient memory was available, with I<errno> "
"set to indicate the error."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOMEM>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Insufficient memory available to allocate duplicate string."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr ""

#. type: tbl table
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid ".na\n"
msgstr ""

#. type: tbl table
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid ".nh\n"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"B<strdup>(),\n"
"B<strndup>(),\n"
"B<strdupa>(),\n"
"B<strndupa>()"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr ""

#. type: TP
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "B<strdup>()"
msgstr ""

#. type: TP
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "B<strndup>()"
msgstr ""

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr ""

#. type: TP
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "B<strdupa>()"
msgstr ""

#. #-#-#-#-#  archlinux: strdup.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  fedora-39: strdup.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  fedora-rawhide: strdup.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  opensuse-leap-15-6: strdup.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: tbl table
#. #-#-#-#-#  opensuse-tumbleweed: strdup.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#: archlinux fedora-39 fedora-rawhide opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<strndupa>()"
msgstr ""

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "GNU."
msgstr ""

#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr ""

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "SVr4, 4.3BSD-Reno, POSIX.1-2001."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<alloca>(3), B<calloc>(3), B<free>(3), B<malloc>(3), B<realloc>(3), "
"B<string>(3), B<wcsdup>(3)"
msgstr ""

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "2023-02-05"
msgstr ""

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr ""

#.  4.3BSD-Reno, not (first) 4.3BSD.
#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid ""
"B<strdup>()  conforms to SVr4, 4.3BSD, POSIX.1-2001.  B<strndup>()  conforms "
"to POSIX.1-2008.  B<strdupa>()  and B<strndupa>()  are GNU extensions."
msgstr ""

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.05"
msgstr ""

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "STRDUP"
msgstr ""

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2017-09-15"
msgstr ""

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "GNU"
msgstr ""

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"B<char *strndup(const char *>I<s>B<, size_t >I<n>B<);>\n"
"B<char *strdupa(const char *>I<s>B<);>\n"
"B<char *strndupa(const char *>I<s>B<, size_t >I<n>B<);>\n"
msgstr ""

#.     || _XOPEN_SOURCE\ &&\ _XOPEN_SOURCE_EXTENDED
#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"_XOPEN_SOURCE\\ E<gt>=\\ 500\n"
"    || /* Since glibc 2.12: */ _POSIX_C_SOURCE\\ E<gt>=\\ 200809L\n"
"    || /* Glibc versions E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"
msgstr ""

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "Since glibc 2.10:"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid "_POSIX_C_SOURCE\\ E<gt>=\\ 200809L"
msgstr ""

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "Before glibc 2.10:"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid "_GNU_SOURCE"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<strdupa>(), B<strndupa>(): _GNU_SOURCE"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"The B<strndup>()  function is similar, but copies at most I<n> bytes.  If "
"I<s> is longer than I<n>, only I<n> bytes are copied, and a terminating null "
"byte (\\(aq\\e0\\(aq) is added."
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"B<strdupa>()  and B<strndupa>()  are similar, but use B<alloca>(3)  to "
"allocate the buffer.  They are available only when using the GNU GCC suite, "
"and suffer from the same limitations described in B<alloca>(3)."
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"On success, the B<strdup>()  function returns a pointer to the duplicated "
"string.  It returns NULL if insufficient memory was available, with I<errno> "
"set to indicate the cause of the error."
msgstr ""

#. type: tbl table
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"B<strdup>(),\n"
"B<strndup>(),\n"
"B<strdupa>(),\n"
msgstr ""

#. type: tbl table
#: opensuse-leap-15-6
#, no-wrap
msgid ".br\n"
msgstr ""

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "CONFORMING TO"
msgstr ""

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "COLOPHON"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr ""

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr ""
