# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2023-08-27 17:06+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "lrint"
msgstr ""

#. type: TH
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid "2023-07-20"
msgstr ""

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"lrint, lrintf, lrintl, llrint, llrintf, llrintl - round to nearest integer"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Math library (I<libm>, I<-lm>)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>math.hE<gt>>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"B<long lrint(double >I<x>B<);>\n"
"B<long lrintf(float >I<x>B<);>\n"
"B<long lrintl(long double >I<x>B<);>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"B<long long llrint(double >I<x>B<);>\n"
"B<long long llrintf(float >I<x>B<);>\n"
"B<long long llrintl(long double >I<x>B<);>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "All functions shown above:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "    _ISOC99_SOURCE || _POSIX_C_SOURCE E<gt>= 200112L\n"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"These functions round their argument to the nearest integer value, using the "
"current rounding direction (see B<fesetround>(3))."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Note that unlike the B<rint>(3)  family of functions, the return type of "
"these functions differs from that of their arguments."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "These functions return the rounded integer value."
msgstr ""

#.  The return value is -(LONG_MAX - 1) or -(LLONG_MAX -1)
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If I<x> is a NaN or an infinity, or the rounded value is too large to be "
"stored in a I<long> (I<long long> in the case of the B<ll*> functions), then "
"a domain error occurs, and the return value is unspecified."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"See B<math_error>(7)  for information on how to determine whether an error "
"has occurred when calling these functions."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The following errors can occur:"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Domain error: I<x> is a NaN or infinite, or the rounded value is too large"
msgstr ""

#.  .I errno
#.  is set to
#.  .BR EDOM .
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "An invalid floating-point exception (B<FE_INVALID>)  is raised."
msgstr ""

#. #-#-#-#-#  archlinux: lrint.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  FIXME . Is it intentional that these functions do not set errno?
#.  Bug raised: https://www.sourceware.org/bugzilla/show_bug.cgi?id=6798
#. type: Plain text
#. #-#-#-#-#  debian-bookworm: lrint.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  FIXME . Is it intentional that these functions do not set errno?
#.  Bug raised: http://sources.redhat.com/bugzilla/show_bug.cgi?id=6798
#. type: Plain text
#. #-#-#-#-#  debian-unstable: lrint.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  FIXME . Is it intentional that these functions do not set errno?
#.  Bug raised: http://sources.redhat.com/bugzilla/show_bug.cgi?id=6798
#. type: Plain text
#. #-#-#-#-#  fedora-39: lrint.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  FIXME . Is it intentional that these functions do not set errno?
#.  Bug raised: https://www.sourceware.org/bugzilla/show_bug.cgi?id=6798
#. type: Plain text
#. #-#-#-#-#  fedora-rawhide: lrint.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  FIXME . Is it intentional that these functions do not set errno?
#.  Bug raised: https://www.sourceware.org/bugzilla/show_bug.cgi?id=6798
#. type: Plain text
#. #-#-#-#-#  mageia-cauldron: lrint.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  FIXME . Is it intentional that these functions do not set errno?
#.  Bug raised: http://sources.redhat.com/bugzilla/show_bug.cgi?id=6798
#. type: Plain text
#. #-#-#-#-#  opensuse-leap-15-6: lrint.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  FIXME . Is it intentional that these functions do not set errno?
#.  Bug raised: http://sources.redhat.com/bugzilla/show_bug.cgi?id=6798
#. type: Plain text
#. #-#-#-#-#  opensuse-tumbleweed: lrint.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  FIXME . Is it intentional that these functions do not set errno?
#.  Bug raised: https://www.sourceware.org/bugzilla/show_bug.cgi?id=6798
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "These functions do not set I<errno>."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr ""

#. type: tbl table
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid ".na\n"
msgstr ""

#. type: tbl table
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid ".nh\n"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"B<lrint>(),\n"
"B<lrintf>(),\n"
"B<lrintl>(),\n"
"B<llrint>(),\n"
"B<llrintf>(),\n"
"B<llrintl>()"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr ""

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "C11, POSIX.1-2008."
msgstr ""

#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr ""

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "glibc 2.1.  C99, POSIX.1-2001."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<ceil>(3), B<floor>(3), B<lround>(3), B<nearbyint>(3), B<rint>(3), "
"B<round>(3)"
msgstr ""

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "2022-12-15"
msgstr ""

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "VERSIONS"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron
msgid "These functions were added in glibc 2.1."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid "C99, POSIX.1-2001, POSIX.1-2008."
msgstr ""

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.05"
msgstr ""

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "LRINT"
msgstr ""

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2017-09-15"
msgstr ""

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"B<long int lrint(double >I<x>B<);>\n"
"B<long int lrintf(float >I<x>B<);>\n"
"B<long int lrintl(long double >I<x>B<);>\n"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"B<long long int llrint(double >I<x>B<);>\n"
"B<long long int llrintf(float >I<x>B<);>\n"
"B<long long int llrintl(long double >I<x>B<);>\n"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid "Link with I<-lm>."
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid "_ISOC99_SOURCE || _POSIX_C_SOURCE\\ E<gt>=\\ 200112L"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid "These functions first appeared in glibc in version 2.1."
msgstr ""

#. type: tbl table
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"B<lrint>(),\n"
"B<lrintf>(),\n"
"B<lrintl>(),\n"
msgstr ""

#. type: tbl table
#: opensuse-leap-15-6
#, no-wrap
msgid ".br\n"
msgstr ""

#. type: tbl table
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"B<llrint>(),\n"
"B<llrintf>(),\n"
"B<llrintl>()"
msgstr ""

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "CONFORMING TO"
msgstr ""

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "COLOPHON"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr ""

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr ""
