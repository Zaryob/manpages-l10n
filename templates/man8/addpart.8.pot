# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2023-08-27 16:48+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "ADDPART"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-05-11"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "util-linux 2.38.1"
msgstr ""

#. type: TH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "System Administration"
msgstr ""

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "addpart - tell the kernel about the existence of a partition"
msgstr ""

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<addpart> I<device partition start length>"
msgstr ""

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"B<addpart> tells the Linux kernel about the existence of the specified "
"partition. The command is a simple wrapper around the \"add partition\" "
"ioctl."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "This command doesn\\(cqt manipulate partitions on a block device."
msgstr ""

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "PARAMETERS"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "I<device>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "The disk device."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "I<partition>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "The partition number."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "I<start>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "The beginning of the partition (in 512-byte sectors)."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "I<length>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "The length of the partition (in 512-byte sectors)."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-h>, B<--help>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Display help text and exit."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-V>, B<--version>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Print version and exit."
msgstr ""

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<delpart>(8), B<fdisk>(8), B<parted>(8), B<partprobe>(8), B<partx>(8)"
msgstr ""

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "REPORTING BUGS"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "For bug reports, use the issue tracker at"
msgstr ""

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "AVAILABILITY"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"The B<addpart> command is part of the util-linux package which can be "
"downloaded from"
msgstr ""

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2022-02-14"
msgstr ""

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "util-linux 2.37.4"
msgstr ""
