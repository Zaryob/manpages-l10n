# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2023-08-27 17:09+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "motd"
msgstr ""

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "2022-10-30"
msgstr ""

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6
msgid "motd - message of the day"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
msgid ""
"The contents of I</etc/motd> are displayed by B<login>(1)  after a "
"successful login but just before it executes the login shell."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6
msgid ""
"The abbreviation \"motd\" stands for \"message of the day\", and this file "
"has been traditionally used for exactly that (it requires much less disk "
"space than mail to all users)."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "FILES"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6
msgid "I</etc/motd>"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
msgid "B<login>(1), B<issue>(5)"
msgstr ""

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr ""

#.  Patched in Debian, maybe other distribs
#.  End of patch
#.  .BR login (1)
#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"The contents of I</etc/motd> are displayed by B<pam_motd>(8)  B<login>(1)  "
"after a successful login but just before it executes the login shell."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"On Debian GNU/Linux, dynamic content configured at I</etc/pam.d/login> is "
"also displayed by I<pam_exec>."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "I</etc/pam.d/login>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "B<login>(1), B<issue>(5)  B<pam_motd>(8)"
msgstr ""

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.05"
msgstr ""

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "MOTD"
msgstr ""

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "1992-12-29"
msgstr ""

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux"
msgstr ""

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr ""

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "COLOPHON"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
