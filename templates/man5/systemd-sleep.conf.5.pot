# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2023-08-27 17:28+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "SYSTEMD-SLEEP\\&.CONF"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-39 fedora-rawhide
#, no-wrap
msgid "systemd 254"
msgstr ""

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "systemd-sleep.conf"
msgstr ""

#.  -----------------------------------------------------------------
#.  * MAIN CONTENT STARTS HERE *
#.  -----------------------------------------------------------------
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6
msgid ""
"systemd-sleep.conf, sleep.conf.d - Suspend and hibernation configuration file"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6
msgid "/etc/systemd/sleep\\&.conf"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6
msgid "/etc/systemd/sleep\\&.conf\\&.d/*\\&.conf"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6
msgid "/run/systemd/sleep\\&.conf\\&.d/*\\&.conf"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6
msgid "/usr/lib/systemd/sleep\\&.conf\\&.d/*\\&.conf"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6
msgid "B<systemd> supports four general power-saving modes:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6
msgid "suspend"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6
msgid ""
"a low-power state where execution of the OS is paused, and complete power "
"loss might result in lost data, and which is fast to enter and exit\\&. This "
"corresponds to suspend, standby, or freeze states as understood by the "
"kernel\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6
msgid "hibernate"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6
msgid ""
"a low-power state where execution of the OS is paused, and complete power "
"loss does not result in lost data, and which might be slow to enter and "
"exit\\&. This corresponds to the hibernation as understood by the kernel\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6
msgid "hybrid-sleep"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6
msgid ""
"a low-power state where execution of the OS is paused, which might be slow "
"to enter, and on complete power loss does not result in lost data but might "
"be slower to exit in that case\\&. This mode is called suspend-to-both by "
"the kernel\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6
msgid "suspend-then-hibernate"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
msgid ""
"A low power state where the system is initially suspended (the state is "
"stored in RAM)\\&. If the system supports low-battery alarms (ACPI _BTP), "
"then the system will be woken up by the ACPI low-battery signal and "
"hibernated (the state is then stored on disk)\\&. Also, if not interrupted "
"within the timespan specified by I<HibernateDelaySec=> or the estimated "
"timespan until the system battery charge level goes down to 5%, then the "
"system will be woken up by the RTC alarm and hibernated\\&. The estimated "
"timespan is calculated from the change of the battery capacity level after "
"the time specified by I<SuspendEstimationSec=> or when the system is woken "
"up from the suspend\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6
msgid ""
"Settings in these files determine what strings will be written to /sys/power/"
"disk and /sys/power/state by B<systemd-sleep>(8)  when B<systemd>(1)  "
"attempts to suspend or hibernate the machine\\&. See B<systemd.syntax>(7)  "
"for a general description of the syntax\\&."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "CONFIGURATION DIRECTORIES AND PRECEDENCE"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6
msgid ""
"The default configuration is set during compilation, so configuration is "
"only needed when it is necessary to deviate from those defaults\\&. "
"Initially, the main configuration file in /etc/systemd/ contains commented "
"out entries showing the defaults as a guide to the administrator\\&. Local "
"overrides can be created by editing this file or by creating drop-ins, as "
"described below\\&. Using drop-ins for local configuration is recommended "
"over modifications to the main configuration file\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6
msgid ""
"In addition to the \"main\" configuration file, drop-in configuration "
"snippets are read from /usr/lib/systemd/*\\&.conf\\&.d/, /usr/local/lib/"
"systemd/*\\&.conf\\&.d/, and /etc/systemd/*\\&.conf\\&.d/\\&. Those drop-ins "
"have higher precedence and override the main configuration file\\&. Files in "
"the *\\&.conf\\&.d/ configuration subdirectories are sorted by their "
"filename in lexicographic order, regardless of in which of the "
"subdirectories they reside\\&. When multiple files specify the same option, "
"for options which accept just a single value, the entry in the file sorted "
"last takes precedence, and for options which accept a list of values, "
"entries are collected as they occur in the sorted files\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6
msgid ""
"When packages need to customize the configuration, they can install drop-ins "
"under /usr/\\&. Files in /etc/ are reserved for the local administrator, who "
"may use this logic to override the configuration files installed by vendor "
"packages\\&. Drop-ins have to be used to override package drop-ins, since "
"the main configuration file has lower precedence\\&. It is recommended to "
"prefix all filenames in those subdirectories with a two-digit number and a "
"dash, to simplify the ordering of the files\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6
msgid ""
"To disable a configuration file supplied by the vendor, the recommended way "
"is to place a symlink to /dev/null in the configuration directory in /etc/, "
"with the same filename as the vendor configuration file\\&."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6
msgid ""
"The following options can be configured in the [Sleep] section of /etc/"
"systemd/sleep\\&.conf or a sleep\\&.conf\\&.d file:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6
msgid ""
"I<AllowSuspend=>, I<AllowHibernation=>, I<AllowSuspendThenHibernate=>, "
"I<AllowHybridSleep=>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6
msgid ""
"By default any power-saving mode is advertised if possible (i\\&.e\\&. the "
"kernel supports that mode, the necessary resources are available)\\&. Those "
"switches can be used to disable specific modes\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6
msgid ""
"If I<AllowHibernation=no> or I<AllowSuspend=no> is used, this implies "
"I<AllowSuspendThenHibernate=no> and I<AllowHybridSleep=no>, since those "
"methods use both suspend and hibernation internally\\&.  "
"I<AllowSuspendThenHibernate=yes> and I<AllowHybridSleep=yes> can be used to "
"override and enable those specific modes\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6
msgid "I<SuspendMode=>, I<HibernateMode=>, I<HybridSleepMode=>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
msgid ""
"The string to be written to /sys/power/disk by, respectively, B<systemd-"
"suspend.service>(8), B<systemd-hibernate.service>(8), or B<systemd-hybrid-"
"sleep.service>(8)\\&. More than one value can be specified by separating "
"multiple values with whitespace\\&. They will be tried in turn, until one is "
"written without error\\&. If none of the writes succeed, the operation will "
"be aborted\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
msgid ""
"The allowed set of values is determined by the kernel and is shown in the "
"file itself (use B<cat /sys/power/disk> to display)\\&. See \\m[blue]B<the "
"kernel documentation>\\m[]\\&\\s-2\\u[1]\\d\\s+2 for more details\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6
msgid ""
"B<systemd-suspend-then-hibernate.service>(8)  uses the value of "
"I<SuspendMode=> when suspending and the value of I<HibernateMode=> when "
"hibernating\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6
msgid "I<SuspendState=>, I<HibernateState=>, I<HybridSleepState=>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
msgid ""
"The string to be written to /sys/power/state by, respectively, B<systemd-"
"suspend.service>(8), B<systemd-hibernate.service>(8), or B<systemd-hybrid-"
"sleep.service>(8)\\&. More than one value can be specified by separating "
"multiple values with whitespace\\&. They will be tried in turn, until one is "
"written without error\\&. If none of the writes succeed, the operation will "
"be aborted\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
msgid ""
"The allowed set of values is determined by the kernel and is shown in the "
"file itself (use B<cat /sys/power/state> to display)\\&. See \\m[blue]B<the "
"kernel documentation>\\m[]\\&\\s-2\\u[1]\\d\\s+2 for more details\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6
msgid ""
"B<systemd-suspend-then-hibernate.service>(8)  uses the value of "
"I<SuspendState=> when suspending and the value of I<HibernateState=> when "
"hibernating\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6
msgid "I<HibernateDelaySec=>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
msgid ""
"The amount of time the system spends in suspend mode before the system is "
"automatically put into hibernate mode\\&. Only used by B<systemd-suspend-"
"then-hibernate.service>(8)\\&. If the system has a battery, then defaults to "
"the estimated timespan until the system battery charge level goes down to 5%"
"\\&. If the system has no battery, then defaults to 2h\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
msgid "I<SuspendEstimationSec=>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
msgid ""
"The RTC alarm will wake the system after the specified timespan to measure "
"the system battery capacity level and estimate battery discharging rate, "
"which is used for estimating timespan until the system battery charge level "
"goes down to 5%\\&. Only used by B<systemd-suspend-then-hibernate."
"service>(8)\\&. Defaults to 1h\\&."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "EXAMPLE: FREEZE"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6
msgid ""
"Example: to exploit the \\(lqfreeze\\(rq mode added in Linux 3\\&.9, one can "
"use B<systemctl suspend> with"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid ""
"[Sleep]\n"
"SuspendState=freeze\n"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6
msgid ""
"B<systemd-sleep>(8), B<systemd-suspend.service>(8), B<systemd-hibernate."
"service>(8), B<systemd-hybrid-sleep.service>(8), B<systemd-suspend-then-"
"hibernate.service>(8), B<systemd>(1), B<systemd.directives>(7)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "NOTES"
msgstr ""

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid " 1."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
msgid "the kernel documentation"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
msgid ""
"\\%https://www.kernel.org/doc/html/latest/admin-guide/pm/sleep-states."
"html#basic-sysfs-interfaces-for-system-suspend-and-hibernation"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "systemd 252"
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "systemd 253"
msgstr ""

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "systemd 249"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"A low power state where the system is initially suspended (the state is "
"stored in RAM)\\&. If not interrupted within the delay specified by "
"B<HibernateDelaySec=>, the system will be woken using an RTC alarm and "
"hibernated (the state is then stored on disk)\\&."
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"The string to be written to /sys/power/disk by, respectively, B<systemd-"
"suspend.service>(8), B<systemd-hibernate.service>(8), or B<systemd-hybrid-"
"sleep.service>(8)\\&. More than one value can be specified by separating "
"multiple values with whitespace\\&. They will be tried in turn, until one is "
"written without error\\&. If neither succeeds, the operation will be "
"aborted\\&."
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"The string to be written to /sys/power/state by, respectively, B<systemd-"
"suspend.service>(8), B<systemd-hibernate.service>(8), or B<systemd-hybrid-"
"sleep.service>(8)\\&. More than one value can be specified by separating "
"multiple values with whitespace\\&. They will be tried in turn, until one is "
"written without error\\&. If neither succeeds, the operation will be "
"aborted\\&."
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"The amount of time the system spends in suspend mode before the system is "
"automatically put into hibernate mode, when using B<systemd-suspend-then-"
"hibernate.service>(8)\\&. Defaults to 2h\\&."
msgstr ""
