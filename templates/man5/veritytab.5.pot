# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2023-08-27 17:32+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "VERITYTAB"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-39 fedora-rawhide
#, no-wrap
msgid "systemd 254"
msgstr ""

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "veritytab"
msgstr ""

#.  -----------------------------------------------------------------
#.  * MAIN CONTENT STARTS HERE *
#.  -----------------------------------------------------------------
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6
msgid "veritytab - Configuration for verity block devices"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6
msgid "/etc/veritytab"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6
msgid ""
"The /etc/veritytab file describes verity protected block devices that are "
"set up during system boot\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6
msgid ""
"Empty lines and lines starting with the \"#\" character are ignored\\&. Each "
"of the remaining lines describes one verity protected block device\\&. "
"Fields are delimited by white space\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6
msgid "Each line is in the form"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "I<volume-name> I<data-device> I<hash-device> I<roothash> I<options>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6
msgid "The first four fields are mandatory, the remaining one is optional\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6
msgid ""
"The first field contains the name of the resulting verity volume; its block "
"device is set up below /dev/mapper/\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6
msgid ""
"The second field contains a path to the underlying block data device, or a "
"specification of a block device via \"UUID=\" followed by the UUID\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6
msgid ""
"The third field contains a path to the underlying block hash device, or a "
"specification of a block device via \"UUID=\" followed by the UUID\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6
msgid "The fourth field is the \"roothash\" in hexadecimal\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6
msgid ""
"The fifth field, if present, is a comma-delimited list of options\\&. The "
"following options are recognized:"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-39 fedora-rawhide
msgid "B<superblock=>I<BOOL>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-39 fedora-rawhide
msgid "Use dm-verity with or without permanent on-disk superblock\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-39 fedora-rawhide
msgid "B<format=>I<NUMBER>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-39 fedora-rawhide
msgid ""
"Specifies the hash version type\\&. Format type 0 is original Chrome OS "
"version\\&. Format type 1 is modern version\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-39 fedora-rawhide
msgid "B<data-block-size=>I<BYTES>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-39 fedora-rawhide
msgid ""
"Used block size for the data device\\&. (Note kernel supports only page-size "
"as maximum here; Multiples of 512 bytes\\&.)"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-39 fedora-rawhide
msgid "B<hash-block-size=>I<BYTES>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-39 fedora-rawhide
msgid ""
"Used block size for the hash device\\&. (Note kernel supports only page-size "
"as maximum here; Multiples of 512 bytes\\&.)"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-39 fedora-rawhide
msgid "B<data-blocks=>I<BLOCKS>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-39 fedora-rawhide
msgid ""
"Number of blocks of data device used in verification\\&. If not specified, "
"the whole device is used\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-39 fedora-rawhide
msgid "B<hash-offset=>I<BYTES>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-39 fedora-rawhide
msgid ""
"Offset of hash area/superblock on \"hash-device\"\\&. (Multiples of 512 "
"bytes\\&.)"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-39 fedora-rawhide
msgid "B<salt=>I<HEX>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-39 fedora-rawhide
msgid ""
"Salt used for format or verification\\&. Format is a hexadecimal string; 256 "
"bytes long maximum; \"-\"is the special value for empty\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-39 fedora-rawhide
msgid "B<uuid=>I<UUID>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-39 fedora-rawhide
msgid ""
"Use the provided UUID for format command instead of generating new one\\&. "
"The UUID must be provided in standard UUID format, e\\&.g\\&. "
"12345678-1234-1234-1234-123456789abc\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6
msgid "B<ignore-corruption>, B<restart-on-corruption>, B<panic-on-corruption>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6
msgid ""
"Defines what to do if a data verity problem is detected (data "
"corruption)\\&. Without these options kernel fails the IO operation with I/O "
"error\\&. With \"--ignore-corruption\" option the corruption is only "
"logged\\&. With \"--restart-on-corruption\" or \"--panic-on-corruption\" the "
"kernel is restarted (panicked) immediately\\&. (You have to provide way how "
"to avoid restart loops\\&.)"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6
msgid "B<ignore-zero-blocks>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6
msgid ""
"Instruct kernel to not verify blocks that are expected to contain zeroes and "
"always directly return zeroes instead\\&. WARNING: Use this option only in "
"very specific cases\\&. This option is available since Linux kernel version "
"4\\&.5\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6
msgid "B<check-at-most-once>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6
msgid ""
"Instruct kernel to verify blocks only the first time they are read from the "
"data device, rather than every time\\&. WARNING: It provides a reduced level "
"of security because only offline tampering of the data device\\*(Aqs content "
"will be detected, not online tampering\\&. This option is available since "
"Linux kernel version 4\\&.17\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-39 fedora-rawhide
msgid "B<hash=>I<HASH>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-39 fedora-rawhide
msgid ""
"Hash algorithm for dm-verity\\&. This should be the name of the algorithm, "
"like \"sha1\"\\&. For default see B<veritysetup --help>\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-39 fedora-rawhide
msgid "B<fec-device=>I<PATH>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-39 fedora-rawhide
msgid ""
"Use forward error correction (FEC) to recover from corruption if hash "
"verification fails\\&. Use encoding data from the specified device\\&. The "
"fec device argument can be block device or file image\\&. For format, if fec "
"device path doesn\\*(Aqt exist, it will be created as file\\&. Note: block "
"sizes for data and hash devices must match\\&. Also, if the verity "
"data_device is encrypted the fec_device should be too\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-39 fedora-rawhide
msgid "B<fec-offset=>I<BYTES>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-39 fedora-rawhide
msgid ""
"This is the offset, in bytes, from the start of the FEC device to the "
"beginning of the encoding data\\&. (Aligned on 512 bytes\\&.)"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-39 fedora-rawhide
msgid "B<fec-roots=>I<NUM>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-39 fedora-rawhide
msgid ""
"Number of generator roots\\&. This equals to the number of parity bytes in "
"the encoding data\\&. In RS(M, N) encoding, the number of roots is M-N\\&. M "
"is 255 and M-N is between 2 and 24 (including)\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
msgid "B<root-hash-signature=>I<PATH>B<|base64:>I<HEX>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6
msgid ""
"A base64 string encoding the root hash signature prefixed by \"base64:\" or "
"a path to roothash signature file used to verify the root hash (in "
"kernel)\\&. This feature requires Linux kernel version 5\\&.4 or more "
"recent\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6
msgid "B<_netdev>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6
msgid ""
"Marks this veritysetup device as requiring network\\&. It will be started "
"after the network is available, similarly to B<systemd.mount>(5)  units "
"marked with B<_netdev>\\&. The service unit to set up this device will be "
"ordered between remote-fs-pre\\&.target and remote-veritysetup\\&.target, "
"instead of veritysetup-pre\\&.target and veritysetup\\&.target\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6
msgid ""
"Hint: if this device is used for a mount point that is specified in "
"B<fstab>(5), the B<_netdev> option should also be used for the mount "
"point\\&. Otherwise, a dependency loop might be created where the mount "
"point will be pulled in by local-fs\\&.target, while the service to "
"configure the network is usually only started I<after> the local file system "
"has been mounted\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6
msgid "B<noauto>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6
msgid ""
"This device will not be added to veritysetup\\&.target\\&. This means that "
"it will not be automatically enabled on boot, unless something else pulls it "
"in\\&. In particular, if the device is used for a mount point, it\\*(Aqll be "
"enabled automatically during boot, unless the mount point itself is also "
"disabled with B<noauto>\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6
msgid "B<nofail>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6
msgid ""
"This device will not be a hard dependency of veritysetup\\&.target\\&. "
"It\\*(Aqll still be pulled in and started, but the system will not wait for "
"the device to show up and be enabled, and boot will not fail if this is "
"unsuccessful\\&. Note that other units that depend on the enabled device may "
"still fail\\&. In particular, if the device is used for a mount point, the "
"mount point itself also needs to have the B<nofail> option, or the boot will "
"fail if the device is not enabled successfully\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6
msgid "B<x-initrd\\&.attach>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
msgid ""
"Setup this verity protected block device in the initrd, similarly to "
"B<systemd.mount>(5)  units marked with B<x-initrd\\&.mount>\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6
msgid ""
"Although it\\*(Aqs not necessary to mark the mount entry for the root file "
"system with B<x-initrd\\&.mount>, B<x-initrd\\&.attach> is still recommended "
"with the verity protected block device containing the root file system as "
"otherwise systemd will attempt to detach the device during the regular "
"system shutdown while it\\*(Aqs still in use\\&. With this option the device "
"will still be detached but later after the root file system is unmounted\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
msgid ""
"All other verity protected block devices that contain file systems mounted "
"in the initrd should use this option\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6
msgid ""
"At early boot and when the system manager configuration is reloaded, this "
"file is translated into native systemd units by B<systemd-veritysetup-"
"generator>(8)\\&."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "EXAMPLES"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6
msgid "B<Example\\ \\&1.\\ \\&/etc/veritytab example>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6
msgid ""
"Set up two verity protected block devices\\&. One using device blocks, "
"another using files\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid ""
"usr  PARTUUID=783e45ae-7aa3-484a-beef-a80ff9c19cbb PARTUUID=21dc1dfe-4c33-8b48-98a9-918a22eb3e37 36e3f740ad502e2c25e2a23d9c7c17bf0fdad2300b7580842d4b7ec1fb0fa263 auto\n"
"data /etc/data /etc/hash a5ee4b42f70ae1f46a08a7c92c2e0a20672ad2f514792730f5d49d7606ab8fdf auto\n"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6
msgid ""
"B<systemd>(1), B<systemd-veritysetup@.service>(8), B<systemd-veritysetup-"
"generator>(8), B<fstab>(5), B<veritysetup>(8),"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "systemd 252"
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "systemd 253"
msgstr ""

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "systemd 249"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<root-hash-signature=>"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Setup this verity protected block device in the initramfs, similarly to "
"B<systemd.mount>(5)  units marked with B<x-initrd\\&.mount>\\&."
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"All other verity protected block devices that contain file systems mounted "
"in the initramfs should use this option\\&."
msgstr ""
