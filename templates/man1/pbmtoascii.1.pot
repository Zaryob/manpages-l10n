# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2023-08-27 17:12+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Pbmtoascii User Manual"
msgstr ""

#. type: TH
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "02 April 2010"
msgstr ""

#. type: TH
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "netpbm documentation"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "pbmtoascii - convert a PBM image to ASCII graphics"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<pbmtoascii>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "[B<-1x2>|B<-2x4>]"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "[I<pbmfile>]"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "This program is part of B<Netpbm>(1)\\&."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<pbmtoascii> reads a PBM image as input and produces a somewhat crude ASCII "
"graphic image as output."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "To convert back, use B<asciitopgm>(1)\\&."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<ppmtoterm> does a similar thing for color images to be displayed on color "
"text terminals."
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"In addition to the options common to all programs based on libnetpbm\n"
"(most notably B<-quiet>, see \n"
"E<.UR index.html#commonoptions>\n"
" Common Options\n"
"E<.UE>\n"
"\\&), B<pbmtoascii> recognizes the following\n"
"command line options:\n"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<-1x2> and B<-2x4> options give you two alternate ways for the pixels "
"to get mapped to characters.  With B<1x2>, the default, each character "
"represents a group of 1 pixel across by 2 pixels down.  With B<-2x4>, each "
"character represents 2 pixels across by 4 pixels down.  With the 1x2 mode "
"you can see the individual pixels, so it's useful for previewing small "
"images on a non-graphics terminal.  The 2x4 mode lets you display larger "
"images on a small display, but it obscures pixel-level details.  2x4 mode is "
"also good for displaying PGM images:"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "pamscale -width 158 | pnmnorm | pamditherbw -threshold | pbmtoascii -2x4\n"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "should give good results."
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide mageia-cauldron
msgid "B<asciitopgm>(1)\\& B<ppmtoascii>(1)\\& B<ppmtoterm>(1)\\& B<pbm>(1)\\&"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "Copyright (C) 1988, 1992 by Jef Poskanzer."
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DOCUMENT SOURCE"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This manual page was generated by the Netpbm tool 'makeman' from HTML "
"source.  The master documentation is at"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<http://netpbm.sourceforge.net/doc/pbmtoascii.html>"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<asciitopgm>(1)\\& B<ppmtoascii>(1)\\& B<ppmtoterm>(1)\\& B<pbm>(5)\\&"
msgstr ""
