# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2023-08-10 18:58+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "LSFD"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-08-04"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "util-linux 2.38.1"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "User Commands"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "lsfd - list file descriptors"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<lsfd> [option]"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"B<lsfd> is intended to be a modern replacement for B<lsof>(8) on Linux "
"systems.  Unlike B<lsof>, B<lsfd> is specialized to Linux kernel; it "
"supports Linux specific features like namespaces with simpler code. B<lsfd> "
"is not a drop-in replacement for B<lsof>; they are different in the command "
"line interface and output formats."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"B<lsfd> uses Libsmartcols for output formatting and filtering. See the "
"description of B<--output> option for customizing the output format, and B<--"
"filter> option for filtering."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-l>, B<--threads>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "List in threads level."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-J>, B<--json>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Use JSON output format."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-n>, B<--noheadings>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Don\\(cqt print headings."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-o>, B<--output> I<list>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Specify which output columns to print. See the B<OUTPUT COLUMNS> section for "
"details of available columns."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The default list of columns may be extended if I<list> is specified in the "
"format I<+list> (e.g., B<lsfd -o +DELETED>)."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-r>, B<--raw>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Use raw output format."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<--notruncate>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Don\\(cqt truncate text in columns."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-p>, B<--pid> I<pids>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Collect information only for specified processes.  I<pids> is a list of "
"pids. A comma or whitespaces can be used as separators.  You can use this "
"option with B<pidof>(1). See B<FILTER EXAMPLES>."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Both B<-Q> option with an expression including PID, e.g. -Q (PID == 1), and "
"B<-p> option, e.g. -p 1, may print the same output but using B<-p> option is "
"much more efficient because B<-p> option works at a much earlier stage of "
"processing than the B<-Q> option."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-Q>, B<--filter> I<expr>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Print only the files matching the condition represented by the I<expr>.  See "
"also B<FILTER EXAMPLES>."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-C>, B<--counter> I<label>:I<filter_expr>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Define a custom counter used in B<--summary> output. B<lsfd> makes a counter "
"named I<label>. During collect information, B<lsfd> counts files matching "
"I<filter_expr>, and stores the counted number to the counter named I<label>. "
"B<lsfd> applies filters defined with B<--filter> options before counting; "
"files excluded by the filters are not counted."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"See B<FILTER EXPRESSION> about I<filter_expr>.  I<label> should not include "
"I<{> nor I<:>. You can define multiple counters by specifying this option "
"multiple times."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "See also B<COUNTER EXAMPLES>."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<--summary>[=I<when>]"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"This option controls summary lines output. The optional argument I<when> can "
"be B<only>, B<append> or B<never>. If the I<when> argument is omitted, it "
"defaults to B<only>."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The summary reports counters. A counter consists of a label and an integer "
"value.  B<--counter> is the option for defining a counter.  If a user "
"defines no counter, B<lsfd> uses the definitions of pre-defined built-in "
"counters (default counters) to make the summary output."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"CAUTION: Using B<--summary> and B<--json> may make the output broken. Only "
"combining B<--summary>=B<only> and B<--json> is valid."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<--debug-filter>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Dump the internal data structure for the filter and exit. This is useful "
"only for B<lsfd> developers."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<--dump-counters>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Dump the definition of counters used in B<--summary> output."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-h>, B<--help>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Display help text and exit."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-V>, B<--version>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Print version and exit."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "OUTPUT COLUMNS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Each column has a type. Types are surround by E<lt> and E<gt>."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"CAUTION: The names and types of columns are not stable yet.  They may be "
"changed in the future releases."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "ASSOC E<lt>I<string>E<gt>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Association between file and process."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "BLKDRV E<lt>I<string>E<gt>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Block device driver name resolved by I</proc/devices>."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "CHRDRV E<lt>I<string>E<gt>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Character device driver name resolved by I</proc/devices>."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "COMMAND E<lt>I<string>E<gt>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Command of the process opening the file."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "DELETED E<lt>I<boolean>E<gt>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Reachability from the file system."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "DEV E<lt>I<string>E<gt>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "ID of the device containing the file."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "DEVTYPE E<lt>I<string>E<gt>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Device type (I<blk>, I<char>, or I<nodev>)."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "FD E<lt>I<number>E<gt>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "File descriptor for the file."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "FLAGS E<lt>I<string>E<gt>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Flags specified when opening the file."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "FUID E<lt>I<number>E<gt>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "User ID number of the file\\(cqs owner."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "INODE E<lt>I<number>E<gt>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Inode number."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "KTHREAD E<lt>I<boolean>E<gt>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Whether the process is a kernel thread or not."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "MAJ:MIN E<lt>I<string>E<gt>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Device ID for special, or ID of device containing file."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "MAPLEN E<lt>I<number>E<gt>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Length of file mapping (in page)."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "MISCDEV E<lt>I<string>E<gt>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Misc character device name resolved by I</proc/misc>."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "MNTID E<lt>I<number>E<gt>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Mount ID."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "MODE E<lt>I<string>E<gt>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Access mode (rwx)."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "NAME E<lt>I<string>E<gt>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Name of the file."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "NLINK E<lt>I<number>E<gt>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Link count."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "OWNER E<lt>I<string>E<gt>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Owner of the file."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "PARTITION E<lt>I<string>E<gt>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Block device name resolved by I</proc/partition>."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "PID E<lt>I<number>E<gt>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "PID of the process opening the file."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "POS E<lt>I<number>E<gt>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "File position."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "PROTONAME E<lt>I<string>E<gt>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Protocol name."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "RDEV E<lt>I<string>E<gt>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Device ID (if special file)."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "SIZE E<lt>I<number>E<gt>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "File size."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "SOURCE E<lt>I<string>E<gt>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "File system, partition, or device containing the file."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "TID E<lt>I<number>E<gt>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Thread ID of the process opening the file."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "TYPE E<lt>I<string>E<gt>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "File type."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "UID E<lt>I<number>E<gt>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "User ID number."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "USER E<lt>I<string>E<gt>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "User of the process."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "FILTER EXPRESSION"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"B<lsfd> evaluates the expression passed to B<--filter> option every time "
"before printing a file line. B<lsfd> prints the line only if the result of "
"evaluation is I<true>."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"An expression consists of column names, literals and, operators like: "
"\\f(CRDELETED\\fR, \\f(CR(PID == 1)\\fR, \\f(CR(NAME == \"/etc/"
"passwd\")\\fR, \\f(CR(PID == 1) && DELETED\\fR.  \\f(CRDELETED\\fR, "
"\\f(CRPID\\fR, and \\f(CRNAME\\fR are column names in the example.  "
"\\f(CR1\\fR and \"/etc/passwd\" are literals.  \\f(CR==\\fR and \\f(CR&&\\fR "
"are operators."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Before evaluation, B<lsfd> substitutes column names in the given expression "
"with actual column values in the line. There are three different data types: "
"I<boolean>, I<string>, and I<number>.  For columns with a I<boolean> type, "
"the value can be stand-alone.  For I<string> and I<number> values, the value "
"must be an operand of an operator, for example, \\f(CR(PID == 1)\\fR. See "
"the \"OUTPUT COLUMNS\" about the types of columns."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Literal is for representing a value directly. See BOOLLIT, STRLIT, and "
"NUMLIT. Different data types have different literal syntax."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"An operator works with one or two operand(s). An operator has an expectation "
"about the data type(s) of its operands. Giving an unexpected data type to an "
"operator causes a syntax error."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Operators taking two operands are I<and>, I<or>, I<eq>, I<ne>, I<le>, I<lt>, "
"I<ge>, I<gt>, I<=~>, I<!~>.  Alphabetically named operators have C-language "
"flavored aliases: I<&&>, I<||>, I<==>, I<!=>, I<E<lt>>, I<\\(lA>, I<E<gt>=>, "
"and I<E<gt>>."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "I<!> is the only operator that takes one operand."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"I<eq>, I<ne>, and their aliases expect operands have the same data type.  "
"Applying these operators return a I<boolean>."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"I<and>, I<or>, I<not> and their aliases expect operands have I<bool> data "
"type. Applying these operators return a I<boolean>."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"I<lt>, I<le>, I<gt>, I<ge>, and their aliases expect operands have I<number> "
"data types. Applying these operators return a I<boolean>."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"I<=~> is for regular expression matching; if a string at the right side "
"matches a regular expression at the left side, the result is true.  The "
"right side operand must be a string literal. See STRLIT about the syntax."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"I<!~> is a short-hand version of \\f(CRnot (STR =~ PAT)\\fR; it inverts the "
"result of I<=~>."
msgstr ""

#. type: SS
#: debian-bookworm
#, no-wrap
msgid "Limitations"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The current implementation does not define precedences within operators.  "
"Use I<(> and I<)> explicitly for grouping the sub-expressions if your "
"expression uses more than two operators."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"About I<number> typed values, the filter engine supports only non-negative "
"integers."
msgstr ""

#. type: SS
#: debian-bookworm
#, no-wrap
msgid "Semi-formal syntax"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "EXPR"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "BOOLEXP"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "BOOLEXP0"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "COLUMN E<lt>I<boolean>E<gt> | BOOLLIT | I<(> BOOLEXP I<)>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "BOOLEXP0 | BOOLOP1 | BOOLOP2 | BOOLOP2BL | BOOLOP2CMP | BOOLOP2REG"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "COLUMN"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "[_A-Za-z][-_:A-Za-z0-9]*"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "BOOLOP1"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "I<!> BOOLEXP0 | I<not> BOOLEXP0"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "STREXP"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "COLUMN E<lt>I<string>E<gt> | STRLIT"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "NUMEXP"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "COLUMN E<lt>I<number>E<gt> | NUMLIT"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "BOOLLIT"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "I<true> | I<false>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "CHARS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "( [^\\(rs] | I<\\(rs\\(rs> | I<\\(rs\\(aq> | I<\\(rs\"> )*"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "STRLIT"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "I<\\(aq> CHARS I<\\(aq> | I<\"> CHARS I<\">"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "NUMLIT"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "[1-9][0-9]* | I<0>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "BOOLOP2"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "STREXP OP2 STREXP | NUMEXP OP2 NUMEXP | BOOLEXP0 OP2 BOOLEXP0"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "OP2"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "I<==> | I<eq> | I<!=> | I<ne>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "BOOLOP2BL"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "BOOLEXP0 OP2BL BOOLEXP0"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "OP2BL"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "I<&&> | I<and> | I<||> | I<or>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "BOOLOP2CMP"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "NUMEXP OP2CMP NUMEXP"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "OP2CMP"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"I<E<lt>> | I<lt> | I<E<lt>=> | I<le> | I<E<gt>> | I<gt> | I<E<gt>=> | I<ge>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "BOOLOP2REG"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "STREXP OP2REG STRLIT"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "OP2REG"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "I<=~> | I<!~>"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "FILTER EXAMPLES"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"B<lsfd> has few options for filtering. In most of cases, what you should "
"know is B<-Q> (or B<--filter>) option.  Combined with B<-o> (or B<--output>) "
"option, you can customize the output as you want."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "List files associated with PID 1 and PID 2 processes:"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid "# lsfd -Q \\(aq(PID == 1) or (PID == 2)\\(aq\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Do the same in an alternative way:"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid "# lsfd -Q \\(aq(PID == 1) || (PID == 2)\\(aq\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Do the same in a more efficient way:"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid "# lsfd --pid 1,2\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Whitescapes can be used instead of a comma:"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid "# lsfd --pid \\(aq1 2\\(aq\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Utilize B<pidof>(1) for list the files associated with \"firefox\":"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid "# lsfd --pid \"$(pidof firefox)\"\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "List the 1st file descriptor opened by PID 1 process:"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid "# lsfd -Q \\(aq(PID == 1) and (FD == 1)\\(aq\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid "# lsfd -Q \\(aq(PID == 1) && (FD == 1)\\(aq\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "List all running executables:"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid "# lsfd -Q \\(aqASSOC == \"exe\"\\(aq\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid "# lsfd -Q \\(aqASSOC eq \"exe\"\\(aq\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Do the same but print only file names:"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid "# lsfd -o NAME -Q \\(aqASSOC eq \"exe\"\\(aq | sort -u\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "List deleted files associated to processes:"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid "# lsfd -Q \\(aqDELETED\\(aq\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "List non-regular files:"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid "# lsfd -Q \\(aqTYPE != \"REG\"\\(aq\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "List block devices:"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid "# lsfd -Q \\(aqDEVTYPE == \"blk\"\\(aq\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Do the same with TYPE column:"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid "# lsfd -Q \\(aqTYPE == \"BLK\"\\(aq\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "List files including \"dconf\" directory in their names:"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid "# lsfd -Q \\(aqNAME =~ \".\\(rs*/dconf/.*\"\\(aq\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "List files opened in a QEMU virtual machine:"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid "# lsfd -Q \\(aq(COMMAND =~ \".\\(rs*qemu.*\") and (FD E<gt>= 0)\\(aq\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Hide files associated to kernel threads:"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid "# lsfd -Q \\(aq!KTHREAD\\(aq\n"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "COUNTER EXAMPLES"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Report the numbers of netlink socket descriptors and unix socket descriptors:"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"# lsfd --summary=only \\(rs\n"
"        -C \\(aqnetlink sockets\\(aq:\\(aq(NAME =~ \"NETLINK:.*\")\\(aq \\(rs\n"
"        -C \\(aqunix sockets\\(aq:\\(aq(NAME =~ \"UNIX:.*\")\\(aq\n"
"VALUE COUNTER\n"
"   57 netlink sockets\n"
" 1552 unix sockets\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Do the same but print in JSON format:"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"# lsfd --summary=only --json \\(rs\n"
"        -C \\(aqnetlink sockets\\(aq:\\(aq(NAME =~ \"NETLINK:.*\")\\(aq \\(rs\n"
"        -C \\(aqunix sockets\\(aq:\\(aq(NAME =~ \"UNIX:.*\")\\(aq\n"
"{\n"
"   \"lsfd-summary\": [\n"
"      {\n"
"         \"value\": 15,\n"
"         \"counter\": \"netlink sockets\"\n"
"      },{\n"
"         \"value\": 798,\n"
"         \"counter\": \"unix sockets\"\n"
"      }\n"
"   ]\n"
"}\n"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "HISTORY"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "The B<lsfd> command is part of the util-linux package since v2.38."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "AUTHORS"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<lsof>(8)  B<pidof>(1)  B<proc>(5)"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "REPORTING BUGS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "For bug reports, use the issue tracker at"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "AVAILABILITY"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The B<lsfd> command is part of the util-linux package which can be "
"downloaded from"
msgstr ""
