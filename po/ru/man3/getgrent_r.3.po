# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Azamat Hackimov <azamat.hackimov@gmail.com>, 2013-2014, 2017.
# Dmitry Bolkhovskikh <d20052005@yandex.ru>, 2017.
# Vladislav <ivladislavefimov@gmail.com>, 2015.
# Yuri Kozlov <yuray@komyakino.ru>, 2011-2019.
# Иван Павлов <pavia00@gmail.com>, 2017, 2019.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-08-27 16:59+0200\n"
"PO-Revision-Date: 2019-10-05 08:06+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <man-pages-ru-talks@lists.sourceforge.net>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<getgrent_r>()"
msgid "getgrent_r"
msgstr "B<getgrent_r>()"

#. type: TH
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid "2023-07-20"
msgstr "20 июля 2023 г."

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Linux man-pages 6.05.01"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "ИМЯ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "getgrent_r, fgetgrent_r - get group file entry reentrantly"
msgstr ""
"getgrent_r, fgetgrent_r - возвращает запись из файла групп (реентерабельные "
"версии)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "СИНТАКСИС"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>grp.hE<gt>>\n"
msgstr "B<#include E<lt>grp.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"B<int getgrent_r(struct group *restrict >I<gbuf>B<,>\n"
"B<               char >I<buf>B<[restrict .>I<buflen>B<], size_t >I<buflen>B<,>\n"
"B<               struct group **restrict >I<gbufp>B<);>\n"
"B<int fgetgrent_r(FILE *restrict >I<stream>B<, struct group *restrict >I<gbuf>B<,>\n"
"B<               char >I<buf>B<[restrict .>I<buflen>B<], size_t >I<buflen>B<,>\n"
"B<               struct group **restrict >I<gbufp>B<);>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Требования макроса тестирования свойств для glibc (см. "
"B<feature_test_macros>(7)):"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid "B<getgrent_r>()"
msgid "B<getgrent_r>():"
msgstr "B<getgrent_r>()"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "    _GNU_SOURCE\n"
msgstr "    _GNU_SOURCE\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid "B<fgetgrent_r>()"
msgid "B<fgetgrent_r>():"
msgstr "B<fgetgrent_r>()"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"    Since glibc 2.19:\n"
"        _DEFAULT_SOURCE\n"
"    glibc 2.19 and earlier:\n"
"        _SVID_SOURCE\n"
msgstr ""
"    начиная с glibc 2.19:\n"
"        _DEFAULT_SOURCE\n"
"    glibc 2.19 и старее:\n"
"        _SVID_SOURCE\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The functions B<getgrent_r>()  and B<fgetgrent_r>()  are the reentrant "
"versions of B<getgrent>(3)  and B<fgetgrent>(3).  The former reads the next "
"group entry from the stream initialized by B<setgrent>(3).  The latter reads "
"the next group entry from I<stream>."
msgstr ""
"Функции B<getgrent_r>() и B<fgetgrent_r>() являются реентерабельными "
"версиями B<getgrent>(3) и B<fgetgrent>(3). Первая читает следующую запись "
"группы из потока, инициализированного B<setgrent>(3). Последняя читает "
"следующую запись группы из I<stream>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The I<group> structure is defined in I<E<lt>grp.hE<gt>> as follows:"
msgstr "Структура I<group> определена в I<E<lt>grp.hE<gt>> следующим образом:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"struct group {\n"
"    char   *gr_name;        /* group name */\n"
"    char   *gr_passwd;      /* group password */\n"
"    gid_t   gr_gid;         /* group ID */\n"
"    char  **gr_mem;         /* NULL-terminated array of pointers\n"
"                               to names of group members */\n"
"};\n"
msgstr ""
"struct group {\n"
"    char   *gr_name;        /* имя группы */\n"
"    char   *gr_passwd;      /* пароль группы */\n"
"    gid_t   gr_gid;         /* ID группы */\n"
"    char  **gr_mem;         /* массив, указателей\n"
"                               имён членов группы, оканчивающийся NULL */\n"
"};\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"For more information about the fields of this structure, see B<group>(5)."
msgstr "Подробней о полях этой структуры смотрите в B<group>(5)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The nonreentrant functions return a pointer to static storage, where this "
#| "static storage contains further pointers to group name, password and "
#| "members.  The reentrant functions described here return all of that in "
#| "caller-provided buffers.  First of all there is the buffer I<gbuf> that "
#| "can hold a I<struct group>.  And next the buffer I<buf> of size I<buflen> "
#| "that can hold additional strings.  The result of these functions, the "
#| "I<struct group> read from the stream, is stored in the provided buffer "
#| "I<*gbuf>, and a pointer to this I<struct group> is returned in I<*gbufp>."
msgid ""
"The nonreentrant functions return a pointer to static storage, where this "
"static storage contains further pointers to group name, password, and "
"members.  The reentrant functions described here return all of that in "
"caller-provided buffers.  First of all there is the buffer I<gbuf> that can "
"hold a I<struct group>.  And next the buffer I<buf> of size I<buflen> that "
"can hold additional strings.  The result of these functions, the I<struct "
"group> read from the stream, is stored in the provided buffer I<*gbuf>, and "
"a pointer to this I<struct group> is returned in I<*gbufp>."
msgstr ""
"Нереентерабельные версии возвращают указатель на статическое хранилище, в "
"котором хранятся другие указатели на имя группы, пароль и список членов. "
"Реентерабельные функции, описанные здесь, возвращают всю информацию в "
"буферах, предоставленных вызывающим. Основным буфером является I<gbuf>, в "
"котором может храниться I<struct group>. В дополнительном буфере I<buf> "
"размера I<buflen> могут храниться дополнительные строки. Результат этих "
"функций, прочитанная из потока I<struct group>, сохраняется в "
"предоставляемый буфер I<*gbuf>, и указатель на эту I<struct group> "
"возвращается в I<*gbufp>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "ВОЗВРАЩАЕМОЕ ЗНАЧЕНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"On success, these functions return 0 and I<*gbufp> is a pointer to the "
"I<struct group>.  On error, these functions return an error value and "
"I<*gbufp> is NULL."
msgstr ""
"При успешном выполнении эти функции возвращают 0 и I<*gbufp> указывает на "
"I<struct group>. При ошибке возвращается значение ошибки и I<*gbufp> равен "
"NULL."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ОШИБКИ"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOENT>"
msgstr "B<ENOENT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "No more entries."
msgstr "Больше записей нет."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ERANGE>"
msgstr "B<ERANGE>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Insufficient buffer space supplied.  Try again with larger buffer."
msgstr "Недостаточно места в буфере. Попробуйте ещё раз с большим буфером."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "АТРИБУТЫ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr "Описание терминов данного раздела смотрите в B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Интерфейс"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Атрибут"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Значение"

#. type: tbl table
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<getgrent_r>()"
msgstr "B<getgrent_r>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Безвредность в нитях"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "MT-Unsafe race:grent locale"
msgstr "MT-Unsafe race:grent locale"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<fgetgrent_r>()"
msgstr "B<fgetgrent_r>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "In the above table, I<grent> in I<race:grent> signifies that if any of "
#| "the functions B<setgrent>(), B<getgrent>(), B<endgrent>(), or "
#| "B<getgrent_r>()  are used in parallel in different threads of a program, "
#| "then data races could occur."
msgid ""
"In the above table, I<grent> in I<race:grent> signifies that if any of the "
"functions B<setgrent>(3), B<getgrent>(3), B<endgrent>(3), or "
"B<getgrent_r>()  are used in parallel in different threads of a program, "
"then data races could occur."
msgstr ""
"В приведённой выше таблице I<grent> в I<race:grent> означает, что если в "
"нескольких нитях программы одновременно используются функции B<setgrent>(), "
"B<getgrent>(), B<endgrent>() или B<getgrent_r>(), то может возникнуть "
"состязательность по данным."

#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "ВЕРСИИ"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "Other systems use the prototype"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"struct group *getgrent_r(struct group *grp, char *buf,\n"
"                         int buflen);\n"
msgstr ""
"struct group *getgrent_r(struct group *grp, char *buf,\n"
"                         int buflen);\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "or, better,"
msgstr "или, лучше,"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"int getgrent_r(struct group *grp, char *buf, int buflen,\n"
"               FILE **gr_fp);\n"
msgstr ""
"int getgrent_r(struct group *grp, char *buf, int buflen,\n"
"               FILE **gr_fp);\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "СТАНДАРТЫ"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "GNU."
msgstr "GNU."

#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "ИСТОРИЯ"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "These functions are GNU extensions, done in a style resembling the POSIX "
#| "version of functions like B<getpwnam_r>(3).  Other systems use the "
#| "prototype"
msgid ""
"These functions are done in a style resembling the POSIX version of "
"functions like B<getpwnam_r>(3)."
msgstr ""
"Эти функции являются расширениями GNU; они выполнены похожими на POSIX-"
"версию функции B<getpwnam_r>(3). В других системах используется прототип"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "ЗАМЕЧАНИЯ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The function B<getgrent_r>()  is not really reentrant since it shares the "
"reading position in the stream with all other threads."
msgstr ""
"Функция B<getgrent_r>() не совсем реентерабельна, так как она использует "
"общую позицию чтения в потоке с другими нитями."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "ПРИМЕРЫ"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide
#, fuzzy, no-wrap
#| msgid ""
#| "    setgrent();\n"
#| "    while (1) {\n"
#| "        i = getgrent_r(&grp, buf, BUFLEN, &grpp);\n"
#| "        if (i)\n"
#| "            break;\n"
#| "        printf(\"%s (%d):\", grpp-E<gt>gr_name, grpp-E<gt>gr_gid);\n"
#| "        for (i = 0; ; i++) {\n"
#| "            if (grpp-E<gt>gr_mem[i] == NULL)\n"
#| "                break;\n"
#| "            printf(\" %s\", grpp-E<gt>gr_mem[i]);\n"
#| "        }\n"
#| "        printf(\"\\en\");\n"
#| "    }\n"
#| "    endgrent();\n"
#| "    exit(EXIT_SUCCESS);\n"
#| "}\n"
msgid ""
"#define _GNU_SOURCE\n"
"#include E<lt>grp.hE<gt>\n"
"#include E<lt>stdint.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#define BUFLEN 4096\n"
"\\&\n"
"int\n"
"main(void)\n"
"{\n"
"    struct group grp;\n"
"    struct group *grpp;\n"
"    char buf[BUFLEN];\n"
"    int i;\n"
"\\&\n"
"    setgrent();\n"
"    while (1) {\n"
"        i = getgrent_r(&grp, buf, sizeof(buf), &grpp);\n"
"        if (i)\n"
"            break;\n"
"        printf(\"%s (%jd):\", grpp-E<gt>gr_name, (intmax_t) grpp-E<gt>gr_gid);\n"
"        for (size_t j = 0; ; j++) {\n"
"            if (grpp-E<gt>gr_mem[j] == NULL)\n"
"                break;\n"
"            printf(\" %s\", grpp-E<gt>gr_mem[j]);\n"
"        }\n"
"        printf(\"\\en\");\n"
"    }\n"
"    endgrent();\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""
"    setgrent();\n"
"    while (1) {\n"
"        i = getgrent_r(&grp, buf, BUFLEN, &grpp);\n"
"        if (i)\n"
"            break;\n"
"        printf(\"%s (%d):\", grpp-E<gt>gr_name, grpp-E<gt>gr_gid);\n"
"        for (i = 0; ; i++) {\n"
"            if (grpp-E<gt>gr_mem[i] == NULL)\n"
"                break;\n"
"            printf(\" %s\", grpp-E<gt>gr_mem[i]);\n"
"        }\n"
"        printf(\"\\en\");\n"
"    }\n"
"    endgrent();\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"

#. #-#-#-#-#  archlinux: getgrent_r.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  perhaps add error checking - should use strerror_r
#.  #include <errno.h>
#.  #include <stdlib.h>
#.          if (i) {
#.                if (i == ENOENT)
#.                      break;
#.                printf("getgrent_r: %s", strerror(i));
#.                exit(EXIT_FAILURE);
#.          }
#.  SRC END
#. type: SH
#. #-#-#-#-#  debian-bookworm: getgrent_r.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  perhaps add error checking - should use strerror_r
#.  #include <errno.h>
#.  #include <stdlib.h>
#.          if (i) {
#.                if (i == ENOENT)
#.                      break;
#.                printf("getgrent_r: %s", strerror(i));
#.                exit(EXIT_FAILURE);
#.          }
#.  SRC END
#. type: SH
#. #-#-#-#-#  debian-unstable: getgrent_r.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  perhaps add error checking - should use strerror_r
#.  #include <errno.h>
#.  #include <stdlib.h>
#.          if (i) {
#.                if (i == ENOENT)
#.                      break;
#.                printf("getgrent_r: %s", strerror(i));
#.                exit(EXIT_FAILURE);
#.          }
#.  SRC END
#. type: SH
#. #-#-#-#-#  fedora-39: getgrent_r.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  perhaps add error checking - should use strerror_r
#.  #include <errno.h>
#.  #include <stdlib.h>
#.          if (i) {
#.                if (i == ENOENT)
#.                      break;
#.                printf("getgrent_r: %s", strerror(i));
#.                exit(EXIT_FAILURE);
#.          }
#.  SRC END
#. type: SH
#. #-#-#-#-#  fedora-rawhide: getgrent_r.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  perhaps add error checking - should use strerror_r
#.  #include <errno.h>
#.  #include <stdlib.h>
#.          if (i) {
#.                if (i == ENOENT)
#.                      break;
#.                printf("getgrent_r: %s", strerror(i));
#.                exit(EXIT_FAILURE);
#.          }
#.  SRC END
#. type: SH
#. #-#-#-#-#  mageia-cauldron: getgrent_r.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  perhaps add error checking - should use strerror_r
#.  #include <errno.h>
#.  #include <stdlib.h>
#.          if (i) {
#.                if (i == ENOENT)
#.                      break;
#.                printf("getgrent_r: %s", strerror(i));
#.                exit(EXIT_FAILURE);
#.          }
#.  SRC END
#. type: SH
#. #-#-#-#-#  opensuse-leap-15-6: getgrent_r.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  perhaps add error checking - should use strerror_r
#.  #include <errno.h>
#.  #include <stdlib.h>
#.          if (i) {
#.                if (i == ENOENT)
#.                      break;
#.                printf("getgrent_r: %s", strerror(i));
#.                exit(EXIT_FAILURE);
#.          }
#. type: SH
#. #-#-#-#-#  opensuse-tumbleweed: getgrent_r.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  perhaps add error checking - should use strerror_r
#.  #include <errno.h>
#.  #include <stdlib.h>
#.          if (i) {
#.                if (i == ENOENT)
#.                      break;
#.                printf("getgrent_r: %s", strerror(i));
#.                exit(EXIT_FAILURE);
#.          }
#.  SRC END
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМ. ТАКЖЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<fgetgrent>(3), B<getgrent>(3), B<getgrgid>(3), B<getgrnam>(3), "
"B<putgrent>(3), B<group>(5)"
msgstr ""
"B<fgetgrent>(3), B<getgrent>(3), B<getgrgid>(3), B<getgrnam>(3), "
"B<putgrent>(3), B<group>(5)"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "2023-02-05"
msgstr "5 февраля 2023 г."

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid ""
"These functions are GNU extensions, done in a style resembling the POSIX "
"version of functions like B<getpwnam_r>(3).  Other systems use the prototype"
msgstr ""
"Эти функции являются расширениями GNU; они выполнены похожими на POSIX-"
"версию функции B<getpwnam_r>(3). В других системах используется прототип"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "#define _GNU_SOURCE\n"
#| "#include E<lt>grp.hE<gt>\n"
#| "#include E<lt>stdio.hE<gt>\n"
#| "#include E<lt>stdlib.hE<gt>\n"
#| "#define BUFLEN 4096\n"
msgid ""
"#define _GNU_SOURCE\n"
"#include E<lt>grp.hE<gt>\n"
"#include E<lt>stdint.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#define BUFLEN 4096\n"
msgstr ""
"#define _GNU_SOURCE\n"
"#include E<lt>grp.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#define BUFLEN 4096\n"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "int\n"
#| "main(void)\n"
#| "{\n"
#| "    struct group grp, *grpp;\n"
#| "    char buf[BUFLEN];\n"
#| "    int i;\n"
msgid ""
"int\n"
"main(void)\n"
"{\n"
"    struct group grp;\n"
"    struct group *grpp;\n"
"    char buf[BUFLEN];\n"
"    int i;\n"
msgstr ""
"int\n"
"main(void)\n"
"{\n"
"    struct group grp, *grpp;\n"
"    char buf[BUFLEN];\n"
"    int i;\n"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "    setgrent();\n"
#| "    while (1) {\n"
#| "        i = getgrent_r(&grp, buf, BUFLEN, &grpp);\n"
#| "        if (i)\n"
#| "            break;\n"
#| "        printf(\"%s (%d):\", grpp-E<gt>gr_name, grpp-E<gt>gr_gid);\n"
#| "        for (i = 0; ; i++) {\n"
#| "            if (grpp-E<gt>gr_mem[i] == NULL)\n"
#| "                break;\n"
#| "            printf(\" %s\", grpp-E<gt>gr_mem[i]);\n"
#| "        }\n"
#| "        printf(\"\\en\");\n"
#| "    }\n"
#| "    endgrent();\n"
#| "    exit(EXIT_SUCCESS);\n"
#| "}\n"
msgid ""
"    setgrent();\n"
"    while (1) {\n"
"        i = getgrent_r(&grp, buf, sizeof(buf), &grpp);\n"
"        if (i)\n"
"            break;\n"
"        printf(\"%s (%jd):\", grpp-E<gt>gr_name, (intmax_t) grpp-E<gt>gr_gid);\n"
"        for (size_t j = 0; ; j++) {\n"
"            if (grpp-E<gt>gr_mem[j] == NULL)\n"
"                break;\n"
"            printf(\" %s\", grpp-E<gt>gr_mem[j]);\n"
"        }\n"
"        printf(\"\\en\");\n"
"    }\n"
"    endgrent();\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""
"    setgrent();\n"
"    while (1) {\n"
"        i = getgrent_r(&grp, buf, BUFLEN, &grpp);\n"
"        if (i)\n"
"            break;\n"
"        printf(\"%s (%d):\", grpp-E<gt>gr_name, grpp-E<gt>gr_gid);\n"
"        for (i = 0; ; i++) {\n"
"            if (grpp-E<gt>gr_mem[i] == NULL)\n"
"                break;\n"
"            printf(\" %s\", grpp-E<gt>gr_mem[i]);\n"
"        }\n"
"        printf(\"\\en\");\n"
"    }\n"
"    endgrent();\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.05"
msgstr "Linux man-pages 6.05"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "GETGRENT_R"
msgstr "GETGRENT_R"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2017-09-15"
msgstr "15 сентября 2017 г."

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "GNU"
msgstr "GNU"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Руководство программиста Linux"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"B<int getgrent_r(struct group *>I<gbuf>B<, char *>I<buf>B<,>\n"
"B<               size_t >I<buflen>B<, struct group **>I<gbufp>B<);>\n"
msgstr ""
"B<int getgrent_r(struct group *>I<gbuf>B<, char *>I<buf>B<,>\n"
"B<               size_t >I<buflen>B<, struct group **>I<gbufp>B<);>\n"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"B<int fgetgrent_r(FILE *>I<stream>B<, struct group *>I<gbuf>B<, char *>I<buf>B<,>\n"
"B<                size_t >I<buflen>B<, struct group **>I<gbufp>B<);>\n"
msgstr ""
"B<int fgetgrent_r(FILE *>I<stream>B<, struct group *>I<gbuf>B<, char *>I<buf>B<,>\n"
"B<                size_t >I<buflen>B<, struct group **>I<gbufp>B<);>\n"

#.  FIXME . The FTM requirements seem inconsistent here.  File a glibc bug?
#. type: Plain text
#: opensuse-leap-15-6
msgid "B<getgrent_r>(): _GNU_SOURCE"
msgstr "B<getgrent_r>(): _GNU_SOURCE"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"B<fgetgrent_r>():\n"
"    Since glibc 2.19:\n"
"        _DEFAULT_SOURCE\n"
"    Glibc 2.19 and earlier:\n"
"        _SVID_SOURCE\n"
msgstr ""
"B<fgetgrent_r>():\n"
"    начиная с glibc 2.19:\n"
"        _DEFAULT_SOURCE\n"
"    в glibc 2.19 и старее:\n"
"        _SVID_SOURCE\n"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"The nonreentrant functions return a pointer to static storage, where this "
"static storage contains further pointers to group name, password and "
"members.  The reentrant functions described here return all of that in "
"caller-provided buffers.  First of all there is the buffer I<gbuf> that can "
"hold a I<struct group>.  And next the buffer I<buf> of size I<buflen> that "
"can hold additional strings.  The result of these functions, the I<struct "
"group> read from the stream, is stored in the provided buffer I<*gbuf>, and "
"a pointer to this I<struct group> is returned in I<*gbufp>."
msgstr ""
"Нереентерабельные версии возвращают указатель на статическое хранилище, в "
"котором хранятся другие указатели на имя группы, пароль и список членов. "
"Реентерабельные функции, описанные здесь, возвращают всю информацию в "
"буферах, предоставленных вызывающим. Основным буфером является I<gbuf>, в "
"котором может храниться I<struct group>. В дополнительном буфере I<buf> "
"размера I<buflen> могут храниться дополнительные строки. Результат этих "
"функций, прочитанная из потока I<struct group>, сохраняется в "
"предоставляемый буфер I<*gbuf>, и указатель на эту I<struct group> "
"возвращается в I<*gbufp>."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"In the above table, I<grent> in I<race:grent> signifies that if any of the "
"functions B<setgrent>(), B<getgrent>(), B<endgrent>(), or B<getgrent_r>()  "
"are used in parallel in different threads of a program, then data races "
"could occur."
msgstr ""
"В приведённой выше таблице I<grent> в I<race:grent> означает, что если в "
"нескольких нитях программы одновременно используются функции B<setgrent>(), "
"B<getgrent>(), B<endgrent>() или B<getgrent_r>(), то может возникнуть "
"состязательность по данным."

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "CONFORMING TO"
msgstr "СООТВЕТСТВИЕ СТАНДАРТАМ"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "EXAMPLE"
msgstr "ПРИМЕР"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"#define _GNU_SOURCE\n"
"#include E<lt>grp.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#define BUFLEN 4096\n"
msgstr ""
"#define _GNU_SOURCE\n"
"#include E<lt>grp.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#define BUFLEN 4096\n"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"int\n"
"main(void)\n"
"{\n"
"    struct group grp, *grpp;\n"
"    char buf[BUFLEN];\n"
"    int i;\n"
msgstr ""
"int\n"
"main(void)\n"
"{\n"
"    struct group grp, *grpp;\n"
"    char buf[BUFLEN];\n"
"    int i;\n"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"    setgrent();\n"
"    while (1) {\n"
"        i = getgrent_r(&grp, buf, BUFLEN, &grpp);\n"
"        if (i)\n"
"            break;\n"
"        printf(\"%s (%d):\", grpp-E<gt>gr_name, grpp-E<gt>gr_gid);\n"
"        for (i = 0; ; i++) {\n"
"            if (grpp-E<gt>gr_mem[i] == NULL)\n"
"                break;\n"
"            printf(\" %s\", grpp-E<gt>gr_mem[i]);\n"
"        }\n"
"        printf(\"\\en\");\n"
"    }\n"
"    endgrent();\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""
"    setgrent();\n"
"    while (1) {\n"
"        i = getgrent_r(&grp, buf, BUFLEN, &grpp);\n"
"        if (i)\n"
"            break;\n"
"        printf(\"%s (%d):\", grpp-E<gt>gr_name, grpp-E<gt>gr_gid);\n"
"        for (i = 0; ; i++) {\n"
"            if (grpp-E<gt>gr_mem[i] == NULL)\n"
"                break;\n"
"            printf(\" %s\", grpp-E<gt>gr_mem[i]);\n"
"        }\n"
"        printf(\"\\en\");\n"
"    }\n"
"    endgrent();\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "COLOPHON"
msgstr "ЗАМЕЧАНИЯ"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Эта страница является частью проекта Linux I<man-pages> версии 4.16. "
"Описание проекта, информацию об ошибках и последнюю версию этой страницы "
"можно найти по адресу \\%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30 марта 2023 г."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"
