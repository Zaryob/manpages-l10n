# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Alexander Golubev <fatzer2@gmail.com>, 2018.
# Azamat Hackimov <azamat.hackimov@gmail.com>, 2011, 2014-2016.
# Hotellook, 2014.
# Nikita <zxcvbnm3230@mail.ru>, 2014.
# Spiros Georgaras <sng@hellug.gr>, 2016.
# Vladislav <ivladislavefimov@gmail.com>, 2015.
# Yuri Kozlov <yuray@komyakino.ru>, 2011-2019.
# Иван Павлов <pavia00@gmail.com>, 2017.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-08-27 17:24+0200\n"
"PO-Revision-Date: 2019-10-15 18:55+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <man-pages-ru-talks@lists.sourceforge.net>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "strcmp"
msgstr ""

#. type: TH
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid "2023-07-20"
msgstr "20 июля 2023 г."

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Linux man-pages 6.05.01"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "ИМЯ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "strcmp, strncmp - compare two strings"
msgstr "strcmp, strncmp - сравнивает две строки"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "СИНТАКСИС"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>string.hE<gt>>\n"
msgstr "B<#include E<lt>string.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "B<char *strndup(const char *>I<s>B<, size_t >I<n>B<);>\n"
#| "B<char *strdupa(const char *>I<s>B<);>\n"
#| "B<char *strndupa(const char *>I<s>B<, size_t >I<n>B<);>\n"
msgid ""
"B<int strcmp(const char *>I<s1>B<, const char *>I<s2>B<);>\n"
"B<int strncmp(const char >I<s1>B<[.>I<n>B<], const char >I<s2>B<[.>I<n>B<], size_t >I<n>B<);>\n"
msgstr ""
"B<char *strndup(const char *>I<s>B<, size_t >I<n>B<);>\n"
"B<char *strdupa(const char *>I<s>B<);>\n"
"B<char *strndupa(const char *>I<s>B<, size_t >I<n>B<);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The B<strcmp>()  function compares the two strings I<s1> and I<s2>.  The "
#| "locale is not taken into account (for a locale-aware comparison, see "
#| "B<strcoll>(3)).  It returns an integer less than, equal to, or greater "
#| "than zero if I<s1> is found, respectively, to be less than, to match, or "
#| "be greater than I<s2>."
msgid ""
"The B<strcmp>()  function compares the two strings I<s1> and I<s2>.  The "
"locale is not taken into account (for a locale-aware comparison, see "
"B<strcoll>(3)).  The comparison is done using unsigned characters."
msgstr ""
"Функция B<strcmp>() сравнивает строки I<s1> и I<s2>. При этом локаль не "
"учитывается (для её учёта используйте функцию B<strcoll>(3)). Она возвращает "
"целое число, которое меньше, больше нуля или равно ему, если выяснится, что "
"I<s1> меньше, равна или больше I<s2> соответственно."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"B<strcmp>()  returns an integer indicating the result of the comparison, as "
"follows:"
msgstr ""

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "\\[bu]"
msgstr "\\[bu]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "0, if the I<s1> and I<s2> are equal;"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "a negative value if I<s1> is less than I<s2>;"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "a positive value if I<s1> is greater than I<s2>."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<strncmp>()  function is similar, except it compares only the first (at "
"most)  I<n> bytes of I<s1> and I<s2>."
msgstr ""
"Функция B<strncmp>() работает аналогичным образом, но сравниваются только "
"первые (не более) I<n> байт строки I<s1> и I<s2>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "ВОЗВРАЩАЕМОЕ ЗНАЧЕНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<strcmp>()  and B<strncmp>()  functions return an integer less than, "
"equal to, or greater than zero if I<s1> (or the first I<n> bytes thereof) is "
"found, respectively, to be less than, to match, or be greater than I<s2>."
msgstr ""
"Функции B<strcmp>() и B<strncmp>() возвращают целое число, которое меньше, "
"больше нуля или равно ему, если строка I<s1> (или её первые I<n> байтов) "
"соответственно меньше, больше или равна I<s2>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "АТРИБУТЫ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr "Описание терминов данного раздела смотрите в B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Интерфейс"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Атрибут"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Значение"

#. type: tbl table
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<strcmp>(),\n"
"B<strncmp>()"
msgstr ""
"B<strcmp>(),\n"
"B<strncmp>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Безвредность в нитях"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "ВЕРСИИ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "POSIX.1 specifies only that:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The sign of a nonzero return value shall be determined by the sign of the "
"difference between the values of the first pair of bytes (both interpreted "
"as type I<unsigned char>)  that differ in the strings being compared."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"In glibc, as in most other implementations, the return value is the "
"arithmetic result of subtracting the last compared byte in I<s2> from the "
"last compared byte in I<s1>.  (If the two characters are equal, this "
"difference is 0.)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "СТАНДАРТЫ"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "C11, POSIX.1-2008."
msgstr "C11, POSIX.1-2008."

#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "ИСТОРИЯ"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "POSIX.1-2001, C89, SVr4, 4.3BSD."
msgstr "POSIX.1-2001, C89, SVr4, 4.3BSD."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "ПРИМЕРЫ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The program below can be used to demonstrate the behavior of "
#| "B<strverscmp>().  It uses B<strverscmp>()  to compare the two strings "
#| "given as its command-line arguments.  An example of its use is the "
#| "following:"
msgid ""
"The program below can be used to demonstrate the operation of B<strcmp>()  "
"(when given two arguments) and B<strncmp>()  (when given three arguments).  "
"First, some examples using B<strcmp>():"
msgstr ""
"Показанная далее программа демонстрирует поведение B<strverscmp>(). Функция "
"B<strverscmp>() используется для сравнения двух строк, заданных в аргументах "
"командной строки. Пример работы:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"$ B<./string_comp ABC ABC>\n"
"E<lt>str1E<gt> and E<lt>str2E<gt> are equal\n"
"$ B<./string_comp ABC AB>      # \\[aq]C\\[aq] is ASCII 67; \\[aq]C\\[aq] - \\[aq]\\e0\\[aq] = 67\n"
"E<lt>str1E<gt> is greater than E<lt>str2E<gt> (67)\n"
"$ B<./string_comp ABA ABZ>     # \\[aq]A\\[aq] is ASCII 65; \\[aq]Z\\[aq] is ASCII 90\n"
"E<lt>str1E<gt> is less than E<lt>str2E<gt> (-25)\n"
"$ B<./string_comp ABJ ABC>\n"
"E<lt>str1E<gt> is greater than E<lt>str2E<gt> (7)\n"
"$ .B</string_comp $\\[aq]\\e201\\[aq] A>   # 0201 - 0101 = 0100 (or 64 decimal)\n"
"E<lt>str1E<gt> is greater than E<lt>str2E<gt> (64)\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The last example uses B<bash>(1)-specific syntax to produce a string "
"containing an 8-bit ASCII code; the result demonstrates that the string "
"comparison uses unsigned characters."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "And then some examples using B<strncmp>():"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"$ B<./string_comp ABC AB 3>\n"
"E<lt>str1E<gt> is greater than E<lt>str2E<gt> (67)\n"
"$ B<./string_comp ABC AB 2>\n"
"E<lt>str1E<gt> and E<lt>str2E<gt> are equal in the first 2 bytes\n"
msgstr ""

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "Program source"
msgstr "Исходный код программы"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid ""
"/* string_comp.c\n"
"\\&\n"
"   Licensed under GNU General Public License v2 or later.\n"
"*/\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>string.hE<gt>\n"
"\\&\n"
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    int res;\n"
"\\&\n"
"    if (argc E<lt> 3) {\n"
"        fprintf(stderr, \"Usage: %s E<lt>str1E<gt> E<lt>str2E<gt> [E<lt>lenE<gt>]\\en\", argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    if (argc == 3)\n"
"        res = strcmp(argv[1], argv[2]);\n"
"    else\n"
"        res = strncmp(argv[1], argv[2], atoi(argv[3]));\n"
"\\&\n"
"    if (res == 0) {\n"
"        printf(\"E<lt>str1E<gt> and E<lt>str2E<gt> are equal\");\n"
"        if (argc E<gt> 3)\n"
"            printf(\" in the first %d bytes\\en\", atoi(argv[3]));\n"
"        printf(\"\\en\");\n"
"    } else if (res E<lt> 0) {\n"
"        printf(\"E<lt>str1E<gt> is less than E<lt>str2E<gt> (%d)\\en\", res);\n"
"    } else {\n"
"        printf(\"E<lt>str1E<gt> is greater than E<lt>str2E<gt> (%d)\\en\", res);\n"
"    }\n"
"\\&\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""

#. #-#-#-#-#  archlinux: strcmp.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  debian-bookworm: strcmp.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  debian-unstable: strcmp.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  fedora-39: strcmp.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  fedora-rawhide: strcmp.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  mageia-cauldron: strcmp.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  opensuse-leap-15-6: strcmp.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#. #-#-#-#-#  opensuse-tumbleweed: strcmp.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМ. ТАКЖЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "B<bcmp>(3), B<memcmp>(3), B<strcasecmp>(3), B<strcoll>(3), B<string>(3), "
#| "B<strncasecmp>(3), B<strverscmp>(3), B<wcscmp>(3), B<wcsncmp>(3)"
msgid ""
"B<memcmp>(3), B<strcasecmp>(3), B<strcoll>(3), B<string>(3), "
"B<strncasecmp>(3), B<strverscmp>(3), B<wcscmp>(3), B<wcsncmp>(3), B<ascii>(7)"
msgstr ""
"B<bcmp>(3), B<memcmp>(3), B<strcasecmp>(3), B<strcoll>(3), B<string>(3), "
"B<strncasecmp>(3), B<strverscmp>(3), B<wcscmp>(3), B<wcsncmp>(3)"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "2023-02-05"
msgstr "5 февраля 2023 г."

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron
msgid "POSIX.1-2001, POSIX.1-2008, C99, SVr4, 4.3BSD."
msgstr "POSIX.1-2001, POSIX.1-2008, C99, SVr4, 4.3BSD."

#. type: SH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "NOTES"
msgstr "ЗАМЕЧАНИЯ"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "/* string_comp.c\n"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "#include E<lt>stdio.hE<gt>\n"
#| "#include E<lt>stdlib.hE<gt>\n"
#| "#include E<lt>string.hE<gt>\n"
msgid ""
"   Licensed under GNU General Public License v2 or later.\n"
"*/\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>string.hE<gt>\n"
msgstr ""
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>string.hE<gt>\n"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    int res;\n"
msgstr ""
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    int res;\n"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "    if (argc != 3) {\n"
#| "        fprintf(stderr, \"Usage: %s E<lt>string1E<gt> E<lt>string2E<gt>\\en\", argv[0]);\n"
#| "        exit(EXIT_FAILURE);\n"
#| "    }\n"
msgid ""
"    if (argc E<lt> 3) {\n"
"        fprintf(stderr, \"Usage: %s E<lt>str1E<gt> E<lt>str2E<gt> [E<lt>lenE<gt>]\\en\", argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
msgstr ""
"    if (argc != 3) {\n"
"        fprintf(stderr, \"Usage: %s E<lt>string1E<gt> E<lt>string2E<gt>\\en\", argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"    if (argc == 3)\n"
"        res = strcmp(argv[1], argv[2]);\n"
"    else\n"
"        res = strncmp(argv[1], argv[2], atoi(argv[3]));\n"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"    if (res == 0) {\n"
"        printf(\"E<lt>str1E<gt> and E<lt>str2E<gt> are equal\");\n"
"        if (argc E<gt> 3)\n"
"            printf(\" in the first %d bytes\\en\", atoi(argv[3]));\n"
"        printf(\"\\en\");\n"
"    } else if (res E<lt> 0) {\n"
"        printf(\"E<lt>str1E<gt> is less than E<lt>str2E<gt> (%d)\\en\", res);\n"
"    } else {\n"
"        printf(\"E<lt>str1E<gt> is greater than E<lt>str2E<gt> (%d)\\en\", res);\n"
"    }\n"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""
"    exit(EXIT_SUCCESS);\n"
"}\n"

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.05"
msgstr "Linux man-pages 6.05"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "STRCMP"
msgstr "STRCMP"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2015-08-08"
msgstr "8 августа 2015 г."

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Руководство программиста Linux"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid "B<int strcmp(const char *>I<s1>B<, const char *>I<s2>B<);>\n"
msgstr "B<int strcmp(const char *>I<s1>B<, const char *>I<s2>B<);>\n"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid "B<int strncmp(const char *>I<s1>B<, const char *>I<s2>B<, size_t >I<n>B<);>\n"
msgstr "B<int strncmp(const char *>I<s1>B<, const char *>I<s2>B<, size_t >I<n>B<);>\n"

#. type: Plain text
#: opensuse-leap-15-6
#, fuzzy
#| msgid ""
#| "The B<strcasecmp>()  function performs a byte-by-byte comparison of the "
#| "strings I<s1> and I<s2>, ignoring the case of the characters.  It returns "
#| "an integer less than, equal to, or greater than zero if I<s1> is found, "
#| "respectively, to be less than, to match, or be greater than I<s2>."
msgid ""
"The B<strcmp>()  function compares the two strings I<s1> and I<s2>.  It "
"returns an integer less than, equal to, or greater than zero if I<s1> is "
"found, respectively, to be less than, to match, or be greater than I<s2>."
msgstr ""
"Функция B<strcasecmp>() выполняет побайтовое сравнение строк I<s1> и I<s2>, "
"игнорируя регистр символов. Она возвращает целое число, которое меньше, "
"равно или больше нуля, если выяснится, что I<s1> меньше, равна или больше "
"I<s2> соответственно."

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "CONFORMING TO"
msgstr "СООТВЕТСТВИЕ СТАНДАРТАМ"

#. type: Plain text
#: opensuse-leap-15-6
msgid "POSIX.1-2001, POSIX.1-2008, C89, C99, SVr4, 4.3BSD."
msgstr "POSIX.1-2001, POSIX.1-2008, C89, C99, SVr4, 4.3BSD."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"B<bcmp>(3), B<memcmp>(3), B<strcasecmp>(3), B<strcoll>(3), B<string>(3), "
"B<strncasecmp>(3), B<strverscmp>(3), B<wcscmp>(3), B<wcsncmp>(3)"
msgstr ""
"B<bcmp>(3), B<memcmp>(3), B<strcasecmp>(3), B<strcoll>(3), B<string>(3), "
"B<strncasecmp>(3), B<strverscmp>(3), B<wcscmp>(3), B<wcsncmp>(3)"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "COLOPHON"
msgstr "ЗАМЕЧАНИЯ"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Эта страница является частью проекта Linux I<man-pages> версии 4.16. "
"Описание проекта, информацию об ошибках и последнюю версию этой страницы "
"можно найти по адресу \\%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30 марта 2023 г."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"
