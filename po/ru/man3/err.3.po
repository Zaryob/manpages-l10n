# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Azamat Hackimov <azamat.hackimov@gmail.com>, 2012.
# Yuri Kozlov <yuray@komyakino.ru>, 2011-2019.
# Иван Павлов <pavia00@gmail.com>, 2017.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-08-27 16:55+0200\n"
"PO-Revision-Date: 2019-10-05 07:57+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <man-pages-ru-talks@lists.sourceforge.net>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "errno"
msgid "err"
msgstr "errno"

#. type: TH
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid "2023-07-20"
msgstr "20 июля 2023 г."

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Linux man-pages 6.05.01"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "ИМЯ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"err, verr, errx, verrx, warn, vwarn, warnx, vwarnx - formatted error messages"
msgstr ""
"err, verr, errx, verrx, warn, vwarn, warnx, vwarnx - форматирует сообщения "
"об ошибках"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "СИНТАКСИС"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>err.hE<gt>>\n"
msgstr "B<#include E<lt>err.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<void error(int >I<status>B<, int >I<errnum>B<, const char *>I<format>B<, ...);>\n"
msgid ""
"B<[[noreturn]] void err(int >I<eval>B<, const char *>I<fmt>B<, ...);>\n"
"B<[[noreturn]] void errx(int >I<eval>B<, const char *>I<fmt>B<, ...);>\n"
msgstr "B<void error(int >I<status>B<, int >I<errnum>B<, const char *>I<format>B<, ...);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<void err(int >I<eval>B<, const char *>I<fmt>B<, ...);>\n"
msgid ""
"B<void warn(const char *>I<fmt>B<, ...);>\n"
"B<void warnx(const char *>I<fmt>B<, ...);>\n"
msgstr "B<void err(int >I<eval>B<, const char *>I<fmt>B<, ...);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>stdarg.hE<gt>>\n"
msgstr "B<#include E<lt>stdarg.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<void verrx(int >I<eval>B<, const char *>I<fmt>B<, va_list >I<args>B<);>\n"
msgid ""
"B<[[noreturn]] void verr(int >I<eval>B<, const char *>I<fmt>B<, va_list >I<args>B<);>\n"
"B<[[noreturn]] void verrx(int >I<eval>B<, const char *>I<fmt>B<, va_list >I<args>B<);>\n"
msgstr "B<void verrx(int >I<eval>B<, const char *>I<fmt>B<, va_list >I<args>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<void verr(int >I<eval>B<, const char *>I<fmt>B<, va_list >I<args>B<);>\n"
msgid ""
"B<void vwarn(const char *>I<fmt>B<, va_list >I<args>B<);>\n"
"B<void vwarnx(const char *>I<fmt>B<, va_list >I<args>B<);>\n"
msgstr "B<void verr(int >I<eval>B<, const char *>I<fmt>B<, va_list >I<args>B<);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<err>()  and B<warn>()  family of functions display a formatted error "
"message on the standard error output.  In all cases, the last component of "
"the program name, a colon character, and a space are output.  If the I<fmt> "
"argument is not NULL, the B<printf>(3)-like formatted error message is "
"output.  The output is terminated by a newline character."
msgstr ""
"Семейства функций B<err>() и B<warn>() выводят форматированное сообщение об "
"ошибке в стандартный поток ошибок. При выводе всегда показывается последний "
"компонент имени программы, двоеточие и пробел. Если значение I<fmt> не равно "
"NULL, то выводится сообщение об ошибке, отформатированное согласно "
"B<printf>(3). Вывод завершается символом новой строки."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<err>(), B<verr>(), B<warn>(), and B<vwarn>()  functions append an "
"error message obtained from B<strerror>(3)  based on the global variable "
"I<errno>, preceded by another colon and space unless the I<fmt> argument is "
"NULL."
msgstr ""
"Функции B<err>(), B<verr>(), B<warn>() и B<vwarn>() добавляют сообщение об "
"ошибке, полученное от B<strerror>(3) для значения глобальной переменной "
"I<errno>, начиная его с двоеточия и пробела, если значение I<fmt> равно NULL."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<errx>()  and B<warnx>()  functions do not append an error message."
msgstr "Функции B<errx>() и B<warnx>() не добавляют сообщение об ошибке."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<err>(), B<verr>(), B<errx>(), and B<verrx>()  functions do not return, "
"but exit with the value of the argument I<eval>."
msgstr ""
"Функции B<err>(), B<verr>(), B<errx>() и B<verrx>() не возвращают управление "
"в программу, а завершают её с кодом выхода, равным значению I<eval>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "АТРИБУТЫ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr "Описание терминов данного раздела смотрите в B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Интерфейс"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Атрибут"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Значение"

#. type: tbl table
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"B<err>(),\n"
"B<errx>(),\n"
"B<warn>(),\n"
"B<warnx>(),\n"
"B<verr>(),\n"
"B<verrx>(),\n"
"B<vwarn>(),\n"
"B<vwarnx>()"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Безвредность в нитях"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe locale"
msgstr "MT-Safe locale"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "СТАНДАРТЫ"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "BSD."
msgstr "BSD."

#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "ИСТОРИЯ"

#. type: TP
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "I<error>"
msgid "B<err>()"
msgstr "I<error>"

#. type: TQ
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<isnan>():"
msgid "B<warn>()"
msgstr "B<isnan>():"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "4.4BSD."
msgstr "4.4BSD."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "ПРИМЕРЫ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Display the current I<errno> information string and exit:"
msgstr "Показать строку с информацией о текущем I<errno> и закончить работу:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "p = malloc(size);\n"
#| "if (p == NULL)\n"
#| "    err(1, NULL);\n"
#| "fd = open(file_name, O_RDONLY, 0);\n"
#| "if (fd == -1)\n"
#| "    err(1, \"%s\", file_name);\n"
msgid ""
"p = malloc(size);\n"
"if (p == NULL)\n"
"    err(EXIT_FAILURE, NULL);\n"
"fd = open(file_name, O_RDONLY, 0);\n"
"if (fd == -1)\n"
"    err(EXIT_FAILURE, \"%s\", file_name);\n"
msgstr ""
"p = malloc(size);\n"
"if (p == NULL)\n"
"    err(1, NULL);\n"
"fd = open(file_name, O_RDONLY, 0);\n"
"if (fd == -1)\n"
"    err(1, \"%s\", file_name);\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Display an error message and exit:"
msgstr "Показать сообщение об ошибке и закончить работу:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "if (tm.tm_hour E<lt> START_TIME)\n"
#| "    errx(1, \"too early, wait until %s\", start_time_string);\n"
msgid ""
"if (tm.tm_hour E<lt> START_TIME)\n"
"    errx(EXIT_FAILURE, \"too early, wait until %s\",\n"
"            start_time_string);\n"
msgstr ""
"if (tm.tm_hour E<lt> START_TIME)\n"
"    errx(1, \"слишком рано, ждём до %s\", start_time_string);\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Warn of an error:"
msgstr "Предупреждение об ошибке:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "fd = open(raw_device, O_RDONLY, 0);\n"
#| "if (fd == -1)\n"
#| "    warnx(\"%s: %s: trying the block device\",\n"
#| "            raw_device, strerror(errno));\n"
#| "fd = open(block_device, O_RDONLY, 0);\n"
#| "if (fd == -1)\n"
#| "    err(1, \"%s\", block_device);\n"
msgid ""
"fd = open(raw_device, O_RDONLY, 0);\n"
"if (fd == -1)\n"
"    warnx(\"%s: %s: trying the block device\",\n"
"            raw_device, strerror(errno));\n"
"fd = open(block_device, O_RDONLY, 0);\n"
"if (fd == -1)\n"
"    err(EXIT_FAILURE, \"%s\", block_device);\n"
msgstr ""
"fd = open(raw_device, O_RDONLY, 0);\n"
"if (fd == -1)\n"
"    warnx(\"%s: %s: пробуем блочное устройство\",\n"
"            raw_device, strerror(errno));\n"
"fd = open(block_device, O_RDONLY, 0);\n"
"if (fd == -1)\n"
"    err(1, \"%s\", block_device);\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМ. ТАКЖЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<error>(3), B<exit>(3), B<perror>(3), B<printf>(3), B<strerror>(3)"
msgstr "B<error>(3), B<exit>(3), B<perror>(3), B<printf>(3), B<strerror>(3)"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "2022-12-15"
msgstr "15 декабря 2022 г."

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#.  .SH HISTORY
#.  The
#.  .BR err ()
#.  and
#.  .BR warn ()
#.  functions first appeared in
#.  4.4BSD.
#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid "These functions are nonstandard BSD extensions."
msgstr "Эти функции являются расширениями BSD."

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.05"
msgstr "Linux man-pages 6.05"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "ERR"
msgstr "ERR"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2017-09-15"
msgstr "15 сентября 2017 г."

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Руководство программиста Linux"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid "B<void err(int >I<eval>B<, const char *>I<fmt>B<, ...);>\n"
msgstr "B<void err(int >I<eval>B<, const char *>I<fmt>B<, ...);>\n"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid "B<void errx(int >I<eval>B<, const char *>I<fmt>B<, ...);>\n"
msgstr "B<void errx(int >I<eval>B<, const char *>I<fmt>B<, ...);>\n"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid "B<void warn(const char *>I<fmt>B<, ...);>\n"
msgstr "B<void warn(const char *>I<fmt>B<, ...);>\n"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid "B<void warnx(const char *>I<fmt>B<, ...);>\n"
msgstr "B<void warnx(const char *>I<fmt>B<, ...);>\n"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid "B<void verr(int >I<eval>B<, const char *>I<fmt>B<, va_list >I<args>B<);>\n"
msgstr "B<void verr(int >I<eval>B<, const char *>I<fmt>B<, va_list >I<args>B<);>\n"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid "B<void verrx(int >I<eval>B<, const char *>I<fmt>B<, va_list >I<args>B<);>\n"
msgstr "B<void verrx(int >I<eval>B<, const char *>I<fmt>B<, va_list >I<args>B<);>\n"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid "B<void vwarn(const char *>I<fmt>B<, va_list >I<args>B<);>\n"
msgstr "B<void vwarn(const char *>I<fmt>B<, va_list >I<args>B<);>\n"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid "B<void vwarnx(const char *>I<fmt>B<, va_list >I<args>B<);>\n"
msgstr "B<void vwarnx(const char *>I<fmt>B<, va_list >I<args>B<);>\n"

#. type: tbl table
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"B<err>(),\n"
"B<errx>(),\n"
msgstr ""
"B<err>(),\n"
"B<errx>(),\n"

#. type: tbl table
#: opensuse-leap-15-6
#, no-wrap
msgid ".br\n"
msgstr ".br\n"

#. type: tbl table
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"B<warn>(),\n"
"B<warnx>(),\n"
msgstr ""
"B<warn>(),\n"
"B<warnx>(),\n"

#. type: tbl table
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"B<verr>(),\n"
"B<verrx>(),\n"
msgstr ""
"B<verr>(),\n"
"B<verrx>(),\n"

#. type: tbl table
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"B<vwarn>(),\n"
"B<vwarnx>()"
msgstr ""
"B<vwarn>(),\n"
"B<vwarnx>()"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "CONFORMING TO"
msgstr "СООТВЕТСТВИЕ СТАНДАРТАМ"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "EXAMPLE"
msgstr "ПРИМЕР"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"p = malloc(size);\n"
"if (p == NULL)\n"
"    err(1, NULL);\n"
"fd = open(file_name, O_RDONLY, 0);\n"
"if (fd == -1)\n"
"    err(1, \"%s\", file_name);\n"
msgstr ""
"p = malloc(size);\n"
"if (p == NULL)\n"
"    err(1, NULL);\n"
"fd = open(file_name, O_RDONLY, 0);\n"
"if (fd == -1)\n"
"    err(1, \"%s\", file_name);\n"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"if (tm.tm_hour E<lt> START_TIME)\n"
"    errx(1, \"too early, wait until %s\", start_time_string);\n"
msgstr ""
"if (tm.tm_hour E<lt> START_TIME)\n"
"    errx(1, \"слишком рано, ждём до %s\", start_time_string);\n"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"fd = open(raw_device, O_RDONLY, 0);\n"
"if (fd == -1)\n"
"    warnx(\"%s: %s: trying the block device\",\n"
"            raw_device, strerror(errno));\n"
"fd = open(block_device, O_RDONLY, 0);\n"
"if (fd == -1)\n"
"    err(1, \"%s\", block_device);\n"
msgstr ""
"fd = open(raw_device, O_RDONLY, 0);\n"
"if (fd == -1)\n"
"    warnx(\"%s: %s: пробуем блочное устройство\",\n"
"            raw_device, strerror(errno));\n"
"fd = open(block_device, O_RDONLY, 0);\n"
"if (fd == -1)\n"
"    err(1, \"%s\", block_device);\n"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "COLOPHON"
msgstr "ЗАМЕЧАНИЯ"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Эта страница является частью проекта Linux I<man-pages> версии 4.16. "
"Описание проекта, информацию об ошибках и последнюю версию этой страницы "
"можно найти по адресу \\%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30 марта 2023 г."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"
