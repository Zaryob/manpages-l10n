# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Alexander Golubev <fatzer2@gmail.com>, 2018.
# Azamat Hackimov <azamat.hackimov@gmail.com>, 2011, 2014-2016.
# Hotellook, 2014.
# Nikita <zxcvbnm3230@mail.ru>, 2014.
# Spiros Georgaras <sng@hellug.gr>, 2016.
# Vladislav <ivladislavefimov@gmail.com>, 2015.
# Yuri Kozlov <yuray@komyakino.ru>, 2011-2019.
# Иван Павлов <pavia00@gmail.com>, 2017.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-08-27 17:20+0200\n"
"PO-Revision-Date: 2019-10-15 18:55+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <man-pages-ru-talks@lists.sourceforge.net>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "scalbln"
msgstr ""

#. type: TH
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid "2023-07-20"
msgstr "20 июля 2023 г."

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Linux man-pages 6.05.01"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "ИМЯ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"scalbn, scalbnf, scalbnl, scalbln, scalblnf, scalblnl - multiply floating-"
"point number by integral power of radix"
msgstr ""
"scalbn, scalbnf, scalbnl, scalbln, scalblnf, scalblnl - умножает число с "
"плавающей запятой на основание в степени целого числа"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Math library (I<libm>, I<-lm>)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "СИНТАКСИС"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>math.hE<gt>>\n"
msgstr "B<#include E<lt>math.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "B<double sin(double >I<x>B<);>\n"
#| "B<float sinf(float >I<x>B<);>\n"
#| "B<long double sinl(long double >I<x>B<);>\n"
msgid ""
"B<double scalbln(double >I<x>B<, long >I<exp>B<);>\n"
"B<float scalblnf(float >I<x>B<, long >I<exp>B<);>\n"
"B<long double scalblnl(long double >I<x>B<, long >I<exp>B<);>\n"
msgstr ""
"B<double sin(double >I<x>B<);>\n"
"B<float sinf(float >I<x>B<);>\n"
"B<long double sinl(long double >I<x>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "B<double sin(double >I<x>B<);>\n"
#| "B<float sinf(float >I<x>B<);>\n"
#| "B<long double sinl(long double >I<x>B<);>\n"
msgid ""
"B<double scalbn(double >I<x>B<, int >I<exp>B<);>\n"
"B<float scalbnf(float >I<x>B<, int >I<exp>B<);>\n"
"B<long double scalbnl(long double >I<x>B<, int >I<exp>B<);>\n"
msgstr ""
"B<double sin(double >I<x>B<);>\n"
"B<float sinf(float >I<x>B<);>\n"
"B<long double sinl(long double >I<x>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Требования макроса тестирования свойств для glibc (см. "
"B<feature_test_macros>(7)):"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<scalbln>(), B<scalblnf>(), B<scalblnl>():"
msgstr "B<scalbln>(), B<scalblnf>(), B<scalblnl>():"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "_ISOC99_SOURCE || _POSIX_C_SOURCE\\ E<gt>=\\ 200112L\n"
#| "    || /* Since glibc 2.19: */ _DEFAULT_SOURCE\n"
msgid ""
"    _ISOC99_SOURCE || _POSIX_C_SOURCE E<gt>= 200112L\n"
"        || /* Since glibc 2.19: */ _DEFAULT_SOURCE\n"
msgstr ""
"_ISOC99_SOURCE || _POSIX_C_SOURCE\\ E<gt>=\\ 200112L\n"
"    || /* начиная с glibc 2.19: */ _DEFAULT_SOURCE\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<scalbn>(), B<scalbnf>(), B<scalbnl>():"
msgstr "B<scalbn>(), B<scalbnf>(), B<scalbnl>():"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"    _ISOC99_SOURCE || _POSIX_C_SOURCE E<gt>= 200112L\n"
"        || /* Since glibc 2.19: */ _DEFAULT_SOURCE\n"
"        || /* glibc E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"
msgstr ""
"    _ISOC99_SOURCE || _POSIX_C_SOURCE E<gt>= 200112L\n"
"        || /* начиная с glibc 2.19: */ _DEFAULT_SOURCE\n"
"        || /* Glibc E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"These functions multiply their first argument I<x> by B<FLT_RADIX> (probably "
"2)  to the power of I<exp>, that is:"
msgstr ""
"Эти функции умножают свой первый аргумент I<x> на B<FLT_RADIX> (обычно, 2), "
"возведённый в степень I<exp>, то есть:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "    x * FLT_RADIX ** exp\n"
msgstr "    x * FLT_RADIX ** exp\n"

#.  not in /usr/include but in a gcc lib
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The definition of B<FLT_RADIX> can be obtained by including I<E<lt>float."
"hE<gt>>."
msgstr ""
"Определение значения B<FLT_RADIX> можно получить включив файл I<E<lt>float."
"hE<gt>>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "ВОЗВРАЩАЕМОЕ ЗНАЧЕНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "On success, these functions return I<x> * B<FLT_RADIX> ** I<exp>."
msgstr ""
"При успешном выполнении данные функции возвращают I<x> * B<FLT_RADIX> ** "
"I<exp>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "If I<x> is a NaN, a NaN is returned."
msgstr "Если I<x> имеет значение NaN, будет возвращено NaN."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If I<x> is positive infinity (negative infinity), positive infinity "
"(negative infinity) is returned."
msgstr ""
"Если I<x>  стремится к плюс или минус бесконечности, будет возвращена плюс "
"или минус бесконечность."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "If I<x> is +0 (-0), +0 (-0) is returned."
msgstr "Если I<x> равно +0 (-0), будет возвращено +0 (-0)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If the result overflows, a range error occurs, and the functions return "
"B<HUGE_VAL>, B<HUGE_VALF>, or B<HUGE_VALL>, respectively, with a sign the "
"same as I<x>."
msgstr ""
"Если в результате превышена разрядность, то возникает ошибка диапазона и "
"функции возвращают B<HUGE_VAL>, B<HUGE_VALF> или B<HUGE_VALL>, "
"соответственно, с тем же знаком что и I<x>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If the result underflows, a range error occurs, and the functions return "
"zero, with a sign the same as I<x>."
msgstr ""
"Если результат исчерпал степень, генерируется ошибка диапазона, а функции "
"возвращают 0 с тем же знаком что и I<x>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ОШИБКИ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"See B<math_error>(7)  for information on how to determine whether an error "
"has occurred when calling these functions."
msgstr ""
"Смотрите B<math_error>(7), чтобы определить, какие ошибки могут возникать "
"при вызове этих функций."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The following errors can occur:"
msgstr "Могут возникать следующие ошибки:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Range error, overflow"
msgstr "Ошибка диапазона, переполнение"

#.  .I errno
#.  is set to
#.  .BR ERANGE .
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "An overflow floating-point exception (B<FE_OVERFLOW>)  is raised."
msgstr "Вызывается исключение переполнения плавающей запятой (B<FE_OVERFLOW>)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Range error, underflow"
msgstr "Ошибка диапазона, исчерпана степень"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"I<errno> is set to B<ERANGE>.  An underflow floating-point exception "
"(B<FE_UNDERFLOW>)  is raised."
msgstr ""
"Значение I<errno> устанавливается в B<ERANGE>. Возникает исключение "
"исчерпания степени чисел с плавающей запятой (B<FE_UNDERFLOW>)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "АТРИБУТЫ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr "Описание терминов данного раздела смотрите в B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Интерфейс"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Атрибут"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Значение"

#. type: tbl table
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "B<scalbln>(),\n"
#| "B<scalblnf>(),\n"
#| "B<scalblnl>()"
msgid ""
"B<scalbn>(),\n"
"B<scalbnf>(),\n"
"B<scalbnl>(),\n"
"B<scalbln>(),\n"
"B<scalblnf>(),\n"
"B<scalblnl>()"
msgstr ""
"B<scalbln>(),\n"
"B<scalblnf>(),\n"
"B<scalblnl>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Безвредность в нитях"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "СТАНДАРТЫ"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "C11, POSIX.1-2008."
msgstr "C11, POSIX.1-2008."

#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "ИСТОРИЯ"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "glibc 2.1.  C99, POSIX.1-2001."
msgstr "glibc 2.1.  C99, POSIX.1-2001."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"These functions differ from the obsolete functions described in B<scalb>(3)  "
"in the type of their second argument.  The functions described on this page "
"have a second argument of an integral type, while those in B<scalb>(3)  have "
"a second argument of type I<double>."
msgstr ""
"Эти функции отличаются от устаревших функций, описанных в B<scalb>(3), типом "
"второго аргумента. У функций, описанных здесь, второй аргумент имеет "
"целочисленный тип, а у описанных в B<scalb>(3) второй аргумент имеет тип "
"I<double>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "ЗАМЕЧАНИЯ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If B<FLT_RADIX> equals 2 (which is usual), then B<scalbn>()  is equivalent "
"to B<ldexp>(3)."
msgstr ""
"Если B<FLT_RADIX> равно 2 (обычно), то B<scalbn>() эквивалентна B<ldexp>(3)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "ДЕФЕКТЫ"

#. #-#-#-#-#  archlinux: scalbln.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  https://www.sourceware.org/bugzilla/show_bug.cgi?id=6803
#. type: Plain text
#. #-#-#-#-#  debian-bookworm: scalbln.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  http://sources.redhat.com/bugzilla/show_bug.cgi?id=6803
#. type: Plain text
#. #-#-#-#-#  debian-unstable: scalbln.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  http://sources.redhat.com/bugzilla/show_bug.cgi?id=6803
#. type: Plain text
#. #-#-#-#-#  fedora-39: scalbln.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  https://www.sourceware.org/bugzilla/show_bug.cgi?id=6803
#. type: Plain text
#. #-#-#-#-#  fedora-rawhide: scalbln.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  https://www.sourceware.org/bugzilla/show_bug.cgi?id=6803
#. type: Plain text
#. #-#-#-#-#  mageia-cauldron: scalbln.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  http://sources.redhat.com/bugzilla/show_bug.cgi?id=6803
#. type: Plain text
#. #-#-#-#-#  opensuse-tumbleweed: scalbln.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  https://www.sourceware.org/bugzilla/show_bug.cgi?id=6803
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid "These functions do not set I<errno> for this case."
msgid ""
"Before glibc 2.20, these functions did not set I<errno> for range errors."
msgstr "В этом случае функции не изменяют I<errno>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМ. ТАКЖЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<ldexp>(3), B<scalb>(3)"
msgstr "B<ldexp>(3), B<scalb>(3)"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "2023-02-05"
msgstr "5 февраля 2023 г."

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: SH
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "VERSIONS"
msgstr "ВЕРСИИ"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron
#, fuzzy
#| msgid "These functions first appeared in glibc in version 2.1."
msgid "These functions were added in glibc 2.1."
msgstr "Эти функции впервые появились в glibc 2.1."

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid "C99, POSIX.1-2001, POSIX.1-2008."
msgstr "C99, POSIX.1-2001, POSIX.1-2008."

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.05"
msgstr "Linux man-pages 6.05"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "SCALBLN"
msgstr "SCALBLN"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2017-09-15"
msgstr "15 сентября 2017 г."

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Руководство программиста Linux"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<#include E<lt>math.hE<gt>>"
msgstr "B<#include E<lt>math.hE<gt>>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<double scalbln(double >I<x>B<, long int >I<exp>B<);>"
msgstr "B<double scalbln(double >I<x>B<, long int >I<exp>B<);>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<float scalblnf(float >I<x>B<, long int >I<exp>B<);>"
msgstr "B<float scalblnf(float >I<x>B<, long int >I<exp>B<);>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<long double scalblnl(long double >I<x>B<, long int >I<exp>B<);>"
msgstr "B<long double scalblnl(long double >I<x>B<, long int >I<exp>B<);>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<double scalbn(double >I<x>B<, int >I<exp>B<);>"
msgstr "B<double scalbn(double >I<x>B<, int >I<exp>B<);>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<float scalbnf(float >I<x>B<, int >I<exp>B<);>"
msgstr "B<float scalbnf(float >I<x>B<, int >I<exp>B<);>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<long double scalbnl(long double >I<x>B<, int >I<exp>B<);>"
msgstr "B<long double scalbnl(long double >I<x>B<, int >I<exp>B<);>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "Link with I<-lm>."
msgstr "Компонуется при указании параметра I<-lm>."

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"_ISOC99_SOURCE || _POSIX_C_SOURCE\\ E<gt>=\\ 200112L\n"
"    || /* Since glibc 2.19: */ _DEFAULT_SOURCE\n"
msgstr ""
"_ISOC99_SOURCE || _POSIX_C_SOURCE\\ E<gt>=\\ 200112L\n"
"    || /* начиная с glibc 2.19: */ _DEFAULT_SOURCE\n"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"_ISOC99_SOURCE || _POSIX_C_SOURCE\\ E<gt>=\\ 200112L\n"
"    || /* Since glibc 2.19: */ _DEFAULT_SOURCE\n"
"    || /* Glibc versions E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"
msgstr ""
"_ISOC99_SOURCE || _POSIX_C_SOURCE\\ E<gt>=\\ 200112L\n"
"    || /* начиная с glibc 2.19: */ _DEFAULT_SOURCE\n"
"    || /* в версиях glibc E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"

#.  .I errno
#.  is set to
#.  .BR ERANGE .
#. type: Plain text
#: opensuse-leap-15-6
msgid "An underflow floating-point exception (B<FE_UNDERFLOW>)  is raised."
msgstr ""
"Возникает исключение исчерпания степени чисел с плавающей запятой "
"(B<FE_UNDERFLOW>)."

#.  FIXME . Is it intentional that these functions do not set errno?
#.  Bug raised: http://sources.redhat.com/bugzilla/show_bug.cgi?id=6803
#. type: Plain text
#: opensuse-leap-15-6
msgid "These functions do not set I<errno>."
msgstr "Эти функции не изменяют I<errno>."

#. type: Plain text
#: opensuse-leap-15-6
msgid "These functions first appeared in glibc in version 2.1."
msgstr "Эти функции впервые появились в glibc 2.1."

#. type: tbl table
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"B<scalbn>(),\n"
"B<scalbnf>(),\n"
"B<scalbnl>(),\n"
msgstr ""
"B<scalbn>(),\n"
"B<scalbnf>(),\n"
"B<scalbnl>(),\n"

#. type: tbl table
#: opensuse-leap-15-6
#, no-wrap
msgid ".br\n"
msgstr ".br\n"

#. type: tbl table
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"B<scalbln>(),\n"
"B<scalblnf>(),\n"
"B<scalblnl>()"
msgstr ""
"B<scalbln>(),\n"
"B<scalblnf>(),\n"
"B<scalblnl>()"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "CONFORMING TO"
msgstr "СООТВЕТСТВИЕ СТАНДАРТАМ"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "COLOPHON"
msgstr "ЗАМЕЧАНИЯ"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Эта страница является частью проекта Linux I<man-pages> версии 4.16. "
"Описание проекта, информацию об ошибках и последнюю версию этой страницы "
"можно найти по адресу \\%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30 марта 2023 г."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"
