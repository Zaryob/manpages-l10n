# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# aereiae <aereiae@gmail.com>, 2014.
# Azamat Hackimov <azamat.hackimov@gmail.com>, 2013-2014, 2016.
# Dmitriy S. Seregin <dseregin@59.ru>, 2013.
# Katrin Kutepova <blackkatelv@gmail.com>, 2018.
# Lockal <lockalsash@gmail.com>, 2013.
# Yuri Kozlov <yuray@komyakino.ru>, 2011-2019.
# Баринов Владимир, 2016.
# Иван Павлов <pavia00@gmail.com>, 2017,2019.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-08-27 17:16+0200\n"
"PO-Revision-Date: 2019-10-06 09:20+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <man-pages-ru-talks@lists.sourceforge.net>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "remquo"
msgstr ""

#. type: TH
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid "2023-07-20"
msgstr "20 июля 2023 г."

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Linux man-pages 6.05.01"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "ИМЯ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "remquo, remquof, remquol - remainder and part of quotient"
msgstr "remquo, remquof, remquol - остаток и неполное частное"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Math library (I<libm>, I<-lm>)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "СИНТАКСИС"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>math.hE<gt>>\n"
msgstr "B<#include E<lt>math.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<double remquo(double >I<x>B<, double >I<y>B<, int *>I<quo>B<);>\n"
"B<float remquof(float >I<x>B<, float >I<y>B<, int *>I<quo>B<);>\n"
"B<long double remquol(long double >I<x>B<, long double >I<y>B<, int *>I<quo>B<);>\n"
msgstr ""
"B<double remquo(double >I<x>B<, double >I<y>B<, int *>I<quo>B<);>\n"
"B<float remquof(float >I<x>B<, float >I<y>B<, int *>I<quo>B<);>\n"
"B<long double remquol(long double >I<x>B<, long double >I<y>B<, int *>I<quo>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Требования макроса тестирования свойств для glibc (см. "
"B<feature_test_macros>(7)):"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<remquo>(), B<remquof>(), B<remquol>():"
msgstr "B<remquo>(), B<remquof>(), B<remquol>():"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "    _ISOC99_SOURCE || _POSIX_C_SOURCE E<gt>= 200112L\n"
msgstr "    _ISOC99_SOURCE || _POSIX_C_SOURCE E<gt>= 200112L\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"These functions compute the remainder and part of the quotient upon division "
"of I<x> by I<y>.  A few bits of the quotient are stored via the I<quo> "
"pointer.  The remainder is returned as the function result."
msgstr ""
"Эти функции вычисляют остаток и неполное частное при делении I<x> на I<y>. "
"Неполное частное сохраняется через указатель I<quo>. Остаток возвращается "
"как результат функции."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The value of the remainder is the same as that computed by the "
"B<remainder>(3)  function."
msgstr "Значение остатка тоже, что и при вычислении функцией B<remainder>(3)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The value stored via the I<quo> pointer has the sign of I<x\\ /\\ y> and "
#| "agrees with the quotient in at least the low order 3 bits."
msgid ""
"The value stored via the I<quo> pointer has the sign of I<x\\~/\\~y> and "
"agrees with the quotient in at least the low order 3 bits."
msgstr ""
"Значение, сохраняемое по указателю I<quo>, имеет знак I<x\\ /\\ y> и "
"сохраняет от частного не менее 3 младших бит."

#.  A possible application of this function might be the computation
#.  of sin(x). Compute remquo(x, pi/2, &quo) or so.
#.  glibc, UnixWare: return 3 bits
#.  MacOS 10: return 7 bits
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"For example, I<remquo(29.0,\\ 3.0)> returns -1.0 and might store 2.  Note "
"that the actual quotient might not fit in an integer."
msgstr ""
"Например, I<remquo(29.0,\\ 3.0)> возвращает -1.0 и может сохранить 2. "
"Заметим, что реальный остаток может не поместиться в integer."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "ВОЗВРАЩАЕМОЕ ЗНАЧЕНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"On success, these functions return the same value as the analogous functions "
"described in B<remainder>(3)."
msgstr ""
"При успешном выполнении данные функции возвращают тоже значение, что и "
"аналогичные функции, описанные в B<remainder>(3)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "If I<x> or I<y> is a NaN, a NaN is returned."
msgstr "Если значение I<x> или I<y> равно NaN, будет возвращено NaN."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If I<x> is an infinity, and I<y> is not a NaN, a domain error occurs, and a "
"NaN is returned."
msgstr ""
"Если I<x> стремится к бесконечности и I<y> не равно NaN, то будет "
"сгенерирована ошибка выхода за пределы области, а в качестве результата "
"будет возвращено NaN."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If I<y> is zero, and I<x> is not a NaN, a domain error occurs, and a NaN is "
"returned."
msgstr ""
"Если I<y> равно 0 и I<x> не равно NaN, то будет сгенерирована ошибка выхода "
"за пределы области, а в качестве результата будет возвращено NaN."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ОШИБКИ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"See B<math_error>(7)  for information on how to determine whether an error "
"has occurred when calling these functions."
msgstr ""
"Смотрите B<math_error>(7), чтобы определить, какие ошибки могут возникать "
"при вызове этих функций."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The following errors can occur:"
msgstr "Могут возникать следующие ошибки:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Domain error: I<x> is an infinity or I<y> is 0, and the other argument is not a NaN"
msgstr "Ошибка области: I<x> стремится к бесконечности или I<y> равно 0, и другой аргумент не равен NaN."

#.  .I errno
#.  is set to
#.  .BR EDOM .
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "An invalid floating-point exception (B<FE_INVALID>)  is raised."
msgstr "Возникает исключение неправильной плавающей точки (B<FE_INVALID>)."

#. #-#-#-#-#  archlinux: remquo.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  FIXME . Is it intentional that these functions do not set errno?
#.  Bug raised: https://www.sourceware.org/bugzilla/show_bug.cgi?id=6802
#. type: Plain text
#. #-#-#-#-#  debian-bookworm: remquo.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  FIXME . Is it intentional that these functions do not set errno?
#.  Bug raised: http://sources.redhat.com/bugzilla/show_bug.cgi?id=6802
#. type: Plain text
#. #-#-#-#-#  debian-unstable: remquo.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  FIXME . Is it intentional that these functions do not set errno?
#.  Bug raised: http://sources.redhat.com/bugzilla/show_bug.cgi?id=6802
#. type: Plain text
#. #-#-#-#-#  fedora-39: remquo.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  FIXME . Is it intentional that these functions do not set errno?
#.  Bug raised: https://www.sourceware.org/bugzilla/show_bug.cgi?id=6802
#. type: Plain text
#. #-#-#-#-#  fedora-rawhide: remquo.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  FIXME . Is it intentional that these functions do not set errno?
#.  Bug raised: https://www.sourceware.org/bugzilla/show_bug.cgi?id=6802
#. type: Plain text
#. #-#-#-#-#  mageia-cauldron: remquo.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  FIXME . Is it intentional that these functions do not set errno?
#.  Bug raised: http://sources.redhat.com/bugzilla/show_bug.cgi?id=6802
#. type: Plain text
#. #-#-#-#-#  opensuse-leap-15-6: remquo.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  FIXME . Is it intentional that these functions do not set errno?
#.  Bug raised: http://sources.redhat.com/bugzilla/show_bug.cgi?id=6802
#. type: Plain text
#. #-#-#-#-#  opensuse-tumbleweed: remquo.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  FIXME . Is it intentional that these functions do not set errno?
#.  Bug raised: https://www.sourceware.org/bugzilla/show_bug.cgi?id=6802
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "These functions do not set I<errno>."
msgstr "Эти функции не изменяют I<errno>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "АТРИБУТЫ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr "Описание терминов данного раздела смотрите в B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Интерфейс"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Атрибут"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Значение"

#. type: tbl table
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<remquo>(),\n"
"B<remquof>(),\n"
"B<remquol>()"
msgstr ""
"B<remquo>(),\n"
"B<remquof>(),\n"
"B<remquol>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Безвредность в нитях"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "СТАНДАРТЫ"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "C11, POSIX.1-2008."
msgstr "C11, POSIX.1-2008."

#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "ИСТОРИЯ"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "glibc 2.1.  C99, POSIX.1-2001."
msgstr "glibc 2.1.  C99, POSIX.1-2001."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМ. ТАКЖЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<fmod>(3), B<logb>(3), B<remainder>(3)"
msgstr "B<fmod>(3), B<logb>(3), B<remainder>(3)"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "2022-12-15"
msgstr "15 декабря 2022 г."

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: SH
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "VERSIONS"
msgstr "ВЕРСИИ"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron
#, fuzzy
#| msgid "These functions first appeared in glibc in version 2.1."
msgid "These functions were added in glibc 2.1."
msgstr "Эти функции впервые появились в glibc 2.1."

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid "C99, POSIX.1-2001, POSIX.1-2008."
msgstr "C99, POSIX.1-2001, POSIX.1-2008."

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.05"
msgstr "Linux man-pages 6.05"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "REMQUO"
msgstr "REMQUO"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2017-09-15"
msgstr "15 сентября 2017 г."

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "GNU"
msgstr "GNU"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Руководство программиста Linux"

#. type: Plain text
#: opensuse-leap-15-6
msgid "Link with I<-lm>."
msgstr "Компонуется при указании параметра I<-lm>."

#. type: Plain text
#: opensuse-leap-15-6
msgid "_ISOC99_SOURCE || _POSIX_C_SOURCE\\ E<gt>=\\ 200112L"
msgstr "_ISOC99_SOURCE || _POSIX_C_SOURCE\\ E<gt>=\\ 200112L"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"The value stored via the I<quo> pointer has the sign of I<x\\ /\\ y> and "
"agrees with the quotient in at least the low order 3 bits."
msgstr ""
"Значение, сохраняемое по указателю I<quo>, имеет знак I<x\\ /\\ y> и "
"сохраняет от частного не менее 3 младших бит."

#. type: Plain text
#: opensuse-leap-15-6
msgid "These functions first appeared in glibc in version 2.1."
msgstr "Эти функции впервые появились в glibc 2.1."

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "CONFORMING TO"
msgstr "СООТВЕТСТВИЕ СТАНДАРТАМ"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "COLOPHON"
msgstr "ЗАМЕЧАНИЯ"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Эта страница является частью проекта Linux I<man-pages> версии 4.16. "
"Описание проекта, информацию об ошибках и последнюю версию этой страницы "
"можно найти по адресу \\%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30 марта 2023 г."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"
