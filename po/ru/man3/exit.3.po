# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Azamat Hackimov <azamat.hackimov@gmail.com>, 2012.
# Yuri Kozlov <yuray@komyakino.ru>, 2011-2019.
# Иван Павлов <pavia00@gmail.com>, 2017.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-08-27 16:56+0200\n"
"PO-Revision-Date: 2019-10-05 07:57+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <man-pages-ru-talks@lists.sourceforge.net>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<exit>()"
msgid "exit"
msgstr "B<exit>()"

#. type: TH
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid "2023-07-20"
msgstr "20 июля 2023 г."

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Linux man-pages 6.05.01"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "ИМЯ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "exit - cause normal process termination"
msgstr "exit - вызывает нормальное завершение процесса"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "СИНТАКСИС"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>stdlib.hE<gt>>\n"
msgstr "B<#include E<lt>stdlib.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<void exit(int >I<status>B<);>\n"
msgid "B<[[noreturn]] void exit(int >I<status>B<);>\n"
msgstr "B<void exit(int >I<status>B<);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The B<exit>()  function causes normal process termination and the value "
#| "of I<status & 0377> is returned to the parent (see B<wait>(2))."
msgid ""
"The B<exit>()  function causes normal process termination and the least "
"significant byte of I<status> (i.e., I<status & 0xFF>) is returned to the "
"parent (see B<wait>(2))."
msgstr ""
"Функция B<exit>() выполняет нормальное завершение процесса и возвращает "
"значение I<status & 0377> породившему процессу (смотрите B<wait>(2))."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"All functions registered with B<atexit>(3)  and B<on_exit>(3)  are called, "
"in the reverse order of their registration.  (It is possible for one of "
"these functions to use B<atexit>(3)  or B<on_exit>(3)  to register an "
"additional function to be executed during exit processing; the new "
"registration is added to the front of the list of functions that remain to "
"be called.)  If one of these functions does not return (e.g., it calls "
"B<_exit>(2), or kills itself with a signal), then none of the remaining "
"functions is called, and further exit processing (in particular, flushing of "
"B<stdio>(3)  streams) is abandoned.  If a function has been registered "
"multiple times using B<atexit>(3)  or B<on_exit>(3), then it is called as "
"many times as it was registered."
msgstr ""
"Вызываются все функции, зарегистрированные с помощью B<atexit>(3) и "
"B<on_exit>(3), в обратном по отношению к регистрации порядке (в этих "
"функциях возможно использовать B<atexit>(3) или B<on_exit>(3) для "
"регистрации дополнительной функции, которая будет также вызвана при выходе; "
"при этом она добавляется в начало списка функций, которые осталось вызвать). "
"Если из одной из функций не происходит возврат (например, она вызывает "
"B<_exit>(2) или завершает себя по сигналу), то оставшиеся функции не "
"вызываются, и дальнейший процесс выхода прекращается (в частности, запись "
"потоков B<stdio>(3)). Если функция зарегистрирована с помощью B<atexit>(3) "
"или B<on_exit>(3) несколько раз, то она вызывается столько раз, сколько "
"зарегистрирована."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"All open B<stdio>(3)  streams are flushed and closed.  Files created by "
"B<tmpfile>(3)  are removed."
msgstr ""
"Все открытые потоки B<stdio>(3) записываются и закрываются. Файлы, созданные "
"B<tmpfile>(3), удаляются."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The C standard specifies two constants, B<EXIT_SUCCESS> and B<EXIT_FAILURE>, "
"that may be passed to B<exit>()  to indicate successful or unsuccessful "
"termination, respectively."
msgstr ""
"В стандарте Си определены две константы, B<EXIT_SUCCESS> и B<EXIT_FAILURE>, "
"которые можно передавать B<exit>() для указания корректности или "
"некорректности завершения, соответственно."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "ВОЗВРАЩАЕМОЕ ЗНАЧЕНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The B<exit>()  function does not return."
msgstr "Функция B<exit>() не возвращает выполнение."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "АТРИБУТЫ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr "Описание терминов данного раздела смотрите в B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Интерфейс"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Атрибут"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Значение"

#. type: tbl table
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<exit>()"
msgstr "B<exit>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Безвредность в нитях"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "MT-Unsafe race:exit"
msgstr "MT-Unsafe race:exit"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<exit>()  function uses a global variable that is not protected, so it "
"is not thread-safe."
msgstr ""
"Функция B<exit>() использует глобальную незащищённую переменную, поэтому "
"функцию нельзя использовать в нескольких нитях одновременно."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "СТАНДАРТЫ"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "C11, POSIX.1-2008."
msgstr "C11, POSIX.1-2008."

#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "ИСТОРИЯ"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, fuzzy
#| msgid "POSIX.1-2001, SVr4, 4.3BSD."
msgid "C89, POSIX.1-2001, SVr4, 4.3BSD."
msgstr "POSIX.1-2001, SVr4, 4.3BSD."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "ЗАМЕЧАНИЯ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The behavior is undefined if one of the functions registered using "
"B<atexit>(3)  and B<on_exit>(3)  calls either B<exit>()  or B<longjmp>(3).  "
"Note that a call to B<execve>(2)  removes registrations created using "
"B<atexit>(3)  and B<on_exit>(3)."
msgstr ""
"Поведение не определено, если одна из функций, зарегистрированных с помощью "
"B<atexit>(3) и B<on_exit>(3), вызовет B<exit>() или B<longjmp>(3). Заметим, "
"что вызов B<execve>(2) удаляет регистрацию функций, сделанную с помощью "
"B<atexit>(3) и B<on_exit>(3)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The use of B<EXIT_SUCCESS> and B<EXIT_FAILURE> is slightly more portable (to "
"non-UNIX environments) than the use of 0 and some nonzero value like 1 or "
"-1.  In particular, VMS uses a different convention."
msgstr ""
"Использование B<EXIT_SUCCESS> и B<EXIT_FAILURE> является более переносимым "
"методом (в не-UNIX окружения), чем указание 0 и какого-то ненулевого "
"значения (например, 1 или -1). В частности, в VMS используется другое "
"соглашение."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"BSD has attempted to standardize exit codes (which some C libraries such as "
"the GNU C library have also adopted); see the file I<E<lt>sysexits.hE<gt>>."
msgstr ""
"В BSD пытались стандартизовать коды завершения (которые также были взяты и "
"другие библиотеки Си, например  GNU C); смотрите файл I<E<lt>sysexits."
"hE<gt>>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"After B<exit>(), the exit status must be transmitted to the parent process.  "
"There are three cases:"
msgstr ""
"После B<exit>() код выхода должен быть передан родительскому процессу. Есть "
"три варианта:"

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "\\[bu]"
msgstr "\\[bu]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If the parent has set B<SA_NOCLDWAIT>, or has set the B<SIGCHLD> handler to "
"B<SIG_IGN>, the status is discarded and the child dies immediately."
msgstr ""
"Если родитель установил обработчик B<SA_NOCLDWAIT> или B<SIGCHLD> равным "
"B<SIG_IGN>, то код выхода отбрасывается и потомок завершается сразу."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If the parent was waiting on the child, it is notified of the exit status "
"and the child dies immediately."
msgstr ""
"Если родитель ожидает завершения потомка, то он получает код выхода и "
"потомок сразу завершается."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Otherwise, the child becomes a \"zombie\" process: most of the process "
"resources are recycled, but a slot containing minimal information about the "
"child process (termination status, resource usage statistics) is retained in "
"process table.  This allows the parent to subsequently use B<waitpid>(2)  "
"(or similar) to learn the termination status of the child; at that point the "
"zombie process slot is released."
msgstr ""
"Иначе потомок становится процессом «зомби»: большинство ресурсов процесс "
"задействуется вторично, но слот с минимумом информации о процессе-потомке "
"(код завершения, статистика по использованию ресурсов) остаётся в таблице "
"процессов. Это позволяет родителю в дальнейшем использовать B<waitpid>(2) "
"(или подобный) для получения кода завершения потомка; после этого слот под "
"процесс-зомби освобождается."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If the implementation supports the B<SIGCHLD> signal, this signal is sent to "
"the parent.  If the parent has set B<SA_NOCLDWAIT>, it is undefined whether "
"a B<SIGCHLD> signal is sent."
msgstr ""
"Если в реализации поддерживается сигнал B<SIGCHLD>, то он посылается "
"родителю. Если родитель установил флаг B<SA_NOCLDWAIT>, то поведение при "
"сигнале B<SIGCHLD> не определено."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Signals sent to other processes"
msgstr "Сигналы, посылаемые другим процессам"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If the exiting process is a session leader and its controlling terminal is "
"the controlling terminal of the session, then each process in the foreground "
"process group of this controlling terminal is sent a B<SIGHUP> signal, and "
"the terminal is disassociated from this session, allowing it to be acquired "
"by a new controlling process."
msgstr ""
"Если существующий процесс является лидером сеанса и управляющим терминала "
"сеанса, то каждому процессу из группы фонового режима этого управляющего "
"терминала посылается сигнал B<SIGHUP>, и терминал отключается от сеанса, "
"чтобы его можно было захватить новому управляющему процессу."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If the exit of the process causes a process group to become orphaned, and if "
"any member of the newly orphaned process group is stopped, then a B<SIGHUP> "
"signal followed by a B<SIGCONT> signal will be sent to each process in this "
"process group.  See B<setpgid>(2)  for an explanation of orphaned process "
"groups."
msgstr ""
"Если завершение процесса приводит к осиротению группы процессов, и если "
"любой член только что осиротевшей группы останавливается, то каждому "
"процессу этой группы после сигнала B<SIGHUP> будет послан сигнал B<SIGCONT>. "
"Описание процесса осиротения группы смотрите в B<setpgid>(2)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Except in the above cases, where the signalled processes may be children of "
"the terminating process, termination of a process does I<not> in general "
"cause a signal to be sent to children of that process.  However, a process "
"can use the B<prctl>(2)  B<PR_SET_PDEATHSIG> operation to arrange that it "
"receives a signal if its parent terminates."
msgstr ""
"За исключением описанные выше случаев, когда процессы, куда посылается "
"сигнал, могут быть потомками завершающегося процесса, завершение процесса, "
"обычно, I<не> приводит к отправке сигнала потомкам этого процесса. Однако, "
"процесс может вызвать B<prctl>(2) с операцией B<PR_SET_PDEATHSIG>, чтобы "
"подготовиться к получению сигнала, если его родитель завершает работу."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМ. ТАКЖЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<_exit>(2), B<get_robust_list>(2), B<setpgid>(2), B<wait>(2), B<atexit>(3), "
"B<on_exit>(3), B<tmpfile>(3)"
msgstr ""
"B<_exit>(2), B<get_robust_list>(2), B<setpgid>(2), B<wait>(2), B<atexit>(3), "
"B<on_exit>(3), B<tmpfile>(3)"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "2023-02-05"
msgstr "5 февраля 2023 г."

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron
msgid "POSIX.1-2001, POSIX.1-2008, C99, SVr4, 4.3BSD."
msgstr "POSIX.1-2001, POSIX.1-2008, C99, SVr4, 4.3BSD."

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.05"
msgstr "Linux man-pages 6.05"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "EXIT"
msgstr "EXIT"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2017-09-15"
msgstr "15 сентября 2017 г."

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Руководство программиста Linux"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid "B<void exit(int >I<status>B<);>\n"
msgstr "B<void exit(int >I<status>B<);>\n"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"The B<exit>()  function causes normal process termination and the value of "
"I<status & 0377> is returned to the parent (see B<wait>(2))."
msgstr ""
"Функция B<exit>() выполняет нормальное завершение процесса и возвращает "
"значение I<status & 0377> породившему процессу (смотрите B<wait>(2))."

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "CONFORMING TO"
msgstr "СООТВЕТСТВИЕ СТАНДАРТАМ"

#. type: Plain text
#: opensuse-leap-15-6
msgid "POSIX.1-2001, POSIX.1-2008, C89, C99, SVr4, 4.3BSD."
msgstr "POSIX.1-2001, POSIX.1-2008, C89, C99, SVr4, 4.3BSD."

#. type: Plain text
#: opensuse-leap-15-6
#, fuzzy
#| msgid ""
#| "BSD has attempted to standardize exit codes (which some C libraries such "
#| "as the GNU C library have also adopted); see the file I<E<lt>sysexits."
#| "hE<gt>>."
msgid ""
"BSD has attempted to standardize exit codes; see the file I<E<lt>sysexits."
"hE<gt>>."
msgstr ""
"В BSD пытались стандартизовать коды завершения (которые также были взяты и "
"другие библиотеки Си, например  GNU C); смотрите файл I<E<lt>sysexits."
"hE<gt>>."

#. type: IP
#: opensuse-leap-15-6
#, no-wrap
msgid "\\(bu"
msgstr "\\(bu"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "COLOPHON"
msgstr "ЗАМЕЧАНИЯ"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Эта страница является частью проекта Linux I<man-pages> версии 4.16. "
"Описание проекта, информацию об ошибках и последнюю версию этой страницы "
"можно найти по адресу \\%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30 марта 2023 г."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"
