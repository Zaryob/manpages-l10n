# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Alexander Golubev <fatzer2@gmail.com>, 2018.
# Azamat Hackimov <azamat.hackimov@gmail.com>, 2011, 2014-2016.
# Hotellook, 2014.
# Nikita <zxcvbnm3230@mail.ru>, 2014.
# Spiros Georgaras <sng@hellug.gr>, 2016.
# Vladislav <ivladislavefimov@gmail.com>, 2015.
# Yuri Kozlov <yuray@komyakino.ru>, 2011-2019.
# Иван Павлов <pavia00@gmail.com>, 2017.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-08-27 17:21+0200\n"
"PO-Revision-Date: 2019-10-15 18:55+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <man-pages-ru-talks@lists.sourceforge.net>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "seteuid"
msgstr "seteuid"

#. type: TH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30 марта 2023 г."

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Linux man-pages 6.05.01"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "ИМЯ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "seteuid, setegid - set effective user or group ID"
msgstr ""
"seteuid, setegid - устанавливает эффективный идентификатор пользователя или "
"группы"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "СИНТАКСИС"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>unistd.hE<gt>>\n"
msgstr "B<#include E<lt>unistd.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"B<int seteuid(uid_t >I<euid>B<);>\n"
"B<int setegid(gid_t >I<egid>B<);>\n"
msgstr ""
"B<int seteuid(uid_t >I<euid>B<);>\n"
"B<int setegid(gid_t >I<egid>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Требования макроса тестирования свойств для glibc (см. "
"B<feature_test_macros>(7)):"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<seteuid>(), B<setegid>():"
msgstr "B<seteuid>(), B<setegid>():"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "    _POSIX_C_SOURCE E<gt>= 200112L\n"
#| "        || /* Glibc E<lt>= 2.19: */ _BSD_SOURCE\n"
msgid ""
"    _POSIX_C_SOURCE E<gt>= 200112L\n"
"        || /* glibc E<lt>= 2.19: */ _BSD_SOURCE\n"
msgstr ""
"    _POSIX_C_SOURCE E<gt>= 200112L\n"
"        || /* Glibc E<lt>= 2.19: */ _BSD_SOURCE\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<seteuid>()  sets the effective user ID of the calling process.  "
"Unprivileged processes may only set the effective user ID to the real user "
"ID, the effective user ID or the saved set-user-ID."
msgstr ""
"Вызов B<seteuid>() устанавливает эффективный идентификатор пользователя "
"вызывающего процесса. Непривилегированные процессы могут менять эффективный "
"идентификатор пользователя только на действительный, эффективный или "
"сохранённый идентификатор пользователя."

#.  When
#.  .I euid
#.  equals \-1, nothing is changed.
#.  (This is an artifact of the implementation in glibc of seteuid()
#.  using setresuid(2).)
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Precisely the same holds for B<setegid>()  with \"group\" instead of "
"\"user\"."
msgstr ""
"Тоже самое справедливо при работе B<setegid>() с «групповым» "
"идентификатором, а не «пользовательским»."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "ВОЗВРАЩАЕМОЕ ЗНАЧЕНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "On success, zero is returned.  On error, -1 is returned, and I<errno> is "
#| "set appropriately."
msgid ""
"On success, zero is returned.  On error, -1 is returned, and I<errno> is set "
"to indicate the error."
msgstr ""
"При успешном выполнении возвращается 0. В случае ошибки возвращается -1, а "
"I<errno> устанавливается в соответствующее значение."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"I<Note>: there are cases where B<seteuid>()  can fail even when the caller "
"is UID 0; it is a grave security error to omit checking for a failure return "
"from B<seteuid>()."
msgstr ""
"I<Замечание>: есть случаи, когда B<seteuid>() может завершиться с ошибкой "
"даже когда UID вызывающего равен 0; это серьёзная ошибка безопасности — не "
"проверять возвращаемое значение B<seteuid>()."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ОШИБКИ"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The target user or group ID is not valid in this user namespace."
msgstr ""
"Целевой идентификатор пользователя или группы некорректен в этом "
"пользовательском пространстве имён."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EPERM>"
msgstr "B<EPERM>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"In the case of B<seteuid>(): the calling process is not privileged (does not "
"have the B<CAP_SETUID> capability in its user namespace) and I<euid> does "
"not match the current real user ID, current effective user ID, or current "
"saved set-user-ID."
msgstr ""
"Для B<seteuid>(): вызывающий процесс не имеет прав (не имеет мандата "
"B<CAP_SETUID> в своём пространстве имён пользователя) и I<euid> не совпадает "
"с текущим действительным, эффективным или сохранённым идентификатором "
"пользователя."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"In the case of B<setegid>(): the calling process is not privileged (does not "
"have the B<CAP_SETGID> capability in its user namespace) and I<egid> does "
"not match the current real group ID, current effective group ID, or current "
"saved set-group-ID."
msgstr ""
"Для B<setegid>(): вызывающий процесс не имеет прав (не имеет мандата "
"B<CAP_SETGID> в своём пространстве имён пользователя) и I<egid> не совпадает "
"с текущим действительным, эффективным или сохранённым идентификатором группы."

#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "ВЕРСИИ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Setting the effective user (group) ID to the saved set-user-ID (saved set-"
"group-ID) is possible since Linux 1.1.37 (1.1.38).  On an arbitrary system "
"one should check B<_POSIX_SAVED_IDS>."
msgstr ""
"Установка эффективного идентификатора пользователя (группы) в сохранённый "
"идентификатор пользователя (группы) возможна с версии Linux 1.1.37 (1.1.38). "
"В других системах надо проверять B<_POSIX_SAVED_IDS>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Under glibc 2.0, B<seteuid(>I<euid>B<)> is equivalent to B<setreuid(-1,>I< "
"euid>B<)> and hence may change the saved set-user-ID.  Under glibc 2.1 and "
"later, it is equivalent to B<setresuid(-1,>I< euid>B<, -1)> and hence does "
"not change the saved set-user-ID.  Analogous remarks hold for B<setegid>(), "
"with the difference that the change in implementation from B<setregid(-1,>I< "
"egid>B<)> to B<setresgid(-1,>I< egid>B<, -1)> occurred in glibc 2.2 or 2.3 "
"(depending on the hardware architecture)."
msgstr ""
"В glibc 2.0 вызов B<seteuid(>I<euid>B<)>эквивалентен B<setreuid(-1,>I< "
"euid>B<)>, и поэтому может изменить сохранённый идентификатор пользователя. "
"В glibc 2.1 и новее он эквивалентен B<setresuid(-1,>I< euid>B<, -1)> и "
"поэтому не изменяет сохранённый идентификатор пользователя. Аналогичные "
"замечания относятся и к B<setegid>(), с той разницей, что изменение в "
"реализации из B<setregid(-1,>I< egid>B<)> в B<setresgid(-1,>I< egid>B<, -1)> "
"произошло в glibc 2.2 или 2.3 (зависит от аппаратной архитектуры)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"According to POSIX.1, B<seteuid>()  (B<setegid>())  need not permit I<euid> "
"(I<egid>)  to be the same value as the current effective user (group) ID, "
"and some implementations do not permit this."
msgstr ""
"Согласно POSIX.1, B<seteuid>() (B<setegid>()) необходимо запрещать I<euid> "
"(I<egid>) равный текущему эффективному идентификатору пользователя (группе), "
"и некоторые реализации не позволяют этого."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "C library/kernel differences"
msgstr "Отличия между библиотекой C и ядром"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"On Linux, B<seteuid>()  and B<setegid>()  are implemented as library "
"functions that call, respectively, B<setreuid>(2)  and B<setregid>(2)."
msgstr ""
"В Linux, B<seteuid>() и B<setegid>() реализованы в виде библиотечных "
"функций, которые вызывают B<setreuid>(2) и B<setregid>(2), соответственно."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "СТАНДАРТЫ"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr "POSIX.1-2008."

#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "ИСТОРИЯ"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "POSIX.1-2001, 4.3BSD."
msgstr "POSIX.1-2001, 4.3BSD."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМ. ТАКЖЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<geteuid>(2), B<setresuid>(2), B<setreuid>(2), B<setuid>(2), "
"B<capabilities>(7), B<credentials>(7), B<user_namespaces>(7)"
msgstr ""
"B<geteuid>(2), B<setresuid>(2), B<setreuid>(2), B<setuid>(2), "
"B<capabilities>(7), B<credentials>(7), B<user_namespaces>(7)"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "2023-02-05"
msgstr "5 февраля 2023 г."

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid "POSIX.1-2001, POSIX.1-2008, 4.3BSD."
msgstr "POSIX.1-2001, POSIX.1-2008, 4.3BSD."

#. type: SH
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "NOTES"
msgstr "ЗАМЕЧАНИЯ"

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.05"
msgstr "Linux man-pages 6.05"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "SETEUID"
msgstr "SETEUID"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2017-09-15"
msgstr "15 сентября 2017 г."

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Руководство программиста Linux"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<#include E<lt>sys/types.hE<gt>>"
msgstr "B<#include E<lt>sys/types.hE<gt>>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<#include E<lt>unistd.hE<gt>>"
msgstr "B<#include E<lt>unistd.hE<gt>>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<int seteuid(uid_t >I<euid>B<);>"
msgstr "B<int seteuid(uid_t >I<euid>B<);>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<int setegid(gid_t >I<egid>B<);>"
msgstr "B<int setegid(gid_t >I<egid>B<);>"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"_POSIX_C_SOURCE\\ E<gt>=\\ 200112L\n"
"    || /* Glibc versions E<lt>= 2.19: */ _BSD_SOURCE\n"
msgstr ""
"_POSIX_C_SOURCE\\ E<gt>=\\ 200112L\n"
"    || /* версии glibc E<lt>= 2.19: */ _BSD_SOURCE\n"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"On success, zero is returned.  On error, -1 is returned, and I<errno> is set "
"appropriately."
msgstr ""
"При успешном выполнении возвращается 0. В случае ошибки возвращается -1, а "
"I<errno> устанавливается в соответствующее значение."

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "CONFORMING TO"
msgstr "СООТВЕТСТВИЕ СТАНДАРТАМ"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "COLOPHON"
msgstr "ЗАМЕЧАНИЯ"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Эта страница является частью проекта Linux I<man-pages> версии 4.16. "
"Описание проекта, информацию об ошибках и последнюю версию этой страницы "
"можно найти по адресу \\%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"
