# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# aereiae <aereiae@gmail.com>, 2014.
# Alexey <a.chepugov@gmail.com>, 2015.
# Azamat Hackimov <azamat.hackimov@gmail.com>, 2013-2017.
# Dmitriy S. Seregin <dseregin@59.ru>, 2013.
# Dmitry Bolkhovskikh <d20052005@yandex.ru>, 2017.
# ITriskTI <ITriskTI@gmail.com>, 2013.
# Max Is <ismax799@gmail.com>, 2016.
# Yuri Kozlov <yuray@komyakino.ru>, 2011-2019.
# Иван Павлов <pavia00@gmail.com>, 2017.
# Малянов Евгений Викторович <maljanow@outlook.com>, 2014.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-08-27 17:09+0200\n"
"PO-Revision-Date: 2019-10-06 08:59+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <man-pages-ru-talks@lists.sourceforge.net>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "move_pages"
msgstr "move_pages"

#. type: TH
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid "2023-07-15"
msgstr "15 июля 2023 г."

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Linux man-pages 6.05.01"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "ИМЯ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "move_pages - move individual pages of a process to another node"
msgstr "move_pages - перемещает отдельные страницы процесса на другой узел"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "NUMA (Non-Uniform Memory Access) policy library (I<libnuma>, I<-lnuma>)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "СИНТАКСИС"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>numaif.hE<gt>>\n"
msgstr "B<#include E<lt>numaif.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "B<long move_pages(int >I<pid>B<, unsigned long count, void **>I<pages>B<,>\n"
#| "B<                const int *>I<nodes>B<, int *>I<status>B<, int >I<flags>B<);>\n"
msgid ""
"B<long move_pages(int >I<pid>B<, unsigned long >I<count>B<, void *>I<pages>B<[.>I<count>B<],>\n"
"B<                const int >I<nodes>B<[.>I<count>B<], int >I<status>B<[.>I<count>B<], int >I<flags>B<);>\n"
msgstr ""
"B<long move_pages(int >I<pid>B<, unsigned long count, void **>I<pages>B<,>\n"
"B<                const int *>I<nodes>B<, int *>I<status>B<, int >I<flags>B<);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<move_pages>()  moves the specified I<pages> of the process I<pid> to the "
"memory nodes specified by I<nodes>.  The result of the move is reflected in "
"I<status>.  The I<flags> indicate constraints on the pages to be moved."
msgstr ""
"Вызов B<move_pages>() перемещает указанные в I<pages> страницы процесса с "
"I<pid> в память узлов, заданных в I<nodes>. Результат перемещения отражается "
"в I<status>. В I<flags> задаются ограничения на перемещаемые страницы."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"I<pid> is the ID of the process in which pages are to be moved.  If I<pid> "
"is 0, then B<move_pages>()  moves pages of the calling process."
msgstr ""
"Значение I<pid> представляет собой идентификатор процесса, в котором "
"перемещаются страницы. Если I<pid> равен 0, то B<move_pages>()  перемещается "
"страницы вызывающего процесса."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "To move pages in another process requires the following privileges:"
msgstr "Для перемещения страниц в другом процессе требуются следующие права:"

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "\\[bu]"
msgstr "\\[bu]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "In kernels up to and including Linux 4.12: the caller must be privileged "
#| "(B<CAP_SYS_NICE>)  or the real or effective user ID of the calling "
#| "process must match the real or saved-set user ID of the target process."
msgid ""
"Up to and including Linux 4.12: the caller must be privileged "
"(B<CAP_SYS_NICE>)  or the real or effective user ID of the calling process "
"must match the real or saved-set user ID of the target process."
msgstr ""
"В ядрах до Linux 4.12 включительно: вызывающий должен иметь права "
"(B<CAP_SYS_NICE>) или реальный или эффективный пользовательский ID "
"вызывающего процесса должен совпадать с реальным или сохранённым "
"пользовательским ID целевого процесса."

#.  commit 197e7e521384a23b9e585178f3f11c9fa08274b9
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The older rules allowed the caller to discover various virtual address "
"choices made by the kernel that could lead to the defeat of address-space-"
"layout randomization for a process owned by the same UID as the caller, the "
"rules were changed starting with Linux 4.13.  Since Linux 4.13, permission "
"is governed by a ptrace access mode B<PTRACE_MODE_READ_REALCREDS> check with "
"respect to the target process; see B<ptrace>(2)."
msgstr ""
"Старые правила позволяли вызывающему обнаруживать различные виртуальные "
"адреса, выбранные ядром, что лишает смысла случайность расположения "
"раскладки адресного пространства процесса, принадлежащему тому же UID что и "
"вызывающий; правила были изменены в Linux 4.13. Начиная с Linux 4.13, права "
"регулируются проверкой режима доступа ptrace B<PTRACE_MODE_READ_REALCREDS> "
"целевого процесса; смотрите B<ptrace>(2)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"I<count> is the number of pages to move.  It defines the size of the three "
"arrays I<pages>, I<nodes>, and I<status>."
msgstr ""
"В аргументе I<count> задаётся количество перемещаемых страниц. Он определяет "
"размер трёх массивов: I<pages>, I<nodes> и I<status>."

#.  FIXME Describe the result if pointers in the 'pages' array are
#.  not aligned to page boundaries
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"I<pages> is an array of pointers to the pages that should be moved.  These "
"are pointers that should be aligned to page boundaries.  Addresses are "
"specified as seen by the process specified by I<pid>."
msgstr ""
"Аргумент I<pages> представляет собой массив указателей на страницы, которые "
"должны быть перемещены. Данные указатели должны быть выровнены на границу "
"страницы. Адреса указываются в том же виде, в каком доступны процессу с "
"заданным I<pid>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"I<nodes> is an array of integers that specify the desired location for each "
"page.  Each element in the array is a node number.  I<nodes> can also be "
"NULL, in which case B<move_pages>()  does not move any pages but instead "
"will return the node where each page currently resides, in the I<status> "
"array.  Obtaining the status of each page may be necessary to determine "
"pages that need to be moved."
msgstr ""
"Аргумент I<nodes> представляет собой массив целых, которыми задаются "
"желаемые местоположения каждой страницы. Каждый элемент массива представляет "
"собой номер узла. Также, значением I<nodes> может быть NULL; в этом случае "
"B<move_pages>() не перемещает страницы, а записывает в массив I<status> "
"номер узла, на котором расположена каждая страница в данный момент. "
"Получение состояния каждой страницы может потребоваться для определения "
"страниц, которые нужно переместить."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "I<status> is an array of integers that return the status of each page.  "
#| "The array contains valid values only if B<move_pages>()  did not return "
#| "an error."
msgid ""
"I<status> is an array of integers that return the status of each page.  The "
"array contains valid values only if B<move_pages>()  did not return an "
"error.  Preinitialization of the array to a value which cannot represent a "
"real numa node or valid error of status array could help to identify pages "
"that have been migrated."
msgstr ""
"Аргумент I<status> представляет собой массив целых, которым определяется "
"состояние каждой страницы. Массив содержит правильные значения только, если "
"вызов B<move_pages>() не завершился с ошибкой."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"I<flags> specify what types of pages to move.  B<MPOL_MF_MOVE> means that "
"only pages that are in exclusive use by the process are to be moved.  "
"B<MPOL_MF_MOVE_ALL> means that pages shared between multiple processes can "
"also be moved.  The process must be privileged (B<CAP_SYS_NICE>)  to use "
"B<MPOL_MF_MOVE_ALL>."
msgstr ""
"В аргументе I<flags> указываются типы перемещаемых страниц. Флаг "
"B<MPOL_MF_MOVE> означает, что будут перемещены только страницы, которые "
"монопольно используются процессом. Флаг B<MPOL_MF_MOVE_ALL> означает, что "
"также будут перемещены страницы, которые используются совместно с другими "
"процессами. Для использования флага B<MPOL_MF_MOVE_ALL> процесс должен быть "
"привилегированным (B<CAP_SYS_NICE>)."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Page states in the status array"
msgstr "Состояние страницы в массиве состояний"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The following values can be returned in each element of the I<status> array."
msgstr ""
"В каждом элементе массива I<status> могут возвращаться следующие значения:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<0..MAX_NUMNODES>"
msgstr "B<0..MAX_NUMNODES>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Identifies the node on which the page resides."
msgstr "Номер узла, в котором расположена страница."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-EACCES>"
msgstr "B<-EACCES>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The page is mapped by multiple processes and can be moved only if "
"B<MPOL_MF_MOVE_ALL> is specified."
msgstr ""
"Страница отображается несколькими процессами и может быть перемещена только, "
"если указан флаг B<MPOL_MF_MOVE_ALL>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-EBUSY>"
msgstr "B<-EBUSY>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The page is currently busy and cannot be moved.  Try again later.  This "
"occurs if a page is undergoing I/O or another kernel subsystem is holding a "
"reference to the page."
msgstr ""
"Страница в данный момент занята и не может быть перемещена. Попробуйте "
"позднее. Такое случается, если со страницей выполняется операция ввода-"
"вывода или другая подсистема ядра удерживается ссылку на эту страницу."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-EFAULT>"
msgstr "B<-EFAULT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "This is a zero page or the memory area is not mapped by the process."
msgstr "Это нулевая страница или область памяти не отображена процессом."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-EIO>"
msgstr "B<-EIO>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Unable to write back a page.  The page has to be written back in order to "
"move it since the page is dirty and the filesystem does not provide a "
"migration function that would allow the move of dirty pages."
msgstr ""
"Не удалось выполнить обратную запись на страницу. Для перемещения требуется "
"выполнить обратную запись, так как данные страницы неактуальны (dirty), но "
"файловая система не предоставляет функцию переноса (migration), которая "
"позволила бы переместить неактуальные страницы."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-EINVAL>"
msgstr "B<-EINVAL>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"A dirty page cannot be moved.  The filesystem does not provide a migration "
"function and has no ability to write back pages."
msgstr ""
"Нельзя переместить неактуальные страницы. Файловая система не предоставляет "
"функцию переноса (migration), поэтому нет возможности выполнить обратную "
"запись на страницы."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-ENOENT>"
msgstr "B<-ENOENT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The page is not present."
msgstr "Страница отсутствует."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-ENOMEM>"
msgstr "B<-ENOMEM>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Unable to allocate memory on target node."
msgstr "Не удалось выделить память на узле назначения."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "ВОЗВРАЩАЕМОЕ ЗНАЧЕНИЕ"

#.  FIXME . Is the following quite true: does the wrapper in numactl
#.  do the right thing?
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "On success B<move_pages>()  returns zero.  On error, it returns -1, and "
#| "sets I<errno> to indicate the error."
msgid ""
"On success B<move_pages>()  returns zero.  On error, it returns -1, and sets "
"I<errno> to indicate the error.  If positive value is returned, it is the "
"number of nonmigrated pages."
msgstr ""
"При нормальном завершении работы B<move_pages>() возвращает ноль. В случае "
"ошибки возвращается -1, а I<errno> устанавливается в соответствующее "
"значение."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ОШИБКИ"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "I<si_value>"
msgid "B<Positive value>"
msgstr "I<si_value>"

#.  commit a49bd4d7163707de377aee062f17befef6da891b
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The number of nonmigrated pages if they were the result of nonfatal reasons "
"(since Linux 4.17)."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<E2BIG>"
msgstr "B<E2BIG>"

#.  commit 3140a2273009c01c27d316f35ab76a37e105fdd8
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Too many pages to move.  Since Linux 2.6.29, the kernel no longer generates "
"this error."
msgstr ""
"Слишком много перемещаемых страниц. Начиная с Linux 2.6.29, ядро не "
"генерирует эту ошибку."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EACCES>"
msgstr "B<EACCES>"

#. #-#-#-#-#  archlinux: move_pages.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  FIXME Clarify "current cpuset" in the description of the EACCES error.
#.  Is that the cpuset of the caller or the target?
#. type: Plain text
#. #-#-#-#-#  debian-bookworm: move_pages.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  FIXME Clarify "current cpuset" in the description of the EACCES error.
#.  Is that the cpuset of the caller or the target?
#. type: Plain text
#. #-#-#-#-#  debian-unstable: move_pages.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  FIXME Clarify "current cpuset" in the description of the EACCES error.
#.  Is that the cpuset of the caller or the target?
#. type: Plain text
#. #-#-#-#-#  fedora-39: move_pages.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  FIXME Clarify "current cpuset" in the description of the EACCES error.
#.  Is that the cpuset of the caller or the target?
#. type: Plain text
#. #-#-#-#-#  fedora-rawhide: move_pages.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  FIXME Clarify "current cpuset" in the description of the EACCES error.
#.  Is that the cpuset of the caller or the target?
#. type: Plain text
#. #-#-#-#-#  mageia-cauldron: move_pages.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  FIXME Clarify "current cpuset" in the description of the EACCES error.
#.  Is that the cpuset of the caller or the target?
#. type: Plain text
#. #-#-#-#-#  opensuse-leap-15-6: move_pages.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  FIXME Clarify "current cpuset" in the description of the EACCESS error.
#.  Is that the cpuset of the caller or the target?
#. type: Plain text
#. #-#-#-#-#  opensuse-tumbleweed: move_pages.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  FIXME Clarify "current cpuset" in the description of the EACCES error.
#.  Is that the cpuset of the caller or the target?
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "One of the target nodes is not allowed by the current cpuset."
msgstr "Один из узлов назначения не разрешён для текущего набора ЦП."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EFAULT>"
msgstr "B<EFAULT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Parameter array could not be accessed."
msgstr "Недоступен массив параметров."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Flags other than B<MPOL_MF_MOVE> and B<MPOL_MF_MOVE_ALL> was specified or an "
"attempt was made to migrate pages of a kernel thread."
msgstr ""
"Указаны флаги, отличные от B<MPOL_MF_MOVE> и B<MPOL_MF_MOVE_ALL>, или "
"предпринята попытка переместить (migrate) страницы нити ядра."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENODEV>"
msgstr "B<ENODEV>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "One of the target nodes is not online."
msgstr "Один из узлов назначения недоступен (not online)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EPERM>"
msgstr "B<EPERM>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The caller specified B<MPOL_MF_MOVE_ALL> without sufficient privileges "
"(B<CAP_SYS_NICE>).  Or, the caller attempted to move pages of a process "
"belonging to another user but did not have privilege to do so "
"(B<CAP_SYS_NICE>)."
msgstr ""
"Вызывающий указал флаг B<MPOL_MF_MOVE_ALL>, но у него не хватает привилегий "
"(B<CAP_SYS_NICE>). Или вызывающий попытался переместить страницы процесса, "
"принадлежащие другому пользователю, но не имеет привилегий для этого "
"(B<CAP_SYS_NICE>)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ESRCH>"
msgstr "B<ESRCH>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Process does not exist."
msgstr "Процесс не существует."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "СТАНДАРТЫ"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "Linux."
msgstr "Linux."

#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "ИСТОРИЯ"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, fuzzy
#| msgid "util-linux 2.38.1"
msgid "Linux 2.6.18."
msgstr "util-linux 2.38.1"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "ЗАМЕЧАНИЯ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "For information on library support, see B<numa>(7)."
msgstr "Информация о библиотеке доступна в B<numa>(7)."

#.  FIXME Clarify "current cpuset".  Is that the cpuset of the caller
#.  or the target?
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Use B<get_mempolicy>(2)  with the B<MPOL_F_MEMS_ALLOWED> flag to obtain the "
"set of nodes that are allowed by the current cpuset.  Note that this "
"information is subject to change at any time by manual or automatic "
"reconfiguration of the cpuset."
msgstr ""
"Используйте B<get_mempolicy>(2) с флагом B<MPOL_F_MEMS_ALLOWED> для "
"получения набора узлов, которые доступны в текущем наборе ЦП. Заметим, что "
"эта информация может измениться в любое время вручную или при автоматической "
"перенастройке набора ЦП."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Use of this function may result in pages whose location (node) violates the "
"memory policy established for the specified addresses (See B<mbind>(2))  and/"
"or the specified process (See B<set_mempolicy>(2)).  That is, memory policy "
"does not constrain the destination nodes used by B<move_pages>()."
msgstr ""
"Использование этого вызова может привести к тому, что расположение страниц "
"(на узле) нарушит политику памяти, установленную для заданных адресов (см. "
"B<mbind>(2)) и/или заданного процесса (см. B<set_mempolicy>(2)). То есть "
"политика памяти не ограничивает узлы назначения, используемые "
"B<move_pages>()."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The I<E<lt>numaif.hE<gt>> header is not included with glibc, but requires "
"installing I<libnuma-devel> or a similar package."
msgstr ""
"Заголовочный файл I<E<lt>numaif.hE<gt>> не включён в glibc, его можно найти "
"в пакете с именем I<libnuma-devel> или подобным названием."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМ. ТАКЖЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<get_mempolicy>(2), B<mbind>(2), B<set_mempolicy>(2), B<numa>(3), "
"B<numa_maps>(5), B<cpuset>(7), B<numa>(7), B<migratepages>(8), B<numastat>(8)"
msgstr ""
"B<get_mempolicy>(2), B<mbind>(2), B<set_mempolicy>(2), B<numa>(3), "
"B<numa_maps>(5), B<cpuset>(7), B<numa>(7), B<migratepages>(8), B<numastat>(8)"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "2023-02-05"
msgstr "5 февраля 2023 г."

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: SH
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "VERSIONS"
msgstr "ВЕРСИИ"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron
#, fuzzy
#| msgid "B<move_pages>()  first appeared on Linux in version 2.6.18."
msgid "B<move_pages>()  first appeared in Linux 2.6.18."
msgstr "Вызов B<move_pages>() впервые появился в Linux в ядре версии 2.6.18."

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid "This system call is Linux-specific."
msgstr "Данный вызов есть только в Linux."

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.05"
msgstr "Linux man-pages 6.05"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "MOVE_PAGES"
msgstr "MOVE_PAGES"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2017-09-15"
msgstr "15 сентября 2017 г."

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Руководство программиста Linux"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"B<long move_pages(int >I<pid>B<, unsigned long count, void **>I<pages>B<,>\n"
"B<                const int *>I<nodes>B<, int *>I<status>B<, int >I<flags>B<);>\n"
msgstr ""
"B<long move_pages(int >I<pid>B<, unsigned long count, void **>I<pages>B<,>\n"
"B<                const int *>I<nodes>B<, int *>I<status>B<, int >I<flags>B<);>\n"

#. type: Plain text
#: opensuse-leap-15-6
msgid "Link with I<-lnuma>."
msgstr "Компонуется при указании параметра I<-lnuma>."

#. type: IP
#: opensuse-leap-15-6
#, no-wrap
msgid "*"
msgstr "*"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"In kernels up to and including Linux 4.12: the caller must be privileged "
"(B<CAP_SYS_NICE>)  or the real or effective user ID of the calling process "
"must match the real or saved-set user ID of the target process."
msgstr ""
"В ядрах до Linux 4.12 включительно: вызывающий должен иметь права "
"(B<CAP_SYS_NICE>) или реальный или эффективный пользовательский ID "
"вызывающего процесса должен совпадать с реальным или сохранённым "
"пользовательским ID целевого процесса."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"I<status> is an array of integers that return the status of each page.  The "
"array contains valid values only if B<move_pages>()  did not return an error."
msgstr ""
"Аргумент I<status> представляет собой массив целых, которым определяется "
"состояние каждой страницы. Массив содержит правильные значения только, если "
"вызов B<move_pages>() не завершился с ошибкой."

#.  FIXME . Is the following quite true: does the wrapper in numactl
#.  do the right thing?
#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"On success B<move_pages>()  returns zero.  On error, it returns -1, and sets "
"I<errno> to indicate the error."
msgstr ""
"При нормальном завершении работы B<move_pages>() возвращает ноль. В случае "
"ошибки возвращается -1, а I<errno> устанавливается в соответствующее "
"значение."

#. type: Plain text
#: opensuse-leap-15-6
msgid "Too many pages to move."
msgstr ""

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "B<ENOENT>"
msgstr "B<ENOENT>"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"No pages were found that require moving.  All pages are either already on "
"the target node, not present, had an invalid address or could not be moved "
"because they were mapped by multiple processes."
msgstr ""
"Не найдены страницы, которые требуется переместить. Все страницы или уже на "
"узле назначения, отсутствуют, имеют неправильный адрес, или не могут быть "
"перемещены, так как отображены несколькими процессами."

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<move_pages>()  first appeared on Linux in version 2.6.18."
msgstr "Вызов B<move_pages>() впервые появился в Linux в ядре версии 2.6.18."

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "CONFORMING TO"
msgstr "СООТВЕТСТВИЕ СТАНДАРТАМ"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "COLOPHON"
msgstr "ЗАМЕЧАНИЯ"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Эта страница является частью проекта Linux I<man-pages> версии 4.16. "
"Описание проекта, информацию об ошибках и последнюю версию этой страницы "
"можно найти по адресу \\%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30 марта 2023 г."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"
