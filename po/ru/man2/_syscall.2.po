# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Yuri Kozlov <yuray@komyakino.ru>, 2011-2013, 2015-2017, 2019.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-08-27 17:25+0200\n"
"PO-Revision-Date: 2019-04-03 15:47+0000\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <man-pages-ru-talks@lists.sourceforge.net>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "_syscall"
msgstr "_syscall"

#. type: TH
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid "2023-05-03"
msgstr "3 мая 2023 г."

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Linux man-pages 6.05.01"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "ИМЯ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "_syscall - invoking a system call without library support (OBSOLETE)"
msgstr ""
"_syscall - выполняет системный вызов, который не поддерживается библиотекой "
"(УСТАРЕЛ)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "СИНТАКСИС"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>linux/unistd.hE<gt>>\n"
msgstr "B<#include E<lt>linux/unistd.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "A _syscall macro\n"
msgstr "Макрос _syscall\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "desired system call\n"
msgstr "желаемый системный вызов\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The important thing to know about a system call is its prototype.  You need "
"to know how many arguments, their types, and the function return type.  "
"There are seven macros that make the actual call into the system easier.  "
"They have the form:"
msgstr ""
"При использовании системного вызова необходима информация о его прототипе. "
"Вам нужно знать количество аргументов, их типы и тип возвращаемого значения. "
"Есть несколько макросов, которые облегчают выполнение системных вызовов. Они "
"имеют вид:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "_syscallI<X>(I<type>,I<name>,I<type1>,I<arg1>,I<type2>,I<arg2>,...)\n"
msgstr "_syscallI<X>(I<type>,I<name>,I<type1>,I<arg1>,I<type2>,I<arg2>,…)\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "where"
msgstr "где"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "I<X> is 0\\(en6, which are the number of arguments taken by the system "
#| "call"
msgid ""
"I<X> is 0\\[en]6, which are the number of arguments taken by the system call"
msgstr "I<X> 0\\(en6, количество аргументов, принимаемых системным вызовом"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<type> is the return type of the system call"
msgstr "I<type> \\(em тип возвращаемого значения системного вызова"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<name> is the name of the system call"
msgstr "I<name> \\(em имя системного вызова"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<typeN> is the Nth argument's type"
msgstr "I<typeN> \\(em тип N-го аргумента"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<argN> is the name of the Nth argument"
msgstr "I<argN> \\(em имя N-го аргумента"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"These macros create a function called I<name> with the arguments you "
"specify.  Once you include the _syscall() in your source file, you call the "
"system call by I<name>."
msgstr ""
"Эти макросы создают функцию с именем I<name> и указанными аргументами. После "
"добавления _syscall() в код, вы можете вызывать системный вызов по имени "
"I<name>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "FILES"
msgstr "ФАЙЛЫ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I</usr/include/linux/unistd.h>"
msgstr "I</usr/include/linux/unistd.h>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "СТАНДАРТЫ"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "Linux."
msgstr "Linux."

#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "ИСТОРИЯ"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "Starting around kernel 2.6.18, the _syscall macros were removed from "
#| "header files supplied to user space.  Use B<syscall>(2)  instead.  (Some "
#| "architectures, notably ia64, never provided the _syscall macros; on those "
#| "architectures, B<syscall>(2)  was always required.)"
msgid ""
"Starting around Linux 2.6.18, the _syscall macros were removed from header "
"files supplied to user space.  Use B<syscall>(2)  instead.  (Some "
"architectures, notably ia64, never provided the _syscall macros; on those "
"architectures, B<syscall>(2)  was always required.)"
msgstr ""
"Начиная, приблизительно, с ядра 2.6.18, макросы _syscall удалены из "
"заголовочных файлов, используемых в пользовательском пространстве. Вместо "
"них используйте B<syscall>(2). (Для некоторых архитектур, например ia64, "
"макросы _syscall никогда не предоставлялись; на таких архитектурах всегда "
"требуется макрос B<syscall>(2).)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "ЗАМЕЧАНИЯ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The _syscall() macros I<do not> produce a prototype.  You may have to create "
"one, especially for C++ users."
msgstr ""
"Макрос _syscall() I<не> создаёт прототип. Вам, вероятно, придётся создавать "
"его самостоятельно, особенно пользователям C++."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"System calls are not required to return only positive or negative error "
"codes.  You need to read the source to be sure how it will return errors.  "
"Usually, it is the negative of a standard error code, for example, -"
"I<EPERM>.  The _syscall() macros will return the result I<r> of the system "
"call when I<r> is nonnegative, but will return -1 and set the variable "
"I<errno> to -I<r> when I<r> is negative.  For the error codes, see "
"B<errno>(3)."
msgstr ""
"Для системных вызовов не требуется возвращать только положительный или "
"отрицательный код ошибки. Вам нужно прочитать исходный код, чтобы выяснить "
"как возвращаются ошибки. Обычно, это отрицательный стандартный код ошибки, "
"например, -I<EPERM>. Макрос _syscall() возвращает результат I<r> из "
"системного вызова, если I<r> неотрицательно, но вернёт -1 и установит "
"значение I<errno> равное -I<r>, когда I<r> отрицательно. Коды ошибок "
"смотрите в B<errno>(3)."

#.  The preferred way to invoke system calls that glibc does not know
#.  about yet is via
#.  .BR syscall (2).
#.  However, this mechanism can be used only if using a libc
#.  (such as glibc) that supports
#.  .BR syscall (2),
#.  and if the
#.  .I <sys/syscall.h>
#.  header file contains the required SYS_foo definition.
#.  Otherwise, the use of a _syscall macro is required.
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"When defining a system call, the argument types I<must> be passed by-value "
"or by-pointer (for aggregates like structs)."
msgstr ""
"При определении системного вызова типы аргументов I<должны> передаваться по "
"значению или по ссылке (для составных типов, например структур)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "ПРИМЕРЫ"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide
#, fuzzy, no-wrap
#| msgid ""
#| "    error = sysinfo(&s_info);\n"
#| "    printf(\"code error = %d\\en\", error);\n"
#| "    printf(\"Uptime = %lds\\enLoad: 1 min %lu / 5 min %lu / 15 min %lu\\en\"\n"
#| "           \"RAM: total %lu / free %lu / shared %lu\\en\"\n"
#| "           \"Memory in buffers = %lu\\enSwap: total %lu / free %lu\\en\"\n"
#| "           \"Number of processes = %d\\en\",\n"
#| "           s_info.uptime, s_info.loads[0],\n"
#| "           s_info.loads[1], s_info.loads[2],\n"
#| "           s_info.totalram, s_info.freeram,\n"
#| "           s_info.sharedram, s_info.bufferram,\n"
#| "           s_info.totalswap, s_info.freeswap,\n"
#| "           s_info.procs);\n"
#| "    exit(EXIT_SUCCESS);\n"
#| "}\n"
msgid ""
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>errno.hE<gt>\n"
"#include E<lt>linux/unistd.hE<gt>       /* for _syscallX macros/related stuff */\n"
"#include E<lt>linux/kernel.hE<gt>       /* for struct sysinfo */\n"
"\\&\n"
"_syscall1(int, sysinfo, struct sysinfo *, info);\n"
"\\&\n"
"int\n"
"main(void)\n"
"{\n"
"    struct sysinfo s_info;\n"
"    int error;\n"
"\\&\n"
"    error = sysinfo(&s_info);\n"
"    printf(\"code error = %d\\en\", error);\n"
"    printf(\"Uptime = %lds\\enLoad: 1 min %lu / 5 min %lu / 15 min %lu\\en\"\n"
"           \"RAM: total %lu / free %lu / shared %lu\\en\"\n"
"           \"Memory in buffers = %lu\\enSwap: total %lu / free %lu\\en\"\n"
"           \"Number of processes = %d\\en\",\n"
"           s_info.uptime, s_info.loads[0],\n"
"           s_info.loads[1], s_info.loads[2],\n"
"           s_info.totalram, s_info.freeram,\n"
"           s_info.sharedram, s_info.bufferram,\n"
"           s_info.totalswap, s_info.freeswap,\n"
"           s_info.procs);\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""
"    error = sysinfo(&s_info);\n"
"    printf(\"code error = %d\\en\", error);\n"
"    printf(\"Uptime = %lds\\enLoad: 1 min %lu / 5 min %lu / 15 min %lu\\en\"\n"
"           \"RAM: total %lu / free %lu / shared %lu\\en\"\n"
"           \"Memory in buffers = %lu\\enSwap: total %lu / free %lu\\en\"\n"
"           \"Number of processes = %d\\en\",\n"
"           s_info.uptime, s_info.loads[0],\n"
"           s_info.loads[1], s_info.loads[2],\n"
"           s_info.totalram, s_info.freeram,\n"
"           s_info.sharedram, s_info.bufferram,\n"
"           s_info.totalswap, s_info.freeswap,\n"
"           s_info.procs);\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"

#. #-#-#-#-#  archlinux: _syscall.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SS
#. #-#-#-#-#  debian-bookworm: _syscall.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SS
#. #-#-#-#-#  debian-unstable: _syscall.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SS
#. #-#-#-#-#  fedora-39: _syscall.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SS
#. #-#-#-#-#  fedora-rawhide: _syscall.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SS
#. #-#-#-#-#  mageia-cauldron: _syscall.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SS
#. #-#-#-#-#  opensuse-leap-15-6: _syscall.2.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SS
#. #-#-#-#-#  opensuse-tumbleweed: _syscall.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Sample output"
msgstr "Результат работы примера"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"code error = 0\n"
"uptime = 502034s\n"
"Load: 1 min 13376 / 5 min 5504 / 15 min 1152\n"
"RAM: total 15343616 / free 827392 / shared 8237056\n"
"Memory in buffers = 5066752\n"
"Swap: total 27881472 / free 24698880\n"
"Number of processes = 40\n"
msgstr ""
"code error = 0\n"
"uptime = 502034s\n"
"Load: 1 min 13376 / 5 min 5504 / 15 min 1152\n"
"RAM: total 15343616 / free 827392 / shared 8237056\n"
"Memory in buffers = 5066752\n"
"Swap: total 27881472 / free 24698880\n"
"Number of processes = 40\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМ. ТАКЖЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<intro>(2), B<syscall>(2), B<errno>(3)"
msgstr "B<intro>(2), B<syscall>(2), B<errno>(3)"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "2023-02-05"
msgstr "5 февраля 2023 г."

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid "The use of these macros is Linux-specific, and deprecated."
msgstr "Эти макросы есть только в Linux и они устарели."

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid ""
"Starting around kernel 2.6.18, the _syscall macros were removed from header "
"files supplied to user space.  Use B<syscall>(2)  instead.  (Some "
"architectures, notably ia64, never provided the _syscall macros; on those "
"architectures, B<syscall>(2)  was always required.)"
msgstr ""
"Начиная, приблизительно, с ядра 2.6.18, макросы _syscall удалены из "
"заголовочных файлов, используемых в пользовательском пространстве. Вместо "
"них используйте B<syscall>(2). (Для некоторых архитектур, например ia64, "
"макросы _syscall никогда не предоставлялись; на таких архитектурах всегда "
"требуется макрос B<syscall>(2).)"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>errno.hE<gt>\n"
"#include E<lt>linux/unistd.hE<gt>       /* for _syscallX macros/related stuff */\n"
"#include E<lt>linux/kernel.hE<gt>       /* for struct sysinfo */\n"
msgstr ""
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>errno.hE<gt>\n"
"#include E<lt>linux/unistd.hE<gt>       /* поддержка _syscallX */\n"
"#include E<lt>linux/kernel.hE<gt>       /* struct sysinfo */\n"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "_syscall1(int, sysinfo, struct sysinfo *, info);\n"
msgstr "_syscall1(int, sysinfo, struct sysinfo *, info);\n"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"int\n"
"main(void)\n"
"{\n"
"    struct sysinfo s_info;\n"
"    int error;\n"
msgstr ""
"int\n"
"main(void)\n"
"{\n"
"    struct sysinfo s_info;\n"
"    int error;\n"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"    error = sysinfo(&s_info);\n"
"    printf(\"code error = %d\\en\", error);\n"
"    printf(\"Uptime = %lds\\enLoad: 1 min %lu / 5 min %lu / 15 min %lu\\en\"\n"
"           \"RAM: total %lu / free %lu / shared %lu\\en\"\n"
"           \"Memory in buffers = %lu\\enSwap: total %lu / free %lu\\en\"\n"
"           \"Number of processes = %d\\en\",\n"
"           s_info.uptime, s_info.loads[0],\n"
"           s_info.loads[1], s_info.loads[2],\n"
"           s_info.totalram, s_info.freeram,\n"
"           s_info.sharedram, s_info.bufferram,\n"
"           s_info.totalswap, s_info.freeswap,\n"
"           s_info.procs);\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""
"    error = sysinfo(&s_info);\n"
"    printf(\"code error = %d\\en\", error);\n"
"    printf(\"Uptime = %lds\\enLoad: 1 min %lu / 5 min %lu / 15 min %lu\\en\"\n"
"           \"RAM: total %lu / free %lu / shared %lu\\en\"\n"
"           \"Memory in buffers = %lu\\enSwap: total %lu / free %lu\\en\"\n"
"           \"Number of processes = %d\\en\",\n"
"           s_info.uptime, s_info.loads[0],\n"
"           s_info.loads[1], s_info.loads[2],\n"
"           s_info.totalram, s_info.freeram,\n"
"           s_info.sharedram, s_info.bufferram,\n"
"           s_info.totalswap, s_info.freeswap,\n"
"           s_info.procs);\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.05"
msgstr "Linux man-pages 6.05"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "_SYSCALL"
msgstr "_SYSCALL"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2017-09-15"
msgstr "15 сентября 2017 г."

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Руководство программиста Linux"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<#include E<lt>linux/unistd.hE<gt>>"
msgstr "B<#include E<lt>linux/unistd.hE<gt>>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "A _syscall macro"
msgstr "Макрос _syscall"

#. type: Plain text
#: opensuse-leap-15-6
msgid "desired system call"
msgstr "желаемый системный вызов"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"I<X> is 0\\(en6, which are the number of arguments taken by the system call"
msgstr "I<X> 0\\(en6, количество аргументов, принимаемых системным вызовом"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "CONFORMING TO"
msgstr "СООТВЕТСТВИЕ СТАНДАРТАМ"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "EXAMPLE"
msgstr "ПРИМЕР"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "COLOPHON"
msgstr "ЗАМЕЧАНИЯ"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Эта страница является частью проекта Linux I<man-pages> версии 4.16. "
"Описание проекта, информацию об ошибках и последнюю версию этой страницы "
"можно найти по адресу \\%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30 марта 2023 г."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"
