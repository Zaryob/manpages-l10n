# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Alexander Golubev <fatzer2@gmail.com>, 2018.
# Azamat Hackimov <azamat.hackimov@gmail.com>, 2011, 2014-2016.
# Hotellook, 2014.
# Nikita <zxcvbnm3230@mail.ru>, 2014.
# Spiros Georgaras <sng@hellug.gr>, 2016.
# Vladislav <ivladislavefimov@gmail.com>, 2015.
# Yuri Kozlov <yuray@komyakino.ru>, 2011-2019.
# Иван Павлов <pavia00@gmail.com>, 2017.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-08-27 17:21+0200\n"
"PO-Revision-Date: 2019-10-15 18:55+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <man-pages-ru-talks@lists.sourceforge.net>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "setxattr"
msgstr "setxattr"

#. type: TH
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid "2023-07-28"
msgstr "28 июля 2023 г."

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Linux man-pages 6.05.01"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "ИМЯ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "setxattr, lsetxattr, fsetxattr - set an extended attribute value"
msgstr ""
"setxattr, lsetxattr, fsetxattr - устанавливает расширенное значение атрибутов"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "СИНТАКСИС"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>sys/xattr.hE<gt>>\n"
msgstr "B<#include E<lt>sys/xattr.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "B<int setxattr(const char *>I<path>B<, const char *>I<name>B<,>\n"
#| "B<              const void *>I<value>B<, size_t >I<size>B<, int >I<flags>B<);>\n"
#| "B<int lsetxattr(const char *>I<path>B<, const char *>I<name>B<,>\n"
#| "B<              const void *>I<value>B<, size_t >I<size>B<, int >I<flags>B<);>\n"
#| "B<int fsetxattr(int >I<fd>B<, const char *>I<name>B<,>\n"
#| "B<              const void *>I<value>B<, size_t >I<size>B<, int >I<flags>B<);>\n"
msgid ""
"B<int setxattr(const char *>I<path>B<, const char *>I<name>B<,>\n"
"B<              const void >I<value>B<[.>I<size>B<], size_t >I<size>B<, int >I<flags>B<);>\n"
"B<int lsetxattr(const char *>I<path>B<, const char *>I<name>B<,>\n"
"B<              const void >I<value>B<[.>I<size>B<], size_t >I<size>B<, int >I<flags>B<);>\n"
"B<int fsetxattr(int >I<fd>B<, const char *>I<name>B<,>\n"
"B<              const void >I<value>B<[.>I<size>B<], size_t >I<size>B<, int >I<flags>B<);>\n"
msgstr ""
"B<int setxattr(const char *>I<path>B<, const char *>I<name>B<,>\n"
"B<              const void *>I<value>B<, size_t >I<size>B<, int >I<flags>B<);>\n"
"B<int lsetxattr(const char *>I<path>B<, const char *>I<name>B<,>\n"
"B<              const void *>I<value>B<, size_t >I<size>B<, int >I<flags>B<);>\n"
"B<int fsetxattr(int >I<fd>B<, const char *>I<name>B<,>\n"
"B<              const void *>I<value>B<, size_t >I<size>B<, int >I<flags>B<);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Extended attributes are I<name>:I<value> pairs associated with inodes "
"(files, directories, symbolic links, etc.).  They are extensions to the "
"normal attributes which are associated with all inodes in the system (i.e., "
"the B<stat>(2)  data).  A complete overview of extended attributes concepts "
"can be found in B<xattr>(7)."
msgstr ""
"Расширенные атрибуты представляют собой пару I<имя>:I<значение> и "
"связываются с записями inode (файлы, каталоги, символьные ссылки и т.п.). "
"Они являются расширениями к обычным атрибутам, связанным со всеми записями "
"inode в системе (например, данные B<stat>(2)). Полное описание модели "
"расширенных атрибутов можно найти в B<xattr>(7)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<setxattr>()  sets the I<value> of the extended attribute identified by "
"I<name> and associated with the given I<path> in the filesystem.  The "
"I<size> argument specifies the size (in bytes) of I<value>; a zero-length "
"value is permitted."
msgstr ""
"Вызов B<setxattr>() устанавливает значение I<value> расширенного атрибута с "
"именем, заданным в I<name> и связанного с заданным I<path> в файловой "
"системе. В аргументе I<size> задаётся размер (в байтах) I<value>; "
"допускается нулевой размер."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<lsetxattr>()  is identical to B<setxattr>(), except in the case of a "
"symbolic link, where the extended attribute is set on the link itself, not "
"the file that it refers to."
msgstr ""
"Вызов B<lsetxattr>() идентичен B<setxattr>(), за исключением случая работы с "
"символьными ссылками; он устанавливает расширенный атрибут на ссылке, а не "
"на том файле, на который она указывает."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<fsetxattr>()  is identical to B<setxattr>(), only the extended attribute "
"is set on the open file referred to by I<fd> (as returned by B<open>(2))  in "
"place of I<path>."
msgstr ""
"Вызов B<fsetxattr>() идентичен B<setxattr>(), отличием является то, что "
"расширенный атрибут устанавливается на открытом файле, на который указывает "
"I<fd> (возвращаемом B<open>(2)), а не на указанном в I<path>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"An extended attribute name is a null-terminated string.  The I<name> "
"includes a namespace prefix; there may be several, disjoint namespaces "
"associated with an individual inode.  The I<value> of an extended attribute "
"is a chunk of arbitrary textual or binary data of specified length."
msgstr ""
"Имя расширенного атрибута представляет собой строку, заканчивающуюся null. "
"Имя I<name> включает префикс пространства имён; их может быть несколько, "
"разрозненные пространства ассоциируются с разными inode. Значением I<value> "
"расширенного атрибута является произвольный кусок текстовых или двоичных "
"данных определённой длины."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"By default (i.e., I<flags> is zero), the extended attribute will be created "
"if it does not exist, or the value will be replaced if the attribute already "
"exists.  To modify these semantics, one of the following values can be "
"specified in I<flags>:"
msgstr ""
"По умолчанию (т. е., значение I<flags> равно), если расширенный атрибут "
"отсутствует, то он создаётся, а если он существует, то заменяется его "
"значение. Для изменения такой работы, в I<flags> можно указать одно из "
"следующих значений:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<XATTR_CREATE>"
msgstr "B<XATTR_CREATE>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Perform a pure create, which fails if the named attribute exists already."
msgstr ""
"Выполнять только создание, если атрибут с таким именем существует — "
"возвращать ошибку."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<XATTR_REPLACE>"
msgstr "B<XATTR_REPLACE>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Perform a pure replace operation, which fails if the named attribute does "
"not already exist."
msgstr ""
"Выполнять только замену, если атрибут с таким именем не существует — "
"возвращать ошибку."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "ВОЗВРАЩАЕМОЕ ЗНАЧЕНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "On success, B<setns>()  returns 0.  On failure, -1 is returned and "
#| "I<errno> is set to indicate the error."
msgid ""
"On success, zero is returned.  On failure, -1 is returned and I<errno> is "
"set to indicate the error."
msgstr ""
"При успешном выполнении B<setns>() возвращает 0. При ошибке возвращается -1, "
"и I<errno> устанавливается в соответствующее значение."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ОШИБКИ"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EDQUOT>"
msgstr "B<EDQUOT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Disk quota limits meant that there is insufficient space remaining to store "
"the extended attribute."
msgstr ""
"Предел по дисковой квоте; не хватает пространства для сохранения "
"расширенного атрибута."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EEXIST>"
msgstr "B<EEXIST>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<XATTR_CREATE> was specified, and the attribute exists already."
msgstr "Указан B<XATTR_CREATE>, но атрибут уже существует."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<ENODATA>"
msgstr "B<ENODATA>"

#.  .RB ( ENOATTR
#.  is defined to be a synonym for
#.  .BR ENODATA
#.  in
#.  .IR <attr/attributes.h> .)
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "B<XATTR_REPLACE> was specified, and the attribute does not exist."
msgstr "Указан B<XATTR_REPLACE>, но атрибут не существует."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOSPC>"
msgstr "B<ENOSPC>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "There is insufficient space remaining to store the extended attribute."
msgstr "Не хватает пространства для сохранения расширенного атрибута."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOTSUP>"
msgstr "B<ENOTSUP>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The namespace prefix of I<name> is not valid."
msgstr "Неверный префикс пространства имён I<name>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Extended attributes are not supported by the filesystem, or are disabled,"
msgstr ""
"Расширенные атрибуты не поддерживаются файловой системой или отключены."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EPERM>"
msgstr "B<EPERM>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The file is marked immutable or append-only.  (See B<ioctl_iflags>(2).)"
msgstr ""
"Файл помечен как неизменяемый (immutable) или только для добавления "
"(смотрите B<ioctl_iflags>(2))."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "In addition, the errors documented in B<stat>(2)  can also occur."
msgstr "Также могут возникать ошибки, описанные в B<stat>(2)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<ERANGE>"
msgstr "B<ERANGE>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "The size of I<name> or I<value> exceeds a filesystem-specific limit."
msgstr ""
"Размер I<name> или I<value> превышает ограничение, задаваемое файловой "
"системой."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "СТАНДАРТЫ"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "Linux."
msgstr "Linux."

#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "ИСТОРИЯ"

#.  .SH AUTHORS
#.  Andreas Gruenbacher,
#.  .RI < a.gruenbacher@computer.org >
#.  and the SGI XFS development team,
#.  .RI < linux-xfs@oss.sgi.com >.
#.  Please send any bug reports or comments to these addresses.
#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, fuzzy
#| msgid "Since glibc 2.2.2:"
msgid "Linux 2.4, glibc 2.3."
msgstr "Начиная с glibc 2.2.2:"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМ. ТАКЖЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<getfattr>(1), B<setfattr>(1), B<getxattr>(2), B<listxattr>(2), B<open>(2), "
"B<removexattr>(2), B<stat>(2), B<symlink>(7), B<xattr>(7)"
msgstr ""
"B<getfattr>(1), B<setfattr>(1), B<getxattr>(2), B<listxattr>(2), B<open>(2), "
"B<removexattr>(2), B<stat>(2), B<symlink>(7), B<xattr>(7)"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "2022-12-04"
msgstr "4 декабря 2022 г."

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: SH
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "VERSIONS"
msgstr "ВЕРСИИ"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron
#, fuzzy
#| msgid ""
#| "These system calls have been available on Linux since kernel 2.4; glibc "
#| "support is provided since version 2.3."
msgid ""
"These system calls have been available since Linux 2.4; glibc support is "
"provided since glibc 2.3."
msgstr ""
"Данные системные вызовы доступны в Linux начиная с ядра версии 2.4; "
"поддержка в glibc появилась в версии 2.3."

#.  .SH AUTHORS
#.  Andreas Gruenbacher,
#.  .RI < a.gruenbacher@computer.org >
#.  and the SGI XFS development team,
#.  .RI < linux-xfs@oss.sgi.com >.
#.  Please send any bug reports or comments to these addresses.
#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid "These system calls are Linux-specific."
msgstr "Данные системные вызовы есть только в Linux."

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.05"
msgstr "Linux man-pages 6.05"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "SETXATTR"
msgstr "SETXATTR"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2017-03-13"
msgstr "13 марта 2017 г."

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Руководство программиста Linux"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"B<#include E<lt>sys/types.hE<gt>>\n"
"B<#include E<lt>sys/xattr.hE<gt>>\n"
msgstr ""
"B<#include E<lt>sys/types.hE<gt>>\n"
"B<#include E<lt>sys/xattr.hE<gt>>\n"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"B<int setxattr(const char\\ *>I<path>B<, const char\\ *>I<name>B<,>\n"
"B<              const void\\ *>I<value>B<, size_t >I<size>B<, int >I<flags>B<);>\n"
"B<int lsetxattr(const char\\ *>I<path>B<, const char\\ *>I<name>B<,>\n"
"B<              const void\\ *>I<value>B<, size_t >I<size>B<, int >I<flags>B<);>\n"
"B<int fsetxattr(int >I<fd>B<, const char\\ *>I<name>B<,>\n"
"B<              const void\\ *>I<value>B<, size_t >I<size>B<, int >I<flags>B<);>\n"
msgstr ""
"B<int setxattr(const char\\ *>I<path>B<, const char\\ *>I<name>B<,>\n"
"B<              const void\\ *>I<value>B<, size_t >I<size>B<, int >I<flags>B<);>\n"
"B<int lsetxattr(const char\\ *>I<path>B<, const char\\ *>I<name>B<,>\n"
"B<              const void\\ *>I<value>B<, size_t >I<size>B<, int >I<flags>B<);>\n"
"B<int fsetxattr(int >I<fd>B<, const char\\ *>I<name>B<,>\n"
"B<              const void\\ *>I<value>B<, size_t >I<size>B<, int >I<flags>B<);>\n"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"On success, zero is returned.  On failure, -1 is returned and I<errno> is "
"set appropriately."
msgstr ""
"При успешном выполнении возвращается ноль. В случае ошибки возвращается -1, "
"а I<errno> устанавливается в соответствующее значение."

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "B<ENOATTR>"
msgstr "B<ENOATTR>"

#. type: Plain text
#: opensuse-leap-15-6
#, fuzzy
#| msgid "B<XATTR_REPLACE> was specified, and the attribute does not exist."
msgid ""
"B<XATTR_REPLACE> was specified, and the attribute does not exist.  "
"(B<ENOATTR> is defined to be a synonym for B<ENODATA> in I<E<lt>attr/xattr."
"hE<gt>>.)"
msgstr "Указан B<XATTR_REPLACE>, но атрибут не существует."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"These system calls have been available on Linux since kernel 2.4; glibc "
"support is provided since version 2.3."
msgstr ""
"Данные системные вызовы доступны в Linux начиная с ядра версии 2.4; "
"поддержка в glibc появилась в версии 2.3."

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "CONFORMING TO"
msgstr "СООТВЕТСТВИЕ СТАНДАРТАМ"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "COLOPHON"
msgstr "ЗАМЕЧАНИЯ"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Эта страница является частью проекта Linux I<man-pages> версии 4.16. "
"Описание проекта, информацию об ошибках и последнюю версию этой страницы "
"можно найти по адресу \\%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30 марта 2023 г."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"
