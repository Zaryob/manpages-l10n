# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Azamat Hackimov <azamat.hackimov@gmail.com>, 2013, 2016.
# Dmitriy Ovchinnikov <dmitriyxt5@gmail.com>, 2012.
# Dmitry Bolkhovskikh <d20052005@yandex.ru>, 2017.
# Katrin Kutepova <blackkatelv@gmail.com>, 2018.
# Yuri Kozlov <yuray@komyakino.ru>, 2011-2019.
# Иван Павлов <pavia00@gmail.com>, 2017.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-08-27 17:31+0200\n"
"PO-Revision-Date: 2019-10-15 18:59+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <man-pages-ru-talks@lists.sourceforge.net>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "uname"
msgstr "uname"

#. type: TH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30 марта 2023 г."

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Linux man-pages 6.05.01"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "ИМЯ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "uname - get name and information about current kernel"
msgstr "uname - получает название и информацию о текущем ядре"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "СИНТАКСИС"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>sys/utsname.hE<gt>>\n"
msgstr "B<#include E<lt>sys/utsname.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<int uname(struct utsname *>I<buf>B<);>\n"
msgstr "B<int uname(struct utsname *>I<buf>B<);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<uname>()  returns system information in the structure pointed to by "
"I<buf>.  The I<utsname> struct is defined in I<E<lt>sys/utsname.hE<gt>>:"
msgstr ""
"B<uname>() возвращает информацию о системе в структуру с адресом I<buf>. "
"Структура I<utsname> определена в I<E<lt>sys/utsname.hE<gt>>:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "struct utsname {\n"
#| "    char sysname[];    /* Operating system name (e.g., \"Linux\") */\n"
#| "    char nodename[];   /* Name within \"some implementation-defined\n"
#| "                          network\" */\n"
#| "    char release[];    /* Operating system release (e.g., \"2.6.28\") */\n"
#| "    char version[];    /* Operating system version */\n"
#| "    char machine[];    /* Hardware identifier */\n"
#| "#ifdef _GNU_SOURCE\n"
#| "    char domainname[]; /* NIS or YP domain name */\n"
#| "#endif\n"
#| "};\n"
msgid ""
"struct utsname {\n"
"    char sysname[];    /* Operating system name (e.g., \"Linux\") */\n"
"    char nodename[];   /* Name within communications network\n"
"                          to which the node is attached, if any */\n"
"    char release[];    /* Operating system release\n"
"                          (e.g., \"2.6.28\") */\n"
"    char version[];    /* Operating system version */\n"
"    char machine[];    /* Hardware type identifier */\n"
"#ifdef _GNU_SOURCE\n"
"    char domainname[]; /* NIS or YP domain name */\n"
"#endif\n"
"};\n"
msgstr ""
"struct utsname {\n"
"    char sysname[];    /* название операционной системы\n"
"                          (например, «Linux») */\n"
"    char nodename[];   /* имя в сети, зависящее от реализации */\n"
"    char release[];    /* идентификатор выпуска ОС (например, «2.6.28») */\n"
"    char version[];    /* версия ОС */\n"
"    char machine[];    /* идентификатор аппаратного обеспечения */\n"
"#ifdef _GNU_SOURCE\n"
"    char domainname[]; /* доменное имя NIS или YP */\n"
"#endif\n"
"};\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The length of the arrays in a I<struct utsname> is unspecified (see NOTES); "
"the fields are terminated by a null byte (\\[aq]\\e0\\[aq])."
msgstr ""
"Размеры массивов в I<struct utsname> не определены (см. ЗАМЕЧАНИЯ); поля "
"завершаются байтом с null (\\[aq]\\e0\\[aq]."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "ВОЗВРАЩАЕМОЕ ЗНАЧЕНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "On success, zero returned.  On failure, -1 is returned and I<errno> is "
#| "set to indicate the error."
msgid ""
"On success, zero is returned.  On error, -1 is returned, and I<errno> is set "
"to indicate the error."
msgstr ""
"При успешном выполнении возвращается 0. При ошибке возвращается -1, а "
"I<errno> присваивается значение ошибки."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ОШИБКИ"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EFAULT>"
msgstr "B<EFAULT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<buf> is not valid."
msgstr "Значение I<buf> не определено."

#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "ВЕРСИИ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The I<domainname> member (the NIS or YP domain name) is a GNU extension."
msgstr "Поле I<domainname> (доменное имя NIS или YP) является расширением GNU."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The length of the fields in the struct varies.  Some operating systems or "
#| "libraries use a hardcoded 9 or 33 or 65 or 257.  Other systems use "
#| "B<SYS_NMLN> or B<_SYS_NMLN> or B<UTSLEN> or B<_UTSNAME_LENGTH>.  Clearly, "
#| "it is a bad idea to use any of these constants; just use sizeof(...).  "
#| "Often 257 is chosen in order to have room for an internet hostname."
msgid ""
"The length of the fields in the struct varies.  Some operating systems or "
"libraries use a hardcoded 9 or 33 or 65 or 257.  Other systems use "
"B<SYS_NMLN> or B<_SYS_NMLN> or B<UTSLEN> or B<_UTSNAME_LENGTH>.  Clearly, it "
"is a bad idea to use any of these constants; just use sizeof(...).  SVr4 "
"uses 257, \"to support Internet hostnames\" \\[em] this is the largest value "
"likely to be encountered in the wild."
msgstr ""
"Длина полей в структуре может быть различна. Некоторые операционные системы "
"или библиотеки используют жёстко заданные значения 9, 33, 65 или 257. Другие "
"используют константы B<SYS_NMLN>, B<_SYS_NMLN>, B<UTSLEN> и "
"B<_UTSNAME_LENGTH>. Несомненно, идея использовать эти константы не очень "
"хороша \\(em можно просто использовать sizeof(...). Часто выбирается 257 для "
"того, чтобы имелось достаточно места для хранения имени машины в сети "
"интернет."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "СТАНДАРТЫ"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr "POSIX.1-2008."

#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "ИСТОРИЯ"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, fuzzy
#| msgid "POSIX.1-2001, SVr4, 4.3BSD."
msgid "POSIX.1-2001, SVr4, 4.4BSD."
msgstr "POSIX.1-2001, SVr4, 4.3BSD."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "C library/kernel differences"
msgstr "Отличия между библиотекой C и ядром"

#.  That was back before Linux 1.0
#.  That was also back before Linux 1.0
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Over time, increases in the size of the I<utsname> structure have led to "
"three successive versions of B<uname>(): I<sys_olduname>()  (slot "
"I<__NR_oldolduname>), I<sys_uname>()  (slot I<__NR_olduname>), and "
"I<sys_newuname>()  (slot I<__NR_uname)>.  The first one used length 9 for "
"all fields; the second used 65; the third also uses 65 but adds the "
"I<domainname> field.  The glibc B<uname>()  wrapper function hides these "
"details from applications, invoking the most recent version of the system "
"call provided by the kernel."
msgstr ""
"Со временем, увеличение размера структуры I<utsname> последовательно привело "
"к трём версиям B<uname>(): I<sys_olduname>() (слот I<__NR_oldolduname>), "
"I<sys_uname>() (слот I<__NR_olduname>) и I<sys_newuname>() (слот "
"I<__NR_uname)>. В первой длина каждого поля равна 9; во второй 65; в третьей "
"также 65, но добавилось поле I<domainname>. Обёрточная функция B<uname>() в "
"glibc скрывает эти подробности от приложений, вызывая самую новую версию "
"системного вызова, предоставляемого ядром."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "ЗАМЕЧАНИЯ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The kernel has the name, release, version, and supported machine type built "
"in.  Conversely, the I<nodename> field is configured by the administrator to "
"match the network (this is what the BSD historically calls the \"hostname\", "
"and is set via B<sethostname>(2)).  Similarly, the I<domainname> field is "
"set via B<setdomainname>(2)."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Part of the utsname information is also accessible via I</proc/sys/kernel/"
">{I<ostype>, I<hostname>, I<osrelease>, I<version>, I<domainname>}."
msgstr ""
"Часть информации из структуры utsname может быть получена также через I</"
"proc/sys/kernel/> {I<ostype>, I<hostname>, I<osrelease>, I<version>, "
"I<domainname>}. "

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМ. ТАКЖЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"B<uname>(1), B<getdomainname>(2), B<gethostname>(2), B<uts_namespaces>(7)"
msgstr ""
"B<uname>(1), B<getdomainname>(2), B<gethostname>(2), B<uts_namespaces>(7)"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "2023-02-05"
msgstr "5 февраля 2023 г."

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron
msgid "POSIX.1-2001, POSIX.1-2008, SVr4, 4.4BSD."
msgstr "POSIX.1-2001, POSIX.1-2008, SVr4, 4.4BSD."

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.05"
msgstr "Linux man-pages 6.05"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "UNAME"
msgstr "UNAME"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2017-09-15"
msgstr "15 сентября 2017 г."

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Руководство программиста Linux"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<#include E<lt>sys/utsname.hE<gt>>"
msgstr "B<#include E<lt>sys/utsname.hE<gt>>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<int uname(struct utsname *>I<buf>B<);>"
msgstr "B<int uname(struct utsname *>I<buf>B<);>"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"struct utsname {\n"
"    char sysname[];    /* Operating system name (e.g., \"Linux\") */\n"
"    char nodename[];   /* Name within \"some implementation-defined\n"
"                          network\" */\n"
"    char release[];    /* Operating system release (e.g., \"2.6.28\") */\n"
"    char version[];    /* Operating system version */\n"
"    char machine[];    /* Hardware identifier */\n"
"#ifdef _GNU_SOURCE\n"
"    char domainname[]; /* NIS or YP domain name */\n"
"#endif\n"
"};\n"
msgstr ""
"struct utsname {\n"
"    char sysname[];    /* название операционной системы\n"
"                          (например, «Linux») */\n"
"    char nodename[];   /* имя в сети, зависящее от реализации */\n"
"    char release[];    /* идентификатор выпуска ОС (например, «2.6.28») */\n"
"    char version[];    /* версия ОС */\n"
"    char machine[];    /* идентификатор аппаратного обеспечения */\n"
"#ifdef _GNU_SOURCE\n"
"    char domainname[]; /* доменное имя NIS или YP */\n"
"#endif\n"
"};\n"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"The length of the arrays in a I<struct utsname> is unspecified (see NOTES); "
"the fields are terminated by a null byte (\\(aq\\e0\\(aq)."
msgstr ""
"Размеры массивов в I<struct utsname> не определены (см. ЗАМЕЧАНИЯ); поля "
"завершаются байтом с null (\\(aq\\e0\\(aq)."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"On success, zero is returned.  On error, -1 is returned, and I<errno> is set "
"appropriately."
msgstr ""
"При успешном выполнении возвращается 0. В случае ошибки возвращается -1, а "
"I<errno> устанавливается в соответствующее значение."

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "CONFORMING TO"
msgstr "СООТВЕТСТВИЕ СТАНДАРТАМ"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"POSIX.1-2001, POSIX.1-2008, SVr4.  There is no B<uname>()  call in 4.3BSD."
msgstr ""
"POSIX.1-2001, POSIX.1-2008, SVr4. Вызов B<uname>() отсутствует в 4.3BSD."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This is a system call, and the operating system presumably knows its name, "
"release and version.  It also knows what hardware it runs on.  So, four of "
"the fields of the struct are meaningful.  On the other hand, the field "
"I<nodename> is meaningless: it gives the name of the present machine in some "
"undefined network, but typically machines are in more than one network and "
"have several names.  Moreover, the kernel has no way of knowing about such "
"things, so it has to be told what to answer here.  The same holds for the "
"additional I<domainname> field."
msgstr ""
"Это системный вызов, и операционная система, предположительно, знает своё "
"имя, номер выпуска и версию. Она также знает, на каком аппаратном "
"обеспечении она работает. Таким образом, четыре поля структуры несут "
"полезную информацию. С другой стороны, поле I<nodename> её не несет: оно "
"указывает имя машины в некой неопределённой сети, но обычно машины находятся "
"в более чем одной сети и имеют несколько имён. Более того, ядро не может "
"каким-либо образом получить информацию о таких вещах, поэтому ему необходимо "
"сообщить что же возвращать в этом поле. То же относится и к дополнительному "
"полю I<domainname>."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"To this end, Linux uses the system calls B<sethostname>(2)  and "
"B<setdomainname>(2).  Note that there is no standard that says that the "
"hostname set by B<sethostname>(2)  is the same string as the I<nodename> "
"field of the struct returned by B<uname>()  (indeed, some systems allow a "
"256-byte hostname and an 8-byte nodename), but this is true on Linux.  The "
"same holds for B<setdomainname>(2)  and the I<domainname> field."
msgstr ""
"С этой целью в Linux используются системные вызовы B<sethostname>(2) и "
"B<setdomainname>(2). Обратите внимание, что хотя не существует стандарта, "
"который бы указывал, что имя машины, установленное B<sethostname>(2), "
"является той же строкой, что возвращается в поле I<nodename> при вызове "
"B<uname>(), для Linux это именно так (в действительности некоторые системы "
"позволяют 256-символьное имя машины и 8-символьное имя узла). То же "
"относится и к B<setdomainname>(2) и полю I<domainname>."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"The length of the fields in the struct varies.  Some operating systems or "
"libraries use a hardcoded 9 or 33 or 65 or 257.  Other systems use "
"B<SYS_NMLN> or B<_SYS_NMLN> or B<UTSLEN> or B<_UTSNAME_LENGTH>.  Clearly, it "
"is a bad idea to use any of these constants; just use sizeof(...).  Often "
"257 is chosen in order to have room for an internet hostname."
msgstr ""
"Длина полей в структуре может быть различна. Некоторые операционные системы "
"или библиотеки используют жёстко заданные значения 9, 33, 65 или 257. Другие "
"используют константы B<SYS_NMLN>, B<_SYS_NMLN>, B<UTSLEN> и "
"B<_UTSNAME_LENGTH>. Несомненно, идея использовать эти константы не очень "
"хороша \\(em можно просто использовать sizeof(...). Часто выбирается 257 для "
"того, чтобы имелось достаточно места для хранения имени машины в сети "
"интернет."

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<uname>(1), B<getdomainname>(2), B<gethostname>(2), B<namespaces>(7)"
msgstr "B<uname>(1), B<getdomainname>(2), B<gethostname>(2), B<namespaces>(7)"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "COLOPHON"
msgstr "ЗАМЕЧАНИЯ"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Эта страница является частью проекта Linux I<man-pages> версии 4.16. "
"Описание проекта, информацию об ошибках и последнюю версию этой страницы "
"можно найти по адресу \\%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"
