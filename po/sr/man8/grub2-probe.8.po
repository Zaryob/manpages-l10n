# Serbian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.11.0\n"
"POT-Creation-Date: 2023-10-02 12:02+0200\n"
"PO-Revision-Date: 2022-07-23 17:28+0200\n"
"Last-Translator: Automatically generated\n"
"Language-Team: Serbian <>\n"
"Language: sr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

#. type: TH
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "GRUB-PROBE"
msgstr "GRUB-PROBE"

#. type: TH
#: fedora-39 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "September 2023"
msgstr "Септембра 2023"

#. type: TH
#: fedora-39 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "GRUB 2.06"
msgstr "ГРУБ 2.06"

#. type: TH
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "System Administration Utilities"
msgstr "Помагала за администрацију система"

#. type: SH
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "НАЗИВ"

#. type: Plain text
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "grub-probe - probe device information for GRUB"
msgstr ""

#. type: SH
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "УВОД"

#. type: Plain text
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"B<grub-probe> [I<\\,OPTION\\/>...] [I<\\,OPTION\\/>]... [I<\\,PATH|DEVICE\\/"
">]"
msgstr ""
"B<grub-probe> [I<\\,ОПЦИЈА\\/>...] [I<\\,ОПЦИЈА\\/>]... [I<\\,ПУТАЊА|"
"УРЕЂАЈ\\/>]"

#. type: SH
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИС"

#. type: Plain text
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"Probe device information for a given path (or device, if the B<-d> option is "
"given)."
msgstr ""
"Испробава податке уређаја за датом путањом (или уређајем, ако је дата опција "
"B<-d>)."

#. type: TP
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-0>"
msgstr "B<-0>"

#. type: Plain text
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "separate items in output using ASCII NUL characters"
msgstr "одваја ставке на излазу користећи АСКРИ НИШТАВНЕ знакове"

#. type: TP
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-d>, B<--device>"
msgstr "B<-d>, B<--device>"

#. type: Plain text
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "given argument is a system device, not a path"
msgstr "дати аргумент је системски уређај, а не путања"

#. type: TP
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-m>, B<--device-map>=I<\\,FILE\\/>"
msgstr "B<-m>, B<--device-map>=I<\\,ДАТОТЕКА\\/>"

#. type: Plain text
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "use FILE as the device map [default=/boot/grub2/device.map]"
msgstr "користи ДАТОТЕКУ као карту уређаја [основно=/boot/grub2/device.map]"

#. type: TP
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-t>, B<--target>=I<\\,TARGET\\/>"
msgstr "B<-t>, B<--target>=I<\\,МЕТА\\/>"

#. type: Plain text
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"print TARGET available targets: abstraction, arc_hints, baremetal_hints, "
"bios_hints, compatibility_hint, cryptodisk_uuid, device, disk, drive, "
"efi_hints, fs, fs_label, fs_uuid, gpt_parttype, hints_string, "
"ieee1275_hints, msdos_parttype, partmap, partuuid, zero_check [default=fs]"
msgstr ""
"Исписује МЕТУ. Доступни мете: abstraction, arc_hints, baremetal_hints, "
"bios_hints, compatibility_hint, cryptodisk_uuid, device, disk, drive, "
"efi_hints, fs, fs_label, fs_uuid, gpt_parttype, hints_string, "
"ieee1275_hints, msdos_parttype, partmap, partuuid, zero_check [основно=fs]"

#. type: TP
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-v>, B<--verbose>"
msgstr "B<-v>, B<--verbose>"

#. type: Plain text
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "print verbose messages (pass twice to enable debug printing)."
msgstr ""

#. type: TP
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "-?, B<--help>"
msgstr "-?, B<--help>"

#. type: Plain text
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "give this help list"
msgstr "приказује овај списак помоћи"

#. type: TP
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--usage>"
msgstr "B<--usage>"

#. type: Plain text
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "give a short usage message"
msgstr "приказује кратку поруку коришћења"

#. type: TP
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "print program version"
msgstr "исписује издање програма"

#. type: Plain text
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"Mandatory or optional arguments to long options are also mandatory or "
"optional for any corresponding short options."
msgstr ""
"Обавезни или изборни аргументи за дуге опције су такође обавезни или изборни "
"за било које одговарајуће кратке опције."

#. type: SH
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "ПРИЈАВЉИВАЊЕ ГРЕШАКА"

#. type: Plain text
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "Report bugs to E<lt>bug-grub@gnu.orgE<gt>."
msgstr "Грешке пријавите на: E<lt>bug-grub@gnu.orgE<gt>."

#. type: SH
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "ВИДИТЕ ТАКОЂЕ"

#. type: Plain text
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "B<grub-fstest>(1)"
msgstr "B<grub-fstest>(1)"

#. type: Plain text
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"The full documentation for B<grub-probe> is maintained as a Texinfo manual.  "
"If the B<info> and B<grub-probe> programs are properly installed at your "
"site, the command"
msgstr ""
"Потпуна документација за B<grub-probe> је одржавана као Тексинфо упутство.  "
"Ако су B<info> и B<grub-probe> исправно инсталирани на вашем сајту, наредба"

#. type: Plain text
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "B<info grub-probe>"
msgstr "B<info grub-probe>"

#. type: Plain text
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "should give you access to the complete manual."
msgstr "треба да вам да приступ потпуном упутству."

#. type: TH
#: opensuse-leap-15-6
#, fuzzy, no-wrap
#| msgid "March 2023"
msgid "May 2023"
msgstr "Марта 2023"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "grub-probe (GRUB2) 2.06"
msgstr "grub-probe (ГРУБ2) 2.06"

#. type: Plain text
#: opensuse-leap-15-6
msgid "print verbose messages."
msgstr "исписује опширне поруке."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "August 2023"
msgstr "Августа 2023"

#. type: TH
#: opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "GRUB 2:2.12rc1-1"
msgid "GRUB2 2.12~rc1"
msgstr "ГРУБ 2:2.12rc1-1"
