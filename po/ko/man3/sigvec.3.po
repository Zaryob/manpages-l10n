# Korean translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# ASPLINUX <man@asp-linux.co.kr>, 2000.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-08-27 17:22+0200\n"
"PO-Revision-Date: 2000-07-29 08:57+0900\n"
"Last-Translator: ASPLINUX <man@asp-linux.co.kr>\n"
"Language-Team: Korean <translation-team-ko@googlegroups.com>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "sigvec"
msgstr "sigvec"

#. type: TH
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid "2023-07-20"
msgstr "2023년 7월 20일"

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Linux man-pages 6.05.01"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "이름"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "sigblock, siggetmask, sigsetmask, sigmask - manipulate the signal mask"
msgid "sigvec, sigblock, sigsetmask, siggetmask, sigmask - BSD signal API"
msgstr "sigblock, siggetmask, sigsetmask, sigmask - 시그날 마스크 조작하기"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "라이브러리"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "표준 C 라이브러리 (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "요약"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>signal.hE<gt>>\n"
msgstr "B<#include E<lt>signal.hE<gt>>\n"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<int sigvec(int >I<sig>B<, const struct sigvec *>I<vec>B<, struct sigvec *>I<ovec>B<);>\n"
msgid ""
"B<[[deprecated]] int sigvec(int >I<sig>B<, const struct sigvec *>I<vec>B<,>\n"
"B<                          struct sigvec *>I<ovec>B<);>\n"
msgstr "B<int sigvec(int >I<sig>B<, const struct sigvec *>I<vec>B<, struct sigvec *>I<ovec>B<);>\n"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<int sigmask(int >I<signum>B<);>\n"
msgid "B<[[deprecated]] int sigmask(int >I<signum>B<);>\n"
msgstr "B<int sigmask(int >I<signum>B<);>\n"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "B<int sigblock(int >I<mask>B<);>\n"
#| "B<int sigsetmask(int >I<mask>B<);>\n"
#| "B<int siggetmask(void);>\n"
msgid ""
"B<[[deprecated]] int sigblock(int >I<mask>B<);>\n"
"B<[[deprecated]] int sigsetmask(int >I<mask>B<);>\n"
"B<[[deprecated]] int siggetmask(void);>\n"
msgstr ""
"B<int sigblock(int >I<mask>B<);>\n"
"B<int sigsetmask(int >I<mask>B<);>\n"
"B<int siggetmask(void);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "All functions shown above:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"    Since glibc 2.19:\n"
"        _DEFAULT_SOURCE\n"
"    glibc 2.19 and earlier:\n"
"        _BSD_SOURCE\n"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "설명"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"These functions are provided in glibc as a compatibility interface for "
"programs that make use of the historical BSD signal API.  This API is "
"obsolete: new applications should use the POSIX signal API (B<sigaction>(2), "
"B<sigprocmask>(2), etc.)."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<sigvec>()  function sets and/or gets the disposition of the signal "
"I<sig> (like the POSIX B<sigaction>(2)).  If I<vec> is not NULL, it points "
"to a I<sigvec> structure that defines the new disposition for I<sig>.  If "
"I<ovec> is not NULL, it points to a I<sigvec> structure that is used to "
"return the previous disposition of I<sig>.  To obtain the current "
"disposition of I<sig> without changing it, specify NULL for I<vec>, and a "
"non-null pointer for I<ovec>."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The dispositions for B<SIGKILL> and B<SIGSTOP> cannot be changed."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The I<sigvec> structure has the following form:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"struct sigvec {\n"
"    void (*sv_handler)(int); /* Signal disposition */\n"
"    int    sv_mask;          /* Signals to be blocked in handler */\n"
"    int    sv_flags;         /* Flags */\n"
"};\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The I<sv_handler> field specifies the disposition of the signal, and is "
"either: the address of a signal handler function; B<SIG_DFL>, meaning the "
"default disposition applies for the signal; or B<SIG_IGN>, meaning that the "
"signal is ignored."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If I<sv_handler> specifies the address of a signal handler, then I<sv_mask> "
"specifies a mask of signals that are to be blocked while the handler is "
"executing.  In addition, the signal for which the handler is invoked is also "
"blocked.  Attempts to block B<SIGKILL> or B<SIGSTOP> are silently ignored."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If I<sv_handler> specifies the address of a signal handler, then the "
"I<sv_flags> field specifies flags controlling what happens when the handler "
"is called.  This field may contain zero or more of the following flags:"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<SV_INTERRUPT>"
msgstr "B<SV_INTERRUPT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"If the signal handler interrupts a blocking system call, then upon return "
"from the handler the system call is not restarted: instead it fails with the "
"error B<EINTR>.  If this flag is not specified, then system calls are "
"restarted by default."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<SV_RESETHAND>"
msgstr "B<SV_RESETHAND>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Reset the disposition of the signal to the default before calling the signal "
"handler.  If this flag is not specified, then the handler remains "
"established until explicitly removed by a later call to B<sigvec>()  or "
"until the process performs an B<execve>(2)."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<SV_ONSTACK>"
msgstr "B<SV_ONSTACK>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Handle the signal on the alternate signal stack (historically established "
"under BSD using the obsolete B<sigstack>()  function; the POSIX replacement "
"is B<sigaltstack>(2))."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<sigmask>()  macro constructs and returns a \"signal mask\" for "
"I<signum>.  For example, we can initialize the I<vec.sv_mask> field given to "
"B<sigvec>()  using code such as the following:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"vec.sv_mask = sigmask(SIGQUIT) | sigmask(SIGABRT);\n"
"            /* Block SIGQUIT and SIGABRT during\n"
"               handler execution */\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<sigblock>()  function adds the signals in I<mask> to the process's "
"signal mask (like POSIX I<sigprocmask(SIG_BLOCK)>), and returns the "
"process's previous signal mask.  Attempts to block B<SIGKILL> or B<SIGSTOP> "
"are silently ignored."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<sigsetmask>()  function sets the process's signal mask to the value "
"given in I<mask> (like POSIX I<sigprocmask(SIG_SETMASK)>), and returns the "
"process's previous signal mask."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<siggetmask>()  function returns the process's current signal mask.  "
"This call is equivalent to I<sigblock(0)>."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "반환값"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<sigvec>()  function returns 0 on success; on error, it returns -1 and "
"sets I<errno> to indicate the error."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "B<sigsetmask> and B<sigblock> return the previous set of masked signals."
msgid ""
"The B<sigblock>()  and B<sigsetmask>()  functions return the previous signal "
"mask."
msgstr "B<sigsetmask> 와 B<sigblock> 는 이전 mask된 신호를 반환한다."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The B<sigmask> macro is provided to construct the mask for a given "
#| "I<signum>."
msgid "The B<sigmask>()  macro returns the signal mask for I<signum>."
msgstr ""
"B<sigmask> 매크로는 주어진 I<signum>.  를 위한 mask를 만드는데 제공된다."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "에러"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "See the ERRORS under B<sigaction>(2)  and B<sigprocmask>(2)."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "속성"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"이 섹션에서 사용되는 용어에 대한 설명은 B<attributes>(7)을 참조하십시오."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "상호 작용"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "속성"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "번호"

#. type: tbl table
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<sigvec>(),\n"
"B<sigmask>(),\n"
"B<sigblock>(),\n"
"B<sigsetmask>(),\n"
"B<siggetmask>()"
msgstr ""
"B<sigvec>(),\n"
"B<sigmask>(),\n"
"B<sigblock>(),\n"
"B<sigsetmask>(),\n"
"B<siggetmask>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "표준"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "None."
msgstr ""

#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "이력"

#. type: TP
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "sigvec"
msgid "B<sigvec>()"
msgstr "sigvec"

#. type: TQ
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<sigprocmask>(2)"
msgid "B<sigblock>()"
msgstr "B<sigprocmask>(2)"

#. type: TQ
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<sigprocmask>(2)"
msgid "B<sigmask>()"
msgstr "B<sigprocmask>(2)"

#. type: TQ
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<sigprocmask>(2)"
msgid "B<sigsetmask>()"
msgstr "B<sigprocmask>(2)"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "4.3BSD."
msgstr "4.3BSD."

#. type: TP
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<sigprocmask>(2)"
msgid "B<siggetmask>()"
msgstr "B<sigprocmask>(2)"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "Unclear origin."
msgstr ""

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, fuzzy
#| msgid "Before glibc 2.10:"
msgid "Removed in glibc 2.21."
msgstr "glibc 2.10 이전:"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "주의"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"On 4.3BSD, the B<signal>()  function provided reliable semantics (as when "
"calling B<sigvec>()  with I<vec.sv_mask> equal to 0).  On System V, "
"B<signal>()  provides unreliable semantics.  POSIX.1 leaves these aspects of "
"B<signal>()  unspecified.  See B<signal>(2)  for further details."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"In order to wait for a signal, BSD and System V both provided a function "
"named B<sigpause>(3), but this function has a different argument on the two "
"systems.  See B<sigpause>(3)  for details."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "추가 참조"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<kill>(2), B<pause>(2), B<sigaction>(2), B<signal>(2), B<sigprocmask>(2), "
"B<raise>(3), B<sigpause>(3), B<sigset>(3), B<signal>(7)"
msgstr ""
"B<kill>(2), B<pause>(2), B<sigaction>(2), B<signal>(2), B<sigprocmask>(2), "
"B<raise>(3), B<sigpause>(3), B<sigset>(3), B<signal>(7)"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "2023-02-05"
msgstr "2023년 2월 5일"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "B<int sigvec(int >I<sig>B<, const struct sigvec *>I<vec>B<, struct sigvec *>I<ovec>B<);>\n"
msgstr "B<int sigvec(int >I<sig>B<, const struct sigvec *>I<vec>B<, struct sigvec *>I<ovec>B<);>\n"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "B<int sigmask(int >I<signum>B<);>\n"
msgstr "B<int sigmask(int >I<signum>B<);>\n"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid ""
"B<int sigblock(int >I<mask>B<);>\n"
"B<int sigsetmask(int >I<mask>B<);>\n"
"B<int siggetmask(void);>\n"
msgstr ""
"B<int sigblock(int >I<mask>B<);>\n"
"B<int sigsetmask(int >I<mask>B<);>\n"
"B<int siggetmask(void);>\n"

#. type: SH
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "VERSIONS"
msgstr "버전S"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron
msgid ""
"Starting with glibc 2.21, the GNU C library no longer exports the "
"B<sigvec>()  function as part of the ABI.  (To ensure backward "
"compatibility, the glibc symbol versioning scheme continues to export the "
"interface to binaries linked against older versions of the library.)"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid ""
"All of these functions were in 4.3BSD, except B<siggetmask>(), whose origin "
"is unclear.  These functions are obsolete: do not use them in new programs."
msgstr ""

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.05"
msgstr "Linux man-pages 6.05"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "SIGVEC"
msgstr "SIGVEC"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2017-09-15"
msgstr "2017년 9월 15일"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux"
msgstr "리눅스"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "리눅스 프로그래머 매뉴얼"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<#include E<lt>signal.hE<gt>>"
msgstr "B<#include E<lt>signal.hE<gt>>"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"B<int sigvec(int >I<sig>B<, const struct sigvec *>I<vec>B<, struct sigvec "
"*>I<ovec>B<);>"
msgstr ""
"B<int sigvec(int >I<sig>B<, const struct sigvec *>I<vec>B<, struct sigvec "
"*>I<ovec>B<);>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<int sigmask(int >I<signum>B<);>"
msgstr "B<int sigmask(int >I<signum>B<);>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<int sigblock(int >I<mask>B<);>"
msgstr "B<int sigblock(int >I<mask>B<);>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<int sigsetmask(int >I<mask>B<);>"
msgstr "B<int sigsetmask(int >I<mask>B<);>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<int siggetmask(void);>"
msgstr "B<int siggetmask(void);>"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"All functions shown above:\n"
"    Since glibc 2.19:\n"
"        _DEFAULT_SOURCE\n"
"    Glibc 2.19 and earlier:\n"
"        _BSD_SOURCE\n"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"If the signal handler interrupts a blocking system call, then upon return "
"from the handler the system call s not be restarted: instead it fails with "
"the error B<EINTR>.  If this flag is not specified, then system calls are "
"restarted by default."
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Starting with version 2.21, the GNU C library no longer exports the "
"B<sigvec>()  function as part of the ABI.  (To ensure backward "
"compatibility, the glibc symbol versioning scheme continues to export the "
"interface to binaries linked against older versions of the library.)"
msgstr ""

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "CONFORMING TO"
msgstr "호환"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "COLOPHON"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "2023년 3월 30일"

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"
