# Korean translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# 정강훈 <skyeyes@soback.kornet.net>, 2001.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-08-27 17:30+0200\n"
"PO-Revision-Date: 2001-05-01 08:57+0900\n"
"Last-Translator: 정강훈 <skyeyes@soback.kornet.net>\n"
"Language-Team: Korean <translation-team-ko@googlegroups.com>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "ttyname"
msgstr "ttyname"

#. type: TH
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid "2023-07-20"
msgstr "2023년 7월 20일"

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Linux man-pages 6.05.01"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "이름"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "ttyname, ttyname_r - return name of a terminal"
msgstr "ttyname, ttyname_r - 터미널 이름을 반환한다."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "라이브러리"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "표준 C 라이브러리 (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "요약"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>unistd.hE<gt>>\n"
msgstr "B<#include E<lt>unistd.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"B<char *ttyname(int >I<fd>B<);>\n"
"B<int ttyname_r(int >I<fd>B<, char >I<buf>B<[.>I<buflen>B<], size_t >I<buflen>B<);>\n"
msgstr ""
"B<char *ttyname(int >I<fd>B<);>\n"
"B<int ttyname_r(int >I<fd>B<, char >I<buf>B<[.>I<buflen>B<], size_t >I<buflen>B<);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "설명"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The function B<ttyname>()  returns a pointer to the null-terminated pathname "
"of the terminal device that is open on the file descriptor I<fd>, or NULL on "
"error (for example, if I<fd> is not connected to a terminal).  The return "
"value may point to static data, possibly overwritten by the next call.  The "
"function B<ttyname_r>()  stores this pathname in the buffer I<buf> of length "
"I<buflen>."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "반환값"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The function B<ttyname>()  returns a pointer to a pathname on success.  On "
"error, NULL is returned, and I<errno> is set to indicate the error.  The "
"function B<ttyname_r>()  returns 0 on success, and an error number upon "
"error."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "에러"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EBADF>"
msgstr "B<EBADF>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid "Invalid file descriptor I<fd>."
msgid "Bad file descriptor."
msgstr "유효 하지 않은 파일 기술자 I<fd>."

#.  glibc commit 15e9a4f378c8607c2ae1aa465436af4321db0e23
#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENODEV>"
msgstr "B<ENODEV>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"I<fd> refers to a slave pseudoterminal device but the corresponding pathname "
"could not be found (see NOTES)."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOTTY>"
msgstr "B<ENOTTY>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "I<fd> does not refer to a terminal device."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ERANGE>"
msgstr "B<ERANGE>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"(B<ttyname_r>())  I<buflen> was too small to allow storing the pathname."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "속성"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"이 섹션에서 사용되는 용어에 대한 설명은 B<attributes>(7)을 참조하십시오."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "상호 작용"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "속성"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "번호"

#. type: tbl table
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ttyname>()"
msgstr "B<ttyname>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "MT-Unsafe race:ttyname"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ttyname_r>()"
msgstr "B<ttyname_r>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "표준"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr "POSIX.1-2008."

#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "이력"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, fuzzy
#| msgid "POSIX.1-2001, POSIX.1-2008, 4.2BSD."
msgid "POSIX.1-2001, 4.2BSD."
msgstr "POSIX.1-2001, POSIX.1-2008, 4.2BSD."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "주의"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"A process that keeps a file descriptor that refers to a B<pts>(4)  device "
"open when switching to another mount namespace that uses a different I</dev/"
"ptmx> instance may still accidentally find that a device path of the same "
"name for that file descriptor exists.  However, this device path refers to a "
"different device and thus can't be used to access the device that the file "
"descriptor refers to.  Calling B<ttyname>()  or B<ttyname_r>()  on the file "
"descriptor in the new mount namespace will cause these functions to return "
"NULL and set I<errno> to B<ENODEV>."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "추가 참조"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<tty>(1), B<fstat>(2), B<ctermid>(3), B<isatty>(3), B<pts>(4)"
msgstr "B<tty>(1), B<fstat>(2), B<ctermid>(3), B<isatty>(3), B<pts>(4)"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "2022-12-15"
msgstr "2022년 12월 15일"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid "POSIX.1-2001, POSIX.1-2008, 4.2BSD."
msgstr "POSIX.1-2001, POSIX.1-2008, 4.2BSD."

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.05"
msgstr "Linux man-pages 6.05"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "TTYNAME"
msgstr "TTYNAME"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2017-05-03"
msgstr "2017년 5월 3일"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux"
msgstr "리눅스"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "리눅스 프로그래머 매뉴얼"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid "B<char *ttyname(int >I<fd>B<);>\n"
msgstr "B<char *ttyname(int >I<fd>B<);>\n"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid "B<int ttyname_r(int >I<fd>B<, char *>I<buf>B<, size_t >I<buflen>B<);>\n"
msgstr "B<int ttyname_r(int >I<fd>B<, char *>I<buf>B<, size_t >I<buflen>B<);>\n"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"The function B<ttyname>()  returns a pointer to a pathname on success.  On "
"error, NULL is returned, and I<errno> is set appropriately.  The function "
"B<ttyname_r>()  returns 0 on success, and an error number upon error."
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
#, fuzzy
#| msgid "File descriptor does not refer to a directory."
msgid "File descriptor does not refer to a terminal device."
msgstr "파일 기술자가 디렉토리를 가리키지 않는다."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"File descriptor refers to a slave pseudoterminal device but the "
"corresponding pathname could not be found (see NOTES)."
msgstr ""

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "CONFORMING TO"
msgstr "호환"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "COLOPHON"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "2023년 3월 30일"

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"
