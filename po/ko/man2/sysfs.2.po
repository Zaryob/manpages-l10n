# Korean translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# ASPLINUX <man@asp-linux.co.kr>, 2000.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-08-27 17:25+0200\n"
"PO-Revision-Date: 2000-07-26 08:57+0900\n"
"Last-Translator: ASPLINUX <man@asp-linux.co.kr>\n"
"Language-Team: Korean <translation-team-ko@googlegroups.com>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "sysfs"
msgstr "sysfs"

#. type: TH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "2023년 3월 30일"

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Linux man-pages 6.05.01"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "이름"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "sysfs - get filesystem type information"
msgstr "sysfs - 파일 시스템 타입 정보를 가져온다"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "요약"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"B<[[deprecated]] int sysfs(int >I<option>B<, const char *>I<fsname>B<);>\n"
"B<[[deprecated]] int sysfs(int >I<option>B<, unsigned int >I<fs_index>B<, char *>I<buf>B<);>\n"
"B<[[deprecated]] int sysfs(int >I<option>B<);>\n"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "설명"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<Note>: if you are looking for information about the B<sysfs> filesystem "
"that is normally mounted at I</sys>, see B<sysfs>(5)."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "B<sysfs> returns information about the file system types currently "
#| "present in the kernel. The specific form of the B<sysfs> call and the "
#| "information returned depends on the I<option> in effect:"
msgid ""
"The (obsolete)  B<sysfs>()  system call returns information about the "
"filesystem types currently present in the kernel.  The specific form of the "
"B<sysfs>()  call and the information returned depends on the I<option> in "
"effect:"
msgstr ""
"B<sysfs> 는 커널상에 현제 있는 파일 시스템 타입에 관한 정보를 반환한다.  The "
"specific form of the B<sysfs> 호출과 정보의 특별한 형태를 작용하는 I<option> "
"에 따라 반환한다:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<1>"
msgstr "B<1>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "Translate the file-system identifier string I<fsname> into a file-system "
#| "type index."
msgid ""
"Translate the filesystem identifier string I<fsname> into a filesystem type "
"index."
msgstr ""
"file-system identifier string I<fsname> 을 file-system type index로 바꾼다."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<2>"
msgstr "B<2>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "Translate the file-system type index I<fs_index> into a null-terminated "
#| "file-system identifier string. This string will be written to the buffer "
#| "pointed to by I<buf>.  Make sure that I<buf> has enough space to accept "
#| "the string."
msgid ""
"Translate the filesystem type index I<fs_index> into a null-terminated "
"filesystem identifier string.  This string will be written to the buffer "
"pointed to by I<buf>.  Make sure that I<buf> has enough space to accept the "
"string."
msgstr ""
"file-system type index I<fs_index> 를 null-terminated file-system identifier "
"string으로 바꾼다. 이 문자열은 d to by I<buf>.  에 의해 지정된 버퍼로 쓰여진"
"다.  I<buf> 가 문자열을 받아들일 충분한 공간이 있는지 확인하라."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<3>"
msgstr "B<3>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Return the total number of filesystem types currently present in the kernel."
msgstr "커널에 현재 있는 파일 시스템의 총 개수를 반환한다."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The numbering of the filesystem type indexes begins with zero."
msgstr "파일 시스템 타입의 번호는 0으로 시작한다."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "반환값"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "On success, B<sysfs> returns the file-system index for option B<1>, zero "
#| "for option B<2>, and the number of currently configured file systems for "
#| "option B<3>.  On error, -1 is returned, and I<errno> is set appropriately."
msgid ""
"On success, B<sysfs>()  returns the filesystem index for option B<1>, zero "
"for option B<2>, and the number of currently configured filesystems for "
"option B<3>.  On error, -1 is returned, and I<errno> is set to indicate the "
"error."
msgstr ""
"성공 시, B<sysfs> 는 옵션 B<1>, 에 대해 파일 시스템 인덱스를 반환하고, 옵션 "
"B<2>, 에 대해 0을 반환하고, 옵션 B<3>.  에 대해 현제 설정된 파일 시스템의 수"
"를 반환한다.  에러 시, -1 이 반환된다. 그리고 I<errno> 는 적절히 설정된다."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "에러"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EFAULT>"
msgstr "B<EFAULT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Either I<fsname> or I<buf> is outside your accessible address space."
msgstr "fsname I<혹은> buf 가 접근 가능한 어드레스 영역을 넘었을 경우."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "I<fsname> is not a valid file-system type identifier; I<fs_index> is out-"
#| "of-bounds; I<option> is invalid."
msgid ""
"I<fsname> is not a valid filesystem type identifier; I<fs_index> is out-of-"
"bounds; I<option> is invalid."
msgstr ""
"I<fsname> 가 유효한 file-system type identifier가 아닌 경우; I<fs_index> 가 "
"범위를 넘어간 경우; I<option> 가 불가한 경우;"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "표준"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "None."
msgstr ""

#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "이력"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "SVr4."
msgstr "SVr4."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"This System-V derived system call is obsolete; don't use it.  On systems "
"with I</proc>, the same information can be obtained via I</proc>; use that "
"interface instead."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "버그"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"There is no libc or glibc support.  There is no way to guess how large "
"I<buf> should be."
msgstr "지원되는 libc나 glibc가 없다.  I<buf>가 얼마나 커야 하는지 알 수 없다."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "추가 참조"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "B<proc>(5), B<sysfs>(5)"
msgstr "B<proc>(5), B<sysfs>(5)"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "2022-10-30"
msgstr "2022년 10월 30일"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: SH
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "NOTES"
msgstr "주의"

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.05"
msgstr "Linux man-pages 6.05"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "SYSFS"
msgstr "SYSFS"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2017-09-15"
msgstr "2017년 9월 15일"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux"
msgstr "리눅스"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "리눅스 프로그래머 매뉴얼"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<int sysfs(int >I<option>B<, const char *>I<fsname>B<);>"
msgstr "B<int sysfs(int >I<option>B<, const char *>I<fsname>B<);>"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"B<int sysfs(int >I<option>B<, unsigned int >I<fs_index>B<, char *>I<buf>B<);>"
msgstr ""
"B<int sysfs(int >I<option>B<, unsigned int >I<fs_index>B<, char *>I<buf>B<);>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<int sysfs(int >I<option>B<);>"
msgstr "B<int sysfs(int >I<option>B<);>"

#. type: Plain text
#: opensuse-leap-15-6
#, fuzzy
#| msgid ""
#| "On success, B<sysfs> returns the file-system index for option B<1>, zero "
#| "for option B<2>, and the number of currently configured file systems for "
#| "option B<3>.  On error, -1 is returned, and I<errno> is set appropriately."
msgid ""
"On success, B<sysfs>()  returns the filesystem index for option B<1>, zero "
"for option B<2>, and the number of currently configured filesystems for "
"option B<3>.  On error, -1 is returned, and I<errno> is set appropriately."
msgstr ""
"성공 시, B<sysfs> 는 옵션 B<1>, 에 대해 파일 시스템 인덱스를 반환하고, 옵션 "
"B<2>, 에 대해 0을 반환하고, 옵션 B<3>.  에 대해 현제 설정된 파일 시스템의 수"
"를 반환한다.  에러 시, -1 이 반환된다. 그리고 I<errno> 는 적절히 설정된다."

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "CONFORMING TO"
msgstr "호환"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This System-V derived system call is obsolete; don't use it.  On systems "
"with I</proc>, the same information can be obtained via I</proc/"
"filesystems>; use that interface instead."
msgstr ""

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "COLOPHON"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"
