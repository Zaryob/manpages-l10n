# Korean translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# 최윤용 <markboy@secsm.org>, 2000.
# 한글 Manpage 프로젝트 <http://man.kldp.org>, 2005.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-08-27 16:59+0200\n"
"PO-Revision-Date: 2005-02-17 08:57+0900\n"
"Last-Translator: 한글 Manpage 프로젝트 <http://man.kldp.org>\n"
"Language-Team: Korean <translation-team-ko@googlegroups.com>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "getpid"
msgstr "getpid"

#. type: TH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "2023년 3월 30일"

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Linux man-pages 6.05.01"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "이름"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "getpid, getppid - get process identification"
msgstr "getpid, getppid - 프로세스 식별값(identification)을 얻는다."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "라이브러리"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "표준 C 라이브러리 (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "요약"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>unistd.hE<gt>>\n"
msgstr "B<#include E<lt>unistd.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"B<pid_t getpid(void);>\n"
"B<pid_t getppid(void);>\n"
msgstr ""
"B<pid_t getpid(void);>\n"
"B<pid_t getppid(void);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "설명"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "B<getpid> returns the process ID of the current process.  (This is often "
#| "used by routines that generate unique temporary file names.)"
msgid ""
"B<getpid>()  returns the process ID (PID) of the calling process.  (This is "
"often used by routines that generate unique temporary filenames.)"
msgstr ""
"B<getpid>는 현재 프로세스의 프로세스 ID를 돌려준다. (이것은 유일한 임시 파일 "
"이름을 만드는 루틴에서 종종 사용된다.)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<getppid>()  returns the process ID of the parent of the calling process.  "
"This will be either the ID of the process that created this process using "
"B<fork>(), or, if that process has already terminated, the ID of the process "
"to which this process has been reparented (either B<init>(1)  or a "
"\"subreaper\" process defined via the B<prctl>(2)  B<PR_SET_CHILD_SUBREAPER> "
"operation)."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "에러"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "These functions are always successful."
msgstr "이 함수들은 항상 성공한다."

#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "버전S"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"On Alpha, instead of a pair of B<getpid>()  and B<getppid>()  system calls, "
"a single B<getxpid>()  system call is provided, which returns a pair of PID "
"and parent PID.  The glibc B<getpid>()  and B<getppid>()  wrapper functions "
"transparently deal with this.  See B<syscall>(2)  for details regarding "
"register mapping."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "표준"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr "POSIX.1-2008."

#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "이력"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "POSIX.1-2001, 4.3BSD, SVr4."
msgstr "POSIX.1-2001, 4.3BSD, SVr4."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "C library/kernel differences"
msgstr ""

#
#
#
#
#.  The following program demonstrates this "feature":
#.  #define _GNU_SOURCE
#.  #include <sys/syscall.h>
#.  #include <sys/wait.h>
#.  #include <stdint.h>
#.  #include <stdio.h>
#.  #include <stdlib.h>
#.  #include <unistd.h>
#.  int
#.  main(int argc, char *argv[])
#.  {
#.     /* The following statement fills the getpid() cache */
#.     printf("parent PID = %ld
#. ", (intmax_t) getpid());
#.     if (syscall(SYS_fork) == 0) {
#.         if (getpid() != syscall(SYS_getpid))
#.             printf("child getpid() mismatch: getpid()=%jd; "
#.                     "syscall(SYS_getpid)=%ld
#. ",
#.                     (intmax_t) getpid(), (long) syscall(SYS_getpid));
#.         exit(EXIT_SUCCESS);
#.     }
#.     wait(NULL);
#. }
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"From glibc 2.3.4 up to and including glibc 2.24, the glibc wrapper function "
"for B<getpid>()  cached PIDs, with the goal of avoiding additional system "
"calls when a process calls B<getpid>()  repeatedly.  Normally this caching "
"was invisible, but its correct operation relied on support in the wrapper "
"functions for B<fork>(2), B<vfork>(2), and B<clone>(2): if an application "
"bypassed the glibc wrappers for these system calls by using B<syscall>(2), "
"then a call to B<getpid>()  in the child would return the wrong value (to be "
"precise: it would return the PID of the parent process).  In addition, there "
"were cases where B<getpid>()  could return the wrong value even when "
"invoking B<clone>(2)  via the glibc wrapper function.  (For a discussion of "
"one such case, see BUGS in B<clone>(2).)  Furthermore, the complexity of the "
"caching code had been the source of a few bugs within glibc over the years."
msgstr ""

#.  commit c579f48edba88380635ab98cb612030e3ed8691e
#.  https://sourceware.org/glibc/wiki/Release/2.25#pid_cache_removal
#.  FIXME .
#.  Review progress of https://bugzilla.redhat.com/show_bug.cgi?id=1469757
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Because of the aforementioned problems, since glibc 2.25, the PID cache is "
"removed: calls to B<getpid>()  always invoke the actual system call, rather "
"than returning a cached value."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "주의"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If the caller's parent is in a different PID namespace (see "
"B<pid_namespaces>(7)), B<getppid>()  returns 0."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"From a kernel perspective, the PID (which is shared by all of the threads in "
"a multithreaded process)  is sometimes also known as the thread group ID "
"(TGID).  This contrasts with the kernel thread ID (TID), which is unique for "
"each thread.  For further details, see B<gettid>(2)  and the discussion of "
"the B<CLONE_THREAD> flag in B<clone>(2)."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "추가 참조"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<clone>(2), B<fork>(2), B<gettid>(2), B<kill>(2), B<exec>(3), "
"B<mkstemp>(3), B<tempnam>(3), B<tmpfile>(3), B<tmpnam>(3), "
"B<credentials>(7), B<pid_namespaces>(7)"
msgstr ""
"B<clone>(2), B<fork>(2), B<gettid>(2), B<kill>(2), B<exec>(3), "
"B<mkstemp>(3), B<tempnam>(3), B<tmpfile>(3), B<tmpnam>(3), "
"B<credentials>(7), B<pid_namespaces>(7)"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "2023-01-22"
msgstr "2023년 1월 22일"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid "POSIX.1-2001, POSIX.1-2008, 4.3BSD, SVr4."
msgstr "POSIX.1-2001, POSIX.1-2008, 4.3BSD, SVr4."

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.05"
msgstr "Linux man-pages 6.05"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "GETPID"
msgstr "GETPID"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2017-11-26"
msgstr "2017년 11월 26일"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux"
msgstr "리눅스"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "리눅스 프로그래머 매뉴얼"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<#include E<lt>sys/types.hE<gt>>"
msgstr "B<#include E<lt>sys/types.hE<gt>>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<#include E<lt>unistd.hE<gt>>"
msgstr "B<#include E<lt>unistd.hE<gt>>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<pid_t getpid(void);>"
msgstr "B<pid_t getpid(void);>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<pid_t getppid(void);>"
msgstr "B<pid_t getppid(void);>"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "CONFORMING TO"
msgstr "호환"

#
#
#
#
#.  The following program demonstrates this "feature":
#.  #define _GNU_SOURCE
#.  #include <sys/syscall.h>
#.  #include <sys/wait.h>
#.  #include <stdio.h>
#.  #include <stdlib.h>
#.  #include <unistd.h>
#.  int
#.  main(int argc, char *argv[])
#.  {
#.     /* The following statement fills the getpid() cache */
#.     printf("parent PID = %ld
#. ", (long) getpid());
#.     if (syscall(SYS_fork) == 0) {
#.         if (getpid() != syscall(SYS_getpid))
#.             printf("child getpid() mismatch: getpid()=%ld; "
#.                     "syscall(SYS_getpid)=%ld
#. ",
#.                     (long) getpid(), (long) syscall(SYS_getpid));
#.         exit(EXIT_SUCCESS);
#.     }
#.     wait(NULL);
#. }
#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"From glibc version 2.3.4 up to and including version 2.24, the glibc wrapper "
"function for B<getpid>()  cached PIDs, with the goal of avoiding additional "
"system calls when a process calls B<getpid>()  repeatedly.  Normally this "
"caching was invisible, but its correct operation relied on support in the "
"wrapper functions for B<fork>(2), B<vfork>(2), and B<clone>(2): if an "
"application bypassed the glibc wrappers for these system calls by using "
"B<syscall>(2), then a call to B<getpid>()  in the child would return the "
"wrong value (to be precise: it would return the PID of the parent process).  "
"In addition, there were cases where B<getpid>()  could return the wrong "
"value even when invoking B<clone>(2)  via the glibc wrapper function.  (For "
"a discussion of one such case, see BUGS in B<clone>(2).)  Furthermore, the "
"complexity of the caching code had been the source of a few bugs within "
"glibc over the years."
msgstr ""

#.  commit c579f48edba88380635ab98cb612030e3ed8691e
#.  https://sourceware.org/glibc/wiki/Release/2.25#pid_cache_removal
#.  FIXME .
#.  Review progress of https://bugzilla.redhat.com/show_bug.cgi?id=1469757
#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Because of the aforementioned problems, since glibc version 2.25, the PID "
"cache is removed: calls to B<getpid>()  always invoke the actual system "
"call, rather than returning a cached value."
msgstr ""

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "COLOPHON"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"
