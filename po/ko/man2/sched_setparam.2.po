# Korean translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# ASPLINUX <man@asp-linux.co.kr>, 2000.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-08-27 17:20+0200\n"
"PO-Revision-Date: 2000-07-28 08:57+0900\n"
"Last-Translator: ASPLINUX <man@asp-linux.co.kr>\n"
"Language-Team: Korean <translation-team-ko@googlegroups.com>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "sched_setparam"
msgstr "sched_setparam"

#. type: TH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "2023년 3월 30일"

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Linux man-pages 6.05.01"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "이름"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "sched_setparam, sched_getparam - set and get scheduling parameters"
msgstr "sched_setparam, sched_getparam - 스케줄링 매개 변수의 설정과 소유"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "라이브러리"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "표준 C 라이브러리 (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "요약"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>sched.hE<gt>>\n"
msgstr "B<#include E<lt>sched.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"B<int sched_setparam(pid_t >I<pid>B<, const struct sched_param *>I<param>B<);>\n"
"B<int sched_getparam(pid_t >I<pid>B<, struct sched_param *>I<param>B<);>\n"
msgstr ""
"B<int sched_setparam(pid_t >I<pid>B<, const struct sched_param *>I<param>B<);>\n"
"B<int sched_getparam(pid_t >I<pid>B<, struct sched_param *>I<param>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<struct sched_param {\n"
"    ...\n"
"    int >I<sched_priority>B<;\n"
"    ...\n"
"};>\n"
msgstr ""
"B<struct sched_param {\n"
"    ...\n"
"    int >I<sched_priority>B<;\n"
"    ...\n"
"};>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "설명"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "B<sched_setparam> sets the scheduling parameters associated with the "
#| "scheduling policy for the process identified by I<pid>. If I<pid> is "
#| "zero, then the parameters of the current process are set. The "
#| "interpretation of the parameter I<p> depends on the selected policy. "
#| "Currently, the following three scheduling policies are supported under "
#| "Linux: I<SCHED_FIFO>, I<SCHED_RR>, and I<SCHED_OTHER.>"
msgid ""
"B<sched_setparam>()  sets the scheduling parameters associated with the "
"scheduling policy for the thread whose thread ID is specified in I<pid>.  If "
"I<pid> is zero, then the parameters of the calling thread are set.  The "
"interpretation of the argument I<param> depends on the scheduling policy of "
"the thread identified by I<pid>.  See B<sched>(7)  for a description of the "
"scheduling policies supported under Linux."
msgstr ""
"B<sched_setparam> 은 스케줄링 매개 변수를 I<pid>에 의해 인식되는 프로세스를 "
"위해 스케줄링 정책과 관련해서 설정한다. I<pid> 가 0이면 현제 프로세스의 매개 "
"변수들이 설정된다.  매개 변수 I<p>의 해석은 선택된 정책에 달렸다. 현재, 아래 "
"세가지 스케줄링 정책이 리눅스에서 지원된다: I<SCHED_FIFO>, I<SCHED_RR>, 와 "
"I<SCHED_OTHER.>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "B<sched_getparam> retrieves the scheduling parameters for the process "
#| "identified by I<pid>. If I<pid> is zero, then the parameters of the "
#| "current process are retrieved."
msgid ""
"B<sched_getparam>()  retrieves the scheduling parameters for the thread "
"identified by I<pid>.  If I<pid> is zero, then the parameters of the calling "
"thread are retrieved."
msgstr ""
"B<sched_getparam> 는 I<pid> 로 구분되는 프로세스를 위해 스케줄링 매개 변수를 "
"검색한다.  I<pid> 가 0이면, 현재 프로세스의 매개 변수는 검색된다."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "B<sched_setparam> checks the validity of I<p> for the scheduling policy "
#| "of the process. The parameter I<p-E<gt>sched_priority> must lie within "
#| "the range given by B<sched_get_priority_min> and "
#| "B<sched_get_priority_max>."
msgid ""
"B<sched_setparam>()  checks the validity of I<param> for the scheduling "
"policy of the thread.  The value I<param-E<gt>sched_priority> must lie "
"within the range given by B<sched_get_priority_min>(2)  and "
"B<sched_get_priority_max>(2)."
msgstr ""
"B<sched_setparam> 는 프로세스의 스케줄링 정책을 위해 I<p>의 유효성을 확인한"
"다.  매개 변수I<p-E<gt>sched_priority>는 반드시 B<sched_get_priority_min>와 "
"B<sched_get_priority_max>에 의해 주어진 범위 안에 있어야 한다."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"For a discussion of the privileges and resource limits related to scheduling "
"priority and policy, see B<sched>(7)."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "POSIX systems on which B<sched_setparam> and B<sched_getparam> are "
#| "available define I<_POSIX_PRIORITY_SCHEDULING> in E<lt>unistd.hE<gt>."
msgid ""
"POSIX systems on which B<sched_setparam>()  and B<sched_getparam>()  are "
"available define B<_POSIX_PRIORITY_SCHEDULING> in I<E<lt>unistd.hE<gt>>."
msgstr ""
"B<sched_setparam> 와 B<sched_getparam> 이 가능한 POSIX 시스템은 E<lt>unistd."
"hE<gt>안에 I<_POSIX_PRIORITY_SCHEDULING> 를 규정한다."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "반환값"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "On success, B<sched_setparam> and B<sched_getparam> return 0.  On error, "
#| "-1 is returned, I<errno> is set appropriately."
msgid ""
"On success, B<sched_setparam>()  and B<sched_getparam>()  return 0.  On "
"error, -1 is returned, and I<errno> is set to indicate the error."
msgstr ""
"성공 시, B<sched_setparam> 와 B<sched_getparam> 은 0을 반환한다..  에러 시, "
"-1이 반환되고, I<errno> 는 적절히 설정된다."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "에러"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Invalid arguments: I<param> is NULL or I<pid> is negative"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The parameter I<p> does not make sense for the current scheduling policy."
msgid ""
"(B<sched_setparam>())  The argument I<param> does not make sense for the "
"current scheduling policy."
msgstr "매개 변수 I<p>가 현제 스케줄링 정책에 맞지 않는다."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EPERM>"
msgstr "B<EPERM>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"(B<sched_setparam>())  The caller does not have appropriate privileges "
"(Linux: does not have the B<CAP_SYS_NICE> capability)."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ESRCH>"
msgstr "B<ESRCH>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid "The process whose ID is I<pid> could not be found."
msgid "The thread whose ID is I<pid> could not be found."
msgstr "ID가 I<pid>인 프로세스를 찾을 수 없다."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "표준"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr "POSIX.1-2008."

#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "이력"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "POSIX.1-2001."
msgstr "POSIX.1-2001."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "추가 참조"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"B<getpriority>(2), B<gettid>(2), B<nice>(2), B<sched_get_priority_max>(2), "
"B<sched_get_priority_min>(2), B<sched_getaffinity>(2), "
"B<sched_getscheduler>(2), B<sched_setaffinity>(2), B<sched_setattr>(2), "
"B<sched_setscheduler>(2), B<setpriority>(2), B<capabilities>(7), B<sched>(7)"
msgstr ""

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "2022-10-30"
msgstr "2022년 10월 30일"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid "POSIX.1-2001, POSIX.1-2008."
msgstr "POSIX.1-2001, POSIX.1-2008."

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.05"
msgstr "Linux man-pages 6.05"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "SCHED_SETPARAM"
msgstr "SCHED_SETPARAM"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2017-09-15"
msgstr "2017년 9월 15일"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux"
msgstr "리눅스"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "리눅스 프로그래머 매뉴얼"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid "B<int sched_setparam(pid_t >I<pid>B<, const struct sched_param *>I<param>B<);>\n"
msgstr "B<int sched_setparam(pid_t >I<pid>B<, const struct sched_param *>I<param>B<);>\n"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid "B<int sched_getparam(pid_t >I<pid>B<, struct sched_param *>I<param>B<);>\n"
msgstr "B<int sched_getparam(pid_t >I<pid>B<, struct sched_param *>I<param>B<);>\n"

#. type: Plain text
#: opensuse-leap-15-6
#, fuzzy
#| msgid ""
#| "B<sched_setparam> sets the scheduling parameters associated with the "
#| "scheduling policy for the process identified by I<pid>. If I<pid> is "
#| "zero, then the parameters of the current process are set. The "
#| "interpretation of the parameter I<p> depends on the selected policy. "
#| "Currently, the following three scheduling policies are supported under "
#| "Linux: I<SCHED_FIFO>, I<SCHED_RR>, and I<SCHED_OTHER.>"
msgid ""
"B<sched_setparam>()  sets the scheduling parameters associated with the "
"scheduling policy for the process identified by I<pid>.  If I<pid> is zero, "
"then the parameters of the calling process are set.  The interpretation of "
"the argument I<param> depends on the scheduling policy of the process "
"identified by I<pid>.  See B<sched>(7)  for a description of the scheduling "
"policies supported under Linux."
msgstr ""
"B<sched_setparam> 은 스케줄링 매개 변수를 I<pid>에 의해 인식되는 프로세스를 "
"위해 스케줄링 정책과 관련해서 설정한다. I<pid> 가 0이면 현제 프로세스의 매개 "
"변수들이 설정된다.  매개 변수 I<p>의 해석은 선택된 정책에 달렸다. 현재, 아래 "
"세가지 스케줄링 정책이 리눅스에서 지원된다: I<SCHED_FIFO>, I<SCHED_RR>, 와 "
"I<SCHED_OTHER.>"

#. type: Plain text
#: opensuse-leap-15-6
#, fuzzy
#| msgid ""
#| "B<sched_getparam> retrieves the scheduling parameters for the process "
#| "identified by I<pid>. If I<pid> is zero, then the parameters of the "
#| "current process are retrieved."
msgid ""
"B<sched_getparam>()  retrieves the scheduling parameters for the process "
"identified by I<pid>.  If I<pid> is zero, then the parameters of the calling "
"process are retrieved."
msgstr ""
"B<sched_getparam> 는 I<pid> 로 구분되는 프로세스를 위해 스케줄링 매개 변수를 "
"검색한다.  I<pid> 가 0이면, 현재 프로세스의 매개 변수는 검색된다."

#. type: Plain text
#: opensuse-leap-15-6
#, fuzzy
#| msgid ""
#| "On success, B<sched_setparam> and B<sched_getparam> return 0.  On error, "
#| "-1 is returned, I<errno> is set appropriately."
msgid ""
"On success, B<sched_setparam>()  and B<sched_getparam>()  return 0.  On "
"error, -1 is returned, and I<errno> is set appropriately."
msgstr ""
"성공 시, B<sched_setparam> 와 B<sched_getparam> 은 0을 반환한다..  에러 시, "
"-1이 반환되고, I<errno> 는 적절히 설정된다."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"(B<sched_setparam>())  The calling process does not have appropriate "
"privileges (Linux: does not have the B<CAP_SYS_NICE> capability)."
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid "The process whose ID is I<pid> could not be found."
msgstr "ID가 I<pid>인 프로세스를 찾을 수 없다."

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "CONFORMING TO"
msgstr "호환"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "NOTES"
msgstr "주의"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Scheduling parameters are in fact per-thread attributes on Linux; see "
"B<sched>(7)."
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"B<getpriority>(2), B<nice>(2), B<sched_get_priority_max>(2), "
"B<sched_get_priority_min>(2), B<sched_getaffinity>(2), "
"B<sched_getscheduler>(2), B<sched_setaffinity>(2), B<sched_setattr>(2), "
"B<sched_setscheduler>(2), B<setpriority>(2), B<capabilities>(7), B<sched>(7)"
msgstr ""
"B<getpriority>(2), B<nice>(2), B<sched_get_priority_max>(2), "
"B<sched_get_priority_min>(2), B<sched_getaffinity>(2), "
"B<sched_getscheduler>(2), B<sched_setaffinity>(2), B<sched_setattr>(2), "
"B<sched_setscheduler>(2), B<setpriority>(2), B<capabilities>(7), B<sched>(7)"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "COLOPHON"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"
