# Italian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Giovanni Bortolozzo <borto@dei.unipd.it>, 1996.
# Alessandro Rubini <rubini@linux.it>, 1998.
# Ottavio G. Rizzo <rizzo@pluto.linux.it>, 1998-1999.
# Giulio Daprelà <giulio@pluto.it>, 2015.
# Elisabetta Galli <lab@kkk.it>, 2007.
# Marco Curreli <marcocurreli@tiscali.it>, 2013-2014, 2016-2018, 2021.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.0.0\n"
"POT-Creation-Date: 2023-08-27 16:55+0200\n"
"PO-Revision-Date: 2021-12-16 23:20+0100\n"
"Last-Translator: Marco Curreli <marcocurreli@tiscali.it>\n"
"Language-Team: Italian <pluto-ildp@lists.pluto.it>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 21.12.0\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "environ"
msgstr ""

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "2023-02-05"
msgstr "5 febbraio 2023"

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Linux man-pages 6.05.01"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOME"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "environ - user environment"
msgstr "environ - ambiente dell'utente"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINTASSI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<extern char **>I<environ>B<;>\n"
msgstr "B<extern char **>I<environ>B<;>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIZIONE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The variable I<environ> points to an array of pointers to strings called the "
"\"environment\".  The last pointer in this array has the value NULL.  This "
"array of strings is made available to the process by the B<execve>(2)  call "
"when a new program is started.  When a child process is created via "
"B<fork>(2), it inherits a I<copy> of its parent's environment."
msgstr ""
"La variabile I<environ> si riferisce a un array di puntatori a stringhe "
"chiamato l'«ambiente». L'ultimo puntatore di questo array ha valore NULL. "
"Questo array di stringhe è reso disponibile al processo dalla chiamata di "
"sistema B<execve>(2) quando viene eseguito un nuovo programmaQuando viene "
"creato un processo figlio tramite B<fork>(2), esso eredita una I<copia> "
"dell'ambiente del suo genitore."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"By convention, the strings in I<environ> have the form "
"\"I<name>B<=>I<value>\".  The name is case-sensitive and may not contain the "
"character \"B<=>\".  The value can be anything that can be represented as a "
"string.  The name and the value may not contain an embedded null byte "
"(\\[aq]\\e0\\[aq]), since this is assumed to terminate the string."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Environment variables may be placed in the shell's environment by the "
"I<export> command in B<sh>(1), or by the I<setenv> command if you use "
"B<csh>(1)."
msgstr ""
"Le variabili d'ambiente possono essere messe nell'ambiente di shell dal "
"comando I<export> e da `\"nome=valore\" in B<sh>(1), o dal comando I<setenv> "
"se si usa B<csh>(1)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The initial environment of the shell is populated in various ways, such as "
"definitions from I</etc/environment> that are processed by B<pam_env>(8)  "
"for all users at login time (on systems that employ B<pam>(8)).  In "
"addition, various shell initialization scripts, such as the system-wide I</"
"etc/profile> script and per-user initializations script may include commands "
"that add variables to the shell's environment; see the manual page of your "
"preferred shell for details."
msgstr ""
"L'ambiente iniziale della shell è riempito in diversi modi, tra i quali le "
"definizioni contenute in I</etc/environment> che sono elaborate da "
"B<pam_env>(8) per tutti gi utenti al momento del login (su sistemi che "
"impiegano B<pam>(8)). In aggiunta, vari script di inizializzazione della "
"shell, come lo script a livello globale I</etc/profile> e lo script delle "
"inizializzazioni per utente possono includere comandi che aggiungolo "
"variabili all'ambiente della shell; per i dettagli vedere la pagina di "
"manuale della shell preferita."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Bourne-style shells support the syntax"
msgstr "Le shell in stile Bourne riconoscono la sintassi"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "    NAME=value command\n"
msgid "NAME=value command\n"
msgstr "    NOME=valore comando\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"to create an environment variable definition only in the scope of the "
"process that executes I<command>.  Multiple variable definitions, separated "
"by white space, may precede I<command>."
msgstr ""
"per creare una definizione di variabile d'ambiente solo nell'ambito del "
"processo che esegue I<comando>. Più definizioni di variabile, separate da "
"spazi bianchi, possono precedere I<comando>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Arguments may also be placed in the environment at the point of an "
"B<exec>(3).  A C program can manipulate its environment using the functions "
"B<getenv>(3), B<putenv>(3), B<setenv>(3), and B<unsetenv>(3)."
msgstr ""
"Argomenti posso essere messi nell'ambiente come parametro di un B<exec>(3). "
"Un programma in C può modificare il suo ambiente usando le funzioni "
"B<getenv>(3), B<putenv>(3), B<setenv>(3) e B<unsetenv>(3)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"What follows is a list of environment variables typically seen on a system.  "
"This list is incomplete and includes only common variables seen by average "
"users in their day-to-day routine.  Environment variables specific to a "
"particular program or library function are documented in the ENVIRONMENT "
"section of the appropriate manual page."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<USER>"
msgstr "B<USER>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The name of the logged-in user (used by some BSD-derived programs).  Set at "
"login time, see section NOTES below."
msgstr ""
"Il nome dell'utente collegato (usato da programmi derivati da BSD). "
"Impostato al momento del login, vedi NOTE più avanti."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<LOGNAME>"
msgstr "B<LOGNAME>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The name of the logged-in user (used by some System-V derived programs).  "
"Set at login time, see section NOTES below."
msgstr ""
"Il nome dell'utente collegato (usato da programmi derivati da System V). "
"Impostato al momento del login, vedi NOTE più avanti."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<HOME>"
msgstr "B<HOME>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "A user's login directory.  Set at login time, see section NOTES below."
msgstr ""
"La directory di login dell'utente. Impostata al momento del login, vedi NOTE "
"più avanti."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<LANG>"
msgstr "B<LANG>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The name of a locale to use for locale categories when not overridden by "
"B<LC_ALL> or more specific environment variables such as B<LC_COLLATE>, "
"B<LC_CTYPE>, B<LC_MESSAGES>, B<LC_MONETARY>, B<LC_NUMERIC>, and B<LC_TIME> "
"(see B<locale>(7)  for further details of the B<LC_*> environment variables)."
msgstr ""
"Il nome della localizzazione da usare per le categorie di localizzazione "
"quando non siano specificate altrimenti da B<LC_ALL> o da variabili "
"d'ambiente più specifiche come B<LC_COLLATE>, B<LC_CTYPE>, B<LC_MESSAGES>, "
"B<LC_MONETARY>, B<LC_NUMERIC> e B<LC_TIME> (vedi B<locale>(7) per ulteriori "
"dettagli sulle variabili d'ambiente B<LC_*>)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<PATH>"
msgstr "B<PATH>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The sequence of directory prefixes that B<sh>(1)  and many other programs "
"employ when searching for an executable file that is specified as a simple "
"filename (i.a., a pathname that contains no slashes).  The prefixes are "
"separated by colons (B<:>).  The list of prefixes is searched from beginning "
"to end, by checking the pathname formed by concatenating a prefix, a slash, "
"and the filename, until a file with execute permission is found."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"As a legacy feature, a zero-length prefix (specified as two adjacent colons, "
"or an initial or terminating colon)  is interpreted to mean the current "
"working directory.  However, use of this feature is deprecated, and POSIX "
"notes that a conforming application shall use an explicit pathname (e.g., I<."
">)  to specify the current working directory."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Analogously to B<PATH>, one has B<CDPATH> used by some shells to find the "
"target of a change directory command, B<MANPATH> used by B<man>(1)  to find "
"manual pages, and so on."
msgstr ""
"Analogamente a B<PATH>, B<CDPATH> è usato da  alcune shell per trovare la "
"destinazione di un cambio di directory, B<MANPATH> è usato da B<man>(1) per "
"trovare pagine di manuale, e così via)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<PWD>"
msgstr "B<PWD>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Absolute path to the current working directory; required to be partially "
"canonical (no I<.\\&> or I<..\\&> components)."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<SHELL>"
msgstr "B<SHELL>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The absolute pathname of the user's login shell.  Set at login time, see "
"section NOTES below."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<TERM>"
msgstr "B<TERM>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The terminal type for which output is to be prepared."
msgstr "Il tipo di terminale per il quale deve essere preparato l'output."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<PAGER>"
msgstr "B<PAGER>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The user's preferred utility to display text files.  Any string acceptable "
"as a command-string operand to the I<sh\\ -c> command shall be valid.  If "
"B<PAGER> is null or is not set, then applications that launch a pager will "
"default to a program such as B<less>(1)  or B<more>(1)."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EDITOR>/B<VISUAL>"
msgstr "B<EDITOR>/B<VISUAL>"

#.  .TP
#.  .B BROWSER
#.  The user's preferred utility to browse URLs. Sequence of colon-separated
#.  browser commands. See http://www.catb.org/\[ti]esr/BROWSER/ .
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The user's preferred utility to edit text files.  Any string acceptable as a "
"command_string operand to the I<sh\\ -c> command shall be valid."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Note that the behavior of many programs and library routines is influenced "
"by the presence or value of certain environment variables.  Examples include "
"the following:"
msgstr ""
"Si noti che il comportamento di molti programmi e routine delle librerie è "
"influenzato dalla presenza o dal valore di alcune variabili d'ambiente. Gli "
"esempi comprendono:"

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "\\[bu]"
msgstr "\\[bu]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The variables B<LANG>, B<LANGUAGE>, B<NLSPATH>, B<LOCPATH>, B<LC_ALL>, "
"B<LC_MESSAGES>, and so on influence locale handling; see B<catopen>(3), "
"B<gettext>(3), and B<locale>(7)."
msgstr ""
"Le variabili B<LANG>, B<LANGUAGE>, B<NLSPATH>, B<LOCPATH>, B<LC_ALL>, "
"B<LC_MESSAGES> eccetera, influenzano l'uso delle localizzazioni; si veda "
"B<catopen>(3), B<gettext>(3) e B<locale>(7)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<TMPDIR> influences the path prefix of names created by B<tempnam>(3)  and "
"other routines, and the temporary directory used by B<sort>(1)  and other "
"programs."
msgstr ""
"B<TMPDIR> influenza il prefisso del percorso di nomi creati da B<tempnam>(3) "
"e altre routine, e la directory temporanea usata da B<sort>(1) e da altri "
"programmi."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"B<LD_LIBRARY_PATH>, B<LD_PRELOAD>, and other B<LD_*> variables influence the "
"behavior of the dynamic loader/linker.  See also B<ld.so>(8)."
msgstr ""
"B<LD_LIBRARY_PATH>, B<LD_PRELOAD> e altre variabili B<LD_*> influenzano il "
"comportamento del loader/linker dinamico. Si veda anche B<ld.so>(8)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<POSIXLY_CORRECT> makes certain programs and library routines follow the "
"prescriptions of POSIX."
msgstr ""
"B<POSIXLY_CORRECT> fa seguire ad alcuni programmi e routine di libreria le "
"norme POSIX."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The behavior of B<malloc>(3)  is influenced by B<MALLOC_*> variables."
msgstr ""
"Il comportamento di B<malloc>(3) è influenzato dalle variabili B<MALLOC_*>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The variable B<HOSTALIASES> gives the name of a file containing aliases to "
"be used with B<gethostbyname>(3)."
msgstr ""
"La variabile B<HOSTALIASES> dà il nome di un file contenente degli alias da "
"usare con B<gethostbyname>(3)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<TZ> and B<TZDIR> give timezone information used by B<tzset>(3)  and "
"through that by functions like B<ctime>(3), B<localtime>(3), B<mktime>(3), "
"B<strftime>(3).  See also B<tzselect>(8)."
msgstr ""
"B<TZ> e B<TZDIR> dà informazioni sulla zona di fuso orario utilizzata da "
"B<tzset>(3) e, attraverso questa, da funzioni come B<ctime>(3), "
"B<localtime>(3), B<mktime>(3), B<strftime>(3). Vedere anche B<tzselect>(8)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<TERMCAP> gives information on how to address a given terminal (or gives "
"the name of a file containing such information)."
msgstr ""
"B<TERMCAP> dà informazioni su come indirizzare un dato terminale (o dà il "
"nome di un file contenente tali informazioni)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<COLUMNS> and B<LINES> tell applications about the window size, possibly "
"overriding the actual size."
msgstr ""
"B<COLUMNS> e B<LINES> informa le applicazioni sulla dimensione della "
"finestra, forse sovrascrivendo la dimensione attuale."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<PRINTER> or B<LPDEST> may specify the desired printer to use.  See "
"B<lpr>(1)."
msgstr ""
"B<PRINTER> o B<LPDEST> può specificare la stampante che si desidera usare. "
"Vedere B<lpr>(1)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "NOTE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Historically and by standard, I<environ> must be declared in the user "
"program.  However, as a (nonstandard) programmer convenience, I<environ> is "
"declared in the header file I<E<lt>unistd.hE<gt>> if the B<_GNU_SOURCE> "
"feature test macro is defined (see B<feature_test_macros>(7))."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<prctl>(2)  B<PR_SET_MM_ENV_START> and B<PR_SET_MM_ENV_END> operations "
"can be used to control the location of the process's environment."
msgstr ""
"Le operazioni B<PR_SET_MM_ENV_START> e B<PR_SET_MM_ENV_END> di B<prctl>(2) "
"possono essere usate per controllare la posizione dell'ambiente dei processi."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The B<HOME>, B<LOGNAME>, B<SHELL>, and B<USER> variables are set when the "
"user is changed via a session management interface, typically by a program "
"such as B<login>(1)  from a user database (such as B<passwd>(5)).  "
"(Switching to the root user using B<su>(1)  may result in a mixed "
"environment where B<LOGNAME> and B<USER> are retained from old user; see the "
"B<su>(1)  manual page.)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "BUG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Clearly there is a security risk here.  Many a system command has been "
"tricked into mischief by a user who specified unusual values for B<IFS> or "
"B<LD_LIBRARY_PATH>."
msgstr ""
"Qui c'è chiaramente un rischio di sicurezza. Molti comandi di sistema sono "
"stati indotti a fare danni da un utente che specificava valori insoliti per "
"B<IFS> o B<LD_LIBRARY_PATH>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"There is also the risk of name space pollution.  Programs like I<make> and "
"I<autoconf> allow overriding of default utility names from the environment "
"with similarly named variables in all caps.  Thus one uses B<CC> to select "
"the desired C compiler (and similarly B<MAKE>, B<AR>, B<AS>, B<FC>, B<LD>, "
"B<LEX>, B<RM>, B<YACC>, etc.).  However, in some traditional uses such an "
"environment variable gives options for the program instead of a pathname.  "
"Thus, one has B<MORE> and B<LESS>.  Such usage is considered mistaken, and "
"to be avoided in new programs."
msgstr ""
"C'è anche il rischio di un inquinamento dello spazio nome. Programmi come "
"I<make> e I<autoconf> permettono la sovrascrittura dei nomi delle utility di "
"default dall'ambiente con variabili dal nome simile cambiato in maiuscole/"
"minuscole. In questo modo esse usano B<CC> per selezionare il compilatore C "
"desiderato (e similarmente B<MAKE>, B<AR>, B<AS>, B<FC>, B<LD>, B<LEX>, "
"B<RM>, B<YACC>, etc.). Tuttavia in alcuni usi tradizionali tale variabile "
"d'ambiente dà opzioni al programma invece di un nome di percorso. In questo "
"modo uno ha B<MORE> e B<LESS>. Tale uso è considerato scorretto, e deve "
"essere evitato nei nuovi programmi."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VEDERE ANCHE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"B<bash>(1), B<csh>(1), B<env>(1), B<login>(1), B<printenv>(1), B<sh>(1), "
"B<su>(1), B<tcsh>(1), B<execve>(2), B<clearenv>(3), B<exec>(3), "
"B<getenv>(3), B<putenv>(3), B<setenv>(3), B<unsetenv>(3), B<locale>(7), B<ld."
"so>(8), B<pam_env>(8)"
msgstr ""
"B<bash>(1), B<csh>(1), B<env>(1), B<login>(1), B<printenv>(1), B<sh>(1), "
"B<su>(1), B<tcsh>(1), B<execve>(2), B<clearenv>(3), B<exec>(3), "
"B<getenv>(3), B<putenv>(3), B<setenv>(3), B<unsetenv>(3), B<locale>(7), B<ld."
"so>(8), B<pam_env>(8)"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.05"
msgstr "Linux man-pages 6.05"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "ENVIRON"
msgstr "ENVIRON"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2017-09-15"
msgstr "15 settembre 2017"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Manuale del programmatore di Linux"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"The variable I<environ> points to an array of pointers to strings called the "
"\"environment\".  The last pointer in this array has the value NULL.  (This "
"variable must be declared in the user program, but is declared in the header "
"file I<E<lt>unistd.hE<gt>> if the B<_GNU_SOURCE> feature test macro is "
"defined.)  This array of strings is made available to the process by the "
"B<exec>(3)  call that started the process.  When a child process is created "
"via B<fork>(2), it inherits a I<copy> of its parent's environment."
msgstr ""
"La variabile I<environ> si riferisce a un array di puntatori a stringhe "
"chiamato l'«ambiente». L'ultimo puntatore di questo array ha valore NULL. "
"(Questa variabile deve essere dichiarata nel programma utente, ma viene "
"dichiarata nel file header I<E<lt>unistd.hE<gt>> se la macro per test di "
"funzionalità B<_GNU_SOURCE> è definita). Questo array di stringhe è reso "
"disponibile al processo dalla chiamata di sistema B<exec>(3) che lo aveva "
"fatto partire. Quando viene creato un processo figlio tramite B<fork>(2), "
"esso eredita una I<copia> dell'ambiente del suo genitore."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"By convention the strings in I<environ> have the form "
"\"I<name>B<=>I<value>\".  Common examples are:"
msgstr ""
"Per convenzione le stringhe in I<environ> sono nella forma "
"\"I<nome>B<=>I<valore>\". Esempi comuni sono:"

#. type: Plain text
#: opensuse-leap-15-6
msgid "The name of the logged-in user (used by some BSD-derived programs)."
msgstr "Il nome dell'utente collegato (usato da programmi derivati da BSD)."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"The name of the logged-in user (used by some System-V derived programs)."
msgstr ""
"Il nome dell'utente collegato (usato da programmi derivati da System V)."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"A user's login directory, set by B<login>(1)  from the password file "
"B<passwd>(5)."
msgstr ""
"La directory di login dell'utente, impostata da B<login>(1) al valore "
"specificato nel file delle password B<passwd>(5)."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"The sequence of directory prefixes that B<sh>(1)  and many other programs "
"apply in searching for a file known by an incomplete pathname.  The prefixes "
"are separated by \\(aqB<:>\\(aq.  (Similarly one has B<CDPATH> used by some "
"shells to find the target of a change directory command, B<MANPATH> used by "
"B<man>(1)  to find manual pages, and so on)"
msgstr ""
"La sequenza dei prefissi di directory che B<sh>(1) e molti altri programmi "
"usano per cercare un file che ha un nome di percorso incompleto. I prefissi "
"sono separati da \\(aqB<:>\\(aq. (Allo stesso modo, B<CDPATH> è usato da "
"alcune shell per trovare la destinazione di un cambio di directory, "
"B<MANPATH> è usato da B<man>(1) per trovare pagine di manuale, e così via)."

#. type: Plain text
#: opensuse-leap-15-6
msgid "The current working directory.  Set by some shells."
msgstr "La directory di lavoro corrente. Impostato da alcune shell."

#. type: Plain text
#: opensuse-leap-15-6
msgid "The pathname of the user's login shell."
msgstr "Il nome di file della shell di login dell'utente."

#. type: Plain text
#: opensuse-leap-15-6
msgid "The user's preferred utility to display text files."
msgstr "L'utilità preferita dall'utente per visualizzare file di testo."

#.  .TP
#.  .B BROWSER
#.  The user's preferred utility to browse URLs. Sequence of colon-separated
#.  browser commands. See http://www.catb.org/~esr/BROWSER/ .
#. type: Plain text
#: opensuse-leap-15-6
msgid "The user's preferred utility to edit text files."
msgstr "L'utilità preferita dall'utente per editare file di testo."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Names may be placed in the shell's environment by the I<export> command in "
"B<sh>(1), or by the I<setenv> command if you use B<csh>(1)."
msgstr ""
"I nomi possono essere messi nell'ambiente di shell dal comando I<export> e "
"da `\"nome=valore\" in B<sh>(1), o dal comando I<setenv> se si usa B<csh>(1)."

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid "    NAME=value command\n"
msgstr "    NOME=valore comando\n"

#. type: IP
#: opensuse-leap-15-6
#, no-wrap
msgid "*"
msgstr "*"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"B<LD_LIBRARY_PATH>, B<LD_PRELOAD>, and other B<LD_*> variables influence the "
"behavior of the dynamic loader/linker."
msgstr ""
"B<LD_LIBRARY_PATH>, B<LD_PRELOAD> e altre variabili B<LD_*> influenzano il "
"comportamento del loader/linker dinamico."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"There is also the risk of name space pollution.  Programs like I<make> and "
"I<autoconf> allow overriding of default utility names from the environment "
"with similarly named variables in all caps.  Thus one uses B<CC> to select "
"the desired C compiler (and similarly B<MAKE>, B<AR>, B<AS>, B<FC>, B<LD>, "
"B<LEX>, B<RM>, B<YACC>, etc.).  However, in some traditional uses such an "
"environment variable gives options for the program instead of a pathname.  "
"Thus, one has B<MORE>, B<LESS>, and B<GZIP>.  Such usage is considered "
"mistaken, and to be avoided in new programs.  The authors of I<gzip> should "
"consider renaming their option to B<GZIP_OPT>."
msgstr ""
"C'è anche il rischio di un inquinamento dello spazio nome. Programmi come "
"I<make> e I<autoconf> permettono la sovrascrittura dei nomi delle utility di "
"default dall'ambiente con variabili dal nome simile cambiato in maiuscole/"
"minuscole. In questo modo esse usano B<CC> per selezionare il compilatore C "
"desiderato (e similarmente B<MAKE>, B<AR>, B<AS>, B<FC>, B<LD>, B<LEX>, "
"B<RM>, B<YACC>, etc.). Tuttavia in alcuni usi tradizionali tale variabile "
"d'ambiente dà opzioni al programma invece di un nome di percorso. In questo "
"modo uno ha B<MORE>, B<LESS> e B<GZIP>. Tale uso è considerato scorretto, e "
"deve essere evitato nei nuovi programmi. Gli autori di I<gzip> devono "
"pensare di rinominare la loro opzione a B<GZIP_OPT>."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"B<bash>(1), B<csh>(1), B<env>(1), B<login>(1), B<printenv>(1), B<sh>(1), "
"B<tcsh>(1), B<execve>(2), B<clearenv>(3), B<exec>(3), B<getenv>(3), "
"B<putenv>(3), B<setenv>(3), B<unsetenv>(3), B<locale>(7), B<ld.so>(8), "
"B<pam_env>(8)"
msgstr ""
"B<bash>(1), B<csh>(1), B<env>(1), B<login>(1), B<printenv>(1), B<sh>(1), "
"B<tcsh>(1), B<execve>(2), B<clearenv>(3), B<exec>(3), B<getenv>(3), "
"B<putenv>(3), B<setenv>(3), B<unsetenv>(3), B<locale>(7), B<ld.so>(8), "
"B<pam_env>(8)"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "COLOPHON"
msgstr "COLOPHON"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Questa pagina fa parte del rilascio 4.16 del progetto Linux I<man-pages>. "
"Una descrizione del progetto, le istruzioni per la segnalazione degli "
"errori, e l'ultima versione di questa pagina si trovano su \\%https://www."
"kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"
