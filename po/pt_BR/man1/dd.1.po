# Brazilian Portuguese translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# André Luiz Fassone <lonely_wolf@ig.com.br>, 2000.
# Ricardo C.O.Freitas <english.quest@best-service.com>, 2000.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-10-02 11:55+0200\n"
"PO-Revision-Date: 2022-02-12 05:54+0100\n"
"Last-Translator: Ricardo C.O.Freitas <english.quest@best-service.com>\n"
"Language-Team: Brazilian Portuguese <debian-l10n-portuguese@lists.debian."
"org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 20.04.1\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DD"
msgstr "DD"

#. type: TH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "September 2023"
msgstr "Setembro de 2023"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "GNU coreutils 9.4"
msgstr "GNU coreutils 9.4"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr "Comandos de usuário"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOME"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "dd - convert and copy a file"
msgstr "dd - converte e copia um arquivo"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<dd> [I<\\,OPERAND\\/>]..."
msgstr "B<dd> [I<\\,OPERANDO\\/>]..."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<dd> I<\\,OPTION\\/>"
msgstr "B<dd> I<\\,OPÇÃO\\/>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIÇÃO"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Copy a file, converting and formatting according to the operands."
msgstr "Copia um arquivo, convertendo-o e formatando-o conforme os operandos."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "bs=BYTES"
msgstr "bs=BYTES"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"read and write up to BYTES bytes at a time (default: 512); overrides ibs and "
"obs"
msgstr ""
"Lê e escreve até BYTES bytes de uma vez (padrão: 512); sobrescreve ibs e obs."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "cbs=BYTES"
msgstr "cbs=BYTES"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "convert BYTES bytes at a time"
msgstr "Converte BYTES bytes de uma vez."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "conv=CONVS"
msgstr "conv=CONVS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "convert the file as per the comma separated symbol list"
msgstr "Converte o arquivo conforme lista de símbolos separados por vírgula."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "count=N"
msgstr "count=N"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "copy only N input blocks"
msgstr "Copia somente N blocos de entrada."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ibs=BYTES"
msgstr "ibs=BYTES"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "read up to BYTES bytes at a time (default: 512)"
msgstr "Lê até BYTES bytes de uma vez (padrão: 512)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "if=FILE"
msgstr "if=ARQUIVO"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "read from FILE instead of stdin"
msgstr "Lê do ARQUIVO em vez de usar a entrada padrão."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "iflag=FLAGS"
msgstr "iflag=SINALIZ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "read as per the comma separated symbol list"
msgstr "Lê conforme lista de símbolos separados por vírgula."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "obs=BYTES"
msgstr "obs=BYTES"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "write BYTES bytes at a time (default: 512)"
msgstr "Escreve a quantidade de I<BYTES> de uma vez. O padrão é 512."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "of=FILE"
msgstr "of=ARQUIVO"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "write to FILE instead of stdout"
msgstr "Escreve em ARQUIVO em vez de usar a saída padrão."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "oflag=FLAGS"
msgstr "oflag=SINALIZ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "write as per the comma separated symbol list"
msgstr "Escreve conforme uma lista de símbolos separados por vírgula."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "seek=N"
msgstr "seek=N"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "(or oseek=N) skip N obs-sized output blocks"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "skip=N"
msgstr "skip=N"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "(or iseek=N) skip N ibs-sized input blocks"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "status=LEVEL"
msgstr "status=NÍVEL"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The LEVEL of information to print to stderr; \\&'none' suppresses everything "
"but error messages, \\&'noxfer' suppresses the final transfer statistics, "
"\\&'progress' shows periodic transfer statistics"
msgstr ""
"O nível de informação para enviar para erro padrão; \\&\"none\" suprime "
"tudo, exceto mensagens de erro, \\&\"noxfer\" suprime as estatísticas finais "
"da transferência, \\&\"progress\" mostra estatísticas de tempo em tempo."

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, fuzzy
msgid ""
"N and BYTES may be followed by the following multiplicative suffixes: c=1, "
"w=2, b=512, kB=1000, K=1024, MB=1000*1000, M=1024*1024, xM=M, "
"GB=1000*1000*1000, G=1024*1024*1024, and so on for T, P, E, Z, Y, R, Q.  "
"Binary prefixes can be used, too: KiB=K, MiB=M, and so on.  If N ends in "
"'B', it counts bytes not blocks."
msgstr ""
"N e BYTES podem ser seguidos pelos seguintes sufixos multiplicativos: c=1, "
"w=2, b=512, kB=1000, K=1024, MB=1000*1000, M=1024*1024, xM=M, "
"GB=1000*1000*1000, G=1024*1024*1024, e assim por diante para T, P, E, Z e Y. "
"Prefixos binários também podem ser usados: KiB=K, MiB=M e assim por diante."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Each CONV symbol may be:"
msgstr "Cada símbolo CONV pode ser:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ascii"
msgstr "ascii"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "from EBCDIC to ASCII"
msgstr "de EBCDIC para ASCII"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ebcdic"
msgstr "ebcdic"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "from ASCII to EBCDIC"
msgstr "de ASCII para EBCDIC"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ibm"
msgstr "ibm"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "from ASCII to alternate EBCDIC"
msgstr "de ASCII para EBCDIC alternativo"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "block"
msgstr "block"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "pad newline-terminated records with spaces to cbs-size"
msgstr ""
"preenche registros terminados por caractere de nova linha com espaços até o "
"tamanho ditado por cbs"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "unblock"
msgstr "unblock"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "replace trailing spaces in cbs-size records with newline"
msgstr ""
"substitui os espaços ao final dos registros de tamanho cbs por caractere de "
"nova linha"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "lcase"
msgstr "lcase"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "change upper case to lower case"
msgstr "altera as maiúsculas para minúsculas"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ucase"
msgstr "ucase"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "change lower case to upper case"
msgstr "altera as minúsculas para maiúsculas"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "sparse"
msgstr "sparse"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "try to seek rather than write all-NUL output blocks"
msgstr "tenta buscar em vez de escrever blocos de saída todos NULOS"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "swab"
msgstr "swab"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "swap every pair of input bytes"
msgstr "troca a cada par de bytes de entrada"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "sync"
msgstr "sync"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"pad every input block with NULs to ibs-size; when used with block or "
"unblock, pad with spaces rather than NULs"
msgstr ""
"preenche cada bloco de entrada com NULOs até o tamanho ibs; se usado com "
"block ou unblock, preenche com espaços em vez de NULOs"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "excl"
msgstr "excl"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "fail if the output file already exists"
msgstr "falha se o arquivo de saída já existir"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "nocreat"
msgstr "nocreat"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "do not create the output file"
msgstr "não cria o arquivo de saída"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "notrunc"
msgstr "notrunc"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "do not truncate the output file"
msgstr "não trunca o arquivo de saída"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "noerror"
msgstr "noerror"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "continue after read errors"
msgstr "continua mesmo após erros de leitura"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "fdatasync"
msgstr "fdatasync"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "physically write output file data before finishing"
msgstr "escreve fisicamente os dados do arquivo de saída antes de concluir"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "fsync"
msgstr "fsync"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "likewise, but also write metadata"
msgstr "similar, mas também escreve metadados"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Each FLAG symbol may be:"
msgstr "Cada SINALIZador pode ser:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "append"
msgstr "append"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "append mode (makes sense only for output; conv=notrunc suggested)"
msgstr "modo anexar (só faz sentido para saída; sugere-se conv=notrunc)"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "direct"
msgstr "direct"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "use direct I/O for data"
msgstr "usa E/S direta para dados"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "directory"
msgstr "diretório"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "fail unless a directory"
msgstr "falha a menos que seja um diretório"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "dsync"
msgstr "dsync"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "use synchronized I/O for data"
msgstr "usa E/S sincronizada para dados"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "likewise, but also for metadata"
msgstr "similar, mas também para metadados"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "fullblock"
msgstr "fullblock"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "accumulate full blocks of input (iflag only)"
msgstr "acumula blocos de entrada completos (iflag apenas)"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "nonblock"
msgstr "nonblock"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "use non-blocking I/O"
msgstr "usa E/S não-bloqueante"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "noatime"
msgstr "noatime"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "do not update access time"
msgstr "não atualiza o horário de acesso"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "nocache"
msgstr "nocache"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Request to drop cache.  See also oflag=sync"
msgstr "requisita descartar o cache; veja também oflag=sync"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "noctty"
msgstr "noctty"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "do not assign controlling terminal from file"
msgstr "não designa o terminal de controle do arquivo"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "nofollow"
msgstr "nofollow"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "do not follow symlinks"
msgstr "não segue os links simbólicos"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Sending a USR1 signal to a running 'dd' process makes it print I/O "
"statistics to standard error and then resume copying."
msgstr ""
"O envio de um sinal USR1 para um processo \"dd\" em execução faz com que ele "
"mostre as estatísticas de E/S para o erro padrão e, então, continue a cópia."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Options are:"
msgstr "As opções são:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "display this help and exit"
msgstr "mostra esta ajuda e sai"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--version>"
msgstr "B<--version>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "output version information and exit"
msgstr "informa a versão e sai"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Written by Paul Rubin, David MacKenzie, and Stuart Kemp."
msgstr "Escrito por Paul Rubin, David MacKenzie e Stuart Kemp."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "RELATANDO PROBLEMAS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"GNU coreutils online help: E<lt>https://www.gnu.org/software/coreutils/E<gt>"
msgstr ""
"Página de ajuda do GNU coreutils: E<lt>https://www.gnu.org/software/"
"coreutils/E<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Report any translation bugs to E<lt>https://translationproject.org/team/E<gt>"
msgstr ""
"Relate erros de tradução para E<lt>https://translationproject.org/team/pt_BR."
"htmlE<gt>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "COPYRIGHT"
msgstr "DIREITOS AUTORAIS"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid ""
"Copyright \\(co 2023 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2023 Free Software Foundation, Inc. Licença GPLv3+: GNU GPL "
"versão 3 ou posterior E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""
"Este é um software livre: você é livre para alterá-lo e redistribuí-lo. NÃO "
"HÁ QUALQUER GARANTIA, na máxima extensão permitida em lei."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VEJA TAMBÉM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Full documentation E<lt>https://www.gnu.org/software/coreutils/ddE<gt>"
msgstr ""
"Documentação completa em E<lt>https://www.gnu.org/software/coreutils/ddE<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "or available locally via: info \\(aq(coreutils) dd invocation\\(aq"
msgstr "ou disponível localmente via: info \\(aq(coreutils) dd invocation\\(aq"

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "September 2022"
msgstr "Setembro de 2022"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "GNU coreutils 9.1"
msgstr "GNU coreutils 9.1"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron
#, fuzzy
msgid ""
"N and BYTES may be followed by the following multiplicative suffixes: c=1, "
"w=2, b=512, kB=1000, K=1024, MB=1000*1000, M=1024*1024, xM=M, "
"GB=1000*1000*1000, G=1024*1024*1024, and so on for T, P, E, Z, Y.  Binary "
"prefixes can be used, too: KiB=K, MiB=M, and so on.  If N ends in 'B', it "
"counts bytes not blocks."
msgstr ""
"N e BYTES podem ser seguidos pelos seguintes sufixos multiplicativos: c=1, "
"w=2, b=512, kB=1000, K=1024, MB=1000*1000, M=1024*1024, xM=M, "
"GB=1000*1000*1000, G=1024*1024*1024, e assim por diante para T, P, E, Z e Y. "
"Prefixos binários também podem ser usados: KiB=K, MiB=M e assim por diante."

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron
msgid ""
"Copyright \\(co 2022 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2022 Free Software Foundation, Inc. Licença GPLv3+: GNU GPL "
"versão 3 ou posterior E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: TH
#: fedora-39
#, no-wrap
msgid "GNU coreutils 9.3"
msgstr "GNU coreutils 9.3"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "April 2022"
msgstr "Abril de 2022"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "October 2021"
msgstr "Outubro de 2021"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "GNU coreutils 8.32"
msgstr "GNU coreutils 8.32"

#. type: Plain text
#: opensuse-leap-15-6
msgid "skip N obs-sized blocks at start of output"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid "skip N ibs-sized blocks at start of input"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"N and BYTES may be followed by the following multiplicative suffixes: c=1, "
"w=2, b=512, kB=1000, K=1024, MB=1000*1000, M=1024*1024, xM=M, "
"GB=1000*1000*1000, G=1024*1024*1024, and so on for T, P, E, Z, Y.  Binary "
"prefixes can be used, too: KiB=K, MiB=M, and so on."
msgstr ""
"N e BYTES podem ser seguidos pelos seguintes sufixos multiplicativos: c=1, "
"w=2, b=512, kB=1000, K=1024, MB=1000*1000, M=1024*1024, xM=M, "
"GB=1000*1000*1000, G=1024*1024*1024, e assim por diante para T, P, E, Z e Y. "
"Prefixos binários também podem ser usados: KiB=K, MiB=M e assim por diante."

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "count_bytes"
msgstr "count_bytes"

#. type: Plain text
#: opensuse-leap-15-6
msgid "treat 'count=N' as a byte count (iflag only)"
msgstr ""

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "skip_bytes"
msgstr "skip_bytes"

#. type: Plain text
#: opensuse-leap-15-6
msgid "treat 'skip=N' as a byte count (iflag only)"
msgstr ""

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "seek_bytes"
msgstr "seek_bytes"

#. type: Plain text
#: opensuse-leap-15-6
msgid "treat 'seek=N' as a byte count (oflag only)"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Copyright \\(co 2020 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2020 Free Software Foundation, Inc. Licença GPLv3+: GNU GPL "
"versão 3 ou posterior E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
