# Brazilian Portuguese translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# André Luiz Fassone <lonely_wolf@ig.com.br>, 2000.
# Carlos Augusto Horylka <horylka@conectiva.com.br>, 2000.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-08-27 17:32+0200\n"
"PO-Revision-Date: 2000-06-02 01:52-0300\n"
"Last-Translator: Carlos Augusto Horylka <horylka@conectiva.com.br>\n"
"Language-Team: Brazilian Portuguese <debian-l10n-portuguese@lists.debian."
"org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Virtaal 1.0.0-beta1\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "wavelan"
msgstr "wavelan"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "2023-02-05"
msgstr "5 fevereiro 2023"

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Linux man-pages 6.05.01"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOME"

#
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "wavelan - AT&T GIS WaveLAN ISA device driver"
msgstr "wavelan - Controlador de dispositivo AT&T GIS WaveLAN ISA"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<insmod wavelan_cs.o [io=>I<B,B..>B<] [ irq=>I<I,I..>B<] [name=>I<N,N..>B<]>\n"
msgstr "B<insmod wavelan_cs.o [io=>I<B,B..>B<] [ irq=>I<I,I..>B<] [name=>I<N,N..>B<]>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIÇÃO"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "I<This driver is obsolete:> it was removed in Linux 2.6.35."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"B<wavelan> is the low-level device driver for the NCR / AT&T / Lucent "
"B<WaveLAN ISA> and Digital (DEC)  B<RoamAbout DS> wireless ethernet "
"adapter.  This driver is available as a module or might be compiled in the "
"kernel.  This driver supports multiple cards in both forms (up to 4) and "
"allocates the next available ethernet device (eth0..eth#) for each card "
"found, unless a device name is explicitly specified (see below).  This "
"device name will be reported in the kernel log file with the MAC address, "
"NWID, and frequency used by the card."
msgstr ""
"B<wavelan> é o controlador de dispositivo, de baixo nível, para adaptadores "
"de rede sem-fio ethernet B<WaveLAN ISA> da NCR, AT&T, Lucent e Digital (DEC) "
"B<RoamAbout DS>. Este controlador está disponível como módulo ou compilado "
"internamente ao kernel. Este controlador suporta multiplos cartões em "
"qualquer das formas (até 4) e alocando o próximo dispositivo ethernet "
"disponível (eth0..eth#) para cada cartão encontrado, a menos que o nome do "
"dispositivo seja explicitamente especificado (veja abaixo). Este nome de "
"dispositivo será relatado no arquivo de registro no kernel com o endereço "
"MAC, NWID e freqüencia usado pelo cartão."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Parameters"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"This section applies to the module form (parameters passed on the "
"B<insmod>(8)  command line).  If the driver is included in the kernel, use "
"the I<ether=IRQ,IO,NAME> syntax on the kernel command line."
msgstr ""
"Esta seção é aplicável para a forma de módulos (sendo os parâmetros passados "
"com o comando de linha I<insmod>(8)). Se o driver é incluído no kernel, use "
"a sintaxe I<ether=IRQ,IO,NAME> na linha da comando do kernel."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<io>"
msgstr "B<io>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Specify the list of base addresses where to search for wavelan cards "
"(setting by dip switch on the card).  If you don't specify any io address, "
"the driver will scan 0x390 and 0x3E0 addresses, which might conflict with "
"other hardware..."
msgstr ""
"Especifica a lista do endereços de base aonde procurar por cartão wavelan "
"(selecionando pelo dip switch no cartão). Se você não especificar um "
"endereço de E/S, o controlador ira procurar nos endereços 0x390 e 0x3E0, o "
"qual pode conflitar com outro hardware..."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<irq>"
msgstr "B<irq>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Set the list of IRQs that each wavelan card should use (the value is saved "
"in permanent storage for future use)."
msgstr ""
"Seleciona a lista de IRQ que cada um dos cartões usa (o valor é guardado em "
"meio permanente para uso futuro)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<name>"
msgstr "B<nome>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Set the list of names to be used for each wavelan card device (name used by "
"B<ifconfig>(8))."
msgstr ""
"Seleciona a lista de nomes para usar em cada dispositivo de cartão wavelan "
"(é o nome usado por I<ifconfig>(8))."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Wireless extensions"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Use B<iwconfig>(8)  to manipulate wireless extensions."
msgstr "Usa-se o I<iwconfig>(8) para manipular as extensões da rede sem fio."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NWID (or domain)"
msgstr "NWID (ou domínio)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Set the network ID [I<0> to I<FFFF>] or disable it [I<off>].  As the NWID is "
"stored in the card Permanent Storage Area, it will be reused at any further "
"invocation of the driver."
msgstr ""
"Seleciona o ID da rede [I<0> para I<FFFF>] ou desabilita [I<off>]. Como o "
"NWID é guardado na área permanente de armazenamento do cartão, ele será "
"usado novamente em qualquer invocação futura do controlador."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Frequency & channels"
msgstr "Freqüencia & canais"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "For the 2.4GHz 2.00 Hardware, you are able to set the frequency by "
#| "specifying one of the 10 defined channels (I<2.412,> I<2.422, 2.425, "
#| "2.4305, 2.432, 2.442, 2.452, 2.460, 2.462> or I<2.484>)  or directly by "
#| "its value. The frequency is changed immediately and permanentely. "
#| "Frequency availability depend on the regulations..."
msgid ""
"For the 2.4\\ GHz 2.00 Hardware, you are able to set the frequency by "
"specifying one of the 10 defined channels (I<2.412,> I<2.422, 2.425, 2.4305, "
"2.432, 2.442, 2.452, 2.460, 2.462> or I<2.484>)  or directly as a numeric "
"value.  The frequency is changed immediately and permanently.  Frequency "
"availability depends on the regulations..."
msgstr ""
"Para o cartão de 2.4GHz 2.00, você é capaz de selecionar a freqëncia pela "
"especificação de um dos 10 canais definidos (I<2.412,> I<2.422, 2.425, "
"2.4305, 2.432, 2.442, 2.452, 2.460, 2.462> ou I<2.484>) ou diretamente para "
"estes valores. A freqüencia é alterada imediata e permanentemente. A "
"disponibilicade de freqüencia depende de regulamentações..."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Statistics spy"
msgstr "Enxergando estatísticas"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Set a list of MAC addresses in the driver (up to 8) and get the last quality "
"of link for each of those (see B<iwspy>(8))."
msgstr ""
"Selecione a lista de endereços MAC em cada controlador (até 8) e obtenha a "
"qualidade do link para cada uma (veja I<iwspy>(8))."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "/proc/net/wireless"
msgstr "/proc/net/wireless"

#
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"I<status> is the status reported by the modem.  I<Link quality> reports the "
"quality of the modulation on the air (direct sequence spread spectrum) [max "
"= 16].  I<Level> and I<Noise> refer to the signal level and noise level [max "
"= 64].  The I<crypt discarded packet> and I<misc discarded packet> counters "
"are not implemented."
msgstr ""
"I<status> este é o status relatado pelo modem. I<Link quality> relata a "
"qualidade da modulação no ar (espectro direto de seqüencia dilatada) [max = "
"16]. I<Level> e I<Noise> reference ao nível de sinal [max = 64]. Os "
"contadores I<crypt discarded packet> e I<misc discarded packet> ainda não "
"estão implementados."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Private ioctl"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "You may use B<iwpriv>(8)  to manipulate private ioctls."
msgstr "Você deve usar I<iwpriv>(8) para manipular os ioctls privados."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Quality and level threshold"
msgstr "Qualidade e nível de threshold"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Enables you to define the quality and level threshold used by the modem "
"(packet below that level are discarded)."
msgstr ""
"Permite que você defina a qualidade e nível de threshold usado pelo modem "
"(pacotes abaixo de nível são descartados)."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Histogram"
msgstr "Histograma"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This functionality makes it possible to set a number of signal level "
"intervals and to count the number of packets received in each of those "
"defined intervals.  This distribution might be used to calculate the mean "
"value and standard deviation of the signal level."
msgstr ""
"Esta funcionalidade permite selecionar um número de intervalo de nivel do "
"sinal e conta o número de pacotes recebidos em cada um dos intervalos "
"definidos. Esta distribuição pode ser usada par calcular o valor médio e "
"desvio padrão do nível de sinal."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Specific notes"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "This driver will fail to detect some B<non NCR/ATT&T/Lucent> Wavelan "
#| "cards. If it's your case, you must look in the source code on how to add "
#| "your card to the detection routine."
msgid ""
"This driver fails to detect some B<non-NCR/AT&T/Lucent> Wavelan cards.  If "
"this happens for you, you must look in the source code on how to add your "
"card to the detection routine."
msgstr ""
"Este controlador falha ao detectar alguns cartões wavelan que não sejam "
"B<NCR/ATT&T/Lucent>. Se este é o seu caso, você deve olhar no código fonte "
"sobre como fazer para adicionar seu cartão na rotina de detecção."

#.  .SH AUTHOR
#.  Bruce Janson \[em] bruce@cs.usyd.edu.au
#.  .br
#.  Jean Tourrilhes \[em] jt@hplb.hpl.hp.com
#.  .br
#.  (and others; see source code for details)
#.  SEE ALSO part
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Some of the mentioned features are optional.  You may enable or disable them "
"by changing flags in the driver header and recompile."
msgstr ""
"Algumas das características mencionadas são opcionais. Você pode ativar ou "
"desativar pela alteração do sinalizador em cada cabeçalho do controlador e "
"recompilar."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VEJA TAMBÉM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<wavelan_cs>(4), B<ifconfig>(8), B<insmod>(8), B<iwconfig>(8), "
"B<iwpriv>(8), B<iwspy>(8)"
msgstr ""
"B<wavelan_cs>(4), B<ifconfig>(8), B<insmod>(8), B<iwconfig>(8), "
"B<iwpriv>(8), B<iwspy>(8)"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.05"
msgstr "Linux man-pages 6.05"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "WAVELAN"
msgstr "WAVELAN"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2017-09-15"
msgstr "15 setembro 2017"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Manual do Programador do Linux"

#
#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"B<insmod wavelan_cs.o [io=>I<B,B..>B<] [ irq=>I<I,I..>B<] [name=>I<N,N..>B<]>"
msgstr ""
"B<insmod wavelan_cs.o [io=>I<B,B..>B<] [ irq=>I<I,I..>B<] [name=>I<N,N..>B<]>"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"I<This driver is obsolete:> it was removed from the kernel in version 2.6.35."
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"I<wavelan> is the low-level device driver for the NCR / AT&T / Lucent "
"B<WaveLAN ISA> and Digital (DEC)  B<RoamAbout DS> wireless ethernet "
"adapter.  This driver is available as a module or might be compiled in the "
"kernel.  This driver supports multiple cards in both forms (up to 4) and "
"allocates the next available ethernet device (eth0..eth#) for each card "
"found, unless a device name is explicitly specified (see below).  This "
"device name will be reported in the kernel log file with the MAC address, "
"NWID and frequency used by the card."
msgstr ""
"I<wavelan> é o controlador de dispositivo, de baixo nível, para adaptadores "
"de rede sem-fio ethernet B<WaveLAN ISA> da NCR, AT&T, Lucent e Digital (DEC) "
"B<RoamAbout DS>. Este controlador está disponível como módulo ou compilado "
"internamente ao kernel. Este controlador suporta multiplos cartões em "
"qualquer das formas (até 4) e alocando o próximo dispositivo ethernet "
"disponível (eth0..eth#) para cada cartão encontrado, a menos que o nome do "
"dispositivo seja explicitamente especificado (veja abaixo). Este nome de "
"dispositivo será relatado no arquivo de registro no kernel com o endereço "
"MAC, NWID e freqüencia usado pelo cartão."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This section apply to the module form (parameters passed on the "
"B<insmod>(8)  command line).  If the driver is included in the kernel, use "
"the I<ether=IRQ,IO,NAME> syntax on the kernel command line."
msgstr ""
"Esta seção é aplicável para a forma de módulos (sendo os parâmetros passados "
"com o comando de linha I<insmod>(8)). Se o driver é incluído no kernel, use "
"a sintaxe I<ether=IRQ,IO,NAME> na linha da comando do kernel."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Specify the list of base address where to search for wavelan cards (setting "
"by dip switch on the card).  If you don't specify any io address, the driver "
"will scan 0x390 and 0x3E0 addresses, which might conflict with other "
"hardware..."
msgstr ""
"Especifica a lista do endereços de base aonde procurar por cartão wavelan "
"(selecionando pelo dip switch no cartão). Se você não especificar um "
"endereço de E/S, o controlador ira procurar nos endereços 0x390 e 0x3E0, o "
"qual pode conflitar com outro hardware..."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Set the list of irq that each wavelan card should use (the value is saved in "
"permanent storage for future use)."
msgstr ""
"Seleciona a lista de irq que cada um dos cartões usa (o valor é guardado em "
"meio permanente para uso futuro)."

#
#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Set the list of name to be used for each wavelan cards device (name used by "
"B<ifconfig>(8))."
msgstr ""
"Seleciona a lista de nomes para usar em cada dispositivo de cartão wavelan "
"(é o nome usado por I<ifconfig>(8))."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Set the network ID [I<0> to I<FFFF>] or disable it [I<off>].  As the NWID is "
"stored in the card Permanent Storage Area, it will be reuse at any further "
"invocation of the driver."
msgstr ""
"Seleciona o ID da rede [I<0> para I<FFFF>] ou desabilita [I<off>]. Como o "
"NWID é guardado na área permanente de armazenamento do cartão, ele será "
"usado novamente em qualquer invocação futura do controlador."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"For the 2.4\\ GHz 2.00 Hardware, you are able to set the frequency by "
"specifying one of the 10 defined channels (I<2.412,> I<2.422, 2.425, 2.4305, "
"2.432, 2.442, 2.452, 2.460, 2.462> or I<2.484>)  or directly by its value.  "
"The frequency is changed immediately and permanently.  Frequency "
"availability depends on the regulations..."
msgstr ""
"Para o cartão de 2.4\\ GHz 2.00, você é capaz de selecionar a freqëncia pela "
"especificação de um dos 10 canais definidos (I<2.412,> I<2.422, 2.425, "
"2.4305, 2.432, 2.442, 2.452, 2.460, 2.462> ou I<2.484>) ou diretamente para "
"estes valores. A freqüencia é alterada imediata e permanentemente. A "
"disponibilicade de freqüencia depende de regulamentações..."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Enable you the define the quality and level threshold used by the modem "
"(packet below that level are discarded)."
msgstr ""
"Permite que você defina a qualidade e nível de threshold usado pelo modem "
"(pacotes abaixo de nível são descartados)."

#.  .SH AUTHOR
#.  Bruce Janson \(em bruce@cs.usyd.edu.au
#.  .br
#.  Jean Tourrilhes \(em jt@hplb.hpl.hp.com
#.  .br
#.  (and others; see source code for details)
#.  SEE ALSO part
#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Some of the mentioned features are optional.  You may enable to disable them "
"by changing flags in the driver header and recompile."
msgstr ""
"Algumas das características mencionadas são opcionais. Você pode ativar ou "
"desativar pela alteração do sinalizador em cada cabeçalho do controlador e "
"recompilar."

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "COLOPHON"
msgstr "COLOFÃO"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Esta página faz parte da versão 4.16 do projeto Linux I<man-pages>. Uma "
"descrição do projeto, informações sobre relatórios de bugs e a versão mais "
"recente desta página podem ser encontradas em \\%https://www.kernel.org/doc/"
"man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"
