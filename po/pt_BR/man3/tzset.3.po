# Brazilian Portuguese translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Ricardo C.O.Freitas <english.quest@best-service.com>, 2001.
# André Luiz Fassone <lonely_wolf@ig.com.br>, 2001.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-08-27 17:30+0200\n"
"PO-Revision-Date: 2001-05-31 17:26+0200\n"
"Last-Translator: André Luiz Fassone <lonely_wolf@ig.com.br>\n"
"Language-Team: Brazilian Portuguese <debian-l10n-portuguese@lists.debian."
"org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 20.04.1\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<tzset>()"
msgid "tzset"
msgstr "B<tzset>()"

#. type: TH
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid "2023-07-20"
msgstr "20 julho 2023"

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Linux man-pages 6.05.01"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOME"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"tzset, tzname, timezone, daylight - initialize time conversion information"
msgstr ""
"tzset, tzname, timezone, daylight - inicializa informação para conversão do "
"horário"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTECA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Biblioteca C Padrão (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>time.hE<gt>>\n"
msgstr "B<#include E<lt>time.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<void tzset (void);>\n"
msgid "B<void tzset(void);>\n"
msgstr "B<void tzset (void);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<extern char *>I<tzname>B<[2];>\n"
"B<extern long >I<timezone>B<;>\n"
"B<extern int >I<daylight>B<;>\n"
msgstr ""
"B<extern char *>I<tzname>B<[2];>\n"
"B<extern long >I<timezone>B<;>\n"
"B<extern int >I<daylight>B<;>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Requisitos de macro de teste de recursos para o glibc (consulte "
"B<feature_test_macros>(7)):"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid "B<tzset>()"
msgid "B<tzset>():"
msgstr "B<tzset>()"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "    _POSIX_C_SOURCE\n"
msgstr "    _POSIX_C_SOURCE\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid "I<name>"
msgid "I<tzname>:"
msgstr "I<nome>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "I<timezone>, I<daylight>:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"    _XOPEN_SOURCE\n"
"        || /* glibc E<gt>= 2.19: */ _DEFAULT_SOURCE\n"
"        || /* glibc E<lt>= 2.19: */ _SVID_SOURCE\n"
msgstr ""
"    _XOPEN_SOURCE\n"
"        || /* glibc E<gt>= 2.19: */ _DEFAULT_SOURCE\n"
"        || /* glibc E<lt>= 2.19: */ _SVID_SOURCE\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIÇÃO"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The B<tzset>()  function initializes the I<tzname> variable from the B<TZ> "
"environment variable.  This function is automatically called by the other "
"time conversion functions that depend on the timezone.  In a System-V-like "
"environment, it will also set the variables I<timezone> (seconds West of "
"UTC) and I<daylight> (to 0 if this timezone does not have any daylight "
"saving time rules, or to nonzero if there is a time, past, present, or "
"future when daylight saving time applies)."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "If the TZ variable does not appear in the environment, the I<tzname> "
#| "variable is initialized with the best approximation of local wall clock "
#| "time, as specified by the B<tzfile>(5)-format file I<localtime> found in "
#| "the system timezone directory (see below).  (One also often sees I</etc/"
#| "localtime> used here, a symlink to the right file in the system timezone "
#| "directory.)"
msgid ""
"If the B<TZ> variable does not appear in the environment, the system "
"timezone is used.  The system timezone is configured by copying, or linking, "
"a file in the B<tzfile>(5)  format to I</etc/localtime>.  A timezone "
"database of these files may be located in the system timezone directory (see "
"the B<FILES> section below)."
msgstr ""
"Se a variável TZ não aparece no ambiente, a variável I<tzname> é "
"inicializada com a melhor aproximação ao horário do relógio de parede, como "
"especificado pelo B<tzfile>(5)-formato de arquivo I<localtime> encontrado no "
"sistema no diretório de fuso horário (veja abaixo). (Pode-se também ver I</"
"etc/localtime> utilizado aqui, uma ligação simbólica para o arquivo correto "
"no diretório de fuso horário do sistema.)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "If the TZ variable does appear in the environment but its value is NULL "
#| "or its value cannot be interpreted using any of the formats specified "
#| "below, Coordinated Universal Time (UTC) is used."
msgid ""
"If the B<TZ> variable does appear in the environment, but its value is "
"empty, or its value cannot be interpreted using any of the formats specified "
"below, then Coordinated Universal Time (UTC) is used."
msgstr ""
"Se a variável TZ não aparece no ambiente mas seu valor é NULO ou seu valor "
"pode ser interpretado utilizando qualquer dos formatos especificados abaixo, "
"UCT (Tempo Coordenado Universal) é utilizado."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The value of TZ can be one of three formats.  The first format is used "
#| "when there is no daylight saving time in the local time zone:"
msgid ""
"The value of B<TZ> can be one of two formats.  The first format is a string "
"of characters that directly represent the timezone to be used:"
msgstr ""
"O valor de TZ pode ser um dos seguintes três formatos. O primeiro formato é "
"utilizado quando não há horário de verão no fuso horário local:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "I<std offset dst [offset],start[/time],end[/time]>"
msgid "I<std offset>[I<dst>[I<offset>][,I<start>[I</time>],I<end>[I</time>]]]\n"
msgstr "I<std offset dst [offset],start[/time],end[/time]>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The I<std> string specifies the name of the time zone and must be three "
#| "or more alphabetic characters.  The I<offset> string immediately follows "
#| "I<std> and specifies the time value to be added to the local time to get "
#| "Coordinated Universal Time (UTC).  The I<offset> is positive if the local "
#| "time zone is west of the Prime Meridian and negative if it is east.  The "
#| "hour must be between 0 and 24, and the minutes and seconds 0 and 59."
msgid ""
"There are no spaces in the specification.  The I<std> string specifies an "
"abbreviation for the timezone and must be three or more alphabetic "
"characters.  When enclosed between the less-than (E<lt>) and greater-than "
"(E<gt>) signs, the character set is expanded to include the plus (+) sign, "
"the minus (-)  sign, and digits.  The I<offset> string immediately follows "
"I<std> and specifies the time value to be added to the local time to get "
"Coordinated Universal Time (UTC).  The I<offset> is positive if the local "
"timezone is west of the Prime Meridian and negative if it is east.  The hour "
"must be between 0 and 24, and the minutes and seconds 00 and 59:"
msgstr ""
"A seqüencia I<std> especifica o nome de uma região de fuso horário e tem que "
"ter três ou mais caracteres alfabéticos. A seqüencia I<offset> segue "
"imediatamente I<std> e especifica o valor do tempo à ser adicionado ao "
"horário local para obter o Tempo Universal Coordenado (UTC). O I<offset> é "
"positivo se a região de fuso for a oeste do Meridiano Principal e negativo "
"se ela for à leste. A hora deve estar entre 0 e 24, e minutos e segundos de "
"0 e 59."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "[I<+>|I<->]I<hh>[I<:mm>[I<:ss>]]\n"
msgstr "[I<+>|I<->]I<hh>[I<:mm>[I<:ss>]]\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The I<dst> string and I<offset> specify the name and offset for the "
"corresponding daylight saving timezone.  If the offset is omitted, it "
"defaults to one hour ahead of standard time."
msgstr ""
"A 'string' I<dst> e I<offset> especificam o nome e desvio para a "
"correspondente zona de horário de verão. Se o desvio for omitido, o padrão é "
"definido em uma hora além do horário padrão."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The I<start> field specifies when daylight saving time goes into effect and "
"the I<end> field specifies when the change is made back to standard time.  "
"These fields may have the following formats:"
msgstr ""
"O campo I<start> especifica quando o horário de verão entra em função e o "
"campo I<end> especifica quando o mudança é retornada ao horário padrão. Este "
"campos podem ter os seguintes formatos:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "JI<n>"
msgstr "JI<n>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "This specifies the Julian day with I<n> between 1 and 365.  February 29 "
#| "is never counted even in leap years."
msgid ""
"This specifies the Julian day with I<n> between 1 and 365.  Leap days are "
"not counted.  In this format, February 29 can't be represented; February 28 "
"is day 59, and March 1 is always day 60."
msgstr ""
"Este especifica o dia Juliano com I<n> entre 1 e 365. Fevereiro 29 nunca é "
"contado, mesmo em ano bisexto."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "I<n>"
msgstr "I<n>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "This specifies the Julian day with I<n> between 1 and 365.  February 29 "
#| "is counted in leap years."
msgid ""
"This specifies the zero-based Julian day with I<n> between 0 and 365.  "
"February 29 is counted in leap years."
msgstr ""
"Este especifica o dia Juliano com I<n> entre 1 e 365. Fevereiro 29 é contado "
"em anos bisextos."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "MI<m>.I<w>.I<d>"
msgstr "MI<m>.I<w>.I<d>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This specifies day I<d> (0 E<lt>= I<d> E<lt>= 6) of week I<w> (1 E<lt>= I<w> "
"E<lt>= 5) of month I<m> (1 E<lt>= I<m> E<lt>= 12).  Week 1 is the first week "
"in which day I<d> occurs and week 5 is the last week in which day I<d> "
"occurs.  Day 0 is a Sunday."
msgstr ""
"Este especifica dia I<d> (0 E<lt>= I<d> E<lt>= 6) da semana I<w> (1 E<lt>= "
"I<w> E<lt>= 5) do mês I<m> (1 E<lt>= I<m> E<lt>= 12). Semana 1 é a primeira "
"semana no qual o dia I<d> ocorre e semana 5 é a última semana na qual o dia "
"I<d> ocorre. Dia 0 é um Domingo."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The I<time> fields specify when, in the local time currently in effect, the "
"change to the other time occurs.  If omitted, the default is 02:00:00."
msgstr ""
"O campo I<time> especifica quando, no horário local atualmente em função, a "
"mudança para o outro horário ocorre. Se omitido, o padrão é 02:00:00."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Here is an example for New Zealand, where the standard time (NZST) is 12 "
"hours ahead of UTC, and daylight saving time (NZDT), 13 hours ahead of UTC, "
"runs from the first Sunday in October to the third Sunday in March, and the "
"changeovers happen at the default time of 02:00:00:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "TZ=\"NZST-12:00:00NZDT-13:00:00,M10.1.0,M3.3.0\"\n"
msgstr "TZ=\"NZST-12:00:00NZDT-13:00:00,M10.1.0,M3.3.0\"\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The third format specifies that the time zone information should be read "
#| "from a file:"
msgid ""
"The second format specifies that the timezone information should be read "
"from a file:"
msgstr ""
"O terceiro formato especifica que a informação do fuso horário deveria ser "
"lida de um arquivo:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ":[filespec]\n"
msgstr ":[filespec]\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "If the file specification I<filespec> is omitted, the time zone "
#| "information is read from the file I<localtime> in the system timezone "
#| "directory, which nowadays usually is I</usr/share/zoneinfo>.  This file "
#| "is in B<tzfile>(5)  format.  If I<filespec> is given, it specifies "
#| "another B<tzfile>(5)-format file to read the time zone information from.  "
#| "If I<filespec> does not begin with a `/', the file specification is "
#| "relative to the system timezone directory."
msgid ""
"If the file specification I<filespec> is omitted, or its value cannot be "
"interpreted, then Coordinated Universal Time (UTC) is used.  If I<filespec> "
"is given, it specifies another B<tzfile>(5)-format file to read the timezone "
"information from.  If I<filespec> does not begin with a \\[aq]/\\[aq], the "
"file specification is relative to the system timezone directory.  If the "
"colon is omitted each of the above B<TZ> formats will be tried."
msgstr ""
"Se a especificação de arquivo I<filespec> for omitida, a informação do fuso "
"horário é lida do arquivo I<localtime> no diretório de sitema de fuso "
"horário, o qual é atualmente I</usr/share/zoneinfo>. O arquivo está no "
"formato B<tzfile>(5). Se I<filespec> for dado, ele especifica um outro "
"B<tzfile>(5) - formato de arquivo para ler a informação de fuso horários. Se "
"I<filespec> não começa com um `/', o arquivo de especificação é relativo ao "
"diretório do sistema de fuso horário."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Here's an example, once more for New Zealand:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "TZ=\":Pacific/Auckland\"\n"
msgstr "TZ=\":Pacific/Auckland\"\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ENVIRONMENT"
msgstr "AMBIENTE"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<TZ>"
msgstr "B<TZ>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If this variable is set its value takes precedence over the system "
"configured timezone."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<TZDIR>"
msgstr "B<TZDIR>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If this variable is set its value takes precedence over the system "
"configured timezone database directory path."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "FILES"
msgstr "ARQUIVOS"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "I</etc/localtime>"
msgstr "I</etc/localtime>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The system timezone file."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "I</usr/share/zoneinfo/>"
msgstr "I</usr/share/zoneinfo/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The system timezone database directory."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "I</usr/share/zoneinfo/posixrules>"
msgstr "I</usr/share/zoneinfo/posixrules>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"When a TZ string includes a dst timezone without anything following it, then "
"this file is used for the start/end rules.  It is in the B<tzfile>(5)  "
"format.  By default, the zoneinfo Makefile hard links it to the I<America/"
"New_York> tzfile."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Above are the current standard file locations, but they are configurable "
"when glibc is compiled."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATRIBUTOS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Para uma explicação dos termos usados nesta seção, consulte B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Interface"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Atributo"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Valor"

#. type: tbl table
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<tzset>()"
msgstr "B<tzset>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Thread safety"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe env locale"
msgstr "MT-Safe env locale"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "PADRÕES"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr "POSIX.1-2008."

#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "HISTÓRICO"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "POSIX.1-2001, SVr4, 4.3BSD."
msgstr "POSIX.1-2001, SVr4, 4.3BSD."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"4.3BSD had a function B<char *timezone(>I<zone>B<, >I<dst>B<)> that returned "
"the name of the timezone corresponding to its first argument (minutes West "
"of UTC).  If the second argument was 0, the standard name was used, "
"otherwise the daylight saving time version."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VEJA TAMBÉM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<date>(1), B<gettimeofday>(2), B<time>(2), B<ctime>(3), B<getenv>(3), "
"B<tzfile>(5)"
msgstr ""
"B<date>(1), B<gettimeofday>(2), B<time>(2), B<ctime>(3), B<getenv>(3), "
"B<tzfile>(5)"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "2023-02-05"
msgstr "5 fevereiro 2023"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid "POSIX.1-2001, POSIX.1-2008, SVr4, 4.3BSD."
msgstr "POSIX.1-2001, POSIX.1-2008, SVr4, 4.3BSD."

#. type: SH
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "NOTES"
msgstr "NOTAS"

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.05"
msgstr "Linux man-pages 6.05"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "TZSET"
msgstr "TZSET"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2017-09-15"
msgstr "15 setembro 2017"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Manual do Programador do Linux"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid "B<void tzset (void);>\n"
msgstr "B<void tzset (void);>\n"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<tzset>(): _POSIX_C_SOURCE"
msgstr "B<tzset>(): _POSIX_C_SOURCE"

#. type: Plain text
#: opensuse-leap-15-6
msgid "I<tzname>: _POSIX_C_SOURCE"
msgstr "I<tzname>: _POSIX_C_SOURCE"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"I<timezone>,\n"
"I<daylight>:\n"
"_XOPEN_SOURCE\n"
"    || /* Glibc since 2.19: */ _DEFAULT_SOURCE\n"
"    || /* Glibc versions E<lt>= 2.19: */ _SVID_SOURCE\n"
msgstr ""
"I<timezone>,\n"
"I<daylight>:\n"
"_XOPEN_SOURCE\n"
"    || /* Desde o glibc 2.19: */ _DEFAULT_SOURCE\n"
"    || /* Glibc versões E<lt>= 2.19: */ _SVID_SOURCE\n"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"The B<tzset>()  function initializes the I<tzname> variable from the B<TZ> "
"environment variable.  This function is automatically called by the other "
"time conversion functions that depend on the timezone.  In a System-V-like "
"environment, it will also set the variables I<timezone> (seconds West of "
"UTC) and I<daylight> (to 0 if this timezone does not have any daylight "
"saving time rules, or to nonzero if there is a time, past, present or future "
"when daylight saving time applies)."
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
#, fuzzy
#| msgid ""
#| "If the TZ variable does not appear in the environment, the I<tzname> "
#| "variable is initialized with the best approximation of local wall clock "
#| "time, as specified by the B<tzfile>(5)-format file I<localtime> found in "
#| "the system timezone directory (see below).  (One also often sees I</etc/"
#| "localtime> used here, a symlink to the right file in the system timezone "
#| "directory.)"
msgid ""
"If the B<TZ> variable does not appear in the environment, the system "
"timezone is used.  The system timezone is configured by copying, or linking, "
"a file in the B<tzfile>(5) format to I</etc/localtime>.  A timezone database "
"of these files may be located in the system timezone directory (see the "
"B<FILES> section below)."
msgstr ""
"Se a variável TZ não aparece no ambiente, a variável I<tzname> é "
"inicializada com a melhor aproximação ao horário do relógio de parede, como "
"especificado pelo B<tzfile>(5)-formato de arquivo I<localtime> encontrado no "
"sistema no diretório de fuso horário (veja abaixo). (Pode-se também ver I</"
"etc/localtime> utilizado aqui, uma ligação simbólica para o arquivo correto "
"no diretório de fuso horário do sistema.)"

#. type: Plain text
#: opensuse-leap-15-6
#, fuzzy
#| msgid ""
#| "The I<std> string specifies the name of the time zone and must be three "
#| "or more alphabetic characters.  The I<offset> string immediately follows "
#| "I<std> and specifies the time value to be added to the local time to get "
#| "Coordinated Universal Time (UTC).  The I<offset> is positive if the local "
#| "time zone is west of the Prime Meridian and negative if it is east.  The "
#| "hour must be between 0 and 24, and the minutes and seconds 0 and 59."
msgid ""
"There are no spaces in the specification.  The I<std> string specifies an "
"abbreviation for the timezone and must be three or more alphabetic "
"characters.  When enclosed between the less-than (E<lt>) and greater-than "
"(E<gt>) signs, the characters set is expanded to include the plus (+) sign, "
"the minus (-)  sign, and digits.  The I<offset> string immediately follows "
"I<std> and specifies the time value to be added to the local time to get "
"Coordinated Universal Time (UTC).  The I<offset> is positive if the local "
"timezone is west of the Prime Meridian and negative if it is east.  The hour "
"must be between 0 and 24, and the minutes and seconds 00 and 59:"
msgstr ""
"A seqüencia I<std> especifica o nome de uma região de fuso horário e tem que "
"ter três ou mais caracteres alfabéticos. A seqüencia I<offset> segue "
"imediatamente I<std> e especifica o valor do tempo à ser adicionado ao "
"horário local para obter o Tempo Universal Coordenado (UTC). O I<offset> é "
"positivo se a região de fuso for a oeste do Meridiano Principal e negativo "
"se ela for à leste. A hora deve estar entre 0 e 24, e minutos e segundos de "
"0 e 59."

#. type: Plain text
#: opensuse-leap-15-6
#, fuzzy
#| msgid ""
#| "If the file specification I<filespec> is omitted, the time zone "
#| "information is read from the file I<localtime> in the system timezone "
#| "directory, which nowadays usually is I</usr/share/zoneinfo>.  This file "
#| "is in B<tzfile>(5)  format.  If I<filespec> is given, it specifies "
#| "another B<tzfile>(5)-format file to read the time zone information from.  "
#| "If I<filespec> does not begin with a `/', the file specification is "
#| "relative to the system timezone directory."
msgid ""
"If the file specification I<filespec> is omitted, or its value cannot be "
"interpreted, then Coordinated Universal Time (UTC) is used.  If I<filespec> "
"is given, it specifies another B<tzfile>(5)-format file to read the timezone "
"information from.  If I<filespec> does not begin with a \\(aq/\\(aq, the "
"file specification is relative to the system timezone directory.  If the "
"colon is omitted each of the above B<TZ> formats will be tried."
msgstr ""
"Se a especificação de arquivo I<filespec> for omitida, a informação do fuso "
"horário é lida do arquivo I<localtime> no diretório de sitema de fuso "
"horário, o qual é atualmente I</usr/share/zoneinfo>. O arquivo está no "
"formato B<tzfile>(5). Se I<filespec> for dado, ele especifica um outro "
"B<tzfile>(5) - formato de arquivo para ler a informação de fuso horários. Se "
"I<filespec> não começa com um `/', o arquivo de especificação é relativo ao "
"diretório do sistema de fuso horário."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"When a TZ string includes a dst timezone without anything following it, then "
"this file is used for the start/end rules.  It is in the B<tzfile>(5) "
"format.  By default, the zoneinfo Makefile hard links it to the I<America/"
"New_York> tzfile."
msgstr ""

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "CONFORMING TO"
msgstr "DE ACORDO COM"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "COLOPHON"
msgstr "COLOFÃO"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Esta página faz parte da versão 4.16 do projeto Linux I<man-pages>. Uma "
"descrição do projeto, informações sobre relatórios de bugs e a versão mais "
"recente desta página podem ser encontradas em \\%https://www.kernel.org/doc/"
"man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30 março 2023"

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"
