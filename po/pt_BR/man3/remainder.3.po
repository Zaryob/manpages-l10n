# Brazilian Portuguese translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Marcelo D. Beckmann <marcelobeckmann@yahoo.com>, 2000.
# André Luiz Fassone <lonely_wolf@ig.com.br>, 2000.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-08-27 17:16+0200\n"
"PO-Revision-Date: 2000-06-02 19:20-0300\n"
"Last-Translator: André Luiz Fassone <lonely_wolf@ig.com.br>\n"
"Language-Team: Brazilian Portuguese <debian-l10n-portuguese@lists.debian."
"org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Virtaal 1.0.0-beta1\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<remainder>():"
msgid "remainder"
msgstr "B<remainder>():"

#. type: TH
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid "2023-07-20"
msgstr "20 julho 2023"

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Linux man-pages 6.05.01"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOME"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"drem, dremf, dreml, remainder, remainderf, remainderl - floating-point "
"remainder function"
msgstr ""
"drem, dremf, dreml, remainder, remainderf, remainderl - funçao resto de "
"ponto flutuante"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTECA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Math library (I<libm>, I<-lm>)"
msgstr "Biblioteca matemática (I<libm>, I<-lm>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>math.hE<gt>>\n"
msgstr "B<#include E<lt>math.hE<gt>>\n"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid ""
"B<double remainder(double >I<x>B<, double >I<y>B<);>\n"
"B<float remainderf(float >I<x>B<, float >I<y>B<);>\n"
"B<long double remainderl(long double >I<x>B<, long double >I<y>B<);>\n"
msgstr ""

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid ""
"/* Obsolete synonyms */\n"
"B<[[deprecated]] double drem(double >I<x>B<, double >I<y>B<);>\n"
"B<[[deprecated]] float dremf(float >I<x>B<, float >I<y>B<);>\n"
"B<[[deprecated]] long double dreml(long double >I<x>B<, long double >I<y>B<);>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Requisitos de macro de teste de recursos para o glibc (consulte "
"B<feature_test_macros>(7)):"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<remainder>():"
msgstr "B<remainder>():"

#.     || _XOPEN_SOURCE && _XOPEN_SOURCE_EXTENDED
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"    _ISOC99_SOURCE || _POSIX_C_SOURCE E<gt>= 200112L\n"
"        || _XOPEN_SOURCE E<gt>= 500\n"
"        || /* Since glibc 2.19: */ _DEFAULT_SOURCE\n"
"        || /* glibc E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"
msgstr ""
"    _ISOC99_SOURCE || _POSIX_C_SOURCE E<gt>= 200112L\n"
"        || _XOPEN_SOURCE E<gt>= 500\n"
"        || /* Desde o glibc 2.19: */ _DEFAULT_SOURCE\n"
"        || /* glibc E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<remainderf>(), B<remainderl>():"
msgstr "B<remainderf>(), B<remainderl>():"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"    _ISOC99_SOURCE || _POSIX_C_SOURCE E<gt>= 200112L\n"
"        || /* Since glibc 2.19: */ _DEFAULT_SOURCE\n"
"        || /* glibc E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"
msgstr ""
"    _ISOC99_SOURCE || _POSIX_C_SOURCE E<gt>= 200112L\n"
"        || /* Desde o glibc 2.19: */ _DEFAULT_SOURCE\n"
"        || /* glibc E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<drem>(), B<dremf>(), B<dreml>():"
msgstr "B<drem>(), B<dremf>(), B<dreml>():"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"    /* Since glibc 2.19: */ _DEFAULT_SOURCE\n"
"        || /* glibc E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"
msgstr ""
"    /* Desde o glibc 2.19: */ _DEFAULT_SOURCE\n"
"        || /* glibc E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIÇÃO"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The B<drem()> function computes the remainder of dividing I<x> by I<y>.  "
#| "The return value is I<x> - I<n> * I<y>, where I<n> is the quotient of "
#| "I<x> / I<y>, rounded to the nearest integer.  If the quotient is 1/2, it "
#| "is rounded to the even number."
msgid ""
"These functions compute the remainder of dividing I<x> by I<y>.  The return "
"value is I<x>-I<n>*I<y>, where I<n> is the value I<x\\ /\\ y>, rounded to "
"the nearest integer.  If the absolute value of I<x>-I<n>*I<y> is 0.5, I<n> "
"is chosen to be even."
msgstr ""
"A função B<drem()> calcula o resto da divisão de I<x> por I<y>. O valor de "
"retorno é I<x> - I<n> * I<y>, onde I<n> é o quociente de I<x> / I<y>, "
"arredondado para o inteiro mais próximo. Se o quociente é 1/2, é arredondado "
"para o mesmo número."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"These functions are unaffected by the current rounding mode (see B<fenv>(3))."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The B<drem>()  function does precisely the same thing."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALOR DE RETORNO"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"On success, these functions return the floating-point remainder, I<x>-"
"I<n>*I<y>.  If the return value is 0, it has the sign of I<x>."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid "If I<x> is a NaN, a NaN is returned."
msgid "If I<x> or I<y> is a NaN, a NaN is returned."
msgstr "Se I<x> é um NaN, um NaN é retornado."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "If I<x> is positive infinity or negative infinity, a domain error occurs, "
#| "and a NaN is returned."
msgid ""
"If I<x> is an infinity, and I<y> is not a NaN, a domain error occurs, and a "
"NaN is returned."
msgstr ""
"Se I<x> é infinito positivo ou infinito negativo, ocorre um erro de domínio "
"e um NaN é retornado."

#
#.  FIXME . Instead, glibc gives a domain error even if x is a NaN
#.  Interestingly, remquo(3) does not have the same problem.
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "If I<x> is outside the range [-1,\\ 1], a domain error occurs, and a NaN "
#| "is returned."
msgid ""
"If I<y> is zero, and I<x> is not a NaN, a domain error occurs, and a NaN is "
"returned."
msgstr ""
"Se I<x> está fora da faixa [-1,\\ 1], ocorre um erro de domínio e um NaN é "
"retornado."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ERROS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"See B<math_error>(7)  for information on how to determine whether an error "
"has occurred when calling these functions."
msgstr ""
"Consulte B<math_error>(7) para obter informações sobre como determinar se "
"ocorreu um erro ao chamar essas funções."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The following errors can occur:"
msgstr "Os seguintes erros podem ocorrer:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "Domain error: I<x> is an infinity"
msgid "Domain error: I<x> is an infinity and I<y> is not a NaN"
msgstr "Domain error: I<x> is an infinity"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"I<errno> is set to B<EDOM> (but see BUGS).  An invalid floating-point "
"exception (B<FE_INVALID>)  is raised."
msgstr ""
"I<errno> é definido com B<EDOM> (mas veja BUGS). Uma exceção de ponto "
"flutuante inválido (B<FE_INVALID>) é levantada."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "These functions do not set I<errno> for this case."
msgstr ""

#.  [XXX see bug above] and \fIx\fP is not a NaN
#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "Domain error: I<x> is an infinity"
msgid "Domain error: I<y> is zero"
msgstr "Domain error: I<x> is an infinity"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"I<errno> is set to B<EDOM>.  An invalid floating-point exception "
"(B<FE_INVALID>)  is raised."
msgstr ""
"I<errno> é definido com B<EDOM>. Uma exceção de ponto flutuante inválida "
"(B<FE_INVALID>) é levantada."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATRIBUTOS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Para uma explicação dos termos usados nesta seção, consulte B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Interface"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Atributo"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Valor"

#. type: tbl table
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"B<drem>(),\n"
"B<dremf>(),\n"
"B<dreml>(),\n"
"B<remainder>(),\n"
"B<remainderf>(),\n"
"B<remainderl>()"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Thread safety"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "PADRÕES"

#. type: TP
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<remainder>():"
msgid "B<remainder>()"
msgstr "B<remainder>():"

#. type: TQ
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<remainder>():"
msgid "B<remainderf>()"
msgstr "B<remainder>():"

#. #-#-#-#-#  archlinux: remainder.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  fedora-39: remainder.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  fedora-rawhide: remainder.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  opensuse-leap-15-6: remainder.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: tbl table
#. #-#-#-#-#  opensuse-tumbleweed: remainder.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#: archlinux fedora-39 fedora-rawhide opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<remainderl>()"
msgstr "B<remainderl>()"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "C11, POSIX.1-2008."
msgstr "C11, POSIX.1-2008."

#. type: TP
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "B<drem>()"
msgstr ""

#. type: TP
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "B<dremf>()"
msgstr ""

#. type: TQ
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<remainderl>()"
msgid "B<dreml>()"
msgstr "B<remainderl>()"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "None."
msgstr ""

#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "HISTÓRICO"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "C99, POSIX.1-2001."
msgstr "C99, POSIX.1-2001."

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "4.3BSD."
msgstr "4.3BSD."

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "Tru64, glibc2."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "BUGS"

#. #-#-#-#-#  archlinux: remainder.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  https://www.sourceware.org/bugzilla/show_bug.cgi?id=6779
#. type: Plain text
#. #-#-#-#-#  debian-bookworm: remainder.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  http://sources.redhat.com/bugzilla/show_bug.cgi?id=6779
#. type: Plain text
#. #-#-#-#-#  debian-unstable: remainder.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  http://sources.redhat.com/bugzilla/show_bug.cgi?id=6779
#. type: Plain text
#. #-#-#-#-#  fedora-39: remainder.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  https://www.sourceware.org/bugzilla/show_bug.cgi?id=6779
#. type: Plain text
#. #-#-#-#-#  fedora-rawhide: remainder.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  https://www.sourceware.org/bugzilla/show_bug.cgi?id=6779
#. type: Plain text
#. #-#-#-#-#  mageia-cauldron: remainder.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  http://sources.redhat.com/bugzilla/show_bug.cgi?id=6779
#. type: Plain text
#. #-#-#-#-#  opensuse-leap-15-6: remainder.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  http://sources.redhat.com/bugzilla/show_bug.cgi?id=6779
#. type: Plain text
#. #-#-#-#-#  opensuse-tumbleweed: remainder.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  https://www.sourceware.org/bugzilla/show_bug.cgi?id=6779
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid "Before glibc 2.10:"
msgid "Before glibc 2.15, the call"
msgstr "Antes do glibc 2.10:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "    remainder(nan(\"\"), 0);\n"
msgid "remainder(nan(\"\"), 0);\n"
msgstr "    remainder(nan(\"\"), 0);\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"returned a NaN, as expected, but wrongly caused a domain error.  Since glibc "
"2.15, a silent NaN (i.e., no domain error) is returned."
msgstr ""

#. #-#-#-#-#  archlinux: remainder.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  https://www.sourceware.org/bugzilla/show_bug.cgi?id=6783
#. type: Plain text
#. #-#-#-#-#  debian-bookworm: remainder.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  http://sources.redhat.com/bugzilla/show_bug.cgi?id=6783
#. type: Plain text
#. #-#-#-#-#  debian-unstable: remainder.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  http://sources.redhat.com/bugzilla/show_bug.cgi?id=6783
#. type: Plain text
#. #-#-#-#-#  fedora-39: remainder.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  https://www.sourceware.org/bugzilla/show_bug.cgi?id=6783
#. type: Plain text
#. #-#-#-#-#  fedora-rawhide: remainder.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  https://www.sourceware.org/bugzilla/show_bug.cgi?id=6783
#. type: Plain text
#. #-#-#-#-#  mageia-cauldron: remainder.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  http://sources.redhat.com/bugzilla/show_bug.cgi?id=6783
#. type: Plain text
#. #-#-#-#-#  opensuse-tumbleweed: remainder.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  https://www.sourceware.org/bugzilla/show_bug.cgi?id=6783
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Before glibc 2.15, I<errno> was not set to B<EDOM> for the domain error that "
"occurs when I<x> is an infinity and I<y> is not a NaN."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "EXEMPLOS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The call \"remainder(29.0, 3.0)\" returns -1."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VEJA TAMBÉM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<div>(3), B<fmod>(3), B<remquo>(3)"
msgstr "B<div>(3), B<fmod>(3), B<remquo>(3)"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "2023-02-05"
msgstr "5 fevereiro 2023"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid ""
"/* The C99 versions */\n"
"B<double remainder(double >I<x>B<, double >I<y>B<);>\n"
"B<float remainderf(float >I<x>B<, float >I<y>B<);>\n"
"B<long double remainderl(long double >I<x>B<, long double >I<y>B<);>\n"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid ""
"/* Obsolete synonyms */\n"
"B<double drem(double >I<x>B<, double >I<y>B<);>\n"
"B<float dremf(float >I<x>B<, float >I<y>B<);>\n"
"B<long double dreml(long double >I<x>B<, long double >I<y>B<);>\n"
msgstr ""

#.  IEC 60559.
#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid ""
"The functions B<remainder>(), B<remainderf>(), and B<remainderl>()  are "
"specified in C99, POSIX.1-2001, and POSIX.1-2008."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid ""
"The function B<drem>()  is from 4.3BSD.  The I<float> and I<long double> "
"variants B<dremf>()  and B<dreml>()  exist on some systems, such as Tru64 "
"and glibc2.  Avoid the use of these functions in favor of B<remainder>()  "
"etc."
msgstr ""

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.05"
msgstr "Linux man-pages 6.05"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "REMAINDER"
msgstr "REMAINDER"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2017-09-15"
msgstr "15 setembro 2017"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Manual do Programador do Linux"

#. type: Plain text
#: opensuse-leap-15-6
msgid "Link with I<-lm>."
msgstr "Vincule com I<-lm>."

#.     || _XOPEN_SOURCE\ &&\ _XOPEN_SOURCE_EXTENDED
#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"_ISOC99_SOURCE || _POSIX_C_SOURCE\\ E<gt>=\\ 200112L\n"
"    || _XOPEN_SOURCE\\ E<gt>=\\ 500\n"
"    || /* Since glibc 2.19: */ _DEFAULT_SOURCE\n"
"    || /* Glibc versions E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"
msgstr ""
"_ISOC99_SOURCE || _POSIX_C_SOURCE\\ E<gt>=\\ 200112L\n"
"    || _XOPEN_SOURCE\\ E<gt>=\\ 500\n"
"    || /* Desde o glibc 2.19: */ _DEFAULT_SOURCE\n"
"    || /* Glibc versões E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"_ISOC99_SOURCE || _POSIX_C_SOURCE\\ E<gt>=\\ 200112L\n"
"    || /* Since glibc 2.19: */ _DEFAULT_SOURCE\n"
"    || /* Glibc versions E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"
msgstr ""
"_ISOC99_SOURCE || _POSIX_C_SOURCE\\ E<gt>=\\ 200112L\n"
"    || /* Desde o glibc 2.19: */ _DEFAULT_SOURCE\n"
"    || /* Glibc versões E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"

#. type: Plain text
#: opensuse-leap-15-6
#, fuzzy, no-wrap
#| msgid ""
#| "_ISOC99_SOURCE || _POSIX_C_SOURCE\\ E<gt>=\\ 200112L\n"
#| "    || /* Since glibc 2.19: */ _DEFAULT_SOURCE\n"
#| "    || /* Glibc versions E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"
msgid ""
"/* Since glibc 2.19: */ _DEFAULT_SOURCE\n"
"    || /* Glibc versions E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"
msgstr ""
"_ISOC99_SOURCE || _POSIX_C_SOURCE\\ E<gt>=\\ 200112L\n"
"    || /* Desde o glibc 2.19: */ _DEFAULT_SOURCE\n"
"    || /* Glibc versões E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"

#. type: tbl table
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"B<drem>(),\n"
"B<dremf>(),\n"
"B<dreml>(),\n"
msgstr ""
"B<drem>(),\n"
"B<dremf>(),\n"
"B<dreml>(),\n"

#. type: tbl table
#: opensuse-leap-15-6
#, no-wrap
msgid ".br\n"
msgstr ".br\n"

#. type: tbl table
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"B<remainder>(),\n"
"B<remainderf>(),\n"
msgstr ""
"B<remainder>(),\n"
"B<remainderf>(),\n"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "CONFORMING TO"
msgstr "DE ACORDO COM"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid "    remainder(nan(\"\"), 0);\n"
msgstr "    remainder(nan(\"\"), 0);\n"

#.  http://sources.redhat.com/bugzilla/show_bug.cgi?id=6783
#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Before glibc 2.15, I<errno> was not set to B<EDOM> for the domain error that "
"occurs when I<x> is an infinity and I<y> is not a NaN.  I<errno was not set>"
msgstr ""

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "EXAMPLE"
msgstr "EXEMPLO"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "COLOPHON"
msgstr "COLOFÃO"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Esta página faz parte da versão 4.16 do projeto Linux I<man-pages>. Uma "
"descrição do projeto, informações sobre relatórios de bugs e a versão mais "
"recente desta página podem ser encontradas em \\%https://www.kernel.org/doc/"
"man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30 março 2023"

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"
