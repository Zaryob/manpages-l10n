# Brazilian Portuguese translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# E. A. Tacão <tacao@conectiva.com.br>, 2001.
# André Luiz Fassone <lonely_wolf@ig.com.br>, 2001.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-08-27 16:59+0200\n"
"PO-Revision-Date: 2001-05-31 17:26+0200\n"
"Last-Translator: André Luiz Fassone <lonely_wolf@ig.com.br>\n"
"Language-Team: Brazilian Portuguese <debian-l10n-portuguese@lists.debian."
"org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 20.04.1\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<getpwnam>()"
msgid "getpwnam"
msgstr "B<getpwnam>()"

#. type: TH
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid "2023-07-20"
msgstr "20 julho 2023"

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Linux man-pages 6.05.01"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOME"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "getpwnam, getpwnam_r, getpwuid, getpwuid_r - get password file entry"
msgstr ""
"getpwnam, getpwnam_r, getpwuid, getpwuid_r - obtêm entrada do arquivo de "
"senhas"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTECA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Biblioteca C Padrão (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<#include E<lt>sys/types.hE<gt>>\n"
"B<#include E<lt>pwd.hE<gt>>\n"
msgstr ""
"B<#include E<lt>sys/types.hE<gt>>\n"
"B<#include E<lt>pwd.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<struct passwd *getpwnam(const char *>I<name>B<);>\n"
msgid ""
"B<struct passwd *getpwnam(const char *>I<name>B<);>\n"
"B<struct passwd *getpwuid(uid_t >I<uid>B<);>\n"
msgstr "B<struct passwd *getpwnam(const char *>I<name>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"B<int getpwnam_r(const char *restrict >I<name>B<, struct passwd *restrict >I<pwd>B<,>\n"
"B<               char >I<buf>B<[restrict .>I<buflen>B<], size_t >I<buflen>B<,>\n"
"B<               struct passwd **restrict >I<result>B<);>\n"
"B<int getpwuid_r(uid_t >I<uid>B<, struct passwd *restrict >I<pwd>B<,>\n"
"B<               char >I<buf>B<[restrict .>I<buflen>B<], size_t >I<buflen>B<,>\n"
"B<               struct passwd **restrict >I<result>B<);>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Requisitos de macro de teste de recursos para o glibc (consulte "
"B<feature_test_macros>(7)):"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<getpwnam_r>(), B<getpwuid_r>():"
msgstr "B<getpwnam_r>(), B<getpwuid_r>():"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"    _POSIX_C_SOURCE\n"
"        || /* glibc E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"
msgstr ""
"    _POSIX_C_SOURCE\n"
"        || /* glibc E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIÇÃO"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The B<getpwnam()> function returns a pointer to a structure containing "
#| "the broken out fields of a line from I</etc/passwd> for the entry that "
#| "matches the user name I<name>."
msgid ""
"The B<getpwnam>()  function returns a pointer to a structure containing the "
"broken-out fields of the record in the password database (e.g., the local "
"password file I</etc/passwd>, NIS, and LDAP)  that matches the username "
"I<name>."
msgstr ""
"A função B<getpwnam()> retorna um ponteiro a uma estrutura contendo os "
"campos de uma linha quebrados do I</etc/passwd> para a entrada que "
"corresponde ao nome de usuário I<nome>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The B<getpwuid()> function returns a pointer to a structure containing "
#| "the broken out fields of a line from I</etc/passwd> for the entry that "
#| "matches the user uid I<uid>."
msgid ""
"The B<getpwuid>()  function returns a pointer to a structure containing the "
"broken-out fields of the record in the password database that matches the "
"user ID I<uid>."
msgstr ""
"A função B<getpwuid()> retorna um ponteiro a uma estrutura contendo os "
"campos de uma linha quebrados do I</etc/passwd> para a entrada que "
"corresponde à uid do usuário I<uid>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The I<passwd> structure is defined in I<E<lt>pwd.hE<gt>> as follows:"
msgstr "A estrutura I<passwd> é definida em I<E<lt>pwd.hE<gt>> como segue:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "struct passwd {\n"
#| "        char    *pw_name;\t\t/* user name */\n"
#| "        char    *pw_passwd;\t\t/* user password */\n"
#| "        uid_t   pw_uid;\t\t\t/* user id */\n"
#| "        gid_t   pw_gid;\t\t\t/* group id */\n"
#| "        char    *pw_gecos;      \t/* real name */\n"
#| "        char    *pw_dir;  \t\t/* home directory */\n"
#| "        char    *pw_shell;      \t/* shell program */\n"
#| "};\n"
msgid ""
"struct passwd {\n"
"    char   *pw_name;       /* username */\n"
"    char   *pw_passwd;     /* user password */\n"
"    uid_t   pw_uid;        /* user ID */\n"
"    gid_t   pw_gid;        /* group ID */\n"
"    char   *pw_gecos;      /* user information */\n"
"    char   *pw_dir;        /* home directory */\n"
"    char   *pw_shell;      /* shell program */\n"
"};\n"
msgstr ""
"struct passwd {\n"
"        char    *pw_name;\t\t/* nome do usuário */\n"
"        char    *pw_passwd;\t\t/* senha do usuário */\n"
"        uid_t   pw_uid;\t\t\t/* id do usuário */\n"
"        gid_t   pw_gid;\t\t\t/* id do grupo */\n"
"        char    *pw_gecos;      \t/* nome real */\n"
"        char    *pw_dir;  \t\t/* diretório inicial */\n"
"        char    *pw_shell;      \t/* programa shell */\n"
"};\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "See B<passwd>(5)  for more information about these fields."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<getpwnam_r>()  and B<getpwuid_r>()  functions obtain the same "
"information as B<getpwnam>()  and B<getpwuid>(), but store the retrieved "
"I<passwd> structure in the space pointed to by I<pwd>.  The string fields "
"pointed to by the members of the I<passwd> structure are stored in the "
"buffer I<buf> of size I<buflen>.  A pointer to the result (in case of "
"success) or NULL (in case no entry was found or an error occurred) is stored "
"in I<*result>."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The call"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "    sysconf(_SC_GETPW_R_SIZE_MAX)\n"
msgid "sysconf(_SC_GETPW_R_SIZE_MAX)\n"
msgstr "    sysconf(_SC_GETPW_R_SIZE_MAX)\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"returns either -1, without changing I<errno>, or an initial suggested size "
"for I<buf>.  (If this size is too small, the call fails with B<ERANGE>, in "
"which case the caller can retry with a larger buffer.)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALOR DE RETORNO"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The B<getpwnam()> and B<getpwuid()> functions return a pointer to the "
#| "passwd structure, or NULL if the matching entry is not found or an error "
#| "occurs."
msgid ""
"The B<getpwnam>()  and B<getpwuid>()  functions return a pointer to a "
"I<passwd> structure, or NULL if the matching entry is not found or an error "
"occurs.  If an error occurs, I<errno> is set to indicate the error.  If one "
"wants to check I<errno> after the call, it should be set to zero before the "
"call."
msgstr ""
"As funções B<getpwnam()> e B<getpwuid()>P retornam um ponteiro à estrutura "
"passwd, ou NULO se a entrada correspondente não for encontrada ou se ocorrer "
"algum erro."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The return value may point to a static area, and may be overwritten by "
"subsequent calls to B<getpwent>(3), B<getpwnam>(), or B<getpwuid>().  (Do "
"not pass the returned pointer to B<free>(3).)"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"On success, B<getpwnam_r>()  and B<getpwuid_r>()  return zero, and set "
"I<*result> to I<pwd>.  If no matching password record was found, these "
"functions return 0 and store NULL in I<*result>.  In case of error, an error "
"number is returned, and NULL is stored in I<*result>."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ERROS"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<0> or B<ENOENT> or B<ESRCH> or B<EBADF> or B<EPERM> or ... "
msgid "B<0> or B<ENOENT> or B<ESRCH> or B<EBADF> or B<EPERM> or ..."
msgstr "B<0> ou B<ENOENT> ou B<ESRCH> ou B<EBADF> ou B<EPERM> or ... "

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The given I<name> or I<uid> was not found."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EINTR>"
msgstr "B<EINTR>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "A signal was caught; see B<signal>(7)."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EIO>"
msgstr "B<EIO>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I/O error."
msgstr "Erro de E/S."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EMFILE>"
msgstr "B<EMFILE>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The per-process limit on the number of open file descriptors has been "
"reached."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENFILE>"
msgstr "B<ENFILE>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The system-wide limit on the total number of open files has been reached."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOMEM>"
msgstr "B<ENOMEM>"

#.  not in POSIX
#.  This structure is static, allocated 0 or 1 times. No memory leak. (libc45)
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Insufficient memory to allocate I<passwd> structure."
msgstr "Memória insuficiente para alocar a estrutura I<passwd>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ERANGE>"
msgstr "B<ERANGE>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Insufficient buffer space supplied."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "FILES"
msgstr "ARQUIVOS"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "I</etc/passwd>"
msgstr "I</etc/passwd>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid "password database file"
msgid "local password database file"
msgstr "arquivo do banco de dados das senhas"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATRIBUTOS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Para uma explicação dos termos usados nesta seção, consulte B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Interface"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Atributo"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Valor"

#. type: tbl table
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<getpwnam>()"
msgstr "B<getpwnam>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Thread safety"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "MT-Safe env locale"
msgid "MT-Unsafe race:pwnam locale"
msgstr "MT-Safe env locale"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<getpwuid>()"
msgstr "B<getpwuid>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "MT-Safe env locale"
msgid "MT-Unsafe race:pwuid locale"
msgstr "MT-Safe env locale"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<getpwnam_r>(), B<getpwuid_r>():"
msgid ""
"B<getpwnam_r>(),\n"
"B<getpwuid_r>()"
msgstr "B<getpwnam_r>(), B<getpwuid_r>():"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe locale"
msgstr "MT-Safe locale"

#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "VERSÕES"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid ""
"The I<pw_gecos> field is not specified in POSIX, but is present on most "
"implementations."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "PADRÕES"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr "POSIX.1-2008."

#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "HISTÓRICO"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "POSIX.1-2001, SVr4, 4.3BSD."
msgstr "POSIX.1-2001, SVr4, 4.3BSD."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "NOTAS"

#. #-#-#-#-#  archlinux: getpwnam.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  more precisely:
#.  AIX 5.1 - gives ESRCH
#.  OSF1 4.0g - gives EWOULDBLOCK
#.  libc, glibc up to glibc 2.6, Irix 6.5 - give ENOENT
#.  since glibc 2.7 - give 0
#.  FreeBSD 4.8, OpenBSD 3.2, NetBSD 1.6 - give EPERM
#.  SunOS 5.8 - gives EBADF
#.  Tru64 5.1b, HP-UX-11i, SunOS 5.7 - give 0
#. type: Plain text
#. #-#-#-#-#  debian-bookworm: getpwnam.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  more precisely:
#.  AIX 5.1 - gives ESRCH
#.  OSF1 4.0g - gives EWOULDBLOCK
#.  libc, glibc up to glibc 2.6, Irix 6.5 - give ENOENT
#.  since glibc 2.7 - give 0
#.  FreeBSD 4.8, OpenBSD 3.2, NetBSD 1.6 - give EPERM
#.  SunOS 5.8 - gives EBADF
#.  Tru64 5.1b, HP-UX-11i, SunOS 5.7 - give 0
#. type: Plain text
#. #-#-#-#-#  debian-unstable: getpwnam.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  more precisely:
#.  AIX 5.1 - gives ESRCH
#.  OSF1 4.0g - gives EWOULDBLOCK
#.  libc, glibc up to glibc 2.6, Irix 6.5 - give ENOENT
#.  since glibc 2.7 - give 0
#.  FreeBSD 4.8, OpenBSD 3.2, NetBSD 1.6 - give EPERM
#.  SunOS 5.8 - gives EBADF
#.  Tru64 5.1b, HP-UX-11i, SunOS 5.7 - give 0
#. type: Plain text
#. #-#-#-#-#  fedora-39: getpwnam.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  more precisely:
#.  AIX 5.1 - gives ESRCH
#.  OSF1 4.0g - gives EWOULDBLOCK
#.  libc, glibc up to glibc 2.6, Irix 6.5 - give ENOENT
#.  since glibc 2.7 - give 0
#.  FreeBSD 4.8, OpenBSD 3.2, NetBSD 1.6 - give EPERM
#.  SunOS 5.8 - gives EBADF
#.  Tru64 5.1b, HP-UX-11i, SunOS 5.7 - give 0
#. type: Plain text
#. #-#-#-#-#  fedora-rawhide: getpwnam.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  more precisely:
#.  AIX 5.1 - gives ESRCH
#.  OSF1 4.0g - gives EWOULDBLOCK
#.  libc, glibc up to glibc 2.6, Irix 6.5 - give ENOENT
#.  since glibc 2.7 - give 0
#.  FreeBSD 4.8, OpenBSD 3.2, NetBSD 1.6 - give EPERM
#.  SunOS 5.8 - gives EBADF
#.  Tru64 5.1b, HP-UX-11i, SunOS 5.7 - give 0
#. type: Plain text
#. #-#-#-#-#  mageia-cauldron: getpwnam.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  more precisely:
#.  AIX 5.1 - gives ESRCH
#.  OSF1 4.0g - gives EWOULDBLOCK
#.  libc, glibc up to glibc 2.6, Irix 6.5 - give ENOENT
#.  since glibc 2.7 - give 0
#.  FreeBSD 4.8, OpenBSD 3.2, NetBSD 1.6 - give EPERM
#.  SunOS 5.8 - gives EBADF
#.  Tru64 5.1b, HP-UX-11i, SunOS 5.7 - give 0
#. type: Plain text
#. #-#-#-#-#  opensuse-leap-15-6: getpwnam.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  more precisely:
#.  AIX 5.1 - gives ESRCH
#.  OSF1 4.0g - gives EWOULDBLOCK
#.  libc, glibc up to version 2.6, Irix 6.5 - give ENOENT
#.  glibc since version 2.7 - give 0
#.  FreeBSD 4.8, OpenBSD 3.2, NetBSD 1.6 - give EPERM
#.  SunOS 5.8 - gives EBADF
#.  Tru64 5.1b, HP-UX-11i, SunOS 5.7 - give 0
#. type: Plain text
#. #-#-#-#-#  opensuse-tumbleweed: getpwnam.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  more precisely:
#.  AIX 5.1 - gives ESRCH
#.  OSF1 4.0g - gives EWOULDBLOCK
#.  libc, glibc up to glibc 2.6, Irix 6.5 - give ENOENT
#.  since glibc 2.7 - give 0
#.  FreeBSD 4.8, OpenBSD 3.2, NetBSD 1.6 - give EPERM
#.  SunOS 5.8 - gives EBADF
#.  Tru64 5.1b, HP-UX-11i, SunOS 5.7 - give 0
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The formulation given above under \"RETURN VALUE\" is from POSIX.1-2001.  It "
"does not call \"not found\" an error, and hence does not specify what value "
"I<errno> might have in this situation.  But that makes it impossible to "
"recognize errors.  One might argue that according to POSIX I<errno> should "
"be left unchanged if an entry is not found.  Experiments on various UNIX-"
"like systems show that lots of different values occur in this situation: 0, "
"ENOENT, EBADF, ESRCH, EWOULDBLOCK, EPERM, and probably others."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The I<pw_dir> field contains the name of the initial working directory of "
"the user.  Login programs use the value of this field to initialize the "
"B<HOME> environment variable for the login shell.  An application that wants "
"to determine its user's home directory should inspect the value of B<HOME> "
"(rather than the value I<getpwuid(getuid())-E<gt>pw_dir>)  since this allows "
"the user to modify their notion of \"the home directory\" during a login "
"session.  To determine the (initial) home directory of another user, it is "
"necessary to use I<getpwnam(\"username\")-E<gt>pw_dir> or similar."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "EXEMPLOS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The program below demonstrates the use of B<getpwnam_r>()  to find the full "
"username and user ID for the username supplied as a command-line argument."
msgstr ""

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid ""
"#include E<lt>errno.hE<gt>\n"
"#include E<lt>pwd.hE<gt>\n"
"#include E<lt>stdint.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"
"\\&\n"
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    struct passwd pwd;\n"
"    struct passwd *result;\n"
"    char *buf;\n"
"    long bufsize;\n"
"    int s;\n"
"\\&\n"
"    if (argc != 2) {\n"
"        fprintf(stderr, \"Usage: %s username\\en\", argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    bufsize = sysconf(_SC_GETPW_R_SIZE_MAX);\n"
"    if (bufsize == -1)          /* Value was indeterminate */\n"
"        bufsize = 16384;        /* Should be more than enough */\n"
"\\&\n"
"    buf = malloc(bufsize);\n"
"    if (buf == NULL) {\n"
"        perror(\"malloc\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    s = getpwnam_r(argv[1], &pwd, buf, bufsize, &result);\n"
"    if (result == NULL) {\n"
"        if (s == 0)\n"
"            printf(\"Not found\\en\");\n"
"        else {\n"
"            errno = s;\n"
"            perror(\"getpwnam_r\");\n"
"        }\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    printf(\"Name: %s; UID: %jd\\en\", pwd.pw_gecos,\n"
"           (intmax_t) pwd.pw_uid);\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""

#. #-#-#-#-#  archlinux: getpwnam.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  debian-bookworm: getpwnam.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  debian-unstable: getpwnam.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  fedora-39: getpwnam.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  fedora-rawhide: getpwnam.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  mageia-cauldron: getpwnam.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  opensuse-leap-15-6: getpwnam.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#. #-#-#-#-#  opensuse-tumbleweed: getpwnam.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VEJA TAMBÉM"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"B<endpwent>(3), B<fgetpwent>(3), B<getgrnam>(3), B<getpw>(3), "
"B<getpwent>(3), B<getspnam>(3), B<putpwent>(3), B<setpwent>(3), B<passwd>(5)"
msgstr ""
"B<endpwent>(3), B<fgetpwent>(3), B<getgrnam>(3), B<getpw>(3), "
"B<getpwent>(3), B<getspnam>(3), B<putpwent>(3), B<setpwent>(3), B<passwd>(5)"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "2023-02-05"
msgstr "5 fevereiro 2023"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "NOTE"
msgstr "NOTA"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"The user password database mostly refers to I</etc/passwd>.  However, with "
"recent systems it also refers to network wide databases using NIS, LDAP and "
"other local files as configured in I</etc/nsswitch.conf>."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "I</etc/nsswitch.conf>"
msgstr "I</etc/nsswitch.conf>"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "System Databases and Name Service Switch configuration file"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid ""
"POSIX.1-2001, POSIX.1-2008, SVr4, 4.3BSD.  The I<pw_gecos> field is not "
"specified in POSIX, but is present on most implementations."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "#include E<lt>pwd.hE<gt>\n"
#| "#include E<lt>stdio.hE<gt>\n"
#| "#include E<lt>stdlib.hE<gt>\n"
#| "#include E<lt>unistd.hE<gt>\n"
#| "#include E<lt>errno.hE<gt>\n"
msgid ""
"#include E<lt>errno.hE<gt>\n"
"#include E<lt>pwd.hE<gt>\n"
"#include E<lt>stdint.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"
msgstr ""
"#include E<lt>pwd.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"
"#include E<lt>errno.hE<gt>\n"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "int\n"
#| "main(int argc, char *argv[])\n"
#| "{\n"
#| "    struct passwd pwd;\n"
#| "    struct passwd *result;\n"
#| "    char *buf;\n"
#| "    size_t bufsize;\n"
#| "    int s;\n"
msgid ""
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    struct passwd pwd;\n"
"    struct passwd *result;\n"
"    char *buf;\n"
"    long bufsize;\n"
"    int s;\n"
msgstr ""
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    struct passwd pwd;\n"
"    struct passwd *result;\n"
"    char *buf;\n"
"    size_t bufsize;\n"
"    int s;\n"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"    if (argc != 2) {\n"
"        fprintf(stderr, \"Usage: %s username\\en\", argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"    bufsize = sysconf(_SC_GETPW_R_SIZE_MAX);\n"
"    if (bufsize == -1)          /* Value was indeterminate */\n"
"        bufsize = 16384;        /* Should be more than enough */\n"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"    buf = malloc(bufsize);\n"
"    if (buf == NULL) {\n"
"        perror(\"malloc\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
msgstr ""
"    buf = malloc(bufsize);\n"
"    if (buf == NULL) {\n"
"        perror(\"malloc\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"    s = getpwnam_r(argv[1], &pwd, buf, bufsize, &result);\n"
"    if (result == NULL) {\n"
"        if (s == 0)\n"
"            printf(\"Not found\\en\");\n"
"        else {\n"
"            errno = s;\n"
"            perror(\"getpwnam_r\");\n"
"        }\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"    printf(\"Name: %s; UID: %jd\\en\", pwd.pw_gecos,\n"
"           (intmax_t) pwd.pw_uid);\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"B<endpwent>(3), B<fgetpwent>(3), B<getgrnam>(3), B<getpw>(3), "
"B<getpwent>(3), B<getspnam>(3), B<putpwent>(3), B<setpwent>(3), B<nsswitch."
"conf>(5), B<passwd>(5)"
msgstr ""
"B<endpwent>(3), B<fgetpwent>(3), B<getgrnam>(3), B<getpw>(3), "
"B<getpwent>(3), B<getspnam>(3), B<putpwent>(3), B<setpwent>(3), B<nsswitch."
"conf>(5), B<passwd>(5)"

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.05"
msgstr "Linux man-pages 6.05"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "GETPWNAM"
msgstr "GETPWNAM"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2017-09-15"
msgstr "15 setembro 2017"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "GNU"
msgstr "GNU"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Manual do Programador do Linux"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid "B<struct passwd *getpwnam(const char *>I<name>B<);>\n"
msgstr "B<struct passwd *getpwnam(const char *>I<name>B<);>\n"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid "B<struct passwd *getpwuid(uid_t >I<uid>B<);>\n"
msgstr "B<struct passwd *getpwuid(uid_t >I<uid>B<);>\n"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"B<int getpwnam_r(const char *>I<name>B<, struct passwd *>I<pwd>B<,>\n"
"B<               char *>I<buf>B<, size_t >I<buflen>B<, struct passwd **>I<result>B<);>\n"
msgstr ""
"B<int getpwnam_r(const char *>I<name>B<, struct passwd *>I<pwd>B<,>\n"
"B<               char *>I<buf>B<, size_t >I<buflen>B<, struct passwd **>I<result>B<);>\n"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"B<int getpwuid_r(uid_t >I<uid>B<, struct passwd *>I<pwd>B<,>\n"
"B<               char *>I<buf>B<, size_t >I<buflen>B<, struct passwd **>I<result>B<);>\n"
msgstr ""
"B<int getpwuid_r(uid_t >I<uid>B<, struct passwd *>I<pwd>B<,>\n"
"B<               char *>I<buf>B<, size_t >I<buflen>B<, struct passwd **>I<result>B<);>\n"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"_POSIX_C_SOURCE\n"
"    || /* Glibc versions E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"
msgstr ""
"_POSIX_C_SOURCE\n"
"    || /* Glibc versões E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid "    sysconf(_SC_GETPW_R_SIZE_MAX)\n"
msgstr "    sysconf(_SC_GETPW_R_SIZE_MAX)\n"

#. type: Plain text
#: opensuse-leap-15-6
#, fuzzy
#| msgid ""
#| "The B<getpwnam()> and B<getpwuid()> functions return a pointer to the "
#| "passwd structure, or NULL if the matching entry is not found or an error "
#| "occurs."
msgid ""
"The B<getpwnam>()  and B<getpwuid>()  functions return a pointer to a "
"I<passwd> structure, or NULL if the matching entry is not found or an error "
"occurs.  If an error occurs, I<errno> is set appropriately.  If one wants to "
"check I<errno> after the call, it should be set to zero before the call."
msgstr ""
"As funções B<getpwnam()> e B<getpwuid()>P retornam um ponteiro à estrutura "
"passwd, ou NULO se a entrada correspondente não for encontrada ou se ocorrer "
"algum erro."

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "B<0> or B<ENOENT> or B<ESRCH> or B<EBADF> or B<EPERM> or ... "
msgstr "B<0> ou B<ENOENT> ou B<ESRCH> ou B<EBADF> ou B<EPERM> or ... "

#. type: tbl table
#: opensuse-leap-15-6
#, no-wrap
msgid "B<getpwnam_r>(),\n"
msgstr "B<getpwnam_r>(),\n"

#. type: tbl table
#: opensuse-leap-15-6
#, no-wrap
msgid ".br\n"
msgstr ".br\n"

#. type: tbl table
#: opensuse-leap-15-6
#, no-wrap
msgid "B<getpwuid_r>()"
msgstr "B<getpwuid_r>()"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "CONFORMING TO"
msgstr "DE ACORDO COM"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "EXAMPLE"
msgstr "EXEMPLO"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"#include E<lt>pwd.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"
"#include E<lt>errno.hE<gt>\n"
msgstr ""
"#include E<lt>pwd.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"
"#include E<lt>errno.hE<gt>\n"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    struct passwd pwd;\n"
"    struct passwd *result;\n"
"    char *buf;\n"
"    size_t bufsize;\n"
"    int s;\n"
msgstr ""
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    struct passwd pwd;\n"
"    struct passwd *result;\n"
"    char *buf;\n"
"    size_t bufsize;\n"
"    int s;\n"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"    printf(\"Name: %s; UID: %ld\\en\", pwd.pw_gecos, (long) pwd.pw_uid);\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "COLOPHON"
msgstr "COLOFÃO"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Esta página faz parte da versão 4.16 do projeto Linux I<man-pages>. Uma "
"descrição do projeto, informações sobre relatórios de bugs e a versão mais "
"recente desta página podem ser encontradas em \\%https://www.kernel.org/doc/"
"man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30 março 2023"

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"
