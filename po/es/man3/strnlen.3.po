# Spanish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Juan Piernas <piernas@ditec.um.es>, 2000.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-08-27 17:24+0200\n"
"PO-Revision-Date: 2000-04-22 19:55+0200\n"
"Last-Translator: Juan Piernas <piernas@ditec.um.es>\n"
"Language-Team: Spanish <debian-l10n-spanish@lists.debian.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 20.04.1\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<strnlen>()"
msgid "strnlen"
msgstr "B<strnlen>()"

#. type: TH
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid "2023-07-20"
msgstr "20 ​​Julio 2023"

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Páginas de manual de Linux 6.05.01"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOMBRE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "strnlen - determine the length of a fixed-size string"
msgstr "strnlen - determina la longitud de una cadena de tamaño fijo"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTECA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Biblioteca Estándar C (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>string.hE<gt>>\n"
msgstr "B<#include E<lt>string.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<size_t strnlen(const char *>I<s>B<, size_t >I<maxlen>B<);>\n"
msgid "B<size_t strnlen(const char >I<s>B<[.>I<maxlen>B<], size_t >I<maxlen>B<);>\n"
msgstr "B<size_t strnlen(const char *>I<s>B<, size_t >I<maxlen>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Requisitos de Macros de Prueba de Características para glibc (véase "
"B<feature_test_macros>(7)):"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<strnlen>():"
msgstr "B<strnlen>():"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"    Since glibc 2.10:\n"
"        _POSIX_C_SOURCE E<gt>= 200809L\n"
"    Before glibc 2.10:\n"
"        _GNU_SOURCE\n"
msgstr ""
"    Desde glibc 2.10:\n"
"        _POSIX_C_SOURCE E<gt>= 200809L\n"
"    Antes de glibc 2.10:\n"
"        _GNU_SOURCE\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPCIÓN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The B<strnlen> function returns the number of characters in the string "
#| "pointed to by I<s>, not including the terminating '\\e0' character, but "
#| "at most I<maxlen>. In doing this, B<strnlen> looks only at the first "
#| "I<maxlen> characters at I<s> and never beyond I<s+maxlen>."
msgid ""
"The B<strnlen>()  function returns the number of bytes in the string pointed "
"to by I<s>, excluding the terminating null byte (\\[aq]\\e0\\[aq]), but at "
"most I<maxlen>.  In doing this, B<strnlen>()  looks only at the first "
"I<maxlen> characters in the string pointed to by I<s> and never beyond "
"I<s[maxlen-1]>."
msgstr ""
"La función B<strnlen> devuelve el número de caracteres de la cadena apunta "
"por I<s>, sin incluir el carácter terminador '\\e0', hasta un máximo de "
"I<maxlen>. Al hacer esto, B<strnlen> mira sólo los primeros I<maxlen> "
"caracteres en I<s>, nunca más allá de I<s+maxlen>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALOR DEVUELTO"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The B<strnlen> function returns I<strlen(s)>, if that is less than "
#| "I<maxlen>, or I<maxlen> if there is no '\\e0' character among the first "
#| "I<maxlen> characters pointed to by I<s>."
msgid ""
"The B<strnlen>()  function returns I<strlen(s)>, if that is less than "
"I<maxlen>, or I<maxlen> if there is no null terminating (\\[aq]\\e0\\[aq]) "
"among the first I<maxlen> characters pointed to by I<s>."
msgstr ""
"La función B<strnlen> devuelve I<strlen(s)>, si dicho valor es menor que "
"I<maxlen>, o I<maxlen> si no existe un carácter '\\e0' entre los primeros "
"I<maxlen> caracteres apuntados por I<s>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATRIBUTOS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Para obtener una explicación de los términos usados en esta sección, véase "
"B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Interfaz"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Atributo"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Valor"

#. type: tbl table
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<strnlen>()"
msgstr "B<strnlen>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Seguridad del hilo"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "Multi-hilo seguro"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "ESTÁNDARES"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr "POSIX.1-2008."

#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "HISTORIAL"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VÉASE TAMBIÉN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<strlen>(3)"
msgstr "B<strlen>(3)"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "2023-02-05"
msgstr "5 Febrero 2023"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Páginas de manual de Linux 6.03"

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.05"
msgstr "Páginas de manual de Linux 6.05"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "STRNLEN"
msgstr "STRNLEN"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2016-03-15"
msgstr "15 Marzo 2016"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "GNU"
msgstr "GNU"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Manual del Programador de Linux"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid "B<size_t strnlen(const char *>I<s>B<, size_t >I<maxlen>B<);>\n"
msgstr "B<size_t strnlen(const char *>I<s>B<, size_t >I<maxlen>B<);>\n"

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "Since glibc 2.10:"
msgstr "Desde glibc 2.10:"

#. type: Plain text
#: opensuse-leap-15-6
msgid "_POSIX_C_SOURCE\\ E<gt>=\\ 200809L"
msgstr "_POSIX_C_SOURCE\\ E<gt>=\\ 200809L"

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "Before glibc 2.10:"
msgstr "Antes de glibc 2.10:"

#. type: Plain text
#: opensuse-leap-15-6
msgid "_GNU_SOURCE"
msgstr "_GNU_SOURCE"

#. type: Plain text
#: opensuse-leap-15-6
#, fuzzy
#| msgid ""
#| "The B<strnlen> function returns the number of characters in the string "
#| "pointed to by I<s>, not including the terminating '\\e0' character, but "
#| "at most I<maxlen>. In doing this, B<strnlen> looks only at the first "
#| "I<maxlen> characters at I<s> and never beyond I<s+maxlen>."
msgid ""
"The B<strnlen>()  function returns the number of characters in the string "
"pointed to by I<s>, excluding the terminating null byte (\\(aq\\e0\\(aq), "
"but at most I<maxlen>.  In doing this, B<strnlen>()  looks only at the first "
"I<maxlen> characters in the string pointed to by I<s> and never beyond "
"I<s+maxlen>."
msgstr ""
"La función B<strnlen> devuelve el número de caracteres de la cadena apunta "
"por I<s>, sin incluir el carácter terminador '\\e0', hasta un máximo de "
"I<maxlen>. Al hacer esto, B<strnlen> mira sólo los primeros I<maxlen> "
"caracteres en I<s>, nunca más allá de I<s+maxlen>."

#. type: Plain text
#: opensuse-leap-15-6
#, fuzzy
#| msgid ""
#| "The B<strnlen> function returns I<strlen(s)>, if that is less than "
#| "I<maxlen>, or I<maxlen> if there is no '\\e0' character among the first "
#| "I<maxlen> characters pointed to by I<s>."
msgid ""
"The B<strnlen>()  function returns I<strlen(s)>, if that is less than "
"I<maxlen>, or I<maxlen> if there is no null terminating (\\(aq\\e0\\(aq) "
"among the first I<maxlen> characters pointed to by I<s>."
msgstr ""
"La función B<strnlen> devuelve I<strlen(s)>, si dicho valor es menor que "
"I<maxlen>, o I<maxlen> si no existe un carácter '\\e0' entre los primeros "
"I<maxlen> caracteres apuntados por I<s>."

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "CONFORMING TO"
msgstr "CONFORME A"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "COLOPHON"
msgstr "COLOFÓN"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Esta página es parte de la versión 4.16 del proyecto Linux I<man-pages>. "
"Puede encontrar una descripción del proyecto, información sobre cómo "
"informar errores y la última versión de esta página en \\%https://www.kernel."
"org/doc/man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30 Marzo 2023"

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"
