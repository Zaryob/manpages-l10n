# Spanish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Gerardo Aburruzaga García <gerardo.aburruzaga@uca.es>, 1998.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-08-27 17:12+0200\n"
"PO-Revision-Date: 1998-01-19 19:53+0200\n"
"Last-Translator: Gerardo Aburruzaga García <gerardo.aburruzaga@uca.es>\n"
"Language-Team: Spanish <debian-l10n-spanish@lists.debian.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 20.04.1\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "personality"
msgstr ""

#. type: TH
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid "2023-04-29"
msgstr "29 Abril 2023"

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Páginas de manual de Linux 6.05.01"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOMBRE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "personality - set the process execution domain"
msgstr "personality - establece el dominio de ejecución del proceso"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTECA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Biblioteca Estándar C (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<#include E<lt>sys/personality.hE<gt>>"
msgid "B<#include E<lt>sys/personality.hE<gt>>\n"
msgstr "B<#include E<lt>sys/personality.hE<gt>>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<int personality(unsigned long >I<persona>B<);>"
msgid "B<int personality(unsigned long >I<persona>B<);>\n"
msgstr "B<int personality(unsigned long >I<persona>B<);>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPCIÓN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Linux supports different execution domains, or personalities, for each "
"process.  Among other things, execution domains tell Linux how to map signal "
"numbers into signal actions.  The execution domain system allows Linux to "
"provide limited support for binaries compiled under other UNIX-like "
"operating systems."
msgstr ""
"Linux admite diferentes dominios de ejecución, o personalidades, para cada "
"proceso. Entre otras cosas, los dominios de ejecución le dicen a Linux cómo "
"asociar números de señal a acciones a tomar para cada señal. El sistema de "
"dominios de ejecución permite a Linux proporcionar un soporte limitado para "
"binarios compilados bajo otros sistemas operativos de tipo UNIX."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If I<persona> is not 0xffffffff, then B<personality>()  sets the caller's "
"execution domain to the value specified by I<persona>.  Specifying "
"I<persona> as 0xffffffff provides a way of retrieving the current persona "
"without changing it."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"A list of the available execution domains can be found in I<E<lt>sys/"
"personality.hE<gt>>.  The execution domain is a 32-bit value in which the "
"top three bytes are set aside for flags that cause the kernel to modify the "
"behavior of certain system calls so as to emulate historical or "
"architectural quirks.  The least significant byte is a value defining the "
"personality the kernel should assume.  The flag values are as follows:"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ADDR_COMPAT_LAYOUT> (since Linux 2.6.9)"
msgstr "B<ADDR_COMPAT_LAYOUT> (desde Linux 2.6.9)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "With this flag set, provide legacy virtual address space layout."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ADDR_NO_RANDOMIZE> (since Linux 2.6.12)"
msgstr "B<ADDR_NO_RANDOMIZE> (desde Linux 2.6.12)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "With this flag set, disable address-space-layout randomization."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ADDR_LIMIT_32BIT> (since Linux 2.2)"
msgstr "B<ADDR_LIMIT_32BIT> (desde Linux 2.2)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Limit the address space to 32 bits."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ADDR_LIMIT_3GB> (since Linux 2.4.0)"
msgstr "B<ADDR_LIMIT_3GB> (desde Linux 2.4.0)"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide
msgid ""
"With this flag set, use 0xc0000000 as the offset at which to search a "
"virtual memory chunk on B<mmap>(2); otherwise use 0xffffe000.  Applies to 32-"
"bit x86 processes only."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<FDPIC_FUNCPTRS> (since Linux 2.6.11)"
msgstr "B<FDPIC_FUNCPTRS> (desde Linux 2.6.11)"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide
msgid ""
"User-space function pointers to signal handlers point to descriptors.  "
"Applies only to ARM if BINFMT_ELF_FDPIC and SuperH."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<MMAP_PAGE_ZERO> (since Linux 2.4.0)"
msgstr "B<MMAP_PAGE_ZERO> (desde Linux 2.4.0)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Map page 0 as read-only (to support binaries that depend on this SVr4 "
"behavior)."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<READ_IMPLIES_EXEC> (since Linux 2.6.8)"
msgstr "B<READ_IMPLIES_EXEC> (desde Linux 2.6.8)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "With this flag set, B<PROT_READ> implies B<PROT_EXEC> for B<mmap>(2)."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<SHORT_INODE> (since Linux 2.4.0)"
msgstr "B<SHORT_INODE> (desde Linux 2.4.0)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "No effect."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<STICKY_TIMEOUTS> (since Linux 1.2.0)"
msgstr "B<STICKY_TIMEOUTS> (desde Linux 1.2.0)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"With this flag set, B<select>(2), B<pselect>(2), and B<ppoll>(2)  do not "
"modify the returned timeout argument when interrupted by a signal handler."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<UNAME26> (since Linux 3.1)"
msgstr "B<UNAME26> (desde Linux 3.1)"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide
msgid ""
"Have B<uname>(2)  report a 2.6.(40+x) version number rather than a MAJOR.x "
"version number.  Added as a stopgap measure to support broken applications "
"that could not handle the kernel version-numbering switch from Linux 2.6.x "
"to Linux 3.x."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<WHOLE_SECONDS> (since Linux 1.2.0)"
msgstr "B<WHOLE_SECONDS> (desde Linux 1.2.0)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The available execution domains are:"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<PER_BSD> (since Linux 1.2.0)"
msgstr "B<PER_BSD> (desde Linux 1.2.0)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "BSD. (No effects.)"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<PER_HPUX> (since Linux 2.4)"
msgstr "B<PER_HPUX> (desde Linux 2.4)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Support for 32-bit HP/UX.  This support was never complete, and was dropped "
"so that since Linux 4.0, this value has no effect."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<PER_IRIX32> (since Linux 2.2)"
msgstr "B<PER_IRIX32> (desde Linux 2.2)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"IRIX 5 32-bit.  Never fully functional; support dropped in Linux 2.6.27.  "
"Implies B<STICKY_TIMEOUTS>."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<PER_IRIX64> (since Linux 2.2)"
msgstr "B<PER_IRIX64> (desde Linux 2.2)"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide
msgid "IRIX 6 64-bit.  Implies B<STICKY_TIMEOUTS>; otherwise no effect."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<PER_IRIXN32> (since Linux 2.2)"
msgstr "B<PER_IRIXN32> (desde Linux 2.2)"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide
msgid "IRIX 6 new 32-bit.  Implies B<STICKY_TIMEOUTS>; otherwise no effect."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<PER_ISCR4> (since Linux 1.2.0)"
msgstr "B<PER_ISCR4> (desde Linux 1.2.0)"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide
msgid "Implies B<STICKY_TIMEOUTS>; otherwise no effect."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<PER_LINUX> (since Linux 1.2.0)"
msgstr "B<PER_LINUX> (desde Linux 1.2.0)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Linux."
msgstr "Linux."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<PER_LINUX32> (since Linux 2.2)"
msgstr "B<PER_LINUX32> (desde Linux 2.2)"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide
msgid ""
"B<uname>(2)  returns the name of the 32-bit architecture in the I<machine> "
"field (\"i686\" instead of \"x86_64\", &c.)."
msgstr ""

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide
msgid ""
"Under ia64 (Itanium), processes with this personality don't have the "
"O_LARGEFILE B<open>(2)  flag forced."
msgstr ""

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide
msgid ""
"Under 64-bit ARM, setting this personality is forbidden if B<execve>(2)ing a "
"32-bit process would also be forbidden (cf. the allow_mismatched_32bit_el0 "
"kernel parameter and I<Documentation/arm64/asymmetric-32bit.rst>)."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<PER_LINUX32_3GB> (since Linux 2.4)"
msgstr "B<PER_LINUX32_3GB> (desde Linux 2.4)"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide
msgid "Same as B<PER_LINUX32>, but implies B<ADDR_LIMIT_3GB>."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<PER_LINUX_32BIT> (since Linux 2.0)"
msgstr "B<PER_LINUX_32BIT> (desde Linux 2.0)"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide
msgid "Same as B<PER_LINUX>, but implies B<ADDR_LIMIT_32BIT>."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<PER_LINUX_FDPIC> (since Linux 2.6.11)"
msgstr "B<PER_LINUX_FDPIC> (desde Linux 2.6.11)"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide
msgid "Same as B<PER_LINUX>, but implies B<FDPIC_FUNCPTRS>."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<PER_OSF4> (since Linux 2.4)"
msgstr "B<PER_OSF4> (desde Linux 2.4)"

#.  commit 987f20a9dcce3989e48d87cff3952c095c994445
#.  Following is from a comment in arch/alpha/kernel/osf_sys.c
#. type: Plain text
#: archlinux fedora-39 fedora-rawhide
msgid ""
"OSF/1 v4.  No effect since Linux 6.1, which removed a.out binary support.  "
"Before, on alpha, would clear top 32 bits of iov_len in the user's buffer "
"for compatibility with old versions of OSF/1 where iov_len was defined as.  "
"I<int>."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<PER_OSR5> (since Linux 2.4)"
msgstr "B<PER_OSR5> (desde Linux 2.4)"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide
msgid ""
"SCO OpenServer 5.  Implies B<STICKY_TIMEOUTS> and B<WHOLE_SECONDS>; "
"otherwise no effect."
msgstr ""

#. type: TP
#: archlinux fedora-39 fedora-rawhide
#, fuzzy, no-wrap
#| msgid "B<PER_RISCOS> (since Linux 2.2)"
msgid "B<PER_RISCOS> (since Linux 2.3.7; macro since Linux 2.3.13)"
msgstr "B<PER_RISCOS> (desde Linux 2.2)"

#.  commit 125ec7b4e90cbae4eed5a7ff1ee479cc331dcf3c
#. type: Plain text
#: archlinux fedora-39 fedora-rawhide
msgid ""
"Acorn RISC OS/Arthur (MIPS).  No effect.  Up to Linux v4.0, would set the "
"emulation altroot to I</usr/gnemul/riscos> (cf.\\& B<PER_SUNOS>, below).  "
"Before then, up to Linux 2.6.3, just Arthur emulation."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<PER_SCOSVR3> (since Linux 1.2.0)"
msgstr "B<PER_SCOSVR3> (desde Linux 1.2.0)"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide
msgid ""
"SCO UNIX System V Release 3.  Same as B<PER_OSR5>, but also implies "
"B<SHORT_INODE>."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<PER_SOLARIS> (since Linux 2.4)"
msgstr "B<PER_SOLARIS> (desde Linux 2.4)"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide
msgid "Solaris.  Implies B<STICKY_TIMEOUTS>; otherwise no effect."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<PER_SUNOS> (since Linux 2.4.0)"
msgstr "B<PER_SUNOS> (desde Linux 2.4.0)"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide
msgid ""
"Sun OS.  Same as B<PER_BSD>, but implies B<STICKY_TIMEOUTS>.  Prior to Linux "
"2.6.26, diverted library and dynamic linker searches to I</usr/gnemul>.  "
"Buggy, largely unmaintained, and almost entirely unused."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<PER_SVR3> (since Linux 1.2.0)"
msgstr "B<PER_SVR3> (desde Linux 1.2.0)"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide
msgid ""
"AT&T UNIX System V Release 3.  Implies B<STICKY_TIMEOUTS> and "
"B<SHORT_INODE>; otherwise no effect."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<PER_SVR4> (since Linux 1.2.0)"
msgstr "B<PER_SVR4> (desde Linux 1.2.0)"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide
msgid ""
"AT&T UNIX System V Release 4.  Implies B<STICKY_TIMEOUTS> and "
"B<MMAP_PAGE_ZERO>; otherwise no effect."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<PER_UW7> (since Linux 2.4)"
msgstr "B<PER_UW7> (desde Linux 2.4)"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide
msgid ""
"UnixWare 7.  Implies B<STICKY_TIMEOUTS> and B<MMAP_PAGE_ZERO>; otherwise no "
"effect."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<PER_WYSEV386> (since Linux 1.2.0)"
msgstr "B<PER_WYSEV386> (desde Linux 1.2.0)"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide
msgid ""
"WYSE UNIX System V/386.  Implies B<STICKY_TIMEOUTS> and B<SHORT_INODE>; "
"otherwise no effect."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<PER_XENIX> (since Linux 1.2.0)"
msgstr "B<PER_XENIX> (desde Linux 1.2.0)"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide
msgid ""
"XENIX.  Implies B<STICKY_TIMEOUTS> and B<SHORT_INODE>; otherwise no effect."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALOR DEVUELTO"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "On success, the previous I<persona> is returned.  On error, -1 is "
#| "returned, and I<errno> is set appropriately."
msgid ""
"On success, the previous I<persona> is returned.  On error, -1 is returned, "
"and I<errno> is set to indicate the error."
msgstr ""
"En caso de éxito, se devuelve la I<persona> anterior. En caso de error, se "
"devuelve -1 y se pone un valor apropiado en I<errno>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ERRORES"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The kernel was unable to change the personality."
msgstr "El núcleo no pudo cambiar la personalidad."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "ESTÁNDARES"

#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "HISTORIAL"

#.  (and thus first in a stable kernel release with Linux 1.2.0)
#.  personality wrapper first appeared in glibc 1.90,
#.  <sys/personality.h> was added later in glibc 2.2.91.
#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "Linux 1.1.20, glibc 2.3."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VÉASE TAMBIÉN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<setarch>(8)"
msgstr "B<setarch>(8)"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "2022-12-04"
msgstr "4 Diciembre 2022"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Páginas de manual de Linux 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"With this flag set, use 0xc0000000 as the offset at which to search a "
"virtual memory chunk on B<mmap>(2); otherwise use 0xffffe000."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"User-space function pointers to signal handlers point (on certain "
"architectures) to descriptors."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "No effects(?)."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-tumbleweed
msgid ""
"Have B<uname>(2)  report a 2.6.40+ version number rather than a 3.x version "
"number.  Added as a stopgap measure to support broken applications that "
"could not handle the kernel version-numbering switch from Linux 2.6.x to "
"Linux 3.x."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "IRIX 6 64-bit.  Implies B<STICKY_TIMEOUTS>; otherwise no effects."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "IRIX 6 new 32-bit.  Implies B<STICKY_TIMEOUTS>; otherwise no effects."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "Implies B<STICKY_TIMEOUTS>; otherwise no effects."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "[To be documented.]"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "Implies B<ADDR_LIMIT_3GB>."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "Implies B<ADDR_LIMIT_32BIT>."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "Implies B<FDPIC_FUNCPTRS>."
msgstr ""

#.  Following is from a comment in arch/alpha/kernel/osf_sys.c
#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"OSF/1 v4.  On alpha, clear top 32 bits of iov_len in the user's buffer for "
"compatibility with old versions of OSF/1 where iov_len was defined as.  "
"I<int>."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "Implies B<STICKY_TIMEOUTS> and B<WHOLE_SECONDS>; otherwise no effects."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "B<PER_RISCOS> (since Linux 2.2)"
msgstr "B<PER_RISCOS> (desde Linux 2.2)"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"Implies B<STICKY_TIMEOUTS>, B<WHOLE_SECONDS>, and B<SHORT_INODE>; otherwise "
"no effects."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"Implies B<STICKY_TIMEOUTS>.  Divert library and dynamic linker searches to "
"I</usr/gnemul>.  Buggy, largely unmaintained, and almost entirely unused; "
"support was removed in Linux 2.6.26."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "Implies B<STICKY_TIMEOUTS> and B<SHORT_INODE>; otherwise no effects."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "Implies B<STICKY_TIMEOUTS> and B<MMAP_PAGE_ZERO>; otherwise no effects."
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "VERSIONS"
msgstr "VERSIONES"

#. #-#-#-#-#  debian-bookworm: personality.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  personality wrapper first appeared in glibc 1.90,
#.  <sys/personality.h> was added later in glibc 2.2.91.
#. type: Plain text
#. #-#-#-#-#  debian-unstable: personality.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  personality wrapper first appeared in glibc 1.90,
#.  <sys/personality.h> was added later in glibc 2.2.91.
#. type: Plain text
#. #-#-#-#-#  mageia-cauldron: personality.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  personality wrapper first appeared in glibc 1.90,
#.  <sys/personality.h> was added later in glibc 2.2.91.
#. type: Plain text
#. #-#-#-#-#  opensuse-leap-15-6: personality.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  personality wrapper first appeared in glibc 1.90,
#.  <sys/personality.h> was added later in 2.2.91.
#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid ""
"This system call first appeared in Linux 1.1.20 (and thus first in a stable "
"kernel release with Linux 1.2.0); library support was added in glibc 2.3."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid ""
"B<personality>()  is Linux-specific and should not be used in programs "
"intended to be portable."
msgstr ""
"B<personality>() es específico de Linux y no debería emplearse en programas "
"pretendidamente transportables."

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.05"
msgstr "Páginas de manual de Linux 6.05"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "PERSONALITY"
msgstr "PERSONALITY"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2017-09-15"
msgstr "15 Septiembre 2017"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Manual del Programador de Linux"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<#include E<lt>sys/personality.hE<gt>>"
msgstr "B<#include E<lt>sys/personality.hE<gt>>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<int personality(unsigned long >I<persona>B<);>"
msgstr "B<int personality(unsigned long >I<persona>B<);>"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"A list of the available execution domains can be found in I<E<lt>sys/"
"personality.hE<gt>>.  The execution domain is a 32-bit value in which the "
"top three bytes are set aside for flags that cause the kernel to modify the "
"behavior of certain system calls so as to emulate historical or "
"architectural quirks.  The least significant byte is value defining the "
"personality the kernel should assume.  The flag values are as follows:"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Have B<uname>(2)  report a 2.6.40+ version number rather than a 3.x version "
"number.  Added as a stopgap measure to support broken applications that "
"could not handle the kernel version-numbering switch from 2.6.x to 3.x."
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"On success, the previous I<persona> is returned.  On error, -1 is returned, "
"and I<errno> is set appropriately."
msgstr ""
"En caso de éxito, se devuelve la I<persona> anterior. En caso de error, se "
"devuelve -1 y se pone un valor apropiado en I<errno>."

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "CONFORMING TO"
msgstr "CONFORME A"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "COLOPHON"
msgstr "COLOFÓN"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Esta página es parte de la versión 4.16 del proyecto Linux I<man-pages>. "
"Puede encontrar una descripción del proyecto, información sobre cómo "
"informar errores y la última versión de esta página en \\%https://www.kernel."
"org/doc/man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30 Marzo 2023"

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"
