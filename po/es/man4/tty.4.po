# Spanish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Ignacio Arenaza <Ignacio.Arenaza@studi.epfl.ch>, 1996.
# Juan Pablo Puerta <jppuerta@full-moon.com>, 1998.
# Marcos Fouces <marcos@debian.org>, 2021-2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-08-27 17:30+0200\n"
"PO-Revision-Date: 2023-01-11 17:56+0100\n"
"Last-Translator: Marcos Fouces <marcos@debian.org>\n"
"Language-Team: Spanish <debian-l10n-spanish@lists.debian.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "tty"
msgstr "tty"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "2022-10-30"
msgstr "30 Octubre 2022"

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Páginas de manual de Linux 6.05.01"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOMBRE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "tty - controlling terminal"
msgstr "tty - terminal en control"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPCIÓN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The file I</dev/tty> is a character file with major number 5 and minor "
"number 0, usually with mode 0666 and ownership root:tty.  It is a synonym "
"for the controlling terminal of a process, if any."
msgstr ""
"El archiovo B</dev/tty> es un dispositivo de carácter con el número mayor 5 "
"y número menor 0, creado normalmente con el modo 0666 y propiedad de root:"
"tty. Es un sinónimo del terminal en control de un processo, si existe."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"In addition to the B<ioctl>(2)  requests supported by the device that B<tty> "
"refers to, the B<ioctl>(2)  request B<TIOCNOTTY> is supported."
msgstr ""
"Además de las peticiones B<ioctl()> implementadas por el dispositivo al que "
"se refiere B<tty>, incluye soporte para la petición B<TIOCNOTTY> de "
"B<ioctl>(2):"

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "TIOCNOTTY"
msgstr "TIOCNOTTY"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Detach the calling process from its controlling terminal."
msgstr "Desligar el proceso actual de su terminal en control."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If the process is the session leader, then B<SIGHUP> and B<SIGCONT> signals "
"are sent to the foreground process group and all processes in the current "
"session lose their controlling tty."
msgstr ""
"Si el proceso está en la sesión principal, se envía las señales B<SIGHUP> y "
"B<SIGCONT> al grupo de procesos principales y todos los procesos de la "
"sesión actual pierden su terminal en control."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This B<ioctl>(2)  call works only on file descriptors connected to I</dev/"
"tty>.  It is used by daemon processes when they are invoked by a user at a "
"terminal.  The process attempts to open I</dev/tty>.  If the open succeeds, "
"it detaches itself from the terminal by using B<TIOCNOTTY>, while if the "
"open fails, it is obviously not attached to a terminal and does not need to "
"detach itself."
msgstr ""
"Esta B<ioctl>(2) sólo funciona con descriptores de fichero conectados a I</"
"dev/tty>.  Los procesos de demonio usan esta B<ioctl>(2) cuando son "
"invocados por el usuario a un terminal. El proceso intenta abrir I</dev/"
"tty>. Si la apertura tiene exito, se desliga del terminal usando "
"B<TIOCNOTTY>, mientras que si la apertura falla, obviamente no esta ligado a "
"un terminal y no necesita desligarse."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "FILES"
msgstr "ARCHIVOS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I</dev/tty>"
msgstr "I</dev/tty>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VÉASE TAMBIÉN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"B<chown>(1), B<mknod>(1), B<ioctl>(2), B<ioctl_console>(2), B<ioctl_tty>(2), "
"B<termios>(3), B<ttyS>(4), B<vcs>(4), B<pty>(7), B<agetty>(8), B<mingetty>(8)"
msgstr ""
"B<chown>(1), B<mknod>(1), B<ioctl>(2), B<ioctl_console>(2), B<ioctl_tty>(2), "
"B<termios>(3), B<ttyS>(4), B<vcs>(4), B<pty>(7), B<agetty>(8), B<mingetty>(8)"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Páginas de manual de Linux 6.03"

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.05"
msgstr "Páginas de manual de Linux 6.05"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "TTY"
msgstr "TTY"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2017-11-26"
msgstr "26 Noviembre 2017"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Manual del Programador de Linux"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"The file I</dev/tty> is a character file with major number 5 and minor "
"number 0, usually of mode 0666 and owner.group root.tty.  It is a synonym "
"for the controlling terminal of a process, if any."
msgstr ""
"El archivo B</dev/tty> es un dispositivo de carácter con el número mayor 5 y "
"número menor 0, creado normalmente con el modo 0666 y root:tty como "
"propietario. Es un sinónimo del terminal en control de un processo, si "
"existe."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"B<chown>(1), B<mknod>(1), B<ioctl>(2), B<ioctl_console>(2), B<ioctl_tty>(2), "
"B<termios>(3), B<ttyS>(4), B<agetty>(8), B<mingetty>(8)"
msgstr ""
"B<chown>(1), B<mknod>(1), B<ioctl>(2), B<ioctl_console>(2), B<ioctl_tty>(2), "
"B<termios>(3), B<ttyS>(4), B<agetty>(8), B<mingetty>(8)"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "COLOPHON"
msgstr "COLOFÓN"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Esta página es parte de la versión 4.16 del proyecto Linux I<man-pages>. "
"Puede encontrar una descripción del proyecto, información sobre cómo "
"informar errores y la última versión de esta página en \\%https://www.kernel."
"org/doc/man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"
