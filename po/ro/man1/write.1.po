# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.18.1\n"
"POT-Creation-Date: 2023-08-27 17:33+0200\n"
"PO-Revision-Date: 2023-05-20 18:57+0200\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.2.2\n"

#. type: TH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "WRITE"
msgstr "WRITE"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-05-11"
msgstr "11 mai 2022"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "util-linux 2.38.1"
msgstr "util-linux 2.38.1"

#. type: TH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "User Commands"
msgstr "Comenzi utilizator"

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "write - send a message to another user"
msgstr "write - trimite un mesaj către un alt utilizator"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<write> I<user> [I<ttyname>]"
msgstr "B<write> I<utilizator> [I<nume_tty>]"

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"B<write> allows you to communicate with other users, by copying lines from "
"your terminal to theirs."
msgstr ""
"B<write> vă permite să comunicați cu alți utilizatori, copiind liniile din "
"terminalul dvs. în cel al lor."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"When you run the B<write> command, the user you are writing to gets a "
"message of the form:"
msgstr ""
"Atunci când executați comanda B<write>, utilizatorul căruia îi scrieți "
"primește un mesaj de forma:"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "Message from yourname@yourhost on yourtty at hh:mm ...\n"
msgstr "Mesaj de la numele-tău@gazda-ta în terminalul-tău la hh:mm ...\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Any further lines you enter will be copied to the specified user\\(cqs "
"terminal. If the other user wants to reply, they must run B<write> as well."
msgstr ""
"Orice alte linii pe care le introduceți vor fi copiate în terminalul "
"utilizatorului\\(cqs specificat. Dacă celălalt utilizator dorește să "
"răspundă, trebuie să execute și el B<write>."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"When you are done, type an end-of-file or interrupt character. The other "
"user will see the message B<EOF> indicating that the conversation is over."
msgstr ""
"Când ați terminat, introduceți un caracter de sfârșit de fișier sau de "
"întrerupere. Celălalt utilizator va vedea mesajul B<EOF> (sfârșit de fișier) "
"care indică faptul că această conversație s-a încheiat."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"You can prevent people (other than the superuser) from writing to you with "
"the B<mesg>(1) command. Some commands, for example B<nroff>(1) and B<pr>(1), "
"may automatically disallow writing, so that the output they produce "
"isn\\(cqt overwritten."
msgstr ""
"Puteți împiedica persoanele (altele decât superutilizatorul) să vă scrie cu "
"ajutorul comenzii B<mesg>(1). Unele comenzi, de exemplu B<nroff>(1) și "
"B<pr>(1), pot interzice în mod automat scrierea, astfel încât ieșirea pe "
"care o produc să nu fien\\(cqt suprascrisă."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"If the user you want to write to is logged in on more than one terminal, you "
"can specify which terminal to write to by giving the terminal name as the "
"second operand to the B<write> command. Alternatively, you can let B<write> "
"select one of the terminals - it will pick the one with the shortest idle "
"time. This is so that if the user is logged in at work and also dialed up "
"from home, the message will go to the right place."
msgstr ""
"În cazul în care utilizatorul căruia doriți să scrieți este conectat la mai "
"multe terminale, puteți specifica pe care terminal să scrieți, indicând "
"numele terminalului ca al doilea operand al comenzii B<write>. Alternativ, "
"puteți lăsa B<write> să selecteze unul dintre terminale - îl va alege pe cel "
"cu cel mai scurt timp de inactivitate. Aceasta pentru ca, în cazul în care "
"utilizatorul este conectat la serviciu și, de asemenea, a apelat de acasă, "
"mesajul să ajungă la locul potrivit."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"The traditional protocol for writing to someone is that the string I<-o>, "
"either at the end of a line or on a line by itself, means that it\\(cqs the "
"other person\\(cqs turn to talk. The string I<oo> means that the person "
"believes the conversation to be over."
msgstr ""
"Protocolul tradițional pentru a scrie cuiva este că șirul I<-o>, fie la "
"sfârșitul rândului, fie pe un rând de unul singur, înseamnă că este "
"rândul\\(cqs celuilalt\\(cqs să vorbească. Șirul de caractere I<oo> înseamnă "
"că persoana respectivă consideră că discuția s-a încheiat."

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "OPTIONS"
msgstr "OPȚIUNI"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-h>, B<--help>"
msgstr "B<-h>, B<--help>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Display help text and exit."
msgstr "Afișează acest mesaj de ajutor și iese."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: debian-bookworm
msgid "Print version and exit."
msgstr "Afișează informațiile despre versiune și iese."

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "HISTORY"
msgstr "ISTORIC"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "A B<write> command appeared in Version 6 AT&T UNIX."
msgstr "Comanda B<write> a apărut în versiunea 6 AT&T UNIX."

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<mesg>(1), B<talk>(1), B<who>(1)"
msgstr "B<mesg>(1), B<talk>(1), B<who>(1)"

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "REPORTING BUGS"
msgstr "RAPORTAREA ERORILOR"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "For bug reports, use the issue tracker at"
msgstr ""
"Pentru rapoarte de eroare, folosiți sistemul de urmărire al problemelor la"

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "AVAILABILITY"
msgstr "DISPONIBILITATE"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"The B<write> command is part of the util-linux package which can be "
"downloaded from"
msgstr ""
"Comanda B<write> face parte din pachetul util-linux care poate fi descărcat "
"de la adresa"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2022-02-14"
msgstr "14 februarie 2022"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "util-linux 2.37.4"
msgstr "util-linux 2.37.4"

#. type: Plain text
#: opensuse-leap-15-6
msgid "Display version information and exit."
msgstr "Afișează informațiile despre versiune și iese."
