# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.18.1\n"
"POT-Creation-Date: 2022-08-19 18:59+0200\n"
"PO-Revision-Date: 2023-05-19 19:35+0200\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.2.2\n"

#. type: TH
#: archlinux
#, no-wrap
msgid "NEWGRP"
msgstr "NEWGRP"

#. type: TH
#: archlinux
#, no-wrap
msgid "2022-05-11"
msgstr "11 mai 2022"

#. type: TH
#: archlinux
#, no-wrap
msgid "util-linux 2.38.1"
msgstr "util-linux 2.38.1"

#. type: TH
#: archlinux
#, no-wrap
msgid "User Commands"
msgstr "Comenzi utilizator"

#. type: SH
#: archlinux
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: archlinux
msgid "newgrp - log in to a new group"
msgstr "newgrp - autentificare într-un grup nou"

#. type: SH
#: archlinux
#, no-wrap
msgid "SYNOPSIS"
msgstr "REZUMAT"

#. type: Plain text
#: archlinux
msgid "B<newgrp> [I<group>]"
msgstr "B<newgrp> [I<grup>]"

#. type: SH
#: archlinux
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: archlinux
msgid ""
"B<newgrp> changes the group identification of its caller, analogously to "
"B<login>(1). The same person remains logged in, and the current directory is "
"unchanged, but calculations of access permissions to files are performed "
"with respect to the new group ID."
msgstr ""
"B<newgrp> modifică identificatorul de grup al apelantului său, în mod analog "
"cu B<login>(1). Aceeași persoană rămâne conectată, iar directorul curent "
"rămâne neschimbat, dar calculele privind permisiunile de acces la fișiere se "
"efectuează în funcție de noul identificator de grup."

#. type: Plain text
#: archlinux
msgid "If no group is specified, the GID is changed to the login GID."
msgstr ""
"Dacă nu este specificat niciun grup, GID este schimbat cu GID-ul de "
"autentificare."

#. type: SH
#: archlinux
#, no-wrap
msgid "OPTIONS"
msgstr "OPȚIUNI"

#. type: Plain text
#: archlinux
msgid "B<-h>, B<--help>"
msgstr "B<-h>, B<--help>"

#. type: Plain text
#: archlinux
msgid "Display help text and exit."
msgstr "Afișează acest mesaj de ajutor și iese."

#. type: Plain text
#: archlinux
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: archlinux
msgid "Print version and exit."
msgstr "Afișează informațiile despre versiune și iese."

#. type: SH
#: archlinux
#, no-wrap
msgid "FILES"
msgstr "FIȘIERE"

#. type: Plain text
#: archlinux
msgid "I</etc/group>, I</etc/passwd>"
msgstr "I</etc/group>, I</etc/passwd>"

#. type: SH
#: archlinux
#, no-wrap
msgid "AUTHORS"
msgstr "AUTORI"

#. type: Plain text
#: archlinux
msgid "Originally by Michael Haardt. Currently maintained by"
msgstr "Scris inițial de Michael Haardt. În prezent este întreținut de"

#. type: SH
#: archlinux
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: archlinux
msgid "B<login>(1), B<group>(5)"
msgstr "B<login>(1), B<group>(5)"

#. type: SH
#: archlinux
#, no-wrap
msgid "REPORTING BUGS"
msgstr "RAPORTAREA ERORILOR"

#. type: Plain text
#: archlinux
msgid "For bug reports, use the issue tracker at"
msgstr ""
"Pentru rapoarte de eroare, folosiți sistemul de urmărire al problemelor la"

#. type: SH
#: archlinux
#, no-wrap
msgid "AVAILABILITY"
msgstr "DISPONIBILITATE"

#. type: Plain text
#: archlinux
msgid ""
"The B<newgrp> command is part of the util-linux package which can be "
"downloaded from"
msgstr ""
"Comanda B<newgrp> face parte din pachetul util-linux care poate fi descărcat "
"de la adresa"
