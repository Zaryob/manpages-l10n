# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.18.1\n"
"POT-Creation-Date: 2023-10-02 12:03+0200\n"
"PO-Revision-Date: 2023-06-29 08:50+0200\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.2.2\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "GRUB-MKIMAGE"
msgstr "GRUB-MKIMAGE"

#. type: TH
#: archlinux
#, no-wrap
msgid "July 2023"
msgstr "iulie 2023"

#. type: TH
#: archlinux
#, no-wrap
msgid "GRUB 2:2.12rc1-1"
msgstr "GRUB 2:2.12rc1-1"

#. type: TH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "User Commands"
msgstr "Comenzi utilizator"

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "grub-mkimage - make a bootable image of GRUB"
msgstr "grub-mkimage - creează o imagine de GRUB care poate fi pornită"

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "SYNOPSIS"
msgstr "REZUMAT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid ""
"B<grub-mkimage> [I<\\,OPTION\\/>...] [I<\\,OPTION\\/>]... [I<\\,MODULES\\/>]"
msgstr ""
"B<grub-mkimage> [I<\\,OPȚIUNE\\/>...] [I<\\,OPȚIUNE\\/>]... [I<\\,MODULE\\/>]"

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Make a bootable image of GRUB."
msgstr "Creează o imagine de GRUB care poate fi pornită."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-c>, B<--config>=I<\\,FILE\\/>"
msgstr "B<-c>, B<--config>=I<\\,FIȘIER\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "embed FILE as an early config"
msgstr "încorporează FIȘIER ca o configurație timpurie"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-C>, B<--compression=>(xz|none|auto)"
msgstr "B<-C>, B<--compression=>(xz|none|auto)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "choose the compression to use for core image"
msgstr "alege comprimarea de utilizat pentru imaginea de bază"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<--disable-shim-lock>"
msgstr "B<--disable-shim-lock>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "disable shim_lock verifier"
msgstr "dezactivează verificarea shim_lock"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-d>, B<--directory>=I<\\,DIR\\/>"
msgstr "B<-d>, B<--directory>=I<\\,DIRECTOR\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid ""
"use images and modules under DIR [default=/usr/lib/grub/E<lt>platformE<gt>]"
msgstr ""
"utilizează imaginile și modulele din DIR [default=/usr/lib/grub/"
"E<lt>platformaE<gt>]]"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-D>, B<--dtb>=I<\\,FILE\\/>"
msgstr "B<-D>, B<--dtb>=I<\\,FIȘIER\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "embed FILE as a device tree (DTB)"
msgstr "încorporează FIȘIER ca arbore de dispozitive (DTB)"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-k>, B<--pubkey>=I<\\,FILE\\/>"
msgstr "B<-k>, B<--pubkey>=I<\\,FIȘIER\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "embed FILE as public key for signature checking"
msgstr ""
"încorporează FIȘIERUL drept cheie publică pentru verificarea semnăturii"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-m>,                              B<--memdisk>=I<\\,FILE\\/>"
msgstr "B<-m>,                              B<--memdisk>=I<\\,FIȘIER\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "embed FILE as a memdisk image"
msgstr "încorporează FIȘIER ca imagine de disc de memorie"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Implies `-p (memdisk)/boot/grub' and overrides"
msgstr "Implică „-p (disc_de_memorie)/boot/grub” și suprascrie"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "any prefix supplied previously, but the prefix"
msgstr "orice prefix furnizat anterior, însă prefixul"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "itself can be overridden by later options"
msgstr "însuși poate fi înlocuit de opțiunile ulterioare"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-n>, B<--note>"
msgstr "B<-n>, B<--note>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "add NOTE segment for CHRP IEEE1275"
msgstr "adaugă segmentul NOTE pentru CHRP IEEE1275"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-o>, B<--output>=I<\\,FILE\\/>"
msgstr "B<-o>, B<--output>=I<\\,FIȘIER\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "output a generated image to FILE [default=stdout]"
msgstr ""
"trimite imaginea generată la FIȘIER [implicit=stdout(ieșirea standard)]"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-O>, B<--format>=I<\\,FORMAT\\/>"
msgstr "B<-O>, B<--format>=I<\\,FORMAT\\/>"

#. type: Plain text
#: archlinux debian-unstable
msgid ""
"generate an image in FORMAT available formats: i386-coreboot, i386-"
"multiboot, i386-pc, i386-xen_pvh, i386-pc-pxe, i386-pc-eltorito, i386-efi, "
"i386-ieee1275, i386-qemu, x86_64-efi, i386-xen, x86_64-xen, mipsel-yeeloong-"
"flash, mipsel-fuloong2f-flash, mipsel-loongson-elf, powerpc-ieee1275, "
"sparc64-ieee1275-raw, sparc64-ieee1275-cdcore, sparc64-ieee1275-aout, ia64-"
"efi, mips-arc, mipsel-arc, mipsel-qemu_mips-elf, mips-qemu_mips-flash, "
"mipsel-qemu_mips-flash, mips-qemu_mips-elf, arm-uboot, arm-coreboot-"
"vexpress, arm-coreboot-veyron, arm-efi, arm64-efi, loongarch64-efi, riscv32-"
"efi, riscv64-efi"
msgstr ""
"generează o imagine în FORMAT; formate disponibile: i386-coreboot, i386-"
"multiboot, i386-pc, i386-xen_pvh, i386-pc-pxe, i386-pc-eltorito, i386-efi, "
"i386-ieee1275, i386-qemu, x86_64-efi, i386-xen, x86_64-xen, mipsel-yeeloong-"
"flash, mipsel-fuloong2f-flash, mipsel-loongson-elf, powerpc-ieee1275, "
"sparc64-ieee1275-raw, sparc64-ieee1275-cdcore, sparc64-ieee1275-aout, ia64-"
"efi, mips-arc, mipsel-arc, mipsel-qemu_mips-elf, mips-qemu_mips-flash, "
"mipsel-qemu_mips-flash, mips-qemu_mips-elf, arm-uboot, arm-coreboot-"
"vexpress, arm-coreboot-veyron, arm-efi, arm64-efi, loongarch64-efi, riscv32-"
"efi, riscv64-efi"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-p>, B<--prefix>=I<\\,DIR\\/>"
msgstr "B<-p>, B<--prefix>=I<\\,DIRECTOR\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "set prefix directory"
msgstr "stabilește prefixul directorului"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-s>, B<--sbat>=I<\\,FILE\\/>"
msgstr "B<-s>, B<--sbat>=I<\\,FIȘIER\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "SBAT metadata"
msgstr "metadate SBAT"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-v>, B<--verbose>"
msgstr "B<-v>, B<--verbose>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "print verbose messages."
msgstr "afișează mesaje detaliate."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "-?, B<--help>"
msgstr "-?, B<--help>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "give this help list"
msgstr "oferă această listă de ajutor"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<--usage>"
msgstr "B<--usage>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "give a short usage message"
msgstr "oferă un mesaj de utilizare scurt"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "print program version"
msgstr "afișează versiunea programului"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid ""
"Mandatory or optional arguments to long options are also mandatory or "
"optional for any corresponding short options."
msgstr ""
"Argumentele obligatorii sau opționale pentru opțiunile lungi sunt "
"obligatorii sau opționale și pentru opțiunile corespunzătoare scurte."

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "REPORTING BUGS"
msgstr "RAPORTAREA ERORILOR"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Report bugs to E<lt>bug-grub@gnu.orgE<gt>."
msgstr "Raportați erorile la E<lt>bug-grub@gnu.orgE<gt>."

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "B<grub-install>(8), B<grub-mkrescue>(1), B<grub-mknetdir>(8)"
msgstr "B<grub-install>(8), B<grub-mkrescue>(1), B<grub-mknetdir>(8)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid ""
"The full documentation for B<grub-mkimage> is maintained as a Texinfo "
"manual.  If the B<info> and B<grub-mkimage> programs are properly installed "
"at your site, the command"
msgstr ""
"Documentația completă pentru B<grub-mkimage> este menținută ca un manual "
"Texinfo.  Dacă programele B<info> și B<grub-mkimage> sunt instalate corect "
"în sistemul dvs., comanda"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "B<info grub-mkimage>"
msgstr "B<info grub-mkimage>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "should give you access to the complete manual."
msgstr "ar trebui să vă permită accesul la manualul complet."

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "April 2023"
msgstr "aprilie 2023"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "GRUB 2.06-13"
msgstr "GRUB 2.06-13"

#. type: Plain text
#: debian-bookworm
msgid ""
"generate an image in FORMAT available formats: i386-coreboot, i386-"
"multiboot, i386-pc, i386-xen_pvh, i386-pc-pxe, i386-pc-eltorito, i386-efi, "
"i386-ieee1275, i386-qemu, x86_64-efi, i386-xen, x86_64-xen, mipsel-yeeloong-"
"flash, mipsel-fuloong2f-flash, mipsel-loongson-elf, powerpc-ieee1275, "
"sparc64-ieee1275-raw, sparc64-ieee1275-cdcore, sparc64-ieee1275-aout, ia64-"
"efi, mips-arc, mipsel-arc, mipsel-qemu_mips-elf, mips-qemu_mips-flash, "
"mipsel-qemu_mips-flash, mips-qemu_mips-elf, arm-uboot, arm-coreboot-"
"vexpress, arm-coreboot-veyron, arm-efi, arm64-efi, riscv32-efi, riscv64-efi"
msgstr ""
"generează o imagine în FORMAT; formate disponibile: i386-coreboot, i386-"
"multiboot, i386-pc, i386-xen_pvh, i386-pc-pxe, i386-pc-eltorito, i386-efi, "
"i386-ieee1275, i386-qemu, x86_64-efi, i386-xen, x86_64-xen, mipsel-yeeloong-"
"flash, mipsel-fuloong2f-flash, mipsel-loongson-elf, powerpc-ieee1275, "
"sparc64-ieee1275-raw, sparc64-ieee1275-cdcore, sparc64-ieee1275-aout, ia64-"
"efi, mips-arc, mipsel-arc, mipsel-qemu_mips-elf, mips-qemu_mips-flash, "
"mipsel-qemu_mips-flash, mips-qemu_mips-elf, arm-uboot, arm-coreboot-"
"vexpress, arm-coreboot-veyron, arm-efi, arm64-efi, riscv32-efi, riscv64-efi"

#. type: TH
#: debian-unstable
#, no-wrap
msgid "September 2023"
msgstr "septembrie 2023"

#. type: TH
#: debian-unstable
#, no-wrap
msgid "GRUB 2.12~rc1-10"
msgstr "GRUB 2.12~rc1-10"
