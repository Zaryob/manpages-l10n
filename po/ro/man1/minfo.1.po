# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.18.1\n"
"POT-Creation-Date: 2023-08-27 17:08+0200\n"
"PO-Revision-Date: 2023-09-09 23:37+0200\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.2.2\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "minfo"
msgstr "minfo"

#. type: TH
#: archlinux debian-unstable fedora-39 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "21Mar23"
msgstr "21 martie 2023"

#. type: TH
#: archlinux debian-unstable fedora-39 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "mtools-4.0.43"
msgstr "mtools-4.0.43"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Name"
msgstr "Nume"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "minfo - print the parameters of a MSDOS filesystem"
msgstr "minfo - afișează parametrii unui sistem de fișiere MSDOS"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Note\\ of\\ warning"
msgstr "Notă\\ de\\ avertisment"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This manpage has been automatically generated from mtools's texinfo "
"documentation, and may not be entirely accurate or complete.  See the end of "
"this man page for details."
msgstr ""
"Această pagină de manual a fost generată automat din documentația „texinfo” "
"a «mtools» și este posibil să nu fie în întregime exactă sau completă.  "
"Pentru detalii, consultați sfârșitul acestei pagini de manual."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Description"
msgstr "Descriere"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The \\&CW<minfo> command prints the parameters of a MS-DOS file system, such "
"as number of sectors, heads and cylinders.  It also prints an mformat "
"command line which can be used to create a similar MS-DOS file system on "
"another media.  However, this doesn't work with 2m or XDF media, and with MS-"
"DOS 1.0 file systems"
msgstr ""
"Comanda \\&CW<minfo> afișează parametrii unui sistem de fișiere MS-DOS, cum "
"ar fi numărul de sectoare, de capete și de cilindri.  De asemenea, imprimă o "
"linie de comandă mformat care poate fi utilizată pentru a crea un sistem de "
"fișiere MS-DOS similar pe un alt suport.  Cu toate acestea, nu funcționează "
"cu suporturi 2m sau XDF, și nici cu sistemele de fișiere MS-DOS 1.0."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "I<\\&>\\&CW<minfo> I<drive>:\n"
msgstr "I<\\&>\\&CW<minfo> I<unitatea>:\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Minfo supports the following option:"
msgstr "Minfo acceptă următoarea opțiune:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "\\&\\&CW<v>\\ "
msgstr "\\&\\&CW<v>\\ "

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Prints a hexdump of the boot sector, in addition to the other information"
msgstr ""
"Afișează o descărcare hexazecimală a sectorului de pornire, în plus față de "
"celelalte informații."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "See\\ Also"
msgstr "Consultați\\ și"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Mtools' texinfo doc"
msgstr "Documentația texinfo de Mtools"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Viewing\\ the\\ texi\\ doc"
msgstr "Vizualizarea\\ documentului\\ texi"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This manpage has been automatically generated from mtools's texinfo "
"documentation. However, this process is only approximative, and some items, "
"such as crossreferences, footnotes and indices are lost in this translation "
"process.  Indeed, these items have no appropriate representation in the "
"manpage format.  Moreover, not all information has been translated into the "
"manpage version.  Thus I strongly advise you to use the original texinfo "
"doc.  See the end of this manpage for instructions how to view the texinfo "
"doc."
msgstr ""
"Această pagină de manual a fost generată automat din documentația texinfo a "
"„mtools”. Cu toate acestea, acest proces este doar aproximativ, iar unele "
"elemente, cum ar fi referințele încrucișate, notele de subsol și indicii, se "
"pierd în acest proces de conversie.  Într-adevăr, aceste elemente nu au o "
"reprezentare adecvată în formatul paginii de manual.  În plus, nu toate "
"informațiile au fost convertite în versiunea pentru pagina de manual.  Prin "
"urmare, vă sfătuiesc cu tărie să folosiți documentul texinfo original.  "
"Consultați sfârșitul acestei pagini de manual pentru instrucțiuni privind "
"modul de vizualizare a documentului texinfo."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "* \\ \\ "
msgstr "* \\ \\ "

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"To generate a printable copy from the texinfo doc, run the following "
"commands:"
msgstr ""
"Pentru a genera o copie imprimabilă din documentul texinfo, executați "
"următoarele comenzi:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<    ./configure; make dvi; dvips mtools.dvi>\n"
msgstr "B<    ./configure; make dvi; dvips mtools.dvi>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "To generate a html copy, run:"
msgstr "Pentru a genera o copie html, executați:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<    ./configure; make html>\n"
msgstr "B<    ./configure; make html>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"\\&A premade html can be found at \\&\\&CW<\\(ifhttp://www.gnu.org/software/"
"mtools/manual/mtools.html\\(is>"
msgstr ""
"\\&Un fișier html preconstruit poate fi găsit la adresa \\&\\&CW<\\(ifhttp://"
"www.gnu.org/software/mtools/manual/mtools.html\\(is>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "To generate an info copy (browsable using emacs' info mode), run:"
msgstr ""
"Pentru a genera o copie info (care poate fi răsfoită folosind modul info al "
"lui emacs), executați:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<    ./configure; make info>\n"
msgstr "B<    ./configure; make info>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The texinfo doc looks most pretty when printed or as html.  Indeed, in the "
"info version certain examples are difficult to read due to the quoting "
"conventions used in info."
msgstr ""
"Documentul texinfo arată cel mai bine atunci când este imprimat sau în "
"format html.  Într-adevăr, în versiunea info, anumite exemple sunt greu de "
"citit din cauza convențiilor de folosire a ghilimelelor utilizate în «info»."

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "10Jul21"
msgstr "10 iulie 2021"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "mtools-4.0.32"
msgstr "mtools-4.0.32"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "06Aug21"
msgstr "6 august 2021"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "mtools-4.0.35"
msgstr "mtools-4.0.35"
