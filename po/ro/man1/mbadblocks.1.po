# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.18.1\n"
"POT-Creation-Date: 2023-08-27 17:07+0200\n"
"PO-Revision-Date: 2023-09-09 23:34+0200\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.2.2\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "mbadblocks"
msgstr "mbadblocks"

#. type: TH
#: archlinux debian-unstable fedora-39 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "21Mar23"
msgstr "21 martie 2023"

#. type: TH
#: archlinux debian-unstable fedora-39 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "mtools-4.0.43"
msgstr "mtools-4.0.43"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Name"
msgstr "Nume"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "mbadblocks - tests a floppy disk, and marks the bad blocks in the FAT"
msgstr ""
"mbadblocks - testează o dischetă și marchează blocurile defectuoase din FAT"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Note\\ of\\ warning"
msgstr "Notă\\ de\\ avertisment"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This manpage has been automatically generated from mtools's texinfo "
"documentation, and may not be entirely accurate or complete.  See the end of "
"this man page for details."
msgstr ""
"Această pagină de manual a fost generată automat din documentația „texinfo” "
"a «mtools» și este posibil să nu fie în întregime exactă sau completă.  "
"Pentru detalii, consultați sfârșitul acestei pagini de manual."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Description"
msgstr "Descriere"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The \\&CW<mbadblocks> command is used to mark some clusters on an MS-DOS "
"filesystem bad. It has the following syntax:"
msgstr ""
"Comanda \\&CW<mbadblocks> este utilizată pentru a marca anumite clustere de "
"pe un sistem de fișiere MS-DOS ca fiind defectuoase. Ea are următoarea "
"sintaxă:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"\\&\\&CW<mbadblocks> [\\&CW<-s> I<sectorlist>|\\&CW<-c> I<clusterlist>|-w] "
"I<drive>\\&CW<:>"
msgstr ""
"\\&\\&CW<mbadblocks> [\\&CW<-s> I<listă-sectoare>|\\&CW<-c> I<listă-"
"clustere>|-w] I<unitatea(discheta)>\\&CW<:>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If no command line flags are supplied, \\&CW<Mbadblocks> scans an MS-DOS "
"filesystem for bad blocks by simply trying to read them and flag them if "
"read fails. All blocks that are unused are scanned, and if detected bad are "
"marked as such in the FAT."
msgstr ""
"Dacă nu se furnizează niciun indicator în linia de comandă, "
"\\&CW<mbadblocks> scanează un sistem de fișiere MS-DOS pentru blocuri "
"defecte, încercând pur și simplu să le citească și să le marcheze dacă "
"citirea eșuează. Toate blocurile nefolosite sunt scanate și, dacă sunt "
"detectate ca fiind defectuoase, sunt marcate ca atare în FAT."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This command is intended to be used right after \\&CW<mformat>.  It is not "
"intended to salvage data from bad disks."
msgstr ""
"Această comandă este destinată a fi utilizată imediat după \\&CW<mformat>.  "
"Nu este destinată salvării datelor de pe discuri defecte."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Command\\ line\\ options"
msgstr "Opțiuni în linia de comandă"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "\\&\\&CW<c\\ >I<file>\\&\\ "
msgstr "\\&\\&CW<c\\ >I<fișier>\\&\\ "

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Use a list of bad clusters, rather than scanning for bad clusters itself."
msgstr ""
"Utilizează o listă de clustere defectuoase, mai degrabă decât să caute "
"clusterele defectuoase în sine."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "\\&\\&CW<s\\ >I<file>\\&\\ "
msgstr "\\&\\&CW<s\\ >I<fișier>\\&\\ "

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Use a list of bad sectors (counted from beginning of filesystem), rather "
"than trying for bad clusters itself."
msgstr ""
"Utilizează o listă de sectoare defectuoase (numărate de la începutul "
"sistemului de fișiere), mai degrabă decât să încerce să caute clusterele "
"defectuoase în sine."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "\\&\\&CW<w>\\ "
msgstr "\\&\\&CW<w>\\ "

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Write a random pattern to each cluster, then read it back and flag cluster "
"as bad if mismatch. Only free clusters are tested in such a way, so any file "
"data is preserved."
msgstr ""
"Scrie un model aleatoriu în fiecare cluster, apoi îl citește din nou și "
"marchează clusterul ca fiind defectuos dacă nu corespunde. Numai clusterele "
"libere sunt testate în acest mod, astfel încât toate datele din fișiere sunt "
"păstrate."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Bugs"
msgstr "ERORI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"\\&\\&CW<Mbadblocks> should (but doesn't yet :-( ) also try to salvage bad "
"blocks which are in use by reading them repeatedly, and then mark them bad."
msgstr ""
"\\&\\&CW<mbadblocks> ar trebui (dar nu o face încă :-( ) să încerce, de "
"asemenea, să salveze blocurile defectuoase care sunt în uz, citindu-le în "
"mod repetat, și apoi să le marcheze ca fiind defectuoase."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "See\\ Also"
msgstr "Consultați\\ și"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Mtools' texinfo doc"
msgstr "Documentația texinfo de Mtools"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Viewing\\ the\\ texi\\ doc"
msgstr "Vizualizarea\\ documentului\\ texi"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This manpage has been automatically generated from mtools's texinfo "
"documentation. However, this process is only approximative, and some items, "
"such as crossreferences, footnotes and indices are lost in this translation "
"process.  Indeed, these items have no appropriate representation in the "
"manpage format.  Moreover, not all information has been translated into the "
"manpage version.  Thus I strongly advise you to use the original texinfo "
"doc.  See the end of this manpage for instructions how to view the texinfo "
"doc."
msgstr ""
"Această pagină de manual a fost generată automat din documentația texinfo a "
"„mtools”. Cu toate acestea, acest proces este doar aproximativ, iar unele "
"elemente, cum ar fi referințele încrucișate, notele de subsol și indicii, se "
"pierd în acest proces de conversie.  Într-adevăr, aceste elemente nu au o "
"reprezentare adecvată în formatul paginii de manual.  În plus, nu toate "
"informațiile au fost convertite în versiunea pentru pagina de manual.  Prin "
"urmare, vă sfătuiesc cu tărie să folosiți documentul texinfo original.  "
"Consultați sfârșitul acestei pagini de manual pentru instrucțiuni privind "
"modul de vizualizare a documentului texinfo."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "* \\ \\ "
msgstr "* \\ \\ "

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"To generate a printable copy from the texinfo doc, run the following "
"commands:"
msgstr ""
"Pentru a genera o copie imprimabilă din documentul texinfo, executați "
"următoarele comenzi:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<    ./configure; make dvi; dvips mtools.dvi>\n"
msgstr "B<    ./configure; make dvi; dvips mtools.dvi>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "To generate a html copy, run:"
msgstr "Pentru a genera o copie html, executați:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<    ./configure; make html>\n"
msgstr "B<    ./configure; make html>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"\\&A premade html can be found at \\&\\&CW<\\(ifhttp://www.gnu.org/software/"
"mtools/manual/mtools.html\\(is>"
msgstr ""
"\\&Un fișier html preconstruit poate fi găsit la adresa \\&\\&CW<\\(ifhttp://"
"www.gnu.org/software/mtools/manual/mtools.html\\(is>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "To generate an info copy (browsable using emacs' info mode), run:"
msgstr ""
"Pentru a genera o copie info (care poate fi răsfoită folosind modul info al "
"lui emacs), executați:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<    ./configure; make info>\n"
msgstr "B<    ./configure; make info>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The texinfo doc looks most pretty when printed or as html.  Indeed, in the "
"info version certain examples are difficult to read due to the quoting "
"conventions used in info."
msgstr ""
"Documentul texinfo arată cel mai bine atunci când este imprimat sau în "
"format html.  Într-adevăr, în versiunea info, anumite exemple sunt greu de "
"citit din cauza convențiilor de folosire a ghilimelelor utilizate în «info»."

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "10Jul21"
msgstr "10 iulie 2021"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "mtools-4.0.32"
msgstr "mtools-4.0.32"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "06Aug21"
msgstr "6 august 2021"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "mtools-4.0.35"
msgstr "mtools-4.0.35"
