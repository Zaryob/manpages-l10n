# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.19.0\n"
"POT-Creation-Date: 2023-08-27 17:05+0200\n"
"PO-Revision-Date: 2023-07-06 18:47+0200\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.2.2\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "libc"
msgstr "libc"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "2023-02-05"
msgstr "5 februarie 2023"

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Pagini de manual de Linux 6.05.01"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "libc - overview of standard C libraries on Linux"
msgstr "libc - prezentare generală a bibliotecilor C standard din Linux"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The term \\[lq]libc\\[rq] is commonly used as a shorthand for the "
"\\[lq]standard C library\\[rq] a library of standard functions that can be "
"used by all C programs (and sometimes by programs in other languages).  "
"Because of some history (see below), use of the term \\[lq]libc\\[rq] to "
"refer to the standard C library is somewhat ambiguous on Linux."
msgstr ""
"Termenul „libc” este utilizat în mod obișnuit ca o prescurtare pentru "
"„biblioteca standard C” o bibliotecă de funcții standard care poate fi "
"utilizată de toate programele C (și uneori de programe în alte limbaje).  "
"Din cauza unor aspecte istorice (a se vedea mai jos), utilizarea termenului "
"„libc” pentru a se referi la biblioteca standard C este oarecum ambiguă în "
"Linux."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "glibc"
msgstr "glibc"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"By far the most widely used C library on Linux is the E<.UR http://www.gnu."
"org\\:/software\\:/libc/> GNU C Library E<.UE ,> often referred to as "
"I<glibc>.  This is the C library that is nowadays used in all major Linux "
"distributions.  It is also the C library whose details are documented in the "
"relevant pages of the I<man-pages> project (primarily in Section 3 of the "
"manual).  Documentation of glibc is also available in the glibc manual, "
"available via the command I<info libc>.  Release 1.0 of glibc was made in "
"September 1992.  (There were earlier 0.x releases.)  The next major release "
"of glibc was 2.0, at the beginning of 1997."
msgstr ""
"De departe, cea mai utilizată bibliotecă C în Linux este biblioteca E<.UR "
"http://www.gnu.org\\:/software\\:/libc/> GNU C Library E<.UE ,>, denumită "
"adesea I<glibc>.  Aceasta este biblioteca C care este utilizată în prezent "
"în toate distribuțiile Linux majore.  Este, de asemenea, biblioteca C ale "
"cărei detalii sunt documentate în paginile relevante ale proiectului I<man-"
"pages> (în principal în secțiunea 3 a manualului).  Documentația despre "
"glibc este de asemenea disponibilă în manualul glibc, disponibil prin "
"intermediul comenzii I<info libc>.  Versiunea 1.0 a glibc a fost realizată "
"în septembrie 1992.  (Au existat versiuni anterioare 0.x.) Următoarea "
"versiune majoră a glibc a fost 2.0, la începutul anului 1997."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The pathname I</lib/libc.so.6> (or something similar)  is normally a "
"symbolic link that points to the location of the glibc library, and "
"executing this pathname will cause glibc to display various information "
"about the version installed on your system."
msgstr ""
"Numele de rută I</lib/libc.so.6> (sau ceva asemănător) este în mod normal o "
"legătură simbolică care indică locația bibliotecii glibc, iar executarea "
"acestui nume de rută va face ca glibc să afișeze diverse informații despre "
"versiunea instalată pe sistemul dumneavoastră."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Linux libc"
msgstr "Linux libc"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"In the early to mid 1990s, there was for a while I<Linux libc>, a fork of "
"glibc 1.x created by Linux developers who felt that glibc development at the "
"time was not sufficing for the needs of Linux.  Often, this library was "
"referred to (ambiguously) as just \\[lq]libc\\[rq].  Linux libc released "
"major versions 2, 3, 4, and 5, as well as many minor versions of those "
"releases.  Linux libc4 was the last version to use the a.out binary format, "
"and the first version to provide (primitive) shared library support.  Linux "
"libc 5 was the first version to support the ELF binary format; this version "
"used the shared library soname I<libc.so.5>.  For a while, Linux libc was "
"the standard C library in many Linux distributions."
msgstr ""
"La începutul și până la mijlocul anilor 1990, a existat pentru o vreme "
"I<Linux libc>, o bifurcație a glibc 1.x creată de dezvoltatorii Linux care "
"au considerat că dezvoltarea glibc de la acea vreme nu era suficientă pentru "
"nevoile Linux.  Adesea, această bibliotecă era denumită (în mod ambiguu) "
"doar „]libc”.  Linux libc a lansat versiunile majore 2, 3, 4 și 5, precum și "
"multe versiuni minore ale acestor versiuni.  Linux libc4 a fost ultima "
"versiune care a folosit formatul binar a.out și prima versiune care a oferit "
"suport (primitiv) pentru biblioteci partajate.  Linux libc 5 a fost prima "
"versiune care a acceptat formatul binar ELF; această versiune a folosit "
"biblioteca partajată soname I<libc.so.5>.  Pentru o vreme, Linux libc a fost "
"biblioteca C standard în multe distribuții Linux."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"However, notwithstanding the original motivations of the Linux libc effort, "
"by the time glibc 2.0 was released (in 1997), it was clearly superior to "
"Linux libc, and all major Linux distributions that had been using Linux libc "
"soon switched back to glibc.  To avoid any confusion with Linux libc "
"versions, glibc 2.0 and later used the shared library soname I<libc.so.6>."
msgstr ""
"Cu toate acestea, în pofida motivațiilor inițiale ale efortului Linux libc, "
"în momentul în care a fost lansat glibc 2.0 (în 1997), acesta era net "
"superior lui Linux libc, iar toate distribuțiile Linux majore care utilizau "
"Linux libc au revenit curând la glibc.  Pentru a evita orice confuzie cu "
"versiunile Linux libc, glibc 2.0 și versiunile ulterioare au folosit "
"biblioteca partajată soname I<libc.so.6>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Since the switch from Linux libc to glibc 2.0 occurred long ago, I<man-"
"pages> no longer takes care to document Linux libc details.  Nevertheless, "
"the history is visible in vestiges of information about Linux libc that "
"remain in a few manual pages, in particular, references to I<libc4> and "
"I<libc5>."
msgstr ""
"Deoarece trecerea de la Linux libc la glibc 2.0 a avut loc cu mult timp în "
"urmă, I<man-pages> nu mai se preocupă să documenteze detaliile Linux libc.  "
"Cu toate acestea, istoricul este vizibil în vestigiile de informații despre "
"Linux libc care au rămas în câteva pagini de manual, în special referințele "
"la I<libc4> și I<libc5>."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Other C libraries"
msgstr "Alte biblioteci C"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"There are various other less widely used C libraries for Linux.  These "
"libraries are generally smaller than glibc, both in terms of features and "
"memory footprint, and often intended for building small binaries, perhaps "
"targeted at development for embedded Linux systems.  Among such libraries "
"are E<.UR http://www\\:.uclibc\\:.org/> I<uClibc> E<.UE ,> E<.UR http://"
"www\\:.fefe\\:.de/\\:dietlibc/> I<dietlibc> E<.UE ,> and E<.UR http://www\\:."
"musl-libc\\:.org/> I<musl libc> E<.UE .> Details of these libraries are "
"covered by the I<man-pages> project, where they are known."
msgstr ""
"Există diverse alte biblioteci C mai puțin utilizate pe scară largă pentru "
"Linux.  Aceste biblioteci sunt, în general, mai mici decât glibc, atât din "
"punct de vedere al funcțiilor, cât și al amprentei de memorie, și sunt "
"adesea destinate construirii de binare mici, poate destinate dezvoltării "
"pentru sisteme Linux integrate.  Printre aceste biblioteci se numără E<.UR "
"http://www\\:.uclibc\\:.org/> I<uClibc> E<.UE ,> E<.UR http://www\\:.fefe\\:."
"de/\\:dietlibc/> I<dietlibc> E<.UE ,> și E<.UR http://www\\:.musl-libc\\:."
"org/> I<musl libc> E<.UE .> Detaliile acestor biblioteci sunt acoperite de "
"proiectul I<man-pages>, unde sunt cunoscute."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<syscalls>(2), B<getauxval>(3), B<proc>(5), B<feature_test_macros>(7), "
"B<man-pages>(7), B<standards>(7), B<vdso>(7)"
msgstr ""
"B<syscalls>(2), B<getauxval>(3), B<proc>(5), B<feature_test_macros>(7), "
"B<man-pages>(7), B<standards>(7), B<vdso>(7)"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Pagini de manual de Linux 6.03"

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.05"
msgstr "Pagini de manual de Linux 6.05"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "LIBC"
msgstr "LIBC"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2016-12-12"
msgstr "12 decembrie 2016"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Manualul programatorului Linux"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"The term \"libc\" is commonly used as a shorthand for the \"standard C "
"library\", a library of standard functions that can be used by all C "
"programs (and sometimes by programs in other languages).  Because of some "
"history (see below), use of the term \"libc\" to refer to the standard C "
"library is somewhat ambiguous on Linux."
msgstr ""
"Termenul „libc” este utilizat în mod obișnuit ca o prescurtare pentru "
"„biblioteca standard C”, o bibliotecă de funcții standard care poate fi "
"utilizată de toate programele C (și, uneori, de programe în alte limbaje).  "
"Din cauza unui anumit istoric (a se vedea mai jos), utilizarea termenului "
"„libc” pentru a se referi la biblioteca standard C este oarecum ambiguă pe "
"Linux."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"By far the most widely used C library on Linux is the GNU C Library E<.UR "
"http://www.gnu.org\\:/software\\:/libc/> E<.UE ,> often referred to as "
"I<glibc>.  This is the C library that is nowadays used in all major Linux "
"distributions.  It is also the C library whose details are documented in the "
"relevant pages of the I<man-pages> project (primarily in Section 3 of the "
"manual).  Documentation of glibc is also available in the glibc manual, "
"available via the command I<info libc>.  Release 1.0 of glibc was made in "
"September 1992.  (There were earlier 0.x releases.)  The next major release "
"of glibc was 2.0, at the beginning of 1997."
msgstr ""
"De departe, cea mai utilizată bibliotecă C în Linux este GNU C Library E<.UR "
"http://www.gnu.org\\:/software\\:/libc/> E<.UE ,>, denumită adesea "
"I<glibc>.  Aceasta este biblioteca C care este utilizată în prezent în toate "
"distribuțiile Linux importante.  Este, de asemenea, biblioteca C ale cărei "
"detalii sunt documentate în paginile relevante ale proiectului I<man-pages> "
"(în principal în secțiunea 3 a manualului).  Documentația despre glibc este "
"de asemenea disponibilă în manualul glibc, disponibil prin intermediul "
"comenzii I<info libc>.  Versiunea 1.0 a glibc a fost realizată în septembrie "
"1992.  (Au existat versiuni anterioare 0.x.) Următoarea versiune majoră a "
"glibc a fost 2.0, la începutul anului 1997."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"The pathname I</lib/libc.so.6> (or something similar) is normally a symbolic "
"link that points to the location of the glibc library, and executing this "
"pathname will cause glibc to display various information about the version "
"installed on your system."
msgstr ""
"Numele de rută I</lib/libc.so.6> (sau ceva similar) este, în mod normal, o "
"legătură simbolică care indică locația bibliotecii glibc, iar executarea "
"acestui nume de rută va face ca glibc să afișeze diverse informații despre "
"versiunea instalată pe sistemul dumneavoastră."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"In the early to mid 1990s, there was for a while I<Linux libc>, a fork of "
"glibc 1.x created by Linux developers who felt that glibc development at the "
"time was not sufficing for the needs of Linux.  Often, this library was "
"referred to (ambiguously) as just \"libc\".  Linux libc released major "
"versions 2, 3, 4, and 5, as well as many minor versions of those releases.  "
"Linux libc4 was the last version to use the a.out binary format, and the "
"first version to provide (primitive) shared library support.  Linux libc 5 "
"was the first version to support the ELF binary format; this version used "
"the shared library soname I<libc.so.5>.  For a while, Linux libc was the "
"standard C library in many Linux distributions."
msgstr ""
"La începutul și până la mijlocul anilor 1990, a existat pentru o vreme "
"I<Linux libc>, o bifurcație a glibc 1.x creată de dezvoltatorii Linux care "
"au considerat că dezvoltarea glibc de la acea vreme nu era suficientă pentru "
"nevoile Linux.  Adesea, această bibliotecă era denumită (în mod ambiguu) "
"doar „libc”.  Linux libc a lansat versiunile majore 2, 3, 4 și 5, precum și "
"multe versiuni minore ale acestor versiuni.  Linux libc4 a fost ultima "
"versiune care a folosit formatul binar a.out și prima versiune care a oferit "
"suport pentru biblioteci partajate (primitiv).  Linux libc 5 a fost prima "
"versiune care a acceptat formatul binar ELF; această versiune a folosit "
"biblioteca partajată soname I<libc.so.5>.  Pentru o vreme, Linux libc a fost "
"biblioteca C standard în multe distribuții Linux."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"There are various other less widely used C libraries for Linux.  These "
"libraries are generally smaller than glibc, both in terms of features and "
"memory footprint, and often intended for building small binaries, perhaps "
"targeted at development for embedded Linux systems.  Among such libraries "
"are E<.UR http://www.uclibc.org/> I<uClibc> E<.UE ,> E<.UR http://www.fefe."
"de/dietlibc/> I<dietlibc> E<.UE ,> and E<.UR http://www.musl-libc.org/> "
"I<musl libc> E<.UE .> Details of these libraries are covered by the I<man-"
"pages> project, where they are known."
msgstr ""
"Există diverse alte biblioteci C mai puțin utilizate pe scară largă pentru "
"Linux.  Aceste biblioteci sunt, în general, mai mici decât glibc, atât din "
"punct de vedere al funcțiilor, cât și al amprentei de memorie, și sunt "
"adesea destinate construirii de binare mici, poate destinate dezvoltării "
"pentru sisteme Linux integrate.  Printre aceste biblioteci se numără E<.UR "
"http://www.uclibc.org/> I<uClibc> E<.UE ,> E<.UR http://www.fefe.de/dietlibc/"
"> I<dietlibc> E<.UE ,> și E<.UR http://www.musl-libc.org/> I<musl libc> E<."
"UE .> Detaliile acestor biblioteci sunt acoperite de proiectul I<man-pages>, "
"unde sunt cunoscute."

# R-GC, scrie:
# COLOPHON (în eng.) = COLOFON (în rom.)
# *****
# Colofon = Notă la sfârșitul unei publicații sau
# pe verso foii de titlu, cuprinzând datele editoriale.
# sau:
# Colofon =  Însemnare la sfârșitul unei cărți în
# epoca manuscriselor și incunabulelor, cuprinzând
# date privind tipograful, locul unde a lucrat,
# autorul și titlul lucrării.
# sau:
# Colofon = Notă, însemnare finală a unei cărți,
# care reproduce sau completează cele spuse în titlu.
# ===============
# M-am decis pentru:
# COLOFON -- NOTĂ FINALĂ
#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "COLOPHON"
msgstr "COLOFON -- NOTĂ FINALĂ"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Această pagină face parte din versiunea 4.16 a proiectului Linux I<man-"
"pages>.  O descriere a proiectului, informații despre raportarea erorilor și "
"cea mai recentă versiune a acestei pagini pot fi găsite la \\%https://www."
"kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Pagini de manual de Linux 6.04"
