# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.19.0\n"
"POT-Creation-Date: 2023-08-27 17:11+0200\n"
"PO-Revision-Date: 2023-09-10 00:00+0200\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.2.2\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "nptl"
msgstr "nptl"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "2023-02-05"
msgstr "5 februarie 2023"

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Pagini de manual de Linux 6.05.01"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "nptl - Native POSIX Threads Library"
msgstr ""
"nptl - biblioteca nativă POSIX pentru fire de execuție „Native POSIX Threads "
"Library”"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"NPTL (Native POSIX Threads Library)  is the GNU C library POSIX threads "
"implementation that is used on modern Linux systems."
msgstr ""
"NPTL (Native POSIX Threads Library) este o implementare POSIX de fire de "
"execuție a bibliotecii GNU C care este utilizată în sistemele Linux moderne."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NPTL and signals"
msgstr "NPTL și semnalele"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"NPTL makes internal use of the first two real-time signals (signal numbers "
"32 and 33).  One of these signals is used to support thread cancelation and "
"POSIX timers (see B<timer_create>(2)); the other is used as part of a "
"mechanism that ensures all threads in a process always have the same UIDs "
"and GIDs, as required by POSIX.  These signals cannot be used in "
"applications."
msgstr ""
"NPTL utilizează intern primele două semnale în timp real (numerele de semnal "
"32 și 33).  Unul dintre aceste semnale este utilizat pentru a susține "
"anularea firelor de execuție și cronometrele POSIX (a se vedea "
"B<timer_create>(2)); celălalt este utilizat ca parte a unui mecanism care "
"asigură că toate firele de execuție dintr-un proces au întotdeauna aceleași "
"UID și GID, așa cum este cerut de POSIX.  Aceste semnale nu pot fi utilizate "
"în aplicații."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"To prevent accidental use of these signals in applications, which might "
"interfere with the operation of the NPTL implementation, various glibc "
"library functions and system call wrapper functions attempt to hide these "
"signals from applications, as follows:"
msgstr ""
"Pentru a preveni utilizarea accidentală a acestor semnale în aplicații, care "
"ar putea să interfereze cu funcționarea implementării NPTL, diverse funcții "
"de bibliotecă glibc și funcții de învăluire a apelurilor de sistem încearcă "
"să ascundă aceste semnale de aplicații, după cum urmează:"

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "\\[bu]"
msgstr "\\[bu]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<SIGRTMIN> is defined with the value 34 (rather than 32)."
msgstr "B<SIGRTMIN> este definit cu valoarea 34 (în loc de 32)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<sigwaitinfo>(2), B<sigtimedwait>(2), and B<sigwait>(3)  interfaces "
"silently ignore requests to wait for these two signals if they are specified "
"in the signal set argument of these calls."
msgstr ""
"Interfețele B<sigwaitinfo>(2), B<sigtimedwait>(2) și B<sigwait>(3) ignoră în "
"mod silențios cererile de așteptare a acestor două semnale dacă acestea sunt "
"specificate în argumentul set de semnale al acestor apeluri."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<sigprocmask>(2)  and B<pthread_sigmask>(3)  interfaces silently ignore "
"attempts to block these two signals."
msgstr ""
"Interfețele B<sigprocmask>(2) și B<pthread_sigmask>(3) ignoră în mod "
"silențios încercările de blocare a acestor două semnale."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<sigaction>(2), B<pthread_kill>(3), and B<pthread_sigqueue>(3)  "
"interfaces fail with the error B<EINVAL> (indicating an invalid signal "
"number) if these signals are specified."
msgstr ""
"Interfețele B<sigaction>(2), B<pthread_kill>(3) și B<pthread_sigqueue>(3) "
"eșuează cu eroarea B<EINVAL> (care indică un număr de semnal nevalid) dacă "
"sunt specificate aceste semnale."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<sigfillset>(3)  does not include these two signals when it creates a full "
"signal set."
msgstr ""
"B<sigfillset>(3) nu include aceste două semnale atunci când creează un set "
"complet de semnale."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NPTL and process credential changes"
msgstr "NPTL și modificări ale acreditărilor de proces"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"At the Linux kernel level, credentials (user and group IDs) are a per-thread "
"attribute.  However, POSIX requires that all of the POSIX threads in a "
"process have the same credentials.  To accommodate this requirement, the "
"NPTL implementation wraps all of the system calls that change process "
"credentials with functions that, in addition to invoking the underlying "
"system call, arrange for all other threads in the process to also change "
"their credentials."
msgstr ""
"La nivelul nucleului Linux, acreditările (ID-urile de utilizator și de grup) "
"sunt un atribut pentru fiecare fir de execuție.  Cu toate acestea, POSIX "
"impune ca toate firele POSIX dintr-un proces să aibă aceleași credențiale.  "
"Pentru a se conforma acestei cerințe, implementarea NPTL include toate "
"apelurile de sistem care modifică acreditările procesului cu funcții care, "
"pe lângă apelul de sistem de bază, se ocupă de modificarea acreditărilor "
"tuturor celorlalte fire din proces."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The implementation of each of these system calls involves the use of a real-"
"time signal that is sent (using B<tgkill>(2))  to each of the other threads "
"that must change its credentials.  Before sending these signals, the thread "
"that is changing credentials saves the new credential(s) and records the "
"system call being employed in a global buffer.  A signal handler in the "
"receiving thread(s) fetches this information and then uses the same system "
"call to change its credentials."
msgstr ""
"Implementarea fiecăruia dintre aceste apeluri de sistem implică utilizarea "
"unui semnal în timp real care este trimis (folosind B<tgkill>(2)) către "
"fiecare dintre celelalte fire care trebuie să își schimbe acreditările.  "
"Înainte de a trimite aceste semnale, firul care schimbă acreditările "
"salvează noile acreditări și înregistrează apelul de sistem utilizat într-o "
"memorie tampon globală.  Un gestionar de semnal din firul (firele) de "
"execuție receptoar(e) preia aceste informații și apoi utilizează același "
"apel de sistem pentru a-și schimba acreditările."

#.  FIXME .
#.  Maybe say something about vfork() not being serialized wrt set*id() APIs?
#.  https://sourceware.org/bugzilla/show_bug.cgi?id=14749
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Wrapper functions employing this technique are provided for B<setgid>(2), "
"B<setuid>(2), B<setegid>(2), B<seteuid>(2), B<setregid>(2), B<setreuid>(2), "
"B<setresgid>(2), B<setresuid>(2), and B<setgroups>(2)."
msgstr ""
"Funcțiile de învăluire care utilizează această tehnică sunt furnizate pentru "
"B<setgid>(2), B<setuid>(2), B<setegid>(2), B<seteuid>(2), B<setregid>(2), "
"B<setreuid>(2), B<setresgid>(2), B<setresuid>(2) și B<setgroups>(2)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"For details of the conformance of NPTL to the POSIX standard, see "
"B<pthreads>(7)."
msgstr ""
"Pentru detalii privind conformitatea NPTL cu standardul POSIX, a se vedea "
"B<pthreads>(7)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "NOTE"

#.  See POSIX.1-2008 specification of pthread_mutexattr_init()
#.  See sysdeps/x86/bits/pthreadtypes.h
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"POSIX says that any thread in any process with access to the memory "
"containing a process-shared (B<PTHREAD_PROCESS_SHARED>)  mutex can operate "
"on that mutex.  However, on 64-bit x86 systems, the mutex definition for "
"x86-64 is incompatible with the mutex definition for i386, meaning that 32-"
"bit and 64-bit binaries can't share mutexes on x86-64 systems."
msgstr ""
"POSIX spune că orice fir din orice proces care are acces la memoria care "
"conține un mutex partajat de proces (B<PTHREAD_PROCESS_SHARED>) poate opera "
"pe acel mutex.  Cu toate acestea, pe sistemele x86 pe 64 de biți, definiția "
"mutex-ului pentru x86-64 este incompatibilă cu definiția mutex-ului pentru "
"i386, ceea ce înseamnă că binarele pe 32 de biți și 64 de biți nu pot "
"partaja mutex-uri pe sistemele x86-64."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<credentials>(7), B<pthreads>(7), B<signal>(7), B<standards>(7)"
msgstr "B<credentials>(7), B<pthreads>(7), B<signal>(7), B<standards>(7)"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Pagini de manual de Linux 6.03"

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.05"
msgstr "Pagini de manual de Linux 6.05"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "NPTL"
msgstr "NPTL"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2015-08-08"
msgstr "8 august 2015"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Manualul programatorului Linux"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"NPTL makes internal use of the first two real-time signals (signal numbers "
"32 and 33).  One of these signals is used to support thread cancellation and "
"POSIX timers (see B<timer_create>(2)); the other is used as part of a "
"mechanism that ensures all threads in a process always have the same UIDs "
"and GIDs, as required by POSIX.  These signals cannot be used in "
"applications."
msgstr ""
"NPTL utilizează intern primele două semnale în timp real (numerele de semnal "
"32 și 33).  Unul dintre aceste semnale este utilizat pentru a susține "
"anularea firelor și cronometrele POSIX (a se vedea B<timer_create>(2)); "
"celălalt este utilizat ca parte a unui mecanism care asigură că toate firele "
"dintr-un proces au întotdeauna aceleași UID și GID, conform cerințelor "
"POSIX.  Aceste semnale nu pot fi utilizate în aplicații."

#. type: IP
#: opensuse-leap-15-6
#, no-wrap
msgid "*"
msgstr "*"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "CONFORMING TO"
msgstr "ÎN CONFORMITATE CU"

# R-GC, scrie:
# COLOPHON (în eng.) = COLOFON (în rom.)
# *****
# Colofon = Notă la sfârșitul unei publicații sau
# pe verso foii de titlu, cuprinzând datele editoriale.
# sau:
# Colofon =  Însemnare la sfârșitul unei cărți în
# epoca manuscriselor și incunabulelor, cuprinzând
# date privind tipograful, locul unde a lucrat,
# autorul și titlul lucrării.
# sau:
# Colofon = Notă, însemnare finală a unei cărți,
# care reproduce sau completează cele spuse în titlu.
# ===============
# M-am decis pentru:
# COLOFON -- NOTĂ FINALĂ
#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "COLOPHON"
msgstr "COLOFON -- NOTĂ FINALĂ"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Această pagină face parte din versiunea 4.16 a proiectului Linux I<man-"
"pages>.  O descriere a proiectului, informații despre raportarea erorilor și "
"cea mai recentă versiune a acestei pagini pot fi găsite la \\%https://www."
"kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Pagini de manual de Linux 6.04"
