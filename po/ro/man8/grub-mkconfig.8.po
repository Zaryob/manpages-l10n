# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.18.1\n"
"POT-Creation-Date: 2023-10-02 12:03+0200\n"
"PO-Revision-Date: 2023-07-02 15:03+0200\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.2.2\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "GRUB-MKCONFIG"
msgstr "GRUB-MKCONFIG"

#. type: TH
#: archlinux
#, no-wrap
msgid "July 2023"
msgstr "iulie 2023"

#. type: TH
#: archlinux
#, no-wrap
msgid "GRUB 2:2.12rc1-1"
msgstr "GRUB 2:2.12rc1-1"

#. type: TH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "System Administration Utilities"
msgstr "Utilitare de administrare a sistemului"

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "grub-mkconfig - generate a GRUB configuration file"
msgstr "grub-mkconfig - Generează fișierul de configurare pentru GRUB"

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "SYNOPSIS"
msgstr "REZUMAT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "B<grub-mkconfig> [I<\\,OPTION\\/>]"
msgstr "B<grub-mkconfig> [I<\\,OPȚIUNE\\/>]"

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Generate a grub config file"
msgstr "Generează un fișier de configurare grub"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-o>, B<--output>=I<\\,FILE\\/>"
msgstr "B<-o>, B<--output>=I<\\,FIȘIER\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "output generated config to FILE [default=stdout]"
msgstr ""
"trimite configurarea generată în FIȘIER [implicit=stdout(ieșirea standard)]"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-h>, B<--help>"
msgstr "B<-h>, B<--help>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "print this message and exit"
msgstr "afișează acest mesaj și iese"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "print the version information and exit"
msgstr "afișează informațiile despre versiune și iese"

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "REPORTING BUGS"
msgstr "RAPORTAREA ERORILOR"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Report bugs to E<lt>bug-grub@gnu.orgE<gt>."
msgstr "Raportați erorile la E<lt>bug-grub@gnu.orgE<gt>."

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "B<grub-install>(8)"
msgstr "B<grub-install>(8)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid ""
"The full documentation for B<grub-mkconfig> is maintained as a Texinfo "
"manual.  If the B<info> and B<grub-mkconfig> programs are properly installed "
"at your site, the command"
msgstr ""
"Documentația completă pentru B<grub-mkconfig> este menținută ca manual "
"Texinfo.  Dacă programele B<info> și B<grub-mkconfig> sunt instalate corect "
"în sistemul dvs., comanda"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "B<info grub-mkconfig>"
msgstr "B<info grub-mkconfig>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "should give you access to the complete manual."
msgstr "ar trebui să vă permită accesul la manualul complet."

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "April 2023"
msgstr "aprilie 2023"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "GRUB 2.06-13"
msgstr "GRUB 2.06-13"

#. type: TH
#: debian-unstable
#, no-wrap
msgid "September 2023"
msgstr "septembrie 2023"

#. type: TH
#: debian-unstable
#, no-wrap
msgid "GRUB 2.12~rc1-10"
msgstr "GRUB 2.12~rc1-10"
