# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.18.1\n"
"POT-Creation-Date: 2023-07-25 19:36+0200\n"
"PO-Revision-Date: 2023-05-18 01:23+0200\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.2.2\n"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "HWCLOCK"
msgstr "HWCLOCK"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Dec 2020"
msgstr "decembrie 2020"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Debian Administrator's Manual"
msgstr "Manualul administratorului de Debian"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: debian-bookworm
msgid "hwclock - settings that affect the behaviour of the hwclock init script"
msgstr ""
"hwclock - configurări care afectează comportamentul scriptului de inițiere "
"hwclock"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: debian-bookworm
msgid "The I</etc/default/hwclock> file contains settings in POSIX format:"
msgstr ""
"Fișierul I</etc/default/hwclock> conține configurările în format POSIX:"

#. type: Plain text
#: debian-bookworm
msgid "VAR=VAL"
msgstr "VAR=VAL"

#. type: Plain text
#: debian-bookworm
msgid ""
"Only one assignment is allowed per line.  Comments (starting with '#') are "
"also allowed."
msgstr ""
"Este permisă doar o singură atribuire pe linie.  Comentariile (care încep cu "
"„#”) sunt, de asemenea, permise."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "SETTINGS"
msgstr "CONFIGURĂRI"

#. type: Plain text
#: debian-bookworm
msgid "The following settings can be set."
msgstr "Următoarele variabile pot fi definite."

#. type: IP
#: debian-bookworm
#, no-wrap
msgid "B<HCTOSYS_DEVICE>"
msgstr "B<HCTOSYS_DEVICE>"

#. type: Plain text
#: debian-bookworm
msgid ""
"The hardware clock device you want to use.  Defaults to B<rtc0>.  It should "
"probably match the CONFIG_RTC_HCTOSYS_DEVICE kernel config option."
msgstr ""
"Dispozitivul hardware de ceas pe care doriți să îl utilizați.  Valoarea "
"implicită este B<rtc0>.  Probabil că ar trebui să se potrivească cu opțiunea "
"de configurare a nucleului CONFIG_RTC_HCTOSYS_DEVICE."

#. type: Plain text
#: debian-bookworm
msgid ""
"If you change this, you also need to install a custom udev rule, mirroring "
"B</usr/lib/udev/rules.d/85-hwclock.rules>. Otherwise, the hardware clock "
"device will be ignored during boot."
msgstr ""
"Dacă schimbați acest lucru, trebuie să instalați și o regulă udev "
"personalizată, care să reflecte B</usr/lib/udev/rules.d/85-hwclock.rules>. "
"În caz contrar, dispozitivul de ceas hardware va fi ignorat în timpul "
"pornirii."

#. type: Plain text
#: debian-bookworm
msgid "On almost all systems, this should not be changed."
msgstr "Pe aproape toate sistemele, acest lucru nu trebuie schimbat."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "NOTES"
msgstr "NOTE"

#. type: Plain text
#: debian-bookworm
msgid "On systems using systemd as init, the hwclock init script is ignored."
msgstr ""
"În cazul sistemelor care utilizează systemd ca init, scriptul de inițiere "
"hwclock este ignorat."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: debian-bookworm
msgid "B<hwclock>(8), B<systemd-timesyncd.service>(8)"
msgstr "B<hwclock>(8), B<systemd-timesyncd.service>(8)"
