# Swedish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Göran Uddeborg <goeran@uddeborg.se>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-10-02 12:02+0200\n"
"PO-Revision-Date: 2023-01-09 15:30+0100\n"
"Last-Translator: Automatically generated\n"
"Language-Team: Swedish <>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "GRUB-KBDCOMP"
msgstr "GRUB-KBDCOMP"

#. type: TH
#: fedora-39 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "September 2023"
msgstr "september 2023"

#. type: TH
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "grub-kbdcomp ()"
msgstr "grub-kbdcomp ()"

#. type: TH
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr "Användarkommandon"

#. type: SH
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NAMN"

#. type: Plain text
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "grub-kbdcomp - generate a GRUB keyboard layout file"
msgstr "grub-kbdcomp - skapa tangentbordslayoutfil för GRUB"

#. type: SH
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "B<grub-kbdcomp> I<\\,-o OUTPUT CKBMAP_ARGUMENTS\\/>..."
msgstr "B<grub-kbdcomp> I<\\,-o UTMATNING CKBMAP_ARGUMENT\\/>..."

#. type: SH
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESKRIVNING"

#. type: Plain text
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"grub-kbdcomp processes a X keyboard layout description in B<keymaps>(5)  "
"format into a format that can be used by GRUB's B<keymap> command."
msgstr ""
"grub-kbdcomp bearbetar en layoutbeskrivning för ett X-tangentbord i formatet "
"B<keymaps>(5) till ett format som kan användas av GRUB:s kommando B<keymap>."

#. type: Plain text
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "Make GRUB keyboard layout file."
msgstr "Skapa tangentbordslayoutfil för GRUB."

#. type: TP
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-h>, B<--help>"
msgstr "B<-h>, B<--help>"

#. type: Plain text
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "print this message and exit"
msgstr "skriv ut detta meddelande och avsluta"

#. type: TP
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "print the version information and exit"
msgstr "skriv ut versionsinformation och avsluta"

#. type: TP
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-o>, B<--output>=I<\\,FILE\\/>"
msgstr "B<-o>, B<--output>=I<\\,FIL\\/>"

#. type: Plain text
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "save output in FILE [required]"
msgstr "spara utmatning i FIL [krävs]"

#. type: Plain text
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "grub-kbdcomp generates a keyboard layout for GRUB using ckbcomp"
msgstr "grub-kbdcomp genererar en tangentbordslayout för GRUB med ckbcomp"

#. type: SH
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "RAPPORTERA FEL"

#. type: Plain text
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "Report bugs to E<lt>bug-grub@gnu.orgE<gt>."
msgstr ""
"Rapportera fel till E<lt>bug-grub@gnu.orgE<gt>. Skicka synpunkter på "
"översättningen till E<gt>tp-sv@listor.tp-sv.seE<lt>."

#. type: SH
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SE ÄVEN"

#. type: Plain text
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "B<grub-mklayout>(8)"
msgstr "B<grub-mklayout>(8)"

#. type: Plain text
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"The full documentation for B<grub-kbdcomp> is maintained as a Texinfo "
"manual.  If the B<info> and B<grub-kbdcomp> programs are properly installed "
"at your site, the command"
msgstr ""
"Den fullständiga dokumentationen för B<grub-kbdcomp> underhålls som en "
"Texinfo-manual. Om programmen B<info> och B<grub-kbdcomp> är ordentligt "
"installerade på ditt system, bör kommandot"

#. type: Plain text
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "B<info grub-kbdcomp>"
msgstr "B<info grub-kbdcomp>"

#. type: Plain text
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "should give you access to the complete manual."
msgstr "ge dig tillgång till den kompletta manualen."

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "May 2023"
msgstr "maj 2023"

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "August 2023"
msgstr "augusti 2023"
