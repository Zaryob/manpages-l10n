# Swedish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Göran Uddeborg <goeran@uddeborg.se>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-10-02 12:06+0200\n"
"PO-Revision-Date: 2023-01-10 10:37+0100\n"
"Last-Translator: Automatically generated\n"
"Language-Team: Swedish <>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "JOIN"
msgstr "JOIN"

#. type: TH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "September 2023"
msgstr "september 2023"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "GNU coreutils 9.4"
msgstr "GNU coreutils 9.4"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr "Användarkommandon"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NAMN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "join - join lines of two files on a common field"
msgstr "join — slå samman rader från två filer enligt ett gemensamt fält"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<join> [I<\\,OPTION\\/>]... I<\\,FILE1 FILE2\\/>"
msgstr "B<join> [I<\\,FLAGGA\\/>]... I<\\,FIL1 FIL2\\/>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESKRIVNING"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"For each pair of input lines with identical join fields, write a line to "
"standard output.  The default join field is the first, delimited by blanks."
msgstr ""
"För varje par av inmatade rader med identiska join-fält, skriv en rad till "
"standard ut.  Standard för join-fält är det första, avgränsat av blanka."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "When FILE1 or FILE2 (not both) is -, read standard input."
msgstr "När FIL1 eller FIL2 (inte båda) är -, läs standard in."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-a> FILENUM"
msgstr "B<-a> FILNUM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"also print unpairable lines from file FILENUM, where FILENUM is 1 or 2, "
"corresponding to FILE1 or FILE2"
msgstr ""
"Skriv även omatchade rader från fil FILNUM, där FILNUM är 1 eller 2, "
"motsvarande FIL1 eller FIL2."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<-e> STRING"
msgstr "B<-e> STRÄNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"replace missing (empty) input fields with STRING; I.e., missing fields "
"specified with '-12jo' options"
msgstr ""
"ersätt saknade (tomma) indatafält med STRÄNG; d.v.s., saknade fält angivna "
"med flaggor som ”-12jo”"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-i>, B<--ignore-case>"
msgstr "B<-i>, B<--ignore-case>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "ignore differences in case when comparing fields"
msgstr "Ignorera skiftläge när fält jämförs."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-j> FIELD"
msgstr "B<-j> FÄLT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "equivalent to '-1 FIELD B<-2> FIELD'"
msgstr "Samma som ”-1 FÄLT -2 FÄLT”."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-o> FORMAT"
msgstr "B<-o> FORMAT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "obey FORMAT while constructing output line"
msgstr "Följ FORMAT när utskriftsrader konstrueras."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-t> CHAR"
msgstr "B<-t> TECKEN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "use CHAR as input and output field separator"
msgstr "Använd TECKEN som fältseparator för in- och utmatning."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-v> FILENUM"
msgstr "B<-v> FILNUM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "like B<-a> FILENUM, but suppress joined output lines"
msgstr "Som B<-a> FILNUM, men undertrycker förenade utmatade rader."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-1> FIELD"
msgstr "B<-1> FÄLT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "join on this FIELD of file 1"
msgstr "Förena med detta FÄLT i fil 1."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-2> FIELD"
msgstr "B<-2> FÄLT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "join on this FIELD of file 2"
msgstr "Förena med detta FÄLT i fil 2."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--check-order>"
msgstr "B<--check-order>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"check that the input is correctly sorted, even if all input lines are "
"pairable"
msgstr ""
"Kontrollera att indata är korrekt sorterad, även om alla indatarader går att "
"para ihop."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--nocheck-order>"
msgstr "B<--nocheck-order>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "do not check that the input is correctly sorted"
msgstr "Kontrollera inte att indata är korrekt sorterad."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--header>"
msgstr "B<--header>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"treat the first line in each file as field headers, print them without "
"trying to pair them"
msgstr ""
"Hantera första raden i varje fil som filhuvuden, skriv dem utan att försöka "
"att para ihop dem."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-z>, B<--zero-terminated>"
msgstr "B<-z>, B<--zero-terminated>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "line delimiter is NUL, not newline"
msgstr "Radavgränsare är NOLL, inte nyrad."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "display this help and exit"
msgstr "visa denna hjälp och avsluta"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--version>"
msgstr "B<--version>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "output version information and exit"
msgstr "visa versionsinformation och avsluta"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Unless B<-t> CHAR is given, leading blanks separate fields and are ignored, "
"else fields are separated by CHAR.  Any FIELD is a field number counted from "
"1.  FORMAT is one or more comma or blank separated specifications, each "
"being 'FILENUM.FIELD' or '0'.  Default FORMAT outputs the join field, the "
"remaining fields from FILE1, the remaining fields from FILE2, all separated "
"by CHAR.  If FORMAT is the keyword 'auto', then the first line of each file "
"determines the number of fields output for each line."
msgstr ""
"Om inte B<-t> TECKEN är givet separeras fält av inledande mellanslag som "
"ignoreras, annars separeras fält av TECKEN.  Varje FÄLT är ett fältnummer "
"räknat från 1.  FORMAT är en eller flera specifikationer åtskilda med komma "
"eller mellanslag, var och en är ”FILNUM.FÄLT” eller ”0”.  Normalvärdet för "
"FORMAT skriver ut de förenade fälten, de kvarvarande fälten från FIL1, de "
"kvarvarande fälten från FIL2, allt separerat med TECKEN.  Om FORMAT är "
"nyckelordet ”auto” avgör första raden av varje fil antalet fält som skrivs "
"ut för varje rad."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Important: FILE1 and FILE2 must be sorted on the join fields.  E.g., use "
"\"sort B<-k> 1b,1\" if 'join' has no options, or use \"join B<-t> ''\" if "
"'sort' has no options.  Note, comparisons honor the rules specified by "
"'LC_COLLATE'.  If the input is not sorted and some lines cannot be joined, a "
"warning message will be given."
msgstr ""
"Viktigt: FIL1 och FIL2 måste vara sorterade på föreningsfälten. T.ex., "
"använd ”sort B<-k> 1b,1” om ”join” inte har några flaggor, eller använd "
"”join B<-t> ''” om ”sort” inte har några flaggor. Observera, jämförelser tar "
"hänsyn till reglerna som anges av ”LC_COLLATE”. Om indata inte är sorterad "
"och några rader inte kan förenas, ges ett varningsmeddelande."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "UPPHOVSMAN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Written by Mike Haertel."
msgstr "Skrivet av Mike Haertel."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "RAPPORTERA FEL"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"GNU coreutils online help: E<lt>https://www.gnu.org/software/coreutils/E<gt>"
msgstr ""
"GNU coreutils hjälp på nätet: E<lt>https://www.gnu.org/software/coreutils/"
"E<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Report any translation bugs to E<lt>https://translationproject.org/team/E<gt>"
msgstr ""
"Rapportera anmärkningar på översättningen till E<lt>tp-sv@listor.tp-sv."
"seE<gt>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "COPYRIGHT"
msgstr "COPYRIGHT"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid ""
"Copyright \\(co 2023 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2023 Free Software Foundation, Inc.  Licens GPLv3+: GNU GPL "
"version 3 eller senare E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""
"Detta är fri programvara: du får fritt ändra och vidaredistribuera den. Det "
"finns INGEN GARANTI, så långt lagen tillåter."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SE ÄVEN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "B<comm>(1), B<uniq>(1)"
msgstr "B<comm>(1), B<uniq>(1)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Full documentation E<lt>https://www.gnu.org/software/coreutils/joinE<gt>"
msgstr ""
"Fullständig dokumentation E<lt>https://www.gnu.org/software/coreutils/"
"joinE<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "or available locally via: info \\(aq(coreutils) join invocation\\(aq"
msgstr ""
"eller tillgängligt lokalt via: info \\(aq(coreutils) join invocation\\(aq"

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "September 2022"
msgstr "september 2022"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "GNU coreutils 9.1"
msgstr "GNU coreutils 9.1"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron
msgid ""
"Copyright \\(co 2022 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2022 Free Software Foundation, Inc.  Licens GPLv3+: GNU GPL "
"version 3 eller senare E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: TH
#: fedora-39
#, no-wrap
msgid "GNU coreutils 9.3"
msgstr "GNU coreutils 9.3"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "April 2022"
msgstr "april 2022"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "October 2021"
msgstr "oktober 2021"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "GNU coreutils 8.32"
msgstr "GNU coreutils 8.32"

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "B<-e> EMPTY"
msgstr "B<-e> TOM"

#. type: Plain text
#: opensuse-leap-15-6
msgid "replace missing input fields with EMPTY"
msgstr "Ersätt tomma inmatningsfält med TOM."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Copyright \\(co 2020 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2020 Free Software Foundation, Inc.  Licens GPLv3+: GNU GPL "
"version 3 eller senare E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: Plain text
#: opensuse-leap-15-6
msgid "comm(1), uniq(1)"
msgstr "B<comm>(1), B<uniq>(1)"
