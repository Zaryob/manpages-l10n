# Swedish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Göran Uddeborg <goeran@uddeborg.se>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-10-02 12:13+0200\n"
"PO-Revision-Date: 2023-01-11 23:19+0100\n"
"Last-Translator: Automatically generated\n"
"Language-Team: Swedish <>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NUMFMT"
msgstr "NUMFMT"

#. type: TH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "September 2023"
msgstr "september 2023"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "GNU coreutils 9.4"
msgstr "GNU coreutils 9.4"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr "Användarkommandon"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NAMN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "numfmt - Convert numbers from/to human-readable strings"
msgstr "numfmt — konvertera tal från/till mänskligt läsbara strängar"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<numfmt> [I<\\,OPTION\\/>]... [I<\\,NUMBER\\/>]..."
msgstr "B<numfmt> [I<\\,FLAGGA\\/>]... [I<\\,TAL\\/>]..."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESKRIVNING"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Reformat NUMBER(s), or the numbers from standard input if none are specified."
msgstr "Formaterar om TAL, eller talen från standard in om inga är givna."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Mandatory arguments to long options are mandatory for short options too."
msgstr ""
"Obligatoriska argument till långa flaggor är obligatoriska även för de korta."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--debug>"
msgstr "B<--debug>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "print warnings about invalid input"
msgstr "Skriv varningar om ogiltig indata."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-d>, B<--delimiter>=I<\\,X\\/>"
msgstr "B<-d>, B<--delimiter>=I<\\,X\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "use X instead of whitespace for field delimiter"
msgstr "Använd X istället för blanka som fältseparator."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--field>=I<\\,FIELDS\\/>"
msgstr "B<--field>=I<\\,FÄLT\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "replace the numbers in these input fields (default=1); see FIELDS below"
msgstr "Ersätt talen i dessa indatarader (standard är 1); se FÄLT nedan."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--format>=I<\\,FORMAT\\/>"
msgstr "B<--format>=I<\\,FORMAT\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "use printf style floating-point FORMAT; see FORMAT below for details"
msgstr ""
"Använd flyttalsformat FORMAT av typ printf; se FORMAT nedan för detaljer."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--from>=I<\\,UNIT\\/>"
msgstr "B<--from>=I<\\,ENHET\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "auto-scale input numbers to UNITs; default is 'none'; see UNIT below"
msgstr ""
"Skala automatiskt indatatal till ENHETer; standard är ”none”; se ENHET nedan."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--from-unit>=I<\\,N\\/>"
msgstr "B<--from-unit>=I<\\,N\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "specify the input unit size (instead of the default 1)"
msgstr "Ange indatas enhetsstorlek (istället för standard 1)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--grouping>"
msgstr "B<--grouping>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"use locale-defined grouping of digits, e.g. 1,000,000 (which means it has no "
"effect in the C/POSIX locale)"
msgstr ""
"Använd lokaldefinierad gruppering av siffror, t.ex. 1 000 000  (vilket "
"betyder att det inte har någon effekt i lokalen C/POSIX)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--header>[=I<\\,N\\/>]"
msgstr "B<--header>[=I<\\,N\\/>]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"print (without converting) the first N header lines; N defaults to 1 if not "
"specified"
msgstr ""
"Skriv (utan konvertering) de första N rubrikraderna; N blir 1 som standard "
"om det inte anges."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--invalid>=I<\\,MODE\\/>"
msgstr "B<--invalid>=I<\\,SÄTT\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"failure mode for invalid numbers: MODE can be: abort (default), fail, warn, "
"ignore"
msgstr ""
"Felhanteringssätt för felaktiga tal: SÄTT kan vara: abort (standard), fail, "
"warn, ignore."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--padding>=I<\\,N\\/>"
msgstr "B<--padding>=I<\\,N\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"pad the output to N characters; positive N will right-align; negative N will "
"left-align; padding is ignored if the output is wider than N; the default is "
"to automatically pad if a whitespace is found"
msgstr ""
"Fyll ut utdata till N tecken; positivt N kommer högerjustera; negativt N "
"kommer vänsterjustera; utfyllnad ignoreras om utdata är bredare än N; "
"standard är att automatiskt fylla ut om en blank finns."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--round>=I<\\,METHOD\\/>"
msgstr "B<--round>=I<\\,METOD\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"use METHOD for rounding when scaling; METHOD can be: up, down, from-zero "
"(default), towards-zero, nearest"
msgstr ""
"Använd METOD för avrundning vid skalning; METOD kan vara: up, down, from-"
"zero (standard), towards-zero, nearest."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--suffix>=I<\\,SUFFIX\\/>"
msgstr "B<--suffix>=I<\\,SUFFIX\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"add SUFFIX to output numbers, and accept optional SUFFIX in input numbers"
msgstr ""
"Använd SUFFIX för att skriva ut tal, och acceptera ett möjligt SUFFIX i "
"indatatal."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--to>=I<\\,UNIT\\/>"
msgstr "B<--to>=I<\\,ENHET\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "auto-scale output numbers to UNITs; see UNIT below"
msgstr "Skala automatiskt utdatatal till ENHETer; se ENHET nedan."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--to-unit>=I<\\,N\\/>"
msgstr "B<--to-unit>=I<\\,N\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "the output unit size (instead of the default 1)"
msgstr "Enhetsstorlek på utdata (istället för standard 1)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-z>, B<--zero-terminated>"
msgstr "B<-z>, B<--zero-terminated>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "line delimiter is NUL, not newline"
msgstr "Radavgränsare är NOLL, inte nyrad."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "display this help and exit"
msgstr "visa denna hjälp och avsluta"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--version>"
msgstr "B<--version>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "output version information and exit"
msgstr "visa versionsinformation och avsluta"

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "UNIT options:"
msgstr "ENHETSflaggor:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "none"
msgstr "none"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "no auto-scaling is done; suffixes will trigger an error"
msgstr "ingen automatisk skalning görs; suffix kommer orsaka ett fel"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "auto"
msgstr "auto"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "accept optional single/two letter suffix:"
msgstr "acceptera valfria en-/tvåbokstavssuffix:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "1K = 1000, 1Ki = 1024, 1M = 1000000, 1Mi = 1048576,"
msgstr "1K = 1000, 1Ki = 1024, 1M = 1000000, 1Mi = 1048576,"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "si"
msgstr "si"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "accept optional single letter suffix:"
msgstr "acceptera valfria enbokstavssuffix:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "1K = 1000, 1M = 1000000, \\&..."
msgstr "1K = 1000, 1M = 1000000, \\&..."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "iec"
msgstr "iec"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "1K = 1024, 1M = 1048576, \\&..."
msgstr "1K = 1024, 1M = 1048576, \\&..."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "iec-i"
msgstr "iec-i"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "accept optional two-letter suffix:"
msgstr "acceptera valfria tvåbokstavssuffix:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "1Ki = 1024, 1Mi = 1048576, \\&..."
msgstr "1Ki = 1024, 1Mi = 1048576, \\&..."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "FIELDS supports cut(1) style field ranges:"
msgstr "FÄLT stödjer fältintervall i stilen för B<cut>(1):"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "N"
msgstr "N"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "N'th field, counted from 1"
msgstr "N:e fältet, räknat från 1"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "N-"
msgstr "N-"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "from N'th field, to end of line"
msgstr "från N:e fältet, till radslut"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "N-M"
msgstr "N-M"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "from N'th to M'th field (inclusive)"
msgstr "från N:e till och med M:e fältet"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-M>"
msgstr "B<-M>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "from first to M'th field (inclusive)"
msgstr "från första till och med M:e fältet"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "-"
msgstr "-"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "all fields"
msgstr "alla fält"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Multiple fields/ranges can be separated with commas"
msgstr "Flera fält/intervall kan separeras med komman."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"FORMAT must be suitable for printing one floating-point argument '%f'.  "
"Optional quote (%'f) will enable B<--grouping> (if supported by current "
"locale).  Optional width value (%10f) will pad output. Optional zero (%010f) "
"width will zero pad the number. Optional negative values (%-10f) will left "
"align.  Optional precision (%.1f) will override the input determined "
"precision."
msgstr ""
"FORMAT måste passa för att skriva ett flyttalsargument ”%f”.  En möjlig "
"apostrof (%'f) kommer aktivera B<--grouping> (om det stödjs av den aktuella "
"lokalen).  Ett möjligt värde på bredd (%10f) kommer fylla utdata.  En möjlig "
"nolla (%010f) i bredden kommer fylla talet med nollor.  Ett möjligt negativt "
"värde (%-10f) kommer vänsterställa utdata.  En möjlig precision (%.1f) "
"kommer åsidosätta precisionen som bestäms av indata."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Exit status is 0 if all input numbers were successfully converted.  By "
"default, numfmt will stop at the first conversion error with exit status 2.  "
"With B<--invalid=>'fail' a warning is printed for each conversion error and "
"the exit status is 2.  With B<--invalid=>'warn' each conversion error is "
"diagnosed, but the exit status is 0.  With B<--invalid=>'ignore' conversion "
"errors are not diagnosed and the exit status is 0."
msgstr ""
"Slutstatus är 0 om alla indatatal kunde konverteras.  Som standard kommer "
"B<numfmt> sluta vid det första konverteringsfelet med slutstatus 2.  Med B<--"
"invalid>='fail' skrivs en varning för varje konverteringsfel och slutstatus "
"är 2.  Med B<--invalid>='warn' diagnostiseras varje konverteringsfel, men "
"slutstatus är 0.  Med B<--invalid>='ignore' diagnostiseras inte "
"konverteringsfel och slutstatusen är 0."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "EXEMPEL"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "CW<$ numfmt --to=si 1000>"
msgstr "CW<$ numfmt --to=si 1000>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "-E<gt> \"1.0K\""
msgstr "-E<gt> ”1.0K”"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "CW<$ numfmt --to=iec 2048>"
msgstr "CW<$ numfmt --to=iec 2048>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "-E<gt> \"2.0K\""
msgstr "-E<gt> ”2.0K”"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "CW<$ numfmt --to=iec-i 4096>"
msgstr "CW<$ numfmt --to=iec-i 4096>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "-E<gt> \"4.0Ki\""
msgstr "-E<gt> ”4.0Ki”"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "CW<$ echo 1K | numfmt --from=si>"
msgstr "CW<$ echo 1K | numfmt --from=si>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "-E<gt> \"1000\""
msgstr "-E<gt> ”1000”"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "CW<$ echo 1K | numfmt --from=iec>"
msgstr "CW<$ echo 1K | numfmt --from=iec>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "-E<gt> \"1024\""
msgstr "-E<gt> ”1024”"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "CW<$ df -B1 | numfmt --header --field 2-4 --to=si>"
msgstr "CW<$ df -B1 | numfmt --header --field 2-4 --to=si>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "CW<$ ls -l | numfmt --header --field 5 --to=iec>"
msgstr "CW<$ ls -l | numfmt --header --field 5 --to=iec>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "CW<$ ls -lh | numfmt --header --field 5 --from=iec --padding=10>"
msgstr "CW<$ ls -lh | numfmt --header --field 5 --from=iec --padding=10>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "CW<$ ls -lh | numfmt --header --field 5 --from=iec --format %10f>"
msgstr "CW<$ ls -lh | numfmt --header --field 5 --from=iec --format %10f>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "UPPHOVSMAN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Written by Assaf Gordon."
msgstr "Skrivet av Assaf Gordon."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "RAPPORTERA FEL"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"GNU coreutils online help: E<lt>https://www.gnu.org/software/coreutils/E<gt>"
msgstr ""
"GNU coreutils hjälp på nätet: E<lt>https://www.gnu.org/software/coreutils/"
"E<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Report any translation bugs to E<lt>https://translationproject.org/team/E<gt>"
msgstr ""
"Rapportera anmärkningar på översättningen till E<lt>tp-sv@listor.tp-sv."
"seE<gt>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "COPYRIGHT"
msgstr "COPYRIGHT"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid ""
"Copyright \\(co 2023 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2023 Free Software Foundation, Inc.  Licens GPLv3+: GNU GPL "
"version 3 eller senare E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""
"Detta är fri programvara: du får fritt ändra och vidaredistribuera den. Det "
"finns INGEN GARANTI, så långt lagen tillåter."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SE ÄVEN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Full documentation E<lt>https://www.gnu.org/software/coreutils/numfmtE<gt>"
msgstr ""
"Fullständig dokumentation E<lt>https://www.gnu.org/software/coreutils/"
"numfmtE<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "or available locally via: info \\(aq(coreutils) numfmt invocation\\(aq"
msgstr ""
"eller tillgängligt lokalt via: info \\(aq(coreutils) numfmt invocation\\(aq"

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "September 2022"
msgstr "september 2022"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "GNU coreutils 9.1"
msgstr "GNU coreutils 9.1"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron
msgid ""
"Copyright \\(co 2022 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2022 Free Software Foundation, Inc.  Licens GPLv3+: GNU GPL "
"version 3 eller senare E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: TH
#: fedora-39
#, no-wrap
msgid "GNU coreutils 9.3"
msgstr "GNU coreutils 9.3"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "April 2022"
msgstr "april 2022"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "October 2021"
msgstr "oktober 2021"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "GNU coreutils 8.32"
msgstr "GNU coreutils 8.32"

#. type: Plain text
#: opensuse-leap-15-6
msgid "replace the numbers in these input fields (default=1)  see FIELDS below"
msgstr "Ersätt talen i dessa indatarader (standard är 1); se FÄLT nedan."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Copyright \\(co 2020 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2020 Free Software Foundation, Inc.  Licens GPLv3+: GNU GPL "
"version 3 eller senare E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
