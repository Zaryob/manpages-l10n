# Swedish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Göran Uddeborg <goeran@uddeborg.se>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-10-02 12:02+0200\n"
"PO-Revision-Date: 2023-01-09 15:41+0100\n"
"Last-Translator: Automatically generated\n"
"Language-Team: Swedish <>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "GRUB-MKRELPATH"
msgstr "GRUB-MKRELPATH"

#. type: TH
#: fedora-39 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "September 2023"
msgstr "september 2023"

#. type: TH
#: fedora-39 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "GRUB 2.06"
msgstr "GRUB 2.06"

#. type: TH
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr "Användarkommandon"

#. type: SH
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NAMN"

#. type: Plain text
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "grub-mkrelpath - make a system path relative to its root"
msgstr "grub-mkrelpath — gör en systemsökväg relativ sin rot"

#. type: SH
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "B<grub-mkrelpath> [I<\\,OPTION\\/>...] I<\\,PATH\\/>"
msgstr "B<grub-mkrelpath> [I<\\,FLAGGA\\/>...] I<\\,SÖKVÄG\\/>"

#. type: SH
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESKRIVNING"

#. type: Plain text
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "Transform a system filename into GRUB one."
msgstr "Transformera ett systemfilnamn till ett GRUB-filnamn."

#. type: TP
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-r>, B<--relative>"
msgstr "B<-r>, B<--relative>"

#. type: Plain text
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "use relative path on btrfs"
msgstr "använd en relativ sökväg i btrfs"

#. type: TP
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "-?, B<--help>"
msgstr "-?, B<--help>"

#. type: Plain text
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "give this help list"
msgstr "visa denna hjälplista"

#. type: TP
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--usage>"
msgstr "B<--usage>"

#. type: Plain text
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "give a short usage message"
msgstr "ge ett kort användningsmeddelande"

#. type: TP
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "print program version"
msgstr "skriv ut programversion"

#. type: SH
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "RAPPORTERA FEL"

#. type: Plain text
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "Report bugs to E<lt>bug-grub@gnu.orgE<gt>."
msgstr ""
"Rapportera fel till E<lt>bug-grub@gnu.orgE<gt>. Skicka synpunkter på "
"översättningen till E<gt>tp-sv@listor.tp-sv.seE<lt>."

#. type: SH
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SE ÄVEN"

#. type: Plain text
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "B<grub-probe>(8)"
msgstr "B<grub-probe>(8)"

#. type: Plain text
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"The full documentation for B<grub-mkrelpath> is maintained as a Texinfo "
"manual.  If the B<info> and B<grub-mkrelpath> programs are properly "
"installed at your site, the command"
msgstr ""
"Den fullständiga dokumentationen för B<grub-mkrelpath> underhålls som en "
"Texinfo-manual. Om programmen B<info> och B<grub-mkrelpath> är ordentligt "
"installerade på ditt system, bör kommandot"

#. type: Plain text
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "B<info grub-mkrelpath>"
msgstr "B<info grub-mkrelpath>"

#. type: Plain text
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "should give you access to the complete manual."
msgstr "ge dig tillgång till den kompletta manualen."

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "May 2023"
msgstr "maj 2023"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "grub-mkrelpath (GRUB2) 2.06"
msgstr "grub-mkrelpath (GRUB2) 2.06"

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "August 2023"
msgstr "augusti 2023"

#. type: TH
#: opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "GRUB2 2.06"
msgid "GRUB2 2.12~rc1"
msgstr "GRUB2 2.06"
