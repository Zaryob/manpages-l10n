# Norwegian bokmål translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.11.0\n"
"POT-Creation-Date: 2023-10-02 12:02+0200\n"
"PO-Revision-Date: 2021-09-03 20:09+0200\n"
"Last-Translator: Automatically generated\n"
"Language-Team: Norwegian bokmål <>\n"
"Language: nb\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "GRUB-MKLAYOUT"
msgstr "GRUB-MKLAYOUT"

#. type: TH
#: fedora-39 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "September 2023"
msgstr "September 2023"

#. type: TH
#: fedora-39 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "GRUB 2.06"
msgstr "GRUB 2.06"

#. type: TH
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr "Brukerkommandoer"

#. type: SH
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NAVN"

#. type: Plain text
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, fuzzy
#| msgid "Make GRUB keyboard layout file."
msgid "grub-mklayout - generate a GRUB keyboard layout file"
msgstr "Lag tastatur-utformingsfil for GRUB."

#. type: SH
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "OVERSIKT"

#. type: Plain text
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "B<grub-mklayout> [I<\\,OPTION\\/>...] [I<\\,OPTIONS\\/>]"
msgstr "B<grub-mklayout> [I<\\,VALG\\/>...] [I<\\,VALG\\/>]"

#. type: SH
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESKRIVELSE"

#. type: Plain text
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"grub-mklayout processes a keyboard layout description in B<keymaps>(5)  "
"format into a format that can be used by GRUB's B<keymap> command."
msgstr ""

#. type: Plain text
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "Generate GRUB keyboard layout from Linux console one."
msgstr "Lag en GRUB-tastaturutforming basert på en som Linux-konsollen bruker."

#. type: TP
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-i>, B<--input>=I<\\,FILE\\/>"
msgstr "B<-i>, B<--input>=I<\\,FIL\\/>"

#. type: Plain text
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "set input filename. Default is STDIN"
msgstr "velg inndata-filnavn. Standard er STDIN"

#. type: TP
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-o>, B<--output>=I<\\,FILE\\/>"
msgstr "B<-o>, B<--output>=I<\\,FIL\\/>"

#. type: Plain text
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "set output filename. Default is STDOUT"
msgstr "velg utdata-filnavn. Standard er STDOUT"

#. type: TP
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-v>, B<--verbose>"
msgstr "B<-v>, B<--verbose>"

#. type: Plain text
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "print verbose messages."
msgstr "skriv ut detaljerte meldinger"

#. type: TP
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "-?, B<--help>"
msgstr "-?, B<--help>"

#. type: Plain text
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "give this help list"
msgstr "vis denne hjelpelista"

#. type: TP
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--usage>"
msgstr "B<--usage>"

#. type: Plain text
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "give a short usage message"
msgstr "vis en kortfattet bruksanvisning"

#. type: TP
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "print program version"
msgstr "skriv ut programversjon"

#. type: Plain text
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"Mandatory or optional arguments to long options are also mandatory or "
"optional for any corresponding short options."
msgstr ""
"Valg er enten obligatoriske både for fullstendige valg og tilsvarende "
"forkortede valg."

#. type: SH
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "RAPPORTERING AV FEIL"

#. type: Plain text
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "Report bugs to E<lt>bug-grub@gnu.orgE<gt>."
msgstr "Rapporter feil til E<lt>bug-grub@gnu.orgE<gt>."

#. type: SH
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SE OGSÅ"

#. type: Plain text
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "B<grub-mkconfig>(8)"
msgstr "B<grub-mkconfig>(8)"

#. type: Plain text
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"The full documentation for B<grub-mklayout> is maintained as a Texinfo "
"manual.  If the B<info> and B<grub-mklayout> programs are properly installed "
"at your site, the command"
msgstr ""
"Den fullstendige dokumentasjonen for B<grub-mklayout> opprettholdes som en "
"Texinfo manual. Dersom B<info> og B<grub-mklayout> programmene er riktig "
"installert på ditt sted burde kommandoen"

#. type: Plain text
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "B<info grub-mklayout>"
msgstr "B<info grub-mklayout>"

#. type: Plain text
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "should give you access to the complete manual."
msgstr "gi deg tilgang til hele manualen."

#. type: TH
#: opensuse-leap-15-6
#, fuzzy, no-wrap
#| msgid "March 2023"
msgid "May 2023"
msgstr "Mars 2023"

#. type: TH
#: opensuse-leap-15-6
#, fuzzy, no-wrap
#| msgid "grub-mklayout (GRUB2) 2.04"
msgid "grub-mklayout (GRUB2) 2.06"
msgstr "grub-mklayout (GRUB2) 2.04"

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "August 2023"
msgstr "August 2023"

#. type: TH
#: opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "GRUB 2:2.12rc1-1"
msgid "GRUB2 2.12~rc1"
msgstr "GRUB 2:2.12rc1-1"
