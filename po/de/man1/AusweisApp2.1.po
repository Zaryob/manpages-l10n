# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Helge Kreutzmann <debian@helgefjell.de>, 2022,2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.19.0\n"
"POT-Creation-Date: 2023-08-27 16:50+0200\n"
"PO-Revision-Date: 2023-06-09 20:31+0200\n"
"Last-Translator: Helge Kreutzmann <debian@helgefjell.de>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "AusweisApp2"
msgstr "AusweisApp2"

#. type: SH
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"AusweisApp2 - Official authentication app for German ID cards and residence "
"permits"
msgstr ""
"AusweisApp2 - Offizielle Authentisierung-App für deutsche Ausweise und "
"Aufenthaltsberechtigungen"

#. type: SH
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "AusweisApp2 [-h|--help]"
msgstr "AusweisApp2 [-h|--help]"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "AusweisApp2 [--help-all]"
msgstr "AusweisApp2 [--help-all]"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "AusweisApp2 [-v|--version]"
msgstr "AusweisApp2 [-v|--version]"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "AusweisApp2 [--show]"
msgstr "AusweisApp2 [--show]"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"AusweisApp2 [--keep] [--no-logfile] [--no-loghandler] [--show] [--no-proxy] "
"[--ui { qml|websocket }] [--port I<\\,PORT\\/>] [--address I<\\,ADDRESS\\/>]"
msgstr ""
"AusweisApp2 [--keep] [--no-logfile] [--no-loghandler] [--show] [--no-proxy] "
"[--ui { qml|websocket }] [--port I<\\,PORT\\/>] [--address I<\\,ADRESSE\\/>]"

#. type: SH
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"AusweisApp2 allows you to authenticate yourself against websites via your "
"German ID card and residence permits."
msgstr ""
"AusweisApp2 erlaubt es Ihnen, sich gegen Websites mittels Ihres deutschen "
"Personalausweises und Ihrer Aufenthaltsberechtigung zu authentisieren."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "You will need:"
msgstr "Folgendes benötigen Sie:"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "* an ID card that is enabled for online identification"
msgstr "* einen Ausweis, bei dem die Online-Identifizierung aktiviert ist"

# FIXME can * also → can also
#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"* A compatible NFC device (most NFC readers should work, NFC-enabled phones "
"can * also be used)"
msgstr ""
"* Ein kompatibles NFC-Gerät (die meisten NFC-Lesegeräte sollten "
"funktionieren, NFC-aktivierte Mobiltelephone können auch verwandt werden)"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "* AusweisApp2"
msgstr "* AusweisApp2"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "* A browser"
msgstr "* Einen Browser"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "* A website that supports authentication via German ID card"
msgstr ""
"* Eine Website, die die Authentisierung mittels deutscher Ausweise erlaubt"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"When you visit such a website, AusweisApp2 will be triggered and will ask "
"you if you want to authenticate against the website."
msgstr ""
"Wenn Sie eine solche Website besuchen, wird AusweisApp2 ausgelöst und wird "
"Sie fragen, ob Sie sich bei der Website authentisieren möchten."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"This program will provide a local webserver for your browser to interface "
"against."
msgstr ""
"Als Schnittstelle wird das Programm Ihrem Browser einen lokalen Webserver "
"bereitstellen."

#. type: SH
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr "OPTIONEN"

#. type: TP
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-h, --help>"
msgstr "B<-h, --help>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "Displays a short help message."
msgstr "Zeigt eine kurze Hilfemeldung an."

#. type: TP
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--help-all>"
msgstr "B<--help-all>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "Displays help including Qt specific options."
msgstr "Zeigt eine Hilfe einschließlich QT-spezifischer Optionen an."

#. type: TP
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-v, --version>"
msgstr "B<-v, --version>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "Displays version information."
msgstr "Zeigt Versionsinformationen an."

#. type: TP
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--keep>"
msgstr "B<--keep>"

# FIXME ${TMP}/AusweisApp2.*.log → I<${TMP}/AusweisApp2.*.log>
#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"By default, AusweisApp2 writes a log to a file matching ${TMP}/AusweisApp2.*."
"log. When the program terminates, it will be deleted. This setting prevents "
"deletion."
msgstr ""
"Standardmäßig schreibt AusweisApp2 eine Protokolldatei, deren Name auf "
"I<${TMP}/AusweisApp2.*.log> passt. Wenn sich das Programm beendet, wird "
"diese gelöscht. Diese Einstellung verhindert das Löschen."

#. type: TP
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--no-logfile>"
msgstr "B<--no-logfile>"

# FIXME ${TMP}/AusweisApp2.*.log → I<${TMP}/AusweisApp2.*.log>
#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"Suppress writing a log file to ${TMP}/AusweisApp2.*.log. Logs will still be "
"written to STDOUT."
msgstr ""
"Unterdrückt das Schreiben einer Protokolldatei nach I<${TMP}/AusweisApp2.*."
"log>. Auf die Standardausgabe wird weiterhin protokolliert."

#. type: TP
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--no-loghandler>"
msgstr "B<--no-loghandler>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "Disable default log handler. This disables logging to STDOUT."
msgstr ""
"Deaktiviert die standarmäßige Aussteuerung des Protokolls. Damit wird die "
"Protokollierung nach STDOUT deaktiviert."

#. type: TP
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--show>"
msgstr "B<--show>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "Show window on startup."
msgstr "Zeigt beim Starten ein Fenster."

#. type: TP
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--no-proxy>"
msgstr "B<--no-proxy>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "Disable system proxy."
msgstr "Deaktiviert den System-Proxy."

#. type: TP
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--ui { qml|webservice|websocket }>"
msgstr "B<--ui { qml|webservice|websocket }>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"This option allows multiple values.  - \"qml\" will start the program with a "
"visible UI.  - \"websocket\" will let it start in the background as an SDK. "
"This is only useful when integrating AusweisApp2 into other programs.  - "
"\"webservice\" starts listening on given port/address."
msgstr ""
"Diese Option erlaubt mehrere Werte. - »qml« wird das Programm mit einer "
"sichtbaren UI starten. - »websocket« lässt es im Hintergrund als SDK "
"starten. Dies ist nur nützlich, wenn Sie AusweisApp2 in andere Programme "
"integrieren. - »webservice« beginnt das Warten auf Anfragen auf dem "
"angegebenen Port/der angegebenen Adresse."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "Default is \"qml,webservice,websocket\"."
msgstr "Vorgabe ist »qml,webservice,websocket«. "

#. type: TP
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--port >I<\\,PORT\\/>"
msgstr "B<--port >I<\\,PORT\\/>"

# FIXME ${TMP}/AusweisApp2.E<lt>PIDE<gt>.port → I<${TMP}/AusweisApp2.E<lt>PIDE<gt>.port>
#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"Change the listening port for the WebSocket. Default is 24727. Selecting "
"\"0\" is a special case. AusweisApp2 will then select a random port and "
"write the port number to a file in ${TMP}/AusweisApp2.E<lt>PIDE<gt>.port."
msgstr ""
"Ändert den Port, an dem der WebSocket auf Anfragen wartet. Vorgabe ist "
"24727. Die Auswahl von »0« ist ein Spezialfall. AusweisApp2 wird dann einen "
"zufälligen Port auswählen und die Port-Nummer in eine Datei in I<${TMP}/"
"AusweisApp2.E<lt>PIDE<gt>.port> schreiben."

#. type: TP
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--address >I<\\,ADDRESS\\/>"
msgstr "B<--address >I<\\,ADRESSE\\/>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"Use given addresses for interface binding. Normally AusweisApp2 is bound to "
"localhost only as it is a security requirement. Useful for testing only.  "
"This option allows multiple values."
msgstr ""
"Verwendet die angegebene Adresse für die Anbindung der Oberfläche. "
"Normalerweise wird AusweisApp2 nur an Localhost gebunden, da dies eine "
"Sicherheitsanforderung ist. Nur zum Testen nützlich. Diese Option erlaubt "
"mehrere Werte."

#. type: SH
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "RÜCKGABEWERT"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "AusweisApp2 will return 0 when successfully terminated."
msgstr "Wenn AusweisApp2 erfolgreich beendet wird, wird 0 zurückgegeben."

#. type: SH
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "ENVIRONMENT"
msgstr "UMGEBUNGSVARIABLEN"

#. type: TP
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "B<QT_QPA_PLATFORM={ wayland|X11 }>"
msgstr "B<QT_QPA_PLATFORM={ wayland|X11 }>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"You may force the session type to wayland or X11. This is only needed for "
"debugging purposes. XDG_SESSION_TYPE will be ignored on gnome."
msgstr ""
"Sie können den Sitzungstyp »wayland« oder »X11« erzwingen. Dies ist nur zur "
"Fehlersuche notwendig. Unter GNOME wird XDG_SESSION_TYPE ignoriert."

#. type: SH
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "FILES"
msgstr "DATEIEN"

# FIXME Use two paragraphs for this
#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"I<~/.config/AusweisApp2_CE/AusweisApp2.conf> File path where the user config "
"is saved."
msgstr ""
"I<~/.config/AusweisApp2_CE/AusweisApp2.conf> Dateipfad, unter dem die "
"Benutzerkonfiguration gespeichert wird."

#. type: SH
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "CAVEATS"
msgstr "WARNUNGEN"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"Currently there is no way to terminate the program on gnome as the TrayIcon "
"is mandatory."
msgstr ""
"Derzeit gibt es unter GNOME keine Möglichkeit, das Programm zu beenden, da "
"das TrayIcon verpflichtend ist."

#. type: SH
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

# FIXME Lee Garrett (debian@rocketjump.eu) → E<.MT debian@rocketjump.eu>Lee GarrettE<.ME>
#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"This man page was written by Lee Garrett (debian@rocketjump.eu) for Debian "
"(but may be used by others)."
msgstr ""
"Diese Handbuchseite wurde von E<.MT debian@rocketjump.eu>Lee GarrettE<.ME> "
"für Debian geschrieben, kann von anderen aber verwandt werden."

#. type: SH
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "https://www.ausweisapp.bund.de/en"
msgstr "https://www.ausweisapp.bund.de/de"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "https://www.ausweisapp.bund.de/sdk"
msgstr "https://www.ausweisapp.bund.de/sdk"
