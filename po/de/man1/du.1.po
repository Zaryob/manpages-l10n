# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Karl Eichwalder <ke@suse.de>
# Lutz Behnke <lutz.behnke@gmx.de>
# Michael Piefel <piefel@debian.org>
# Michael Schmidt <michael@guug.de>
# Martin Eberhard Schauer <Martin.E.Schauer@gmx.de>, 2010.
# Dr. Tobias Quathamer <toddy@debian.org>, 2012.
# Mario Blättermann <mario.blaettermann@gmail.com>, 2013-2014, 2016, 2019, 2021.
msgid ""
msgstr ""
"Project-Id-Version: manpages-de\n"
"POT-Creation-Date: 2023-10-02 11:56+0200\n"
"PO-Revision-Date: 2021-10-10 14:50+0200\n"
"Last-Translator: Mario Blättermann <mario.blaettermann@gmail.com>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 21.08.2\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DU"
msgstr "DU"

#. type: TH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "September 2023"
msgstr "September 2023"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "GNU coreutils 9.4"
msgstr "GNU coreutils 9.4"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr "Dienstprogramme für Benutzer"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "du - estimate file space usage"
msgstr "du - gibt den Platzverbrauch von Dateien an"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<du> [I<\\,OPTION\\/>]... [I<\\,FILE\\/>]..."
msgstr "B<du> [I<\\,OPTION\\/>]… [I<\\,DATEI\\/>]…"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<du> [I<\\,OPTION\\/>]... I<\\,--files0-from=F\\/>"
msgstr "B<du> [I<\\,OPTION\\/>]… I<\\,--files0-from=F\\/>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Summarize device usage of the set of FILEs, recursively for directories."
msgstr ""
"Gibt den Geräteplatzverbrauch jeder DATEI an, rekursiv für Verzeichnisse."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Mandatory arguments to long options are mandatory for short options too."
msgstr ""
"Die obligatorischen Argumente für Optionen sind für deren Kurz- und Langform "
"gleich."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-0>, B<--null>"
msgstr "B<-0>, B<--null>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "end each output line with NUL, not newline"
msgstr ""
"Jede Ausgabezeile mit einem Nullbyte statt des Zeilenumbruchs abschließen"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-a>, B<--all>"
msgstr "B<-a>, B<--all>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "write counts for all files, not just directories"
msgstr "Daten für jede Datei ausgeben, nicht nur für Verzeichnisse"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--apparent-size>"
msgstr "B<--apparent-size>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"print apparent sizes rather than device usage; although the apparent size is "
"usually smaller, it may be larger due to holes in ('sparse') files, internal "
"fragmentation, indirect blocks, and the like"
msgstr ""
"Die scheinbare Größe statt des Geräteplatzverbrauchs ausgeben. Sie ist meist "
"kleiner als der belegte Platz, kann aber auch durch Löcher in speziellen "
"Dateien (»sparse files«), interne Fragmentierung, indirekte Blöcke und "
"ähnliches größer sein."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-B>, B<--block-size>=I<\\,SIZE\\/>"
msgstr "B<-B>, B<--block-size>=I<\\,GRÖSSE\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"scale sizes by SIZE before printing them; e.g., \\&'-BM' prints sizes in "
"units of 1,048,576 bytes; see SIZE format below"
msgstr ""
"Größen mit GRÖSSE skalieren, bevor sie ausgegeben werden. Beispielsweise "
"gibt \\&»-BM« Größen in Einheiten von 1.048.576 Byte aus. Siehe das Format "
"von GRÖSSE unten."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-b>, B<--bytes>"
msgstr "B<-b>, B<--bytes>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "equivalent to '--apparent-size B<--block-size>=I<\\,1\\/>'"
msgstr "entspricht »--apparent-size B<--block-size>=I<\\,1\\/>«"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-c>, B<--total>"
msgstr "B<-c>, B<--total>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "produce a grand total"
msgstr "Gesamtsumme erstellen"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-D>, B<--dereference-args>"
msgstr "B<-D>, B<--dereference-args>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "dereference only symlinks that are listed on the command line"
msgstr ""
"Löst nur symbolische Verknüpfungen auf, die auf der Befehlszeile angegeben "
"wurden."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-d>, B<--max-depth>=I<\\,N\\/>"
msgstr "B<-d>, B<--max-depth>=I<\\,N\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"print the total for a directory (or file, with B<--all>)  only if it is N or "
"fewer levels below the command line argument; B<--max-depth>=I<\\,0\\/> is "
"the same as B<--summarize>"
msgstr ""
"Summe für ein Verzeichnis ausgeben (oder einer Datei, mit B<--all>) nur, "
"wenn es N oder weniger Ebenen unterhalb des Befehlszeilenarguments ist. B<--"
"max-depth>=I<\\,0\\/> ist dasselbe wie B<--summarize>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--files0-from>=I<\\,F\\/>"
msgstr "B<--files0-from>=I<\\,D\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"summarize device usage of the NUL-terminated file names specified in file F; "
"if F is -, then read names from standard input"
msgstr ""
"Berechnet den gesamten Geräteplatzverbrauch der in der Datei D angegebenen "
"Dateien (mit Nullbytes abgeschlossene Dateinamen). Ist D »-«, so wird aus "
"der Standardeingabe gelesen."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-H>"
msgstr "B<-H>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "equivalent to B<--dereference-args> (B<-D>)"
msgstr "entspricht B<--dereference-args> (B<-D>)"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-h>, B<--human-readable>"
msgstr "B<-h>, B<--human-readable>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "print sizes in human readable format (e.g., 1K 234M 2G)"
msgstr "Größen in menschenlesbarem Format ausgeben (z. B. 1K 234M 2G)"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--inodes>"
msgstr "B<--inodes>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "list inode usage information instead of block usage"
msgstr "Platzverbrauch anhand von Inodes statt anhand von Blöcken anzeigen"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-k>"
msgstr "B<-k>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "like B<--block-size>=I<\\,1K\\/>"
msgstr "Wie B<--block-size>=I<\\,1K\\/>"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-L>, B<--dereference>"
msgstr "B<-L>, B<--dereference>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "dereference all symbolic links"
msgstr "alle symbolischen Verknüpfungen auflösen"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-l>, B<--count-links>"
msgstr "B<-l>, B<--count-links>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "count sizes many times if hard linked"
msgstr ""
"Größen mehrfach zählen, wenn sie durch harte Verknüpfungen verbunden sind."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-m>"
msgstr "B<-m>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "like B<--block-size>=I<\\,1M\\/>"
msgstr "wie B<--block-size>=I<\\,1M\\/>"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-P>, B<--no-dereference>"
msgstr "B<-P>, B<--no-dereference>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "don't follow any symbolic links (this is the default)"
msgstr "symbolischen Verknüpfungen nicht folgen (Voreinstellung)"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-S>, B<--separate-dirs>"
msgstr "B<-S>, B<--separate-dirs>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "for directories do not include size of subdirectories"
msgstr "Bei Verzeichnissen die Größe von Unterverzeichnissen nicht mitzählen"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--si>"
msgstr "B<--si>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "like B<-h>, but use powers of 1000 not 1024"
msgstr "wie B<-h>, aber mit Potenzen von 1000 statt 1024"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-s>, B<--summarize>"
msgstr "B<-s>, B<--summarize>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "display only a total for each argument"
msgstr "nur eine Summe für jedes Argument anzeigen"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-t>, B<--threshold>=I<\\,SIZE\\/>"
msgstr "B<-t>, B<--threshold>=I<\\,GRÖSSE\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"exclude entries smaller than SIZE if positive, or entries greater than SIZE "
"if negative"
msgstr ""
"Einträge kleiner als GRÖSSE ignorieren (falls positiv) oder Einträge größer "
"als GRÖSSE ignorieren (falls negativ)"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--time>"
msgstr "B<--time>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"show time of the last modification of any file in the directory, or any of "
"its subdirectories"
msgstr ""
"Zeitpunkt der letzten Änderung einer Datei im Verzeichnis oder einem seiner "
"Unterverzeichnisse anzeigen"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--time>=I<\\,WORD\\/>"
msgstr "B<--time>=I<\\,WORT\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"show time as WORD instead of modification time: atime, access, use, ctime or "
"status"
msgstr ""
"Zeit als WORT anstelle der Änderungszeit anzeigen: atime, access, use, ctime "
"oder status"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--time-style>=I<\\,STYLE\\/>"
msgstr "B<--time-style>=I<\\,STIL\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"show times using STYLE, which can be: full-iso, long-iso, iso, or +FORMAT; "
"FORMAT is interpreted like in 'date'"
msgstr ""
"Zeit in bestimmtem STIL anzeigen: full-iso, long-iso, iso, +FORMAT. FORMAT "
"wird wie bei »date« interpretiert."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-X>, B<--exclude-from>=I<\\,FILE\\/>"
msgstr "B<-X>, B<--exclude-from>=I<\\,DATEI\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "exclude files that match any pattern in FILE"
msgstr "Dateien ausschließen, die auf irgendein Muster in DATEI passen"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--exclude>=I<\\,PATTERN\\/>"
msgstr "B<--exclude>=I<\\,MUSTER\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "exclude files that match PATTERN"
msgstr "Dateien ausschließen, die auf MUSTER passen"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-x>, B<--one-file-system>"
msgstr "B<-x>, B<--one-file-system>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "skip directories on different file systems"
msgstr "Verzeichnisse auf anderen Dateisystemen überspringen"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "display this help and exit"
msgstr "zeigt Hilfeinformationen an und beendet das Programm."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--version>"
msgstr "B<--version>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "output version information and exit"
msgstr "gibt Versionsinformationen aus und beendet das Programm."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Display values are in units of the first available SIZE from B<--block-"
"size>, and the DU_BLOCK_SIZE, BLOCK_SIZE and BLOCKSIZE environment "
"variables.  Otherwise, units default to 1024 bytes (or 512 if "
"POSIXLY_CORRECT is set)."
msgstr ""
"Die Einheit für die angezeigten Größen ist die erste verfügbare Angabe aus "
"der Option B<--block-size> und den Umgebungsvariablen DU_BLOCK_SIZE, "
"BLOCK_SIZE und BLOCKSIZE. Ansonsten wird als Einheit standardmäßig 1024 Byte "
"verwendet (oder 512, wenn POSIXLY_CORRECT gesetzt ist)."

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid ""
"The SIZE argument is an integer and optional unit (example: 10K is "
"10*1024).  Units are K,M,G,T,P,E,Z,Y,R,Q (powers of 1024) or KB,MB,... "
"(powers of 1000).  Binary prefixes can be used, too: KiB=K, MiB=M, and so on."
msgstr ""
"GRÖSSE ist eine Ganzzahl und optionale Einheit (Beispiel: 10K ist 10*1024). "
"Einheiten sind K, M, G, T, P, E, Z, Y, R, Q (Potenzen von 1024) oder KB, MB, "
"… (Potenzen von 1000). Es können auch binäre Präfixe verwandt werden: KiB=K, "
"MiB=M und so weiter."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "PATTERNS"
msgstr "MUSTER"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"PATTERN is a shell pattern (not a regular expression).  The pattern B<?\\&> "
"matches any one character, whereas B<*> matches any string (composed of "
"zero, one or multiple characters).  For example, B<*.o> will match any files "
"whose names end in B<.o>.  Therefore, the command"
msgstr ""
"MUSTER ist ein Shell-Muster (kein regulärer Ausdruck). Das Muster B<?\\&> "
"ersetzt ein beliebiges Zeichen, wohingegen B<*> für eine beliebige "
"Zeichenkette (aus keinem, einem oder mehreren Zeichen) steht. Beispielsweise "
"steht B<*.o> für alle Dateien, deren Namen mit B<.o> enden. Daher wird der "
"Befehl"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<du --exclude=\\(aq*.o\\(aq>"
msgstr "B<du --exclude=\\(aq*.o\\(aq>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"will skip all files and subdirectories ending in B<.o> (including the file "
"B<.o> itself)."
msgstr ""
"alle Dateien und Unterverzeichnisse überspringen, die auf B<.o> enden "
"(einschließlich der Datei B<.o>)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Written by Torbjorn Granlund, David MacKenzie, Paul Eggert, and Jim Meyering."
msgstr ""
"Geschrieben von Torbjörn Granlund, David MacKenzie, Paul Eggert und Jim "
"Meyering."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "FEHLER MELDEN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"GNU coreutils online help: E<lt>https://www.gnu.org/software/coreutils/E<gt>"
msgstr ""
"Onlinehilfe für GNU coreutils: E<.UR https://www.gnu.org/software/coreutils/"
">E<.UE>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Report any translation bugs to E<lt>https://translationproject.org/team/E<gt>"
msgstr ""
"Melden Sie Fehler in der Übersetzung an E<.UR https://translationproject.org/"
"team/de.html> das deutschsprachige Team beim GNU Translation ProjectE<.UE .>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "COPYRIGHT"
msgstr "COPYRIGHT"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid ""
"Copyright \\(co 2023 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2023 Free Software Foundation, Inc. Lizenz GPLv3+: E<.UR "
"https://gnu.org/licenses/gpl.html> GNU GPL Version 3 E<.UE> oder neuer."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""
"Dies ist freie Software: Sie können sie verändern und weitergeben. Es gibt "
"KEINE GARANTIE, soweit gesetzlich zulässig."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Full documentation E<lt>https://www.gnu.org/software/coreutils/duE<gt>"
msgstr ""
"Vollständige Dokumentation unter: E<lt>https://www.gnu.org/software/"
"coreutils/duE<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "or available locally via: info \\(aq(coreutils) du invocation\\(aq"
msgstr "oder lokal verfügbar mit: info \\(aq(coreutils) du invocation\\(aq"

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "September 2022"
msgstr "September 2022"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "GNU coreutils 9.1"
msgstr "GNU coreutils 9.1"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid ""
"The SIZE argument is an integer and optional unit (example: 10K is "
"10*1024).  Units are K,M,G,T,P,E,Z,Y (powers of 1024) or KB,MB,... (powers "
"of 1000).  Binary prefixes can be used, too: KiB=K, MiB=M, and so on."
msgstr ""
"GRÖSSE ist eine Ganzzahl und optionale Einheit (Beispiel: 10K ist 10*1024). "
"Einheiten sind K, M, G, T, P, E, Z, Y (Potenzen von 1024) oder KB, MB, … "
"(Potenzen von 1000). Es können auch binäre Präfixe verwandt werden: KiB=K, "
"MiB=M und so weiter."

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron
msgid ""
"Copyright \\(co 2022 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2022 Free Software Foundation, Inc. Lizenz GPLv3+: E<.UR "
"https://gnu.org/licenses/gpl.html> GNU GPL Version 3 E<.UE> oder neuer."

#. type: TH
#: fedora-39
#, no-wrap
msgid "GNU coreutils 9.3"
msgstr "GNU coreutils 9.3"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "April 2022"
msgstr "April 2022"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "October 2021"
msgstr "Oktober 2021"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "GNU coreutils 8.32"
msgstr "GNU Coreutils 8.32"

#. type: Plain text
#: opensuse-leap-15-6
msgid "Summarize disk usage of the set of FILEs, recursively for directories."
msgstr ""
"Gibt den Plattenplatzverbrauch jeder DATEI an, rekursiv für Verzeichnisse."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"print apparent sizes, rather than disk usage; although the apparent size is "
"usually smaller, it may be larger due to holes in ('sparse') files, internal "
"fragmentation, indirect blocks, and the like"
msgstr ""
"Die scheinbare Größe statt des Platzverbrauchs ausgeben. Sie ist meist "
"kleiner als der belegte Platz, kann aber auch durch Löcher in speziellen "
"Dateien (»sparse files«), interne Fragmentierung, indirekte Blöcke und "
"ähnliches größer sein."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"summarize disk usage of the NUL-terminated file names specified in file F; "
"if F is -, then read names from standard input"
msgstr ""
"Berechnet den gesamten Platzverbrauch der in der Datei D angegebenen Dateien "
"(mit Nullbytes abgeschlossene Dateinamen). Ist D »-«, so wird aus der "
"Standardeingabe gelesen."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Copyright \\(co 2020 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2020 Free Software Foundation, Inc. Lizenz GPLv3+: E<.UR "
"https://gnu.org/licenses/gpl.html> GNU GPL Version 3 E<.UE> oder neuer."
