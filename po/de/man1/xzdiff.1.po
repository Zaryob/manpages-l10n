# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Mario Blättermann <mario.blaettermann@gmail.com>, 2015.
msgid ""
msgstr ""
"Project-Id-Version: manpages-de\n"
"POT-Creation-Date: 2023-08-27 17:33+0200\n"
"PO-Revision-Date: 2015-02-13 14:20+0100\n"
"Last-Translator: Mario Blättermann <mario.blaettermann@gmail.com>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.7.3\n"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "XZDIFF"
msgstr "XZDIFF"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2011-03-19"
msgstr "19. März 2011"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Tukaani"
msgstr "Tukaani"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "XZ Utils"
msgstr "XZ-Dienstprogramme"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: opensuse-leap-15-6
msgid "xzcmp, xzdiff, lzcmp, lzdiff - compare compressed files"
msgstr "xzcmp, xzdiff, lzcmp, lzdiff - komprimierte Dateien vergleichen"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<xzcmp> [I<cmp_options>] I<file1> [I<file2>]"
msgstr "B<xzcmp> [I<cmp-Optionen>] I<Datei1> [I<Datei2>]"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<xzdiff> [I<diff_options>] I<file1> [I<file2>]"
msgstr "B<xzdiff> [I<diff-Optionen>] I<Datei1> [I<Datei2>]"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<lzcmp> [I<cmp_options>] I<file1> [I<file2>]"
msgstr "B<lzcmp> [I<cmp-Optionen>] I<Datei1> [I<Datei2>]"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<lzdiff> [I<diff_options>] I<file1> [I<file2>]"
msgstr "B<lzdiff> [I<diff-Optionen>] I<Datei1> [I<Datei2>]"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"B<xzcmp> and B<xzdiff> invoke B<cmp>(1)  or B<diff>(1)  on files compressed "
"with B<xz>(1), B<lzma>(1), B<gzip>(1), B<bzip2>(1), or B<lzop>(1).  All "
"options specified are passed directly to B<cmp>(1)  or B<diff>(1).  If only "
"one file is specified, then the files compared are I<file1> (which must have "
"a suffix of a supported compression format) and I<file1> from which the "
"compression format suffix has been stripped.  If two files are specified, "
"then they are uncompressed if necessary and fed to B<cmp>(1)  or "
"B<diff>(1).  The exit status from B<cmp>(1)  or B<diff>(1)  is preserved."
msgstr ""
"Die Dienstprogramme B<xzcmp> und B<xzdiff> führen die Programme B<cmp>(1) "
"beziehungsweise B<diff>(1) mit Dateien aus, die mittels B<xz>(1), "
"B<lzma>(1), B<gzip>(1), B<bzip2>(1) oder B<lzop>(1) komprimiert wurden. Alle "
"angegebenen Optionen werden direkt an B<cmp>(1) oder B<diff>(1) übergeben. "
"Wird nur eine Datei angegeben, wird diese I<Datei1> (die eine Endung "
"entsprechend eines der unterstützten Kompressionsformate haben muss) mit der "
"I<Datei1> verglichen, von der die Kompressionsformat-Endung entfernt wird. "
"Werden zwei Dateien angegeben, dann werden deren Inhalte (falls nötig, "
"unkomprimiert) an B<cmp>(1) oder B<diff>(1) weitergeleitet. Der Exit-Status "
"von B<cmp>(1) oder B<diff>(1) wird dabei bewahrt."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"The names B<lzcmp> and B<lzdiff> are provided for backward compatibility "
"with LZMA Utils."
msgstr ""
"Die Namen B<lzcmp> und B<lzdiff> dienen der Abwärtskompatibilität zu den "
"LZMA-Dienstprogrammen."

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"B<cmp>(1), B<diff>(1), B<xz>(1), B<gzip>(1), B<bzip2>(1), B<lzop>(1), "
"B<zdiff>(1)"
msgstr ""
"B<cmp>(1), B<diff>(1), B<xz>(1), B<gzip>(1), B<bzip2>(1), B<lzop>(1), "
"B<zdiff>(1)"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "BUGS"
msgstr "FEHLER"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Messages from the B<cmp>(1)  or B<diff>(1)  programs refer to temporary "
"filenames instead of those specified."
msgstr ""
"Die Meldungen der Programme B<cmp>(1) oder B<diff>(1) können auf temporäre "
"Dateinamen verweisen anstatt auf die tatsächlich angegebenen Dateinamen."
