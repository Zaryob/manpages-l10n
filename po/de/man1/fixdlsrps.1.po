# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Helge Kreutzmann <debian@helgefjell.de>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.13\n"
"POT-Creation-Date: 2023-06-27 19:27+0200\n"
"PO-Revision-Date: 2022-03-12 14:18+0100\n"
"Last-Translator: Helge Kreutzmann <debian@helgefjell.de>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "FIXDLSRPS"
msgstr "FIXDLSRPS"

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "PSUtils Release 1 Patchlevel 17"
msgstr "PSUtils Release 1 Patchlevel 17"

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "fixdlsrps - filter to fix DviLaser/PS documents to work with PSUtils"
msgstr ""
"fixdlsrps - Filter zur Korrektur von DviLaser/PS-Dokumenten, damit sie mit "
"PSUtils funktionieren"

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "B<fixdlsrps> E<lt> I<dvilaser.ps> E<gt> I<Fixed.ps>"
msgstr "B<fixdlsrps> E<lt> I<dvilaser.ps> E<gt> I<Korrigiert.ps>"

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"I<Fixdlsrps> is a I<perl> filter which \"fixes\" PostScript generated from "
"the DviLaser/PS driver so that it works correctly with Angus Duggan's "
"B<psutils> package."
msgstr ""
"I<Fixdlsrps> ist ein I<perl>-Filter, der vom Treiber DviLaser/PS erstelltes "
"PostScript »korrigiert«, so dass es korrekt mit dem Paket B<psutils> von "
"Angus Duggan funktioniert."

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "Copyright (C) Angus J. C. Duggan 1991-1995"
msgstr "Copyright \\(co Angus J. C. Duggan 1991-1995"

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

# FIXME Missing B<> around names, e.g. pstops(1) → B<pstops>(1)
# FIXME Remove psbook(1), see also to itself does not make sense
#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"psbook(1), psselect(1), pstops(1), epsffit(1), psnup(1), psresize(1), "
"psmerge(1), fixscribeps(1), getafm(1), fixdlsrps(1), fixfmps(1), "
"fixpsditps(1), fixpspps(1), fixtpps(1), fixwfwps(1), fixwpps(1), fixwwps(1), "
"extractres(1), includeres(1), showchar(1)"
msgstr ""
"B<psbook>(1), B<psselect>(1), B<pstops>(1), B<epsffit>(1), B<psnup>(1), "
"B<psresize>(1), B<psmerge>(1), B<fixscribeps>(1), B<getafm>(1), "
"B<fixdlsrps>(1), B<fixfmps>(1), B<fixpsditps>(1), B<fixpspps>(1), "
"B<fixtpps>(1), B<fixwfwps>(1), B<fixwpps>(1), B<fixwwps>(1), "
"B<extractres>(1), B<includeres>(1), B<showchar>(1)"

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "TRADEMARKS"
msgstr "WARENZEICHEN"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "B<PostScript> is a trademark of Adobe Systems Incorporated."
msgstr ""
"B<PostScript> ist ein eingetragenes Warenzeichen von Adobe Systems "
"Incorporated."
