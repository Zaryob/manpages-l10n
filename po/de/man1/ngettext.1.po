# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Mario Blättermann <mario.blaettermann@gmail.com>, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.13\n"
"POT-Creation-Date: 2023-10-02 12:13+0200\n"
"PO-Revision-Date: 2023-07-02 16:03+0200\n"
"Last-Translator: Mario Blättermann <mario.blaettermann@gmail.com>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 23.04.1\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "NGETTEXT"
msgstr "NGETTEXT"

#. type: TH
#: archlinux debian-unstable
#, no-wrap
msgid "September 2023"
msgstr "September 2023"

#. type: TH
#: archlinux
#, no-wrap
msgid "GNU gettext-runtime 0.22.2"
msgstr "GNU gettext-runtime 0.22.2"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "User Commands"
msgstr "Dienstprogramme für Benutzer"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
msgid "ngettext - translate message and choose plural form"
msgstr "ngettext - Meldungen übersetzen und Pluralform wählen"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
msgid ""
"B<ngettext> [I<\\,OPTION\\/>] [I<\\,TEXTDOMAIN\\/>] I<\\,MSGID MSGID-PLURAL "
"COUNT\\/>"
msgstr ""
"B<ngettext> [I<\\,OPTION\\/>] [I<\\,TEXTDOMAIN\\/>] I<\\,MSGID MSGID-PLURAL-"
"ANZAHL\\/>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

# FIXME What is a »natural« language? I assume it means American English, but this is quite far from being »natural«… I propose to use something like »message in the original language«.
#.  Add any additional description here
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
msgid ""
"The B<ngettext> program translates a natural language message into the "
"user's language, by looking up the translation in a message catalog, and "
"chooses the appropriate plural form, which depends on the number I<COUNT> "
"and the language of the message catalog where the translation was found."
msgstr ""
"Das Programm B<ngettext> übersetzt eine Meldung aus der Ausgangssprache in "
"die Sprache des Benutzers, indem die Übersetzung im Meldungskatalog "
"nachgeschlagen wird, und wählt die entsprechende Pluralform. Diese hängt von "
"der angegebenen I<ANZAHL> und der Sprache des Meldungskatalogs ab, in dem "
"die Übersetzung gefunden wurde."

# FIXME What is the »native« language? Moreover, a »textual message« is what Gettext accepts in general, a »non-textual« message doesn't exist either.
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
msgid ""
"Display native language translation of a textual message whose grammatical "
"form depends on a number."
msgstr ""
"Zeigt die Übersetzung einer Meldung an, deren grammatische Form von einer "
"Zahl abhängt."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "B<-d>, B<--domain>=I<\\,TEXTDOMAIN\\/>"
msgstr "B<-d>, B<--domain>=I<\\,TEXTDOMAIN\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
msgid "retrieve translated message from TEXTDOMAIN"
msgstr "bezieht die übersetzte Meldung aus der angegebenen TEXTDOMAIN."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "B<-c>, B<--context>=I<\\,CONTEXT\\/>"
msgstr "B<-c>, B<--context>=I<\\,KONTEXT\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
msgid "specify context for MSGID"
msgstr "gibt den Kontext für MSGID an."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "B<-e>"
msgstr "B<-e>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
msgid "enable expansion of some escape sequences"
msgstr "aktiviert die Expansion einiger Maskiersequenzen."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "B<-E>"
msgstr "B<-E>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
msgid "(ignored for compatibility)"
msgstr "(wird ignoriert; nur für Kompatibilitätszwecke)"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "[TEXTDOMAIN]"
msgstr "[TEXTDOMAIN]"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "MSGID MSGID-PLURAL"
msgstr "MSGID MSGID-PLURAL"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
msgid "translate MSGID (singular) / MSGID-PLURAL (plural)"
msgstr "übersetzt MSGID (Singular) / MSGID-PLURAL (Plural)"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "COUNT"
msgstr "ANZAHL"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
msgid "choose singular/plural form based on this value"
msgstr "wählt die Singular- bzw. Pluralform basierend auf diesem Wert."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "Informative output:"
msgstr "Informative Ausgabe:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "B<-h>, B<--help>"
msgstr "B<-h>, B<--help>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
msgid "display this help and exit"
msgstr "zeigt Hilfeinformationen an und beendet das Programm."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
msgid "display version information and exit"
msgstr "zeigt Versionsinformationen an und beendet das Programm."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
msgid ""
"If the TEXTDOMAIN parameter is not given, the domain is determined from the "
"environment variable TEXTDOMAIN.  If the message catalog is not found in the "
"regular directory, another location can be specified with the environment "
"variable TEXTDOMAINDIR.  Standard search directory: /usr/share/locale"
msgstr ""
"Falls der Parameter TEXTDOMAIN nicht angegeben ist, wird die Domain aus der "
"Umgebungsvariable TEXTDOMAIN ermittelt. Wird der Meldungskatalog im durch "
"diese Variable vorgegebenen Verzeichnis nicht gefunden, dann können Sie mit "
"der Umgebungsvariable TEXTDOMAINDIR einen anderen Ort angeben. Standardmäßig "
"wird I</usr/share/locale> verwendet."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
msgid "Written by Ulrich Drepper."
msgstr "Geschrieben von Ulrich Drepper."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "REPORTING BUGS"
msgstr "FEHLER MELDEN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
msgid ""
"Report bugs in the bug tracker at E<lt>https://savannah.gnu.org/projects/"
"gettextE<gt> or by email to E<lt>bug-gettext@gnu.orgE<gt>."
msgstr ""
"Melden Sie Fehler im Fehlererfassungssystem auf E<.UR https://savannah.gnu."
"org/projects/gettext> E<.UE> oder per E-Mail an E<.MT bug-gettext@gnu.org> "
"E<.ME> (jeweils auf Englisch)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "COPYRIGHT"
msgstr "COPYRIGHT"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide
msgid ""
"Copyright \\(co 1995-1997, 2000-2023 Free Software Foundation, Inc.  License "
"GPLv3+: GNU GPL version 3 or later E<lt>https://gnu.org/licenses/gpl."
"htmlE<gt>"
msgstr ""
"Copyright \\(co 1995-1997, 2000-2023 Free Software Foundation, Inc. Lizenz "
"GPLv3+: E<.UR https://gnu.org/licenses/gpl.html> GNU GPL Version 3 E<.UE> "
"oder neuer."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""
"Dies ist freie Software: Sie können sie verändern und weitergeben. Es gibt "
"KEINE GARANTIE, soweit gesetzlich zulässig."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
msgid ""
"The full documentation for B<ngettext> is maintained as a Texinfo manual.  "
"If the B<info> and B<ngettext> programs are properly installed at your site, "
"the command"
msgstr ""
"Die vollständige Dokumentation für B<ngettext> wird als ein Texinfo-Handbuch "
"gepflegt. Wenn die Programme B<info>(1) und B<ngettext> auf Ihrem Rechner "
"ordnungsgemäß installiert sind, können Sie mit dem Befehl"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
msgid "B<info ngettext>"
msgstr "B<info ngettext>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
msgid "should give you access to the complete manual."
msgstr "auf das vollständige Handbuch zugreifen."

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "February 2023"
msgstr "Februar 2023"

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "GNU gettext-runtime 0.21"
msgstr "GNU gettext-runtime 0.21"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"Copyright \\(co 1995-1997, 2000-2020 Free Software Foundation, Inc.  License "
"GPLv3+: GNU GPL version 3 or later E<lt>https://gnu.org/licenses/gpl."
"htmlE<gt>"
msgstr ""
"Copyright \\(co 1995-1997, 2000-2020 Free Software Foundation, Inc. Lizenz "
"GPLv3+: E<.UR https://gnu.org/licenses/gpl.html> GNU GPL Version 3 E<.UE> "
"oder neuer."

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "June 2023"
msgstr "Juni 2023"

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "GNU gettext-runtime 0.22"
msgstr "GNU gettext-runtime 0.22"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "October 2022"
msgstr "Oktober 2022"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "GNU gettext-runtime 0.21.1"
msgstr "GNU gettext-runtime 0.21.1"

#. type: Plain text
#: mageia-cauldron
msgid ""
"Copyright \\(co 1995-1997, 2000-2022 Free Software Foundation, Inc.  License "
"GPLv3+: GNU GPL version 3 or later E<lt>https://gnu.org/licenses/gpl."
"htmlE<gt>"
msgstr ""
"Copyright \\(co 1995-1997, 2000-2022 Free Software Foundation, Inc. Lizenz "
"GPLv3+: E<.UR https://gnu.org/licenses/gpl.html> GNU GPL Version 3 E<.UE> "
"oder neuer."
