# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Helge Kreutzmann <debian@helgefjell.de>, 2022
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.16.0\n"
"POT-Creation-Date: 2023-08-27 17:29+0200\n"
"PO-Revision-Date: 2023-01-11 15:33+0100\n"
"Last-Translator: Helge Kreutzmann <debian@helgefjell.de>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "thread-keyring"
msgstr "thread-keyring"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "2022-10-30"
msgstr "30. Oktober 2022"

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Linux man-pages 6.05.01"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "thread-keyring - per-thread keyring"
msgstr "thread-keyring - Thread-bezogener Schlüsselbund"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The thread keyring is a keyring used to anchor keys on behalf of a process.  "
"It is created only when a thread requests it.  The thread keyring has the "
"name (description)  I<_tid>."
msgstr ""
"Der Thread-Schlüsselbund ist ein Schlüsselbund, um Schlüssel im Auftrag "
"eines Prozesses aufzuhängen. Er wird erstellt, wenn ein Thread das "
"beantragt. Der Thread-Schlüsselbund hat den Namen (die Beschreibung) I<_tid>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"A special serial number value, B<KEY_SPEC_THREAD_KEYRING>, is defined that "
"can be used in lieu of the actual serial number of the calling thread's "
"thread keyring."
msgstr ""
"Eine besondere Seriennummer (B<KEY_SPEC_THREAD_KEYRING>) ist definiert, die "
"anstelle der tatsächlichen Seriennummer des Thread-Schlüsselbundes des "
"aufrufenden Threads verwandt werden kann."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"From the B<keyctl>(1)  utility, 'B<@t>' can be used instead of a numeric key "
"ID in much the same way, but as B<keyctl>(1)  is a program run after "
"forking, this is of no utility."
msgstr ""
"Gemäß des Hilfswerkzeugs B<keyctl>(1) kann »B<@t>« anstelle der numerischen "
"Schlüsselkennung auf nahezu die gleiche Weise verwandt werden, aber "
"B<keyctl>(1) ist ein Programm, das nach einem Fork aufgerufen wird und hilft "
"daher nicht."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Thread keyrings are not inherited across B<clone>(2)  and B<fork>(2)  and "
"are cleared by B<execve>(2).  A thread keyring is destroyed when the thread "
"that refers to it terminates."
msgstr ""
"Thread-Schlüsselbunde werden nicht über B<clone>(2) und B<fork>(2) hinweg "
"vererbt und werden durch B<execve>(2) bereinigt. Ein Thread-Schlüsselbund "
"wird zerstört, wenn der Thread, der auf ihn verweist, sich beendet."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Initially, a thread does not have a thread keyring.  If a thread doesn't "
"have a thread keyring when it is accessed, then it will be created if it is "
"to be modified; otherwise the operation fails with the error B<ENOKEY>."
msgstr ""
"Anfänglich hat ein Thread keinen Thread-Schlüsselbund. Falls ein Thread beim "
"Zugriff darauf keinen Thread-Schlüsselbund hat, dann wird dieser erstellt, "
"falls er verändert werden soll, ansonsten schlägt die Aktion mit dem Fehler "
"B<ENOKEY> fehl."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<keyctl>(1), B<keyctl>(3), B<keyrings>(7), B<persistent-keyring>(7), "
"B<process-keyring>(7), B<session-keyring>(7), B<user-keyring>(7), B<user-"
"session-keyring>(7)"
msgstr ""
"B<keyctl>(1), B<keyctl>(3), B<keyrings>(7), B<persistent-keyring>(7), "
"B<process-keyring>(7), B<session-keyring>(7), B<user-keyring>(7), B<user-"
"session-keyring>(7)"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.05"
msgstr "Linux man-pages 6.05"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "THREAD-KEYRING"
msgstr "THREAD-KEYRING"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2017-03-13"
msgstr "13. März 2017"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Linux-Programmierhandbuch"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "COLOPHON"
msgstr "KOLOPHON"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Diese Seite ist Teil der Veröffentlichung 4.16 des Projekts Linux-I<man-"
"pages>. Eine Beschreibung des Projekts, Informationen, wie Fehler gemeldet "
"werden können, sowie die aktuelle Version dieser Seite finden sich unter \\"
"%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux-Handbuchseiten 6.04"
