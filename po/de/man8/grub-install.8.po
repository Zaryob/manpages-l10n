# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Mario Blättermann <mario.blaettermann@gmail.com>, 2020, 2021, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.0.0\n"
"POT-Creation-Date: 2023-08-27 17:01+0200\n"
"PO-Revision-Date: 2023-07-02 15:58+0200\n"
"Last-Translator: Mario Blättermann <mario.blaettermann@gmail.com>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 23.04.1\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "GRUB-INSTALL"
msgstr "GRUB-INSTALL"

#. type: TH
#: archlinux
#, no-wrap
msgid "July 2023"
msgstr "Juli 2023"

#. type: TH
#: archlinux
#, no-wrap
msgid "GRUB 2:2.12rc1-1"
msgstr "GRUB 2:2.12rc1-1"

#. type: TH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "System Administration Utilities"
msgstr "Systemverwaltungswerkzeuge"

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux
msgid "grub-install - install GRUB to a device"
msgstr "grub-install - GRUB auf einem Gerät installieren"

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: archlinux
msgid ""
"B<grub-install> [I<\\,OPTION\\/>...] [I<\\,OPTION\\/>] [I<\\,"
"INSTALL_DEVICE\\/>]"
msgstr ""
"B<grub-install> [I<\\,OPTION\\/> …] [I<\\,OPTION\\/>] [I<\\,"
"INSTALLATIONSGERÄT\\/>]"

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Install GRUB on your drive."
msgstr "Installiert GRUB auf Ihrem Laufwerk."

#. type: TP
#: archlinux
#, no-wrap
msgid "B<--compress>=I<\\,no\\/>|xz|gz|lzo"
msgstr "B<--compress>=I<\\,no\\/>|xz|gz|lzo"

#. type: Plain text
#: archlinux
msgid "compress GRUB files [optional]"
msgstr "komprimiert GRUB-Dateien (optional)."

#. type: TP
#: archlinux
#, no-wrap
msgid "B<--disable-shim-lock>"
msgstr "B<--disable-shim-lock>"

#. type: Plain text
#: archlinux
msgid "disable shim_lock verifier"
msgstr "deaktiviert die shim_lock-Überprüfung."

#. type: TP
#: archlinux
#, no-wrap
msgid "B<--dtb>=I<\\,FILE\\/>"
msgstr "B<--dtb>=I<\\,DATEI\\/>"

#. type: Plain text
#: archlinux
msgid "embed a specific DTB"
msgstr "bettet eine spezifische DTB ein."

#. type: TP
#: archlinux
#, no-wrap
msgid "B<-d>, B<--directory>=I<\\,DIR\\/>"
msgstr "B<-d>, B<--directory>=I<\\,VERZEICHNIS\\/>"

#. type: Plain text
#: archlinux
msgid ""
"use images and modules under DIR [default=/usr/lib/grub/E<lt>platformE<gt>]"
msgstr ""
"verwendet Abbilder und Module im angegebenen VERZEICHNIS (Vorgabe ist /usr/"
"lib/grub/E<lt>PlattformE<gt>)."

#. type: TP
#: archlinux
#, no-wrap
msgid "B<--fonts>=I<\\,FONTS\\/>"
msgstr "B<--fonts>=I<\\,SCHRIFTEN\\/>"

#. type: Plain text
#: archlinux
msgid "install FONTS [default=unicode]"
msgstr "installiert die angegebenen SCHRIFTEN (Vorgabe=unicode)."

#. type: TP
#: archlinux
#, no-wrap
msgid "B<--install-modules>=I<\\,MODULES\\/>"
msgstr "B<--install-modules>=I<\\,MODULE\\/>"

#. type: Plain text
#: archlinux
msgid "install only MODULES and their dependencies [default=all]"
msgstr ""
"installiert nur die angegebenen MODULE und deren Abhängigkeiten "
"(Vorgabe=alle)."

#. type: TP
#: archlinux
#, no-wrap
msgid "B<-k>, B<--pubkey>=I<\\,FILE\\/>"
msgstr "B<-k>, B<--pubkey>=I<\\,DATEI\\/>"

#. type: Plain text
#: archlinux
msgid "embed FILE as public key for signature checking"
msgstr ""
"bettet die angegebene DATEI als öffentlichen Schlüssel für die Überprüfung "
"der Signatur ein."

# FIXME argument and description need to be separated in two gettext messages
#. type: TP
#: archlinux
#, no-wrap
msgid "B<--locale-directory>=I<\\,DIR\\/> use translations under DIR"
msgstr "B<--locale-directory>=I<\\,VERZEICHNIS\\/> verwendet die Übersetzungen im angegebenen Verzeichnis."

#. type: Plain text
#: archlinux
msgid "[default=/usr/share/locale]"
msgstr "[Vorgabe=/usr/share/locale]"

#. type: TP
#: archlinux
#, no-wrap
msgid "B<--locales>=I<\\,LOCALES\\/>"
msgstr "B<--locales>=I<\\,LOCALES\\/>"

#. type: Plain text
#: archlinux
msgid "install only LOCALES [default=all]"
msgstr "installiert nur die angegebenen LOCALES (Vorgabe=alle)."

#. type: TP
#: archlinux
#, no-wrap
msgid "B<--modules>=I<\\,MODULES\\/>"
msgstr "B<--modules>=I<\\,MODULE\\/>"

#. type: Plain text
#: archlinux
msgid "pre-load specified modules MODULES"
msgstr "lädt die angegebenen MODULE vor."

#. type: TP
#: archlinux
#, no-wrap
msgid "B<--sbat>=I<\\,FILE\\/>"
msgstr "B<--sbat>=I<\\,DATEI\\/>"

#. type: Plain text
#: archlinux
msgid "SBAT metadata"
msgstr "SBAT-Metadaten"

#. type: TP
#: archlinux
#, no-wrap
msgid "B<--themes>=I<\\,THEMES\\/>"
msgstr "B<--themes>=I<\\,THEMEN\\/>"

#. type: Plain text
#: archlinux
msgid "install THEMES [default=starfield]"
msgstr "installiert die angegebenen THEMEN (Vorgabe=starfield)."

#. type: TP
#: archlinux
#, no-wrap
msgid "B<-v>, B<--verbose>"
msgstr "B<-v>, B<--verbose>"

#. type: Plain text
#: archlinux
msgid "print verbose messages."
msgstr "gibt ausführliche Meldungen aus."

#. type: TP
#: archlinux
#, no-wrap
msgid "B<--allow-floppy>"
msgstr "B<--allow-floppy>"

#. type: Plain text
#: archlinux
msgid ""
"make the drive also bootable as floppy (default for fdX devices). May break "
"on some BIOSes."
msgstr ""
"macht das Laufwerk auch als Diskette startfähig (Vorgabe für fdX-Geräte). "
"Dies kann auf einigen BIOS fehlschlagen."

#. type: TP
#: archlinux
#, no-wrap
msgid "B<--boot-directory>=I<\\,DIR\\/>"
msgstr "B<--boot-directory>=I<\\,VERZEICHNIS\\/>"

#. type: Plain text
#: archlinux
msgid ""
"install GRUB images under the directory DIR/grub instead of the I<\\,/boot/"
"grub\\/> directory"
msgstr ""
"installiert GRUB-Abbilder im Verzeichnis VERZEICHNIS/grub anstelle des "
"Verzeichnisses I<\\,/boot/grub\\/>."

#. type: TP
#: archlinux
#, no-wrap
msgid "B<--bootloader-id>=I<\\,ID\\/>"
msgstr "B<--bootloader-id>=I<\\,KENNUNG\\/>"

#. type: Plain text
#: archlinux
msgid "the ID of bootloader. This option is only available on EFI and Macs."
msgstr ""
"gibt die Kennung des Bootloaders an. Diese Option ist nur für EFI und Mac "
"verfügbar."

#. type: TP
#: archlinux
#, no-wrap
msgid "B<--core-compress>=I<\\,xz\\/>|none|auto"
msgstr "B<--core-compress>=I<\\,xz\\/>|none|auto"

#. type: Plain text
#: archlinux
msgid "choose the compression to use for core image"
msgstr "wählt die für das Speicherkern-Abbild zu verwendende Kompression."

#. type: TP
#: archlinux
#, no-wrap
msgid "B<--disk-module>=I<\\,MODULE\\/>"
msgstr "B<--disk-module>=I<\\,MODUL\\/>"

# FIXME biosdisk or native → B<biosdisk> or <native>
#. type: Plain text
#: archlinux
msgid ""
"disk module to use (biosdisk or native). This option is only available on "
"BIOS target."
msgstr ""
"gibt das zu verwendende Platten-Modul an (B<biosdisk> oder B<native>). Diese "
"Option ist nur für das BIOS-Ziel verfügbar."

#. type: TP
#: archlinux
#, no-wrap
msgid "B<--efi-directory>=I<\\,DIR\\/>"
msgstr "B<--efi-directory>=I<\\,VERZEICHNIS\\/>"

#. type: Plain text
#: archlinux
msgid "use DIR as the EFI System Partition root."
msgstr "verwendet das angegebene VERZEICHNIS als EFI-Systempartitionswurzel."

#. type: TP
#: archlinux
#, no-wrap
msgid "B<--force>"
msgstr "B<--force>"

#. type: Plain text
#: archlinux
msgid "install even if problems are detected"
msgstr "installiert selbst dann, wenn Probleme erkannt werden."

#. type: TP
#: archlinux
#, no-wrap
msgid "B<--force-file-id>"
msgstr "B<--force-file-id>"

#. type: Plain text
#: archlinux
msgid "use identifier file even if UUID is available"
msgstr ""
"verwendet die Bezeichner-Datei selbst dann, wenn die UUID verfügbar ist."

#. type: TP
#: archlinux
#, no-wrap
msgid "B<--label-bgcolor>=I<\\,COLOR\\/>"
msgstr "B<--label-bgcolor>=I<\\,FARBE\\/>"

#. type: Plain text
#: archlinux
msgid "use COLOR for label background"
msgstr "verwendet die angegebene FARBE für den Hintergrund des Bezeichners."

#. type: TP
#: archlinux
#, no-wrap
msgid "B<--label-color>=I<\\,COLOR\\/>"
msgstr "B<--label-color>=I<\\,FARBE\\/>"

#. type: Plain text
#: archlinux
msgid "use COLOR for label"
msgstr "verwendet die angegebene FARBE für den Bezeichner."

#. type: TP
#: archlinux
#, no-wrap
msgid "B<--label-font>=I<\\,FILE\\/>"
msgstr "B<--label-font>=I<\\,DATEI\\/>"

#. type: Plain text
#: archlinux
msgid "use FILE as font for label"
msgstr "verwendet die angegebene DATEI als Schrift für den Bezeichner."

# FIXME argument and description need to be separated in two gettext messages
#. type: Plain text
#: archlinux
msgid "B<--macppc-directory>=I<\\,DIR\\/> use DIR for PPC MAC install."
msgstr ""
"B<--macppc-directory>=I<\\,VERZEICHNIS\\/> verwendet das angegebene "
"VERZEICHNIS für eine PPC-MAC-Installation."

#. type: TP
#: archlinux
#, no-wrap
msgid "B<--no-bootsector>"
msgstr "B<--no-bootsector>"

#. type: Plain text
#: archlinux
msgid "do not install bootsector"
msgstr "installiert keinen Bootsektor."

#. type: TP
#: archlinux
#, no-wrap
msgid "B<--no-nvram>"
msgstr "B<--no-nvram>"

#. type: Plain text
#: archlinux
msgid ""
"don't update the `boot-device'/`Boot*' NVRAM variables. This option is only "
"available on EFI and IEEE1275 targets."
msgstr ""
"aktualisiert die NVRAM-Variablen »Boot-Gerät« bzw. »Boot*« nicht. Diese "
"Option ist nur für EFI- und IEEE1275-Ziele verfügbar."

#. type: TP
#: archlinux
#, no-wrap
msgid "B<--no-rs-codes>"
msgstr "B<--no-rs-codes>"

#. type: Plain text
#: archlinux
msgid ""
"Do not apply any reed-solomon codes when embedding core.img. This option is "
"only available on x86 BIOS targets."
msgstr ""
"wendet beim Einbetten von core.img keinerlei Reed-Solomon-Codes an. Diese "
"Option ist nur für BIOS-Ziele auf x86-Architekturen verfügbar."

#. type: TP
#: archlinux
#, no-wrap
msgid "B<--product-version>=I<\\,STRING\\/>"
msgstr "B<--product-version>=I<\\,ZEICHENKETTE\\/>"

#. type: Plain text
#: archlinux
msgid "use STRING as product version"
msgstr "verwendet die angegebene ZEICHENKETTE als Produktversion."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<--recheck>"
msgstr "B<--recheck>"

#. type: Plain text
#: archlinux
msgid "delete device map if it already exists"
msgstr "löscht die Gerätezuordnung, falls diese bereits existiert."

#. type: TP
#: archlinux
#, no-wrap
msgid "B<--removable>"
msgstr "B<--removable>"

#. type: Plain text
#: archlinux
msgid ""
"the installation device is removable. This option is only available on EFI."
msgstr ""
"gibt an, dass das Installationsgerät ein Wechseldatenträger ist. Diese "
"Option ist nur für EFI verfügbar."

#. type: TP
#: archlinux
#, no-wrap
msgid "B<-s>, B<--skip-fs-probe>"
msgstr "B<-s>, B<--skip-fs-probe>"

#. type: Plain text
#: archlinux
msgid "do not probe for filesystems in DEVICE"
msgstr "prüft im angegebenen GERÄT nicht auf Dateisysteme."

#. type: TP
#: archlinux
#, no-wrap
msgid "B<--target>=I<\\,TARGET\\/>"
msgstr "B<--target>=I<\\,ZIEL\\/>"

#. type: Plain text
#: archlinux
msgid ""
"install GRUB for TARGET platform [default=i386-pc]; available targets: arm-"
"coreboot, arm-efi, arm-uboot, arm64-efi, i386-coreboot, i386-efi, i386-"
"ieee1275, i386-multiboot, i386-pc, i386-qemu, i386-xen, i386-xen_pvh, ia64-"
"efi, loongarch64-efi, mips-arc, mips-qemu_mips, mipsel-arc, mipsel-loongson, "
"mipsel-qemu_mips, powerpc-ieee1275, riscv32-efi, riscv64-efi, sparc64-"
"ieee1275, x86_64-efi, x86_64-xen"
msgstr ""
"installiert GRUB für das angegebene ZIEL [Vorgabe=i386-pc]. Folgende "
"Plattformen sind als ZIEL verfügbar: arm-coreboot, arm-efi, arm-uboot, arm64-"
"efi, i386-coreboot, i386-efi, i386-ieee1275, i386-multiboot, i386-pc, i386-"
"qemu, i386-xen, i386-xen_pvh, ia64-efi, loongarch64-efi, mips-arc, mips-"
"qemu_mips, mipsel-arc, mipsel-loongson, mipsel-qemu_mips, powerpc-ieee1275, "
"riscv32-efi, riscv64-efi, sparc64-ieee1275, x86_64-efi, x86_64-xen"

#. type: TP
#: archlinux
#, no-wrap
msgid "-?, B<--help>"
msgstr "B<-?>, B<--help>"

#. type: Plain text
#: archlinux
msgid "give this help list"
msgstr "gibt eine kurze Hilfe aus."

#. type: TP
#: archlinux
#, no-wrap
msgid "B<--usage>"
msgstr "B<--usage>"

#. type: Plain text
#: archlinux
msgid "give a short usage message"
msgstr "gibt eine kurze Meldung zur Verwendung aus."

#. type: TP
#: archlinux
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: archlinux
msgid "print program version"
msgstr "gibt die Programmversion aus."

#. type: Plain text
#: archlinux
msgid ""
"Mandatory or optional arguments to long options are also mandatory or "
"optional for any corresponding short options."
msgstr ""
"Erforderliche oder optionale Argumente für lange Optionen sind ebenso "
"erforderlich bzw. optional für die entsprechenden Kurzoptionen."

#. type: Plain text
#: archlinux
msgid ""
"INSTALL_DEVICE must be system device filename.  grub-install copies GRUB "
"images into I<\\,/boot/grub\\/>.  On some platforms, it may also install "
"GRUB into the boot sector."
msgstr ""
"Als INSTALLATIONSGERÄT muss der Dateiname eines Systemgeräts angegeben "
"werden. B<grub-install> kopiert GRUB-Abbilder nach I<\\,/boot/grub\\/>. Auf "
"einigen Plattformen könnte GRUB auch in den Bootsektor installiert werden."

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "REPORTING BUGS"
msgstr "FEHLER MELDEN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Report bugs to E<lt>bug-grub@gnu.orgE<gt>."
msgstr "Melden Sie Fehler (auf Englisch) an E<.MT bug-grub@gnu.org> E<.ME .>"

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux
msgid "B<grub-mkconfig>(8), B<grub-mkimage>(1), B<grub-mkrescue>(1)"
msgstr "B<grub-mkconfig>(8), B<grub-mkimage>(1), B<grub-mkrescue>(1)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid ""
"The full documentation for B<grub-install> is maintained as a Texinfo "
"manual.  If the B<info> and B<grub-install> programs are properly installed "
"at your site, the command"
msgstr ""
"Die vollständige Dokumentation für B<grub-install> wird als ein Texinfo-"
"Handbuch gepflegt. Wenn die Programme B<info>(1) und B<grub-install> auf "
"Ihrem Rechner ordnungsgemäß installiert sind, können Sie mit dem Befehl"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "B<info grub-install>"
msgstr "B<info grub-install>"

#. type: Plain text
#: archlinux
msgid "should give you access to the complete manual."
msgstr "auf das vollständige Handbuch zugreifen."

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "January 2022"
msgstr "Januar 2022"

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "grub-install (GNU GRUB 0.97)"
msgstr "grub-install (GNU GRUB 0.97)"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "grub-install - install GRUB on your drive"
msgstr "grub-install - GRUB auf Ihrem Laufwerk installieren"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "B<grub-install> [I<\\,OPTION\\/>] I<\\,install_device\\/>"
msgstr "B<grub-install> [I<\\,OPTION\\/>] I<\\,Installationsgerät\\/>"

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "B<-h>, B<--help>"
msgstr "B<-h>, B<--help>"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "print this message and exit"
msgstr "gibt eine Hilfemeldung aus und beendet das Programm."

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "B<-v>, B<--version>"
msgstr "B<-v>, B<--version>"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "print the version information and exit"
msgstr "gibt die Versionsinformation aus und beendet das Programm."

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "B<--root-directory>=I<\\,DIR\\/>"
msgstr "B<--root-directory>=I<\\,VERZEICHNIS\\/>"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"install GRUB images under the directory DIR instead of the root directory"
msgstr ""
"installiert GRUB-Abbilder im angegebenen VERZEICHNIS anstelle des "
"Wurzelverzeichnisses."

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "B<--grub-shell>=I<\\,FILE\\/>"
msgstr "B<--grub-shell>=I<\\,DATEI\\/>"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "use FILE as the grub shell"
msgstr "verwendet die angegebene DATEI als Grub-Shell."

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "B<--no-floppy>"
msgstr "B<--no-floppy>"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "do not probe any floppy drive"
msgstr "versucht kein Disketten-Gerät."

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "B<--force-lba>"
msgstr "B<--force-lba>"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "force GRUB to use LBA mode even for a buggy BIOS"
msgstr "zwingt GRUB in den LBA-Modus, selbst wenn das BIOS fehlerhaft ist."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "probe a device map even if it already exists"
msgstr "versucht eine Gerätezuordnung, selbst wenn diese bereits existiert."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "INSTALL_DEVICE can be a GRUB device name or a system device filename."
msgstr ""
"Als I<Installationsgerät> kann der Name eines GRUB-Geräts oder der Dateiname "
"eines Systemgeräts angegeben werden."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"grub-install copies GRUB images into the DIR/boot directory specfied by B<--"
"root-directory>, and uses the grub shell to install grub into the boot "
"sector."
msgstr ""
"B<grub-install> kopiert GRUB-Abbilder in das Verzeichnis VERZEICHNIS/boot, "
"das durch die Option B<--root-directory> angegeben ist, und verwendet die "
"Grub-Shell, um Grub in den Bootsektor zu installieren."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "B<grub>(8), B<update-grub>(8)."
msgstr "B<grub>(8), B<update-grub>(8)."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"should give you access to the complete manual.  You may need to install the "
"B<grub-legacy-doc> package."
msgstr ""
"auf das vollständige Handbuch zugreifen. Es könnte notwendig sein, das Paket "
"B<grub-legacy-doc> zu installieren."

#. type: TH
#: debian-unstable
#, no-wrap
msgid "August 2023"
msgstr "August 2023"
