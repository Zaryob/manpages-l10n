# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Martin Schulze <joey@infodrom.org>
# René Tschirley <gremlin@cs.tu-berlin.de>
# Chris Leick <c.leick@vollbio.de>, 2010-2011.
# Mario Blättermann <mario.blaettermann@gmail.com>, 2014, 2016, 2021, 2022, 2023.
# Dr. Tobias Quathamer <toddy@debian.org>, 2017.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.9.3\n"
"POT-Creation-Date: 2023-08-27 16:59+0200\n"
"PO-Revision-Date: 2023-05-03 09:35+0200\n"
"Last-Translator: Mario Blättermann <mario.blaettermann@gmail.com>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 22.12.3\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "getgroups"
msgstr "getgroups"

#. type: TH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30. März 2023"

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Linux man-pages 6.05.01"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "getgroups, setgroups - get/set list of supplementary group IDs"
msgstr ""
"getgroups, setgroups - abfragen/setzen von zusätzlichen Gruppenkennungen"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTHEK"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Standard-C-Bibliothek (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>unistd.hE<gt>>\n"
msgstr "B<#include E<lt>unistd.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<int getgroups(int >I<size>B<, gid_t >I<list>B<[]);>\n"
msgstr "B<int getgroups(int >I<Größe>B<, gid_t >I<Liste>B<[]);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>grp.hE<gt>>\n"
msgstr "B<#include E<lt>grp.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<int setgroups(size_t >I<size>B<, const gid_t *_Nullable >I<list>B<);>\n"
msgstr "B<int setgroups(size_t >I<Größe>B<, const gid_t *_Nullable >I<Liste>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Mit Glibc erforderliche Feature-Test-Makros (siehe "
"B<feature_test_macros>(7)):"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "B<setgroups>():"
msgstr "B<setgroups>():"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"    Since glibc 2.19:\n"
"        _DEFAULT_SOURCE\n"
"    glibc 2.19 and earlier:\n"
"        _BSD_SOURCE\n"
msgstr ""
"    Seit Glibc 2.19:\n"
"        _DEFAULT_SOURCE\n"
"    Glibc 2.19 und älter:\n"
"        _BSD_SOURCE\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"B<getgroups>()  returns the supplementary group IDs of the calling process "
"in I<list>.  The argument I<size> should be set to the maximum number of "
"items that can be stored in the buffer pointed to by I<list>.  If the "
"calling process is a member of more than I<size> supplementary groups, then "
"an error results."
msgstr ""
"B<getgroups>() gibt die zusätzlichen Gruppenkennungen des aufrufenden "
"Prozesses in I<Liste> zurück. Das Argument I<Größe> sollte auf die maximale "
"Anzahl der Elemente gesetzt werden, die in dem Puffer gespeichert werden "
"können, auf den I<Liste> zeigt. Falls der aufrufende Prozess Mitglied von "
"mehr als I<Größe> zusätzlichen Gruppen ist, führt dies zu einem Fehler."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"It is unspecified whether the effective group ID of the calling process is "
"included in the returned list.  (Thus, an application should also call "
"B<getegid>(2)  and add or remove the resulting value.)"
msgstr ""
"Es ist nicht beschrieben, ob die effektive Gruppenkennungen des aufrufenden "
"Prozesses in der zurückgegebenen Liste enthalten ist. (Daher sollte eine "
"Anwendung auch B<getegid>(2) aufrufen und den resultierenden Wert hinzufügen "
"oder entfernen.)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If I<size> is zero, I<list> is not modified, but the total number of "
"supplementary group IDs for the process is returned.  This allows the caller "
"to determine the size of a dynamically allocated I<list> to be used in a "
"further call to B<getgroups>()."
msgstr ""
"Wenn I<Größe> Null ist, wird I<liste> nicht verändert, es wird aber die "
"Gesamtzahl der zusätzlichen Gruppenkennungen für den Prozess zurückgegeben. "
"Dies erlaubt es dem Aufrufenden, die Größe einer dynamisch reservierten "
"I<Liste> festzulegen, die in einem weiteren Aufruf von B<getgroups>() "
"benutzt wird."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"B<setgroups>()  sets the supplementary group IDs for the calling process.  "
"Appropriate privileges are required (see the description of the B<EPERM> "
"error, below).  The I<size> argument specifies the number of supplementary "
"group IDs in the buffer pointed to by I<list>.  A process can drop all of "
"its supplementary groups with the call:"
msgstr ""
"B<setgroups>() setzt die zusätzlichen Gruppenkennungen für den aufrufenden "
"Prozess. Es werden geeignete Privilegien benötigt (siehe die Beschreibung "
"des Fehlers B<EPERM> unten). Das Argument I<Größe> gibt die Anzahl der "
"zusätzlichen Gruppenkennungen im Puffer an, auf den I<Liste> zeigt. Ein "
"Prozess kann sämtliche zusätzliche Gruppen mit folgendem Aufruf abgeben:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "setgroups(0, NULL);\n"
msgstr "setgroups(0, NULL);\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "RÜCKGABEWERT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"On success, B<getgroups>()  returns the number of supplementary group IDs.  "
"On error, -1 is returned, and I<errno> is set to indicate the error."
msgstr ""
"Bei Erfolg gibt B<getgroups>() die Anzahl der zusätzlichen Gruppenkennungen "
"zurück. Bei aufgetretenem Fehler wird -1 geliefert und I<errno> wird "
"gesetzt, um den Fehler anzuzeigen."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"On success, B<setgroups>()  returns 0.  On error, -1 is returned, and "
"I<errno> is set to indicate the error."
msgstr ""
"Bei Erfolg gibt B<setgroups>() 0 zurück. Bei aufgetretenem Fehler wird -1 "
"geliefert und I<errno> wird gesetzt, um den Fehler anzuzeigen."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "FEHLER"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EFAULT>"
msgstr "B<EFAULT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<list> has an invalid address."
msgstr "I<liste> hat eine ungültige Adresse."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<getgroups>()  can additionally fail with the following error:"
msgstr "B<getgroups>() kann außerdem mit dem folgenden Fehler fehlschlagen:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"I<size> is less than the number of supplementary group IDs, but is not zero."
msgstr ""
"I<Größe> ist kleiner als die Anzahl der zusätzlichen Gruppenkennungen, aber "
"nicht Null."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<setgroups>()  can additionally fail with the following errors:"
msgstr "B<setgroups>() kann überdies mit den folgenden Fehlern fehlschlagen:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"I<size> is greater than B<NGROUPS_MAX> (32 before Linux 2.6.4; 65536 since "
"Linux 2.6.4)."
msgstr ""
"I<Größe> ist größer als B<NGROUPS_MAX> (32 vor Linux 2.6.4; 65536 seit Linux "
"2.6.4)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOMEM>"
msgstr "B<ENOMEM>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Out of memory."
msgstr "Speicher aufgebraucht."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EPERM>"
msgstr "B<EPERM>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The calling process has insufficient privilege (the caller does not have the "
"B<CAP_SETGID> capability in the user namespace in which it resides)."
msgstr ""
"Der aufrufende Prozess hat unzureichende Rechte (dem Aufrufenden fehlt die "
"B<CAP_SETGID>-Capability in dem Benutzernamensraum, in dem er sich befindet)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EPERM> (since Linux 3.19)"
msgstr "B<EPERM> (seit Linux 3.19)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The use of B<setgroups>()  is denied in this user namespace.  See the "
"description of I</proc/>pidI</setgroups> in B<user_namespaces>(7)."
msgstr ""
"Die Verwendung von B<setgroups>() wird in diesem Benutzer-Namensraum "
"verweigert. Siehe die Beschreibung von I</proc/>pidI</setgroups> in "
"B<user_namespaces>(7)."

#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "VERSIONEN"

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "C library/kernel differences"
msgstr "Unterschiede C-Bibliothek/Kernel"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"At the kernel level, user IDs and group IDs are a per-thread attribute.  "
"However, POSIX requires that all threads in a process share the same "
"credentials.  The NPTL threading implementation handles the POSIX "
"requirements by providing wrapper functions for the various system calls "
"that change process UIDs and GIDs.  These wrapper functions (including the "
"one for B<setgroups>())  employ a signal-based technique to ensure that when "
"one thread changes credentials, all of the other threads in the process also "
"change their credentials.  For details, see B<nptl>(7)."
msgstr ""
"Auf der Kernelebene sind Benutzer- und Gruppenkennungen Attribute pro "
"Thread. POSIX verlangt aber, dass sich alle Threads in einem Prozess die "
"gleichen Berechtigungsnachweise teilen. Die NPTL-Threading-Implementierung "
"behandelt die POSIX-Anforderungen durch Bereitstellung von Wrapper-"
"Funktionen für die verschiedenen Systemaufrufe, die die UIDs und GIDs der "
"Prozesse ändern. Diese Wrapper-Funktionen (darunter die für B<setgroups>()) "
"verwenden eine signalbasierte Technik, um sicherzustellen, dass bei der "
"Änderung der Berechtigungsnachweise eines Threads auch alle anderen Threads "
"des Prozesses ihre Berechtigungsnachweise ändern. Für Details siehe "
"B<nptl>(7)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDS"

#. type: TP
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "B<getgroups>()"
msgstr "B<getgroups>()"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr "POSIX.1-2008."

#. type: TP
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "B<setgroups>()"
msgstr "B<setgroups>()"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "None."
msgstr "Keine."

#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "GESCHICHTE"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "SVr4, 4.3BSD, POSIX.1-2001."
msgstr "SVr4, 4.3BSD, POSIX.1-2001."

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid ""
"SVr4, 4.3BSD.  Since B<setgroups>()  requires privilege, it is not covered "
"by POSIX.1."
msgstr ""
"SVr4, 4.3BSD. Da B<setgroups>() Privilegien benötigt, ist es nicht durch "
"POSIX.1 abgedeckt."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The original Linux B<getgroups>()  system call supported only 16-bit group "
"IDs.  Subsequently, Linux 2.4 added B<getgroups32>(), supporting 32-bit "
"IDs.  The glibc B<getgroups>()  wrapper function transparently deals with "
"the variation across kernel versions."
msgstr ""
"Der Original-Linux-Systemaufruf B<getgroups>() unterstützte nur 16-Bit-"
"Gruppenkennungen. Nachfolgend fügte Linux 2.4 B<getgroups32>() hinzu, das 32-"
"Bit-Kennungen unterstützte. Die Glibc-Wrapper-Funktion B<getgroups>() stellt "
"die Änderungen transparent über Kernel-Versionen hinweg bereit."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "ANMERKUNGEN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"A process can have up to B<NGROUPS_MAX> supplementary group IDs in addition "
"to the effective group ID.  The constant B<NGROUPS_MAX> is defined in "
"I<E<lt>limits.hE<gt>>.  The set of supplementary group IDs is inherited from "
"the parent process, and preserved across an B<execve>(2)."
msgstr ""
"Ein Prozess kann bis zu B<NGROUPS_MAX> zusätzliche Gruppenkennungen "
"ergänzend zur effektiven Gruppenkennung haben. Die Konstante B<NGROUPS_MAX> "
"ist in I<E<lt>limits.hE<gt>> definiert. Die Zusammenstellung zusätzlicher "
"Gruppenkennungen wird vom Elternprozess geerbt und über ein B<execve>(2) "
"aufbewahrt."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The maximum number of supplementary group IDs can be found at run time using "
"B<sysconf>(3):"
msgstr ""
"Die maximale Anzahl von zusätzlichen Gruppenkennungen kann zur Laufzeit "
"durch Benutzung von B<sysconf>(3) bestimmt werden:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"long ngroups_max;\n"
"ngroups_max = sysconf(_SC_NGROUPS_MAX);\n"
msgstr ""
"long ngroups_max;\n"
"ngroups_max = sysconf(_SC_NGROUPS_MAX);\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The maximum return value of B<getgroups>()  cannot be larger than one more "
"than this value.  Since Linux 2.6.4, the maximum number of supplementary "
"group IDs is also exposed via the Linux-specific read-only file, I</proc/sys/"
"kernel/ngroups_max>."
msgstr ""
"Der maximale Rückgabewert von B<getgroups>() kann nicht um mehr als eins "
"größer als dieser Wert sein. Seit Linux 2.6.4 wird die maximale Anzahl "
"zusätzlicher Gruppenkennungen außerdem über die nur lesbare Linux-"
"spezifische Datei I</proc/sys/kernel/ngroups_max> offengelegt."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<getgid>(2), B<setgid>(2), B<getgrouplist>(3), B<group_member>(3), "
"B<initgroups>(3), B<capabilities>(7), B<credentials>(7)"
msgstr ""
"B<getgid>(2), B<setgid>(2), B<getgrouplist>(3), B<group_member>(3), "
"B<initgroups>(3), B<capabilities>(7), B<credentials>(7)"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "2023-02-05"
msgstr "5. Februar 2023"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid "B<getgroups>(): SVr4, 4.3BSD, POSIX.1-2001, POSIX.1-2008."
msgstr "B<getgroups>(): SVr4, 4.3BSD, POSIX.1-2001, POSIX.1-2008."

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid ""
"B<setgroups>(): SVr4, 4.3BSD.  Since B<setgroups>()  requires privilege, it "
"is not covered by POSIX.1."
msgstr ""
"B<setgroups>(): SVr4, 4.3BSD. Da B<setgroups>() Privilegien benötigt, ist es "
"nicht durch POSIX.1 abgedeckt."

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.05"
msgstr "Linux man-pages 6.05"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "GETGROUPS"
msgstr "GETGROUPS"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2017-09-15"
msgstr "15. September 2017"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Linux-Programmierhandbuch"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<#include E<lt>sys/types.hE<gt>>"
msgstr "B<#include E<lt>sys/types.hE<gt>>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<#include E<lt>unistd.hE<gt>>"
msgstr "B<#include E<lt>unistd.hE<gt>>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<int getgroups(int >I<size>B<, gid_t >I<list>B<[]);>"
msgstr "B<int getgroups(int >I<Größe>B<, gid_t >I<Liste>B<[]);>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<#include E<lt>grp.hE<gt>>"
msgstr "B<#include E<lt>grp.hE<gt>>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<int setgroups(size_t >I<size>B<, const gid_t *>I<list>B<);>"
msgstr "B<int setgroups(size_t >I<Größe>B<, const gid_t *>I<Liste>B<);>"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"B<setgroups>():\n"
"    Since glibc 2.19:\n"
"        _DEFAULT_SOURCE\n"
"    Glibc 2.19 and earlier:\n"
"        _BSD_SOURCE\n"
msgstr ""
"B<setgroups>():\n"
"    Seit Glibc 2.19:\n"
"        _DEFAULT_SOURCE\n"
"    Glibc 2.19 und älter:\n"
"        _BSD_SOURCE\n"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"B<getgroups>()  returns the supplementary group IDs of the calling process "
"in I<list>.  The argument I<size> should be set to the maximum number of "
"items that can be stored in the buffer pointed to by I<list>.  If the "
"calling process is a member of more than I<size> supplementary groups, then "
"an error results.  It is unspecified whether the effective group ID of the "
"calling process is included in the returned list.  (Thus, an application "
"should also call B<getegid>(2)  and add or remove the resulting value.)"
msgstr ""
"B<getgroups>() gibt die zusätzlichen Gruppenkennungen des aufrufenden "
"Prozesses in I<Liste> zurück. Das Argument I<Größe> sollte auf die maximale "
"Anzahl der Elemente gesetzt werden, die in dem Puffer gespeichert werden "
"können, auf den I<Liste> zeigt. Falls der aufrufende Prozess Mitglied von "
"mehr als I<Größe> zusätzlichen Gruppen ist, führt dies zu einem Fehler. Es "
"ist nicht spezifiziert, ob die effektive Gruppenkennung des aufrufenden "
"Prozesses in der zurückgegebenen Liste enthalten ist. Daher sollte eine "
"Anwendung auch B<getegid>(2) aufrufen und den resultierenden Wert hinzufügen "
"oder entfernen."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"B<setgroups>()  sets the supplementary group IDs for the calling process.  "
"Appropriate privileges are required (see the description of the B<EPERM> "
"error, below).  The I<size> argument specifies the number of supplementary "
"group IDs in the buffer pointed to by I<list>."
msgstr ""
"B<setgroups>() setzt die zusätzlichen Gruppenkennungen für den aufrufenden "
"Prozess. Es werden geeignete Privilegien benötigt (siehe die Beschreibung "
"des Fehlers B<EPERM> unten). Das Argument I<Größe> gibt die Anzahl der "
"zusätzlichen Gruppenkennungen im Puffer an, auf den I<Liste> zeigt."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"On success, B<getgroups>()  returns the number of supplementary group IDs.  "
"On error, -1 is returned, and I<errno> is set appropriately."
msgstr ""
"Bei Erfolg gibt B<getgroups>() die Anzahl der zusätzlichen Gruppenkennungen "
"zurück. Bei aufgetretenem Fehler wird -1 geliefert und I<errno> wird "
"entsprechend gesetzt."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"On success, B<setgroups>()  returns 0.  On error, -1 is returned, and "
"I<errno> is set appropriately."
msgstr ""
"Bei Erfolg gibt B<setgroups>() 0 zurück. Bei aufgetretenem Fehler wird -1 "
"geliefert und I<errno> wird entsprechend gesetzt."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"The use of B<setgroups>()  is denied in this user namespace.  See the "
"description of I</proc/[pid]/setgroups> in B<user_namespaces>(7)."
msgstr ""
"Die Verwendung von B<setgroups>() wird in diesem Benutzer-Namensraum "
"verweigert. Siehe die Beschreibung von I</proc/[pid]/setgroups> in "
"B<user_namespaces>(7)."

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "CONFORMING TO"
msgstr "KONFORM ZU"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "COLOPHON"
msgstr "KOLOPHON"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Diese Seite ist Teil der Veröffentlichung 4.16 des Projekts Linux-I<man-"
"pages>. Eine Beschreibung des Projekts, Informationen, wie Fehler gemeldet "
"werden können, sowie die aktuelle Version dieser Seite finden sich unter \\"
"%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux-Handbuchseiten 6.04"
