# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Mario Blättermann <mario.blaettermann@gmail.com>, 2021.
# Helge Kreutzmann <debian@helgefjell.de>, 2021,2022.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.12.1\n"
"POT-Creation-Date: 2023-08-27 17:19+0200\n"
"PO-Revision-Date: 2022-01-30 06:12+0100\n"
"Last-Translator: Helge Kreutzmann <debian@helgefjell.de>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 21.04.2\n"

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "sane-teco1"
msgstr "sane-teco1"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "14 Jul 2008"
msgstr "14. Juli 2008"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SANE Scanner Access Now Easy"
msgstr "SANE Scanner Access Now Easy"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "sane-teco1 - SANE backend for TECO / RELISYS scanners"
msgstr "sane-teco1 - SANE-Backend für Scanner von TECO/RELISYS"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<sane-teco1> library implements a SANE (Scanner Access Now Easy) "
"backend that provides access to some TECO SCSI flatbed scanners. This "
"backend should be considered B<beta-quality> software! TECO scanners are "
"sold under various brands like RELISYS, PIOTECH, TRUST. This backend may or "
"may not support yours."
msgstr ""
"Die Bibliothek B<sane-teco1> implementiert ein SANE-(Scanner Access Now "
"Easy) Backend zum Zugriff auf einige TECO-SCSI-Flachbett-Scanner. Dieses "
"Backend sollte als B<Beta-Qualität>-Software betrachtet werden! TECO-Scanner "
"werden unter verschiedenen Marken wie Relisys, Piotech und Trust verkauft. "
"Dieses Backend könnte Ihren Scanner unterstützen oder auch nicht."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The scanners that should work with this backend are:"
msgstr "Die folgenden Scanner sollten mit diesem Backend funktionieren:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"\\f(CR   Vendor Model           TECO model      status\n"
"----------------------  --------------  -----------\n"
"  Relisys AVEC 2400        VM3520        tested\n"
"  Relisys AVEC 2412        VM3520+       tested\n"
"  Relisys AVEC 4800        VM4530        untested\n"
"  Relisys AVEC 4816        VM4530+       untested\n"
"  Relisys RELI 2400        VM3530        untested\n"
"  Relisys RELI 2412        VM3530+       tested\n"
"  Relisys RELI 2412        VM3530+       untested\n"
"  Relisys RELI 4816        VM4540        tested\n"
"  Relisys RELI 4830        VM4542        tested\n"
"  Relisys RELI 9600        VM6530        untested\n"
"  Relisys RELI 9612        VM6530*       untested\n"
"  Relisys RELI 9624        VM6530+       untested\n"
"  Relisys RELI 9630        VM6540        untested\n"
"  Relisys RELI DS15        VM3440        untested\n"
"  Relisys RELI DS6         VM3420        untested\n"
"  Dextra  DF-600P          VM3510        tested\n"
"  Dextra  DF-4830T         VM4542        untested\n"
"  Dextra  DF-1200T+        VM3530+       untested\n"
"  Dextra  DF-9624          VM6530+       untested\\fR\n"
msgstr ""
"\\f(CR   Marke Modell           TECO-Modell     Status\n"
"----------------------  --------------  -----------\n"
"  Relisys AVEC 2400        VM3520        getestet\n"
"  Relisys AVEC 2412        VM3520+       getestet\n"
"  Relisys AVEC 4800        VM4530        nicht getestet\n"
"  Relisys AVEC 4816        VM4530+       nicht getestet\n"
"  Relisys RELI 2400        VM3530        nicht getestet\n"
"  Relisys RELI 2412        VM3530+       getestet\n"
"  Relisys RELI 2412        VM3530+       nicht getestet\n"
"  Relisys RELI 4816        VM4540        getestet\n"
"  Relisys RELI 4830        VM4542        getestet\n"
"  Relisys RELI 9600        VM6530        nicht getestet\n"
"  Relisys RELI 9612        VM6530*       nicht getestet\n"
"  Relisys RELI 9624        VM6530+       nicht getestet\n"
"  Relisys RELI 9630        VM6540        nicht getestet\n"
"  Relisys RELI DS15        VM3440        nicht getestet\n"
"  Relisys RELI DS6         VM3420        nicht getestet\n"
"  Dextra  DF-600P          VM3510        getestet\n"
"  Dextra  DF-4830T         VM4542        nicht getestet\n"
"  Dextra  DF-1200T+        VM3530+       nicht getestet\n"
"  Dextra  DF-9624          VM6530+       nicht getestet\\fR\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Note that the untested scanner will not be directly supported. You should "
"contact the author for that."
msgstr ""
"Beachten Sie, dass nicht getestete Scanner nicht direkt unterstützt werden. "
"Sie sollten dafür zum Autor Kontakt aufnehmen."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The TECO VM number can usually be found at the back of the scanner. It is "
"also part of the FCC ID.  I<sane-find-scanner -v> will also show the SCSI "
"inquiry, and if it is a TECO scanner, the name will be there too."
msgstr ""
"Die TECO-VM-Nummer kann normalerweise auf der Rückseite des Scanners "
"gefunden werden. Sie ist auch Teil der FCC-Kennung. I<sane-find-scanner -v> "
"wird auch die SCSI-Anfrage zeigen, und falls es ein TECO-Scanner ist, wird "
"der Name auch dort sein."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The options the backend supports can either be selected through command line "
"options to programs like B<scanimage>(1)  or through GUI elements in "
"B<xscanimage>(1)  or B<xsane>(1)."
msgstr ""
"Die vom Backend unterstützten Optionen können entweder in der Befehlszeile "
"an Programme wie B<scanimage>(1) übergeben oder über Bedienelemente der "
"grafischen Benutzeroberoberfläche in B<xscanimage>(1) oder B<xsane>(1) "
"gesteuert werden."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If you have any success with a scanner not listed here, or if you notice any "
"strange behavior, please report to the backend maintainer or to the SANE "
"mailing list."
msgstr ""
"Falls Sie mit einem hier nicht aufgeführten Scanner Erfolg haben oder falls "
"Sie merkwürdiges Verhalten beobachten, berichten Sie das bitte (auf "
"Englisch) an den Backend-Betreuer oder an die SANE-Mailingliste."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Valid command line options and their syntax can be listed by using:"
msgstr ""
"Gültige Befehlszeilenoptionen und deren Syntax können Sie mit folgendem "
"Befehl auflisten:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "scanimage --help -d teco1"
msgstr "scanimage --help -d teco1"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<Scan Mode>"
msgstr "B<Scanmodus>"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--mode Black & White|Grayscale|Color>"
msgstr "B<--mode Black & White|Grayscale|Color>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Selects the basic mode of operation of the scanner. Valid choices are "
"I<Black & White>, I<Grayscale> and I<Color>."
msgstr ""
"wählt den grundlegenden Betriebsmodus des Scanners. Gültige Auswahlen sind "
"I<Black & White>, I<Grayscale> und I<Color>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The I<Black & White> mode is for black and white only (1 bit).  I<Grayscale> "
"will produce 256 levels of gray (8 bits).  I<Color> will produce a 24 bit "
"color image."
msgstr ""
"Der Modus I<Black & White> ist nur für schwarz und weiß (1 bit). "
"I<Grayscale> wird 256 Graustufen erzeugen (8 bit). I<Color> erstellt ein 24-"
"bit-Farbbild."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--resolution 1..600>"
msgstr "B<--resolution 1…600>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Selects the resolution for a scan. The scanner can do all resolutions "
"between 1 and 600, in increments of 1."
msgstr ""
"wählt die Scan-Auflösung. Der Scanner beherrscht alle Auflösungen von 1 bis "
"600 in Schritten von 1."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<Geometry options>"
msgstr "B<Geometrieoptionen>"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-l -t -x -y>"
msgstr "B<-l -t -x -y>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Controls the scan area: B<-l> sets the top left x coordinate, B<-t> the top "
"left y coordinate, B<-x> selects the width and B<-y> the height of the scan "
"area. All parameters are specified in millimeters by default."
msgstr ""
"steuert den Scan-Bereich: B<-l> setzt die obere linke x-Koordinate, B<-t> "
"die obere linke y-Koordinate, B<-x> wählt die Breite und B<-y> die Höhe des "
"Scan-Bereichs. Alle Parameter werden standardmäßig in Millimetern angegeben."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<Enhancement options>"
msgstr "B<Verbesserungsoptionen>"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--custom-gamma>"
msgstr "B<--custom-gamma>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"(color mode only) allows the user to specify a gamma table (see the next 3 "
"parameters)."
msgstr ""
"erlaubt dem Benutzer, eine Gamma-Tabelle anzugeben (nur im Farb-Modus, siehe "
"die nächsten drei Parameter)"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--red-gamma-table>"
msgstr "B<--red-gamma-table>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Can be used to download a user defined gamma table for the red channel.  The "
"table must be 256 bytes long. Color mode only."
msgstr ""
"kann zum Herunterladen einer benutzerdefinierten Gamma-Tabelle für den roten "
"Kanal verwendet werden. Die Tabelle muss 256 byte lang sein. Nur im "
"Farbmodus."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--green-gamma-table>"
msgstr "B<--green-gamma-table>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Can be used to download a user defined gamma table for the green channel.  "
"The table must be 256 bytes long. Color mode only."
msgstr ""
"kann zum Herunterladen einer benutzerdefinierten Gamma-Tabelle für den "
"grünen Kanal verwendet werden. Die Tabelle muss 256 byte lang sein. Nur im "
"Farbmodus."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--blue-gamma-table>"
msgstr "B<--blue-gamma-table>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Can be used to download a user defined gamma table for the blue channel.  "
"The table must be 256 bytes long. Color mode only."
msgstr ""
"kann zum Herunterladen einer benutzerdefinierten Gamma-Tabelle für den "
"blauen Kanal verwendet werden. Die Tabelle muss 256 byte lang sein. Nur im "
"Farbmodus."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--dither Line art|2x2|3x3|4x4 bayer|4x4 smooth|8x8 bayer|8x8 smooth|8x8 horizontal|8x8 vertical>"
msgstr "B<--dither Line art|2x2|3x3|4x4 bayer|4x4 smooth|8x8 bayer|8x8 smooth|8x8 horizontal|8x8 vertical>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Select the dither mask to use. Black & White only."
msgstr "wählt die zu verwendende Dither-Maske aus. Nur bei schwarz/weiß."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--preview>"
msgstr "B<--preview>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Requests a preview scan. The resolution used is 22 dpi and the scan area is "
"the maximum allowed. The scan mode is user selected. The default is \"no\"."
msgstr ""
"fordert einen Vorschau-Scan an. Die verwendete Auflösung ist 22 DPI und der "
"Scan-Bereich ist der größte erlaubte. Der Scan-Modus wird vom Anwender "
"ausgewählt. Die Vorgabe ist »no«."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "CONFIGURATION FILE"
msgstr "KONFIGURATIONSDATEI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The configuration file I</etc/sane.d/teco1.conf> supports only one item: the "
"device name to use (eg I</dev/scanner>)."
msgstr ""
"Die Konfigurationsdatei I</etc/sane.d/teco1.conf> unterstützt nur einen "
"Eintrag: den Namen des zu verwendenden Geräts (zum Beispiel I</dev/scanner>)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "FILES"
msgstr "DATEIEN"

#. type: TP
#: archlinux
#, no-wrap
msgid "I</usr/lib/sane/libsane-teco1.a>"
msgstr "I</usr/lib/sane/libsane-teco1.a>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The static library implementing this backend."
msgstr "Die statische Bibliothek, die dieses Backend implementiert."

#. type: TP
#: archlinux
#, no-wrap
msgid "I</usr/lib/sane/libsane-teco1.so>"
msgstr "I</usr/lib/sane/libsane-teco1.so>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The shared library implementing this backend (present on systems that "
"support dynamic loading)."
msgstr ""
"Die dynamische Bibliothek, die dieses Backend implementiert (auf Systemen "
"verfügbar, die dynamisches Laden unterstützen)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ENVIRONMENT"
msgstr "UMGEBUNGSVARIABLEN"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<SANE_DEBUG_TECO1>"
msgstr "B<SANE_DEBUG_TECO1>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If the library was compiled with debug support enabled, this environment "
"variable controls the debug level for this backend. E.g., a value of 128 "
"requests all debug output to be printed. Smaller levels reduce verbosity."
msgstr ""
"Falls die Bibliothek mit Debug-Unterstützung kompiliert wurde, steuert diese "
"Umgebungsvariable die Debug-Stufe für dieses Backend. Beispielsweise bewirkt "
"ein Wert von 128 die Anzeige sämtlicher Debug-Ausgaben. Kleinere Werte "
"reduzieren die Ausführlichkeit."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "LIMITATIONS"
msgstr "EINSCHRÄNKUNGEN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The windows TWAIN driver has many more options than this SANE backend. "
"However they are only software adjustments. This backend only implements "
"what the scanner can support."
msgstr ""
"Der TWAIN-Treiber für Windows verfügt über weitaus mehr Optionen als dieses "
"SANE-Backend. Allerdings sind diese nur softwareseitige Anpassungen. Dieses "
"Backend implementiert nur das, was der Scanner unterstützen kann."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "FEHLER"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "None known."
msgstr "Keine bekannt."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<sane-scsi>(5), B<scanimage>(1), B<xscanimage>(1), B<xsane>(1), B<sane>(7)"
msgstr ""
"B<sane-scsi>(5), B<scanimage>(1), B<xscanimage>(1), B<xsane>(1), B<sane>(7)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "The package is actively maintained by Frank Zago."
msgstr "Das Paket wird aktiv von Frank Zago betreut."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<http://www.zago.net/sane/#teco>"
msgstr "I<http://www.zago.net/sane/#teco>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "CREDITS"
msgstr "DANKSAGUNGEN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Thanks to Gerard Delafond for the VM4542 support.  Thanks to Jean-Yves Simon "
"for the VM3510 support."
msgstr ""
"Danke an Gerard Delafond für die VM4542-Unterstützung. Danke an Jean-Yves "
"Simon für die VM3510-Unterstützung."

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "I</usr/lib/x86_64-linux-gnu/sane/libsane-teco1.a>"
msgstr "I</usr/lib/x86_64-linux-gnu/sane/libsane-teco1.a>"

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "I</usr/lib/x86_64-linux-gnu/sane/libsane-teco1.so>"
msgstr "I</usr/lib/x86_64-linux-gnu/sane/libsane-teco1.so>"

#. type: TP
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "I</usr/lib64/sane/libsane-teco1.a>"
msgstr "I</usr/lib64/sane/libsane-teco1.a>"

#. type: TP
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "I</usr/lib64/sane/libsane-teco1.so>"
msgstr "I</usr/lib64/sane/libsane-teco1.so>"

# FIXME scanner valid → scanner. Valid
# FIXME choices are → choices are I<Black & White>, I<Grayscale> and I<Color>
#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Selects the basic mode of operation of the scanner valid choices are The "
"I<Black & White> mode is black and white only (1 bit).  I<Grayscale> will "
"produce 256 levels of gray (8 bits).  I<Color> will produce a 24 bits color "
"image."
msgstr ""
"wählt den grundlegenden Betriebsmodus des Scanners. Der Modus I<Black & "
"White> ist nur schwarz und weiß (1 bit). I<Grayscale> wird 256 Graustufen "
"erzeugen (8 bit). I<Color> erstellt ein 24-bit-Farbbild."
