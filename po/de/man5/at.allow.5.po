# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Helge Kreutzmann <debian@helgefjell.de>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.14.0\n"
"POT-Creation-Date: 2023-08-27 16:49+0200\n"
"PO-Revision-Date: 2022-09-29 19:26+0200\n"
"Last-Translator: Helge Kreutzmann <debian@helgefjell.de>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "AT.ALLOW"
msgstr "AT.ALLOW"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Sep 1997"
msgstr "Sep. 1997"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Linux-Programmierhandbuch"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "at.allow, at.deny - determine who can submit jobs via at or batch"
msgstr ""
"at.allow, at.deny - Bestimmung, wer Aufträge über at oder batch einreichen "
"kann"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

# FIXME Final fullstop should not be marked bold
# FIXME which user → which users
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The I</etc/at.allow> and I</etc/at.deny> files determine which user can "
"submit commands for later execution via B<at>(1)  or B<batch>(1)B<.>"
msgstr ""
"Die Dateien I</etc/at.allow> und I</etc/at.deny> legen fest, welche Benutzer "
"Befehle zur späteren Ausführung mittels B<at>(1) oder B<batch>(1) einreichen "
"können."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The format of the files is a list of usernames, one on each line.  "
"Whitespace is not permitted."
msgstr ""
"Das Format der Datei ist eine Liste von Benutzernamen, einem pro Zeile. "
"Leerraum ist nicht erlaubt."

# FIXME B<at> → B<at>(1)
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If the file I</etc/at.allow> exists, only usernames mentioned in it are "
"allowed to use B<at>."
msgstr ""
"Falls die Datei I</etc/at.allow> existiert, wird nur Benutzern, deren "
"Benutzernamen darin erwähnt sind, erlaubt, B<at>(1) zu verwenden."

# FIXME B<at> → B<at>(1)
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If I</etc/at.allow> does not exist, I</etc/at.deny> is checked, every "
"username not mentioned in it is then allowed to use B<at>."
msgstr ""
"Falls I</etc/at.allow> nicht existiert, wird I</etc/at.deny> überprüft und "
"jeder Benutzer, der dort nicht erwähnt ist, darf dann B<at>(1) verwenden."

# FIXME B<at> → B<at>(1)
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "An empty I</etc/at.deny> means that every user may use B<at>."
msgstr ""
"Eine leere I</etc/at.deny> bedeutet, dass jeder Benutzer B<at>(1) verwenden "
"darf."

# FIXME at → B<at>(1)
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "If neither exists, only the superuser is allowed to use at."
msgstr ""
"Falls keine der beiden Dateien existiert, darf nur der Systemverwalter "
"B<at>(1) verwenden."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<at>(1), B<cron>(8), B<crontab>(1), B<atd>(8)."
msgstr "B<at>(1), B<cron>(8), B<crontab>(1), B<atd>(8)."
