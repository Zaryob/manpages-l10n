# Finnish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Tuukka Forssell <taf@jytol.fi>, 1998.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2021-09-24 11:22+0200\n"
"PO-Revision-Date: 1998-04-08 15:46+0200\n"
"Last-Translator: Tuukka Forssell <taf@jytol.fi>\n"
"Language-Team: Finnish <>\n"
"Language: fi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: xv.1:1
#, no-wrap
msgid "XV"
msgstr "XV"

#. type: TH
#: xv.1:1
#, no-wrap
msgid "2 December 1994"
msgstr "2. joulukuuta 1994"

#. type: TH
#: xv.1:1
#, no-wrap
msgid "Rev. 3.10"
msgstr "Rev. 3.10"

#. type: SH
#: xv.1:2
#, no-wrap
msgid "NAME"
msgstr "NIMI"

#. type: Plain text
#: xv.1:4
msgid "B<xv> - interactive image display for the X Window System"
msgstr "B<xv> - interaktiivinen kuvan näyttäjä X Window System:ille"

#. type: SH
#: xv.1:4
#, no-wrap
msgid "SYNTAX"
msgstr "SYNTAX"

#. type: Plain text
#: xv.1:8
msgid "B<xv> [I<options>] [I<filename>[I<filename...>]]"
msgstr "B<xv> [I<optiont>] [I<tiedosto>[I<tiedosto...>]]"

#. type: SH
#: xv.1:8
#, no-wrap
msgid "DESCRIPTION"
msgstr "KUVAUS"

#. type: Plain text
#: xv.1:15
msgid ""
"The I<xv> program displays images in the GIF, JPEG, TIFF, PBM, PGM, PPM, X11 "
"bitmap, Utah Raster Toolkit RLE, PDS/VICAR, Sun Rasterfile, BMP, PCX, IRIS "
"RGB, XPM, Targa, XWD, possibly PostScript, and PM formats on workstations and "
"terminals running the X Window System, Version 11."
msgstr ""
"I<xv> näyttää kuvia jotka ovat GIF, JPEG, TIFF, PBM, PGM, PPM, X11 bitmap, "
"Utah Raster Toolkit RLE, PDS/VICAR, Sun Rasterfile, BMP, PCX, IRIS RGB, XPM, "
"Targa, XWD, mahdollisesti PostScript, ja PM formaatissa, työasemilla ja "
"terminaaleille, joilla ajetaan X Window System version 11:ta."

#. type: Plain text
#: xv.1:25
msgid ""
"The documentation for XV is now distributed I<only> as a PostScript file, as "
"it has gotten enormous, and is no longer very well suited to the 'man' page "
"format.  Print a copy of the (100-ish page) manual found in I<docs/xvdocs."
"ps>.  If you are unable to get the manual to print on your printer, you may "
"purchase a printed copy of the XV manual.  Click on 'About XV' in the program "
"to get further information."
msgstr ""
"XV:n dokumentaatiota levitetään nyt I<vain> PostScript tiedostona, koska "
"siitä on tullut suunnaton ja se ei ole enää solveltuva 'man' -sivu muotoon.  "
"Tulosta kopio (100 sivuisesta) ohjekirjasta I<docs/xvdocs.ps>.  Jos et pysty "
"tulostamaan ohjekirjaa tulostimellasi, sinun täytyy ostaa painettu kopio XV "
"ohjekirjasta. Valitse 'About XV' ohjelmassa saadaksesi lisätietoja."

#. type: Plain text
#: xv.1:32
msgid ""
"If you don't I<have> the PostScript file, it is part of the standard XV "
"distribution, the latest version of which can be obtained via anonymous ftp "
"from I<ftp.cis.upenn.edu> in the directory pub/xv"
msgstr ""
"Jos sinulle ei ole PostScript tiedostoa, joka on osa standardia XV levitystä, "
"uusin versio on saatavilla anonyymi ftp palvelimelta, I<ftp.cis.upenn.edu> "
"hakemistosta /pub/xv."

#. type: SH
#: xv.1:33
#, no-wrap
msgid "AUTHOR"
msgstr "TEKIJÄ"

#. type: Plain text
#: xv.1:34
msgid "John Bradley"
msgstr "John Bradley"
