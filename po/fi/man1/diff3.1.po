# Finnish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Tommi Vainikainen <mucus@pcuf.fi>, 1998.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.11.0\n"
"POT-Creation-Date: 2023-08-27 16:54+0200\n"
"PO-Revision-Date: 1998-09-03 20:09+0200\n"
"Last-Translator: Tommi Vainikainen <mucus@pcuf.fi>\n"
"Language-Team: Finnish <>\n"
"Language: fi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DIFF3"
msgstr "DIFF3"

#. type: TH
#: archlinux fedora-39 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "May 2023"
msgstr "Toukokuuta 2023"

#. type: TH
#: archlinux fedora-39 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "diffutils 3.10"
msgstr "diffutils 3.10"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr "Käyttäjän sovellukset"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NIMI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "diff3 - compare three files line by line"
msgstr "diff3 - vertaa kolmea tiedostoa rivi riviltä"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "YLEISKATSAUS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<diff3> [I<OPTION>]... I<MYFILE OLDFILE YOURFILE>"
msgstr "B<diff3> [I<VALITSIN>]... I<TIEDOSTONI VANHATIEDOSTO TIEDOSTOSI>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "KUVAUS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Compare three files line by line."
msgstr "Vertaa kolmea tiedostoa rivi riviltä."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Mandatory arguments to long options are mandatory for short options too."
msgstr ""
"Pitkien valitsinten pakolliset argumentit ovat pakollisia myös lyhyille."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-A>, B<--show-all>"
msgstr "B<-A>, B<--show-all>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "output all changes, bracketing conflicts"
msgstr "tulosta kaikki muutokset, ristiriidat merkiten"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-e>, B<--ed>"
msgstr "B<-e>, B<--ed>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"output ed script incorporating changes from OLDFILE to YOURFILE into MYFILE"
msgstr ""
"tulosta TIEDOSTOONI ed-skripti, joka sisältää VANHANTIEDOSTON muutokset "
"verrattuna TIEDOSTOOSI"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-E>, B<--show-overlap>"
msgstr "B<-E>, B<--show-overlap>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "like B<-e>, but bracket conflicts"
msgstr "kuin B<-e>, mutta ristiriidat merkiten"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-3>, B<--easy-only>"
msgstr "B<-3>, B<--easy-only>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "like B<-e>, but incorporate only nonoverlapping changes"
msgstr "kuin B<-e>, mutta vain ei-päällekkäiset muutokset"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-x>, B<--overlap-only>"
msgstr "B<-x>, B<--overlap-only>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "like B<-e>, but incorporate only overlapping changes"
msgstr "kuin B<-e>, mutta vain päällekkäiset muutokset"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-X>"
msgstr "B<-X>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "like B<-x>, but bracket conflicts"
msgstr "kuin B<-x>, mutta ristiriidat merkiten"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-i>"
msgstr "B<-i>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "append 'w' and 'q' commands to ed scripts"
msgstr "lisää komennot ”w” ja ”q” ed-skripteihin"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-m>, B<--merge>"
msgstr "B<-m>, B<--merge>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"output actual merged file, according to B<-A> if no other options are given"
msgstr ""
"tulosta varsinainen yhdistetty tiedosto B<-A>:n mukaisesti ellei muita "
"valitsimia annettu"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-a>, B<--text>"
msgstr "B<-a>, B<--text>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "treat all files as text"
msgstr "käsittele kaikki tiedostot tekstinä"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--strip-trailing-cr>"
msgstr "B<--strip-trailing-cr>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "strip trailing carriage return on input"
msgstr "poista vaununpalautus syötteen lopusta"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-T>, B<--initial-tab>"
msgstr "B<-T>, B<--initial-tab>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "make tabs line up by prepending a tab"
msgstr "kohdista sarkaimet lisäämällä sarkaimia"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--diff-program>=I<PROGRAM>"
msgstr "B<--diff-program>=I<OHJELMA>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "use PROGRAM to compare files"
msgstr "käytä OHJELMAa tiedostojen vertaamiseen"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-L>, B<--label>=I<LABEL>"
msgstr "B<-L>, B<--label>=I<NIMIÖ>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "use LABEL instead of file name (can be repeated up to three times)"
msgstr "käytä NIMIÖtä tiedostonimen sijaan (voidaan toistaa enintään kolmesti)"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "display this help and exit"
msgstr "näytä tämä ohje ja poistu"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-v>, B<--version>"
msgstr "B<-v>, B<--version>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "output version information and exit"
msgstr "näytä versiotiedot ja poistu"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The default output format is a somewhat human-readable representation of the "
"changes."
msgstr ""
"Oletustulostusmuoto on jossain määrin ihmisluettava esitys muutoksista."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<-e>, B<-E>, B<-x>, B<-X> (and corresponding long) options cause an ed "
"script to be output instead of the default."
msgstr ""
"Valitsimet B<-e>, B<-E>, B<-x>, B<-X> (ja vastaavat pitkät) johtavat "
"B<ed>(1)-skriptin tulostamiseen oletusmuodon sijaan."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Finally, the B<-m> (B<--merge>) option causes diff3 to do the merge "
"internally and output the actual merged file.  For unusual input, this is "
"more robust than using ed."
msgstr ""
"Valitsin B<-m> (B<--merge>) saa B<diff3>:n tekemään yhdistämisen sisäisesti "
"ja tulostamaan varsinaisen yhdistetyn tiedoston. Epätavalliselle syötteelle "
"tämä on vankempaa kuin B<ed>in käyttäminen."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If a FILE is '-', read standard input.  Exit status is 0 if successful, 1 if "
"conflicts, 2 if trouble."
msgstr ""
"Jos TIEDOSTOa ei ole annettu, tai se on ”-”, luetaan vakiosyötettä. "
"Paluuarvo on onnistuessa 0, ristiriitatilanteissa 1, ja ongelmatilanteissa 2."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "TEKIJÄ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Written by Randy Smith."
msgstr "Kirjoittanut Randy Smith."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "VIRHEISTÄ ILMOITTAMINEN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Report bugs to: bug-diffutils@gnu.org"
msgstr ""
"Ilmoita ohjelmistovioista (englanniksi) osoitteeseen bug-DIFFutils@gnu.org"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"GNU diffutils home page: E<lt>https://www.gnu.org/software/diffutils/E<gt>"
msgstr ""
"GNU diffutils-kotisivu: E<lt>https://www.gnu.org/software/diffutils/E<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "General help using GNU software: E<lt>https://www.gnu.org/gethelp/E<gt>"
msgstr ""
"Yleisohjeita GNU-ohjelmistojen käyttöön: E<lt>https://www.gnu.org/gethelp/"
"E<gt>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "COPYRIGHT"
msgstr "TEKIJÄNOIKEUDET"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"Copyright \\(co 2023 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2023 Free Software Foundation, Inc.  Lisenssi GPLv3+: GNU "
"GPL versio 3 tai myöhempi E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""
"Tämä on vapaa ohjelmisto; sitä saa vapaasti muuttaa ja levittää edelleen. "
"Siinä määrin kuin laki sallii, TAKUUTA EI OLE."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "KATSO MYÖS"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid "cmp(1), diff(1), sdiff(1)"
msgid "B<cmp>(1), B<diff>(1), B<sdiff>(1)"
msgstr "B<cmp>(1), B<diff>(1), B<sdiff>(1)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The full documentation for B<diff3> is maintained as a Texinfo manual.  If "
"the B<info> and B<diff3> programs are properly installed at your site, the "
"command"
msgstr ""
"Ohjelman B<diff3> täydellinen dokumentaatio ylläpidetään Texinfo-"
"manuaalissa. Mikäli ohjelmat B<info> ja B<diff3> on täysin asennettu, "
"komennon"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<info diff3>"
msgstr "B<info diff3>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "should give you access to the complete manual."
msgstr "pitäisi antaa täydellinen manuaali luettavaksi."

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "August 2021"
msgstr "Elokuu 2021"

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "diffutils 3.8"
msgstr "diffutils 3.8"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"Copyright \\(co 2021 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2021 Free Software Foundation, Inc.  Lisenssi GPLv3+: GNU "
"GPL versio 3 tai myöhempi E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: Plain text
#: debian-bookworm debian-unstable opensuse-leap-15-6
msgid "cmp(1), diff(1), sdiff(1)"
msgstr "B<cmp>(1), B<diff>(1), B<sdiff>(1)"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "May 2017"
msgstr "Toukokuuta 2017"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "diffutils 3.6"
msgstr "diffutils 3.6"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"GNU diffutils home page: E<lt>http://www.gnu.org/software/diffutils/E<gt>"
msgstr ""
"GNU diffutilskotisivu: E<lt>http://www.gnu.org/software/diffutils/E<gt>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "General help using GNU software: E<lt>http://www.gnu.org/gethelp/E<gt>"
msgstr ""
"Yleisohjeita GNU-ohjelmistojen käyttöön: E<lt>https://www.gnu.org/gethelp/"
"E<gt>"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Copyright \\(co 2017 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>http://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2017 Free Software Foundation, Inc.  Lisenssi GPLv3+: GNU "
"GPL versio 3 tai myöhempi E<lt>http://gnu.org/licenses/gpl.htmlE<gt>."
