# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Christophe Blaess <ccb@club-internet.fr>, 1997, 2002, 2003.
# Michel Quercia <quercia AT cal DOT enst DOT fr>, 1997.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999, 2000
# Frédéric Delanoy <delanoy_f@yahoo.com>, 2000.
# Christophe Sauthier <christophe@sauthier.com>, 2001.
# Sébastien Blanchet, 2002.
# Jérôme Perzyna <jperzyna@yahoo.fr>, 2004.
# Aymeric Nys <aymeric AT nnx POINT com>, 2004.
# Alain Portal <aportal@univ-montp2.fr>, 2005, 2006.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006.
# Yves Rütschlé <l10n@rutschle.net>, 2006.
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006.
# Julien Cristau <jcristau@debian.org>, 2006.
# Philippe Piette <foudre-blanche@skynet.be>, 2006.
# Jean-Baka Domelevo-Entfellner <domelevo@gmail.com>, 2006.
# Nicolas Haller <nicolas@boiteameuh.org>, 2006.
# Sylvain Archenault <sylvain.archenault@laposte.net>, 2006.
# Valéry Perrin <valery.perrin.debian@free.fr>, 2006.
# Jade Alglave <jade.alglave@ens-lyon.org>, 2006.
# Nicolas François <nicolas.francois@centraliens.net>, 2007.
# Alexandre Kuoch <alex.kuoch@gmail.com>, 2008.
# Lyes Zemmouche <iliaas@hotmail.fr>, 2008.
# Florentin Duneau <fduneau@gmail.com>, 2006, 2008, 2009, 2010.
# Alexandre Normand <aj.normand@free.fr>, 2010.
# David Prévot <david@tilapin.org>, 2010-2015.
msgid ""
msgstr ""
"Project-Id-Version: manpages-fr-extra-util-linux\n"
"POT-Creation-Date: 2023-08-27 17:06+0200\n"
"PO-Revision-Date: 2021-03-23 13:56+0100\n"
"Last-Translator: Jean-Paul Guillonneau <guillonneau.jeanpaul@free.fr>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: vim\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#. type: TH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "LSLOCKS"
msgstr "LSLOCKS"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-05-11"
msgstr "11 mai 2022"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "util-linux 2.38.1"
msgstr "util-linux 2.38.1"

#. type: TH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "System Administration"
msgstr "Administration Système"

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "lslocks - list local system locks"
msgstr "lslocks - Afficher les verrous système locaux"

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<lslocks> [options]"
msgstr "B<lslocks> [I<options>]"

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"B<lslocks> lists information about all the currently held file locks in a "
"Linux system."
msgstr ""
"B<lslocks> affiche des renseignements sur les verrous de fichier "
"actuellement maintenus dans un système Linux."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, fuzzy
#| msgid ""
#| "Note that lslocks also lists OFD (Open File Description) locks, these "
#| "locks are not associated with any process (PID is -1).  OFD locks are "
#| "associated with the open file description on which they are acquired.  "
#| "This lock type is available since Linux 3.15, see B<fcntl>(2) for more "
#| "details."
msgid ""
"Note that lslocks also lists OFD (Open File Description) locks, these locks "
"are not associated with any process (PID is -1). OFD locks are associated "
"with the open file description on which they are acquired. This lock type is "
"available since Linux 3.15, see B<fcntl>(2) for more details."
msgstr ""
"Remarquez que B<lslocks> liste aussi les verrous OFD (Open File "
"Description), ceux-ci ne sont associés à aucun processus (PID de B<-1>). Les "
"verrous OFD sont associés aux descripteurs de fichier ouvert pour lesquels "
"ils sont obtenus. Ce type de fichier est disponible depuis Linux 3.15, "
"consultez B<fcntl>(2) pour des détails complémentaires."

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "OPTIONS"
msgstr "OPTIONS"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-b>, B<--bytes>"
msgstr "B<-b>, B<--bytes>"

#. type: Plain text
#: debian-bookworm
msgid "Print the sizes in bytes rather than in a human-readable format."
msgstr ""
"Afficher la taille (colonne SIZE) en octets plutôt qu'en format lisible."

#. type: Plain text
#: debian-bookworm
msgid ""
"By default, the unit, sizes are expressed in, is byte, and unit prefixes are "
"in power of 2^10 (1024). Abbreviations of symbols are exhibited truncated in "
"order to reach a better readability, by exhibiting alone the first letter of "
"them; examples: \"1 KiB\" and \"1 MiB\" are respectively exhibited as \"1 "
"K\" and \"1 M\", then omitting on purpose the mention \"iB\", which is part "
"of these abbreviations."
msgstr ""
"Par défaut l'unité dans laquelle les tailles sont exprimées est l'octet et "
"les préfixes d'unité sont des puissances de 2^10 (1024). Les abréviations "
"des symboles sont tronqués pour obtenir une meilleur lisibilité, en "
"n'affichant que la première lettre, par exemple : « 1 Kio » et « 1 Mio » "
"sont affichés « 1 K » et « 1 M » en omettant délibérément l'indication "
"« io » qui fait partie de ces abréviations."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, fuzzy
#| msgid "B<-i>,B< --noinaccessible>"
msgid "B<-i>, B<--noinaccessible>"
msgstr "B<-i>, B<--noinaccessible>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Ignore lock files which are inaccessible for the current user."
msgstr "Ignorer les fichiers de verrou inaccessibles à l’utilisateur actuel."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-J>, B<--json>"
msgstr "B<-J>, B<--json>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Use JSON output format."
msgstr "Utiliser le format de sortie JSON."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-n>, B<--noheadings>"
msgstr "B<-n>, B<--noheadings>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Do not print a header line."
msgstr "Ne pas imprimer de ligne d'en-tête."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-o>, B<--output> I<list>"
msgstr "B<-o>, B<--output> I<liste>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Specify which output columns to print. Use B<--help> to get a list of all "
"supported columns."
msgstr ""
"Indiquer les colonnes à afficher. Utilisez B<--help> pour obtenir une liste "
"de toutes les colonnes disponibles."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"The default list of columns may be extended if I<list> is specified in the "
"format I<+list> (e.g., B<lslocks -o +BLOCKER>)."
msgstr ""
"La liste de colonnes par défaut peut être étendue si I<liste> est indiquée "
"sous la forme B<+>I<liste> (par exemple, B<lslocks -o +BLOCKER>)."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<--output-all>"
msgstr "B<--output-all>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Output all available columns."
msgstr "Afficher toutes les colonnes disponibles."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, fuzzy
#| msgid "B<-p>,B< --pid >I<pid>"
msgid "B<-p>, B<--pid> I<pid>"
msgstr "B<-p>, B<--pid> I<PID>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Display only the locks held by the process with this I<pid>."
msgstr "N’afficher que les verrous maintenus par le processus avec ce I<PID>."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-r>, B<--raw>"
msgstr "B<-r>, B<--raw>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Use the raw output format."
msgstr "Utiliser l'affichage au format brut."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, fuzzy
#| msgid "B<-u>,B< --notruncate>"
msgid "B<-u>, B<--notruncate>"
msgstr "B<-u>, B<--notruncate>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Do not truncate text in columns."
msgstr "Ne pas tronquer le texte des colonnes."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-h>, B<--help>"
msgstr "B<-h>, B<--help>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Display help text and exit."
msgstr "Afficher l’aide-mémoire puis quitter."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: debian-bookworm
msgid "Print version and exit."
msgstr "Afficher la version puis quitter."

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "OUTPUT"
msgstr "SORTIE"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "COMMAND"
msgstr "COMMAND"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "The command name of the process holding the lock."
msgstr "Le nom de commande du processus qui détient le verrou."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "PID"
msgstr "PID"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "The process ID of the process which holds the lock or -1 for OFDLCK."
msgstr ""
"L’identifiant de processus du processus qui détient le verrou ou B<-1> pour "
"OFDLCK."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "TYPE"
msgstr "TYPE"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, fuzzy
#| msgid ""
#| "The type of lock; can be FLOCK (created with B<flock>(2)), POSIX (created "
#| "with B<fcntl>(2) and B<lockf>(3)) or OFDLCK (created with fcntl(2)."
msgid ""
"The type of lock; can be FLOCK (created with B<flock>(2)), POSIX (created "
"with B<fcntl>(2) and B<lockf>(3)) or OFDLCK (created with B<fcntl>(2))."
msgstr ""
"Le type de verrou ; il peut être FLOCK (créé avec B<flock>(2)), POSIX (créé "
"avec B<fcntl>(2) et B<lockf>(3)) ou OFDLCK (créé avec B<fcntl>(2))."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "SIZE"
msgstr "SIZE"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Size of the locked file."
msgstr "La taille du fichier verrouillé."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "MODE"
msgstr "MODE"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, fuzzy
#| msgid ""
#| "The lock's access permissions (read, write).  If the process is blocked "
#| "and waiting for the lock, then the mode is postfixed with an "
#| "'*' (asterisk)."
msgid ""
"The lock\\(cqs access permissions (read, write). If the process is blocked "
"and waiting for the lock, then the mode is postfixed with an \\(aq*\\(aq "
"(asterisk)."
msgstr ""
"Les droits d’accès (lecture, écriture) du verrou. Si le processus est bloqué "
"en attente du verrou, alors le mode a un « * » (astérisque) en préfixe"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "M"
msgstr "M"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, fuzzy
#| msgid ""
#| "Whether the lock is mandatory; 0 means no (meaning the lock is only "
#| "advisory), 1 means yes.  (See B<fcntl>(2).)"
msgid ""
"Whether the lock is mandatory; 0 means no (meaning the lock is only "
"advisory), 1 means yes. (See B<fcntl>(2).)"
msgstr ""
"Si le verrou est obligatoire : B<0> signifie non (c’est-à-dire que le verrou "
"n’est que coopératif), B<1> signifie oui (consultez B<fcntl>(2))."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "START"
msgstr "START"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Relative byte offset of the lock."
msgstr "Position relative en octet du verrou."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "END"
msgstr "END"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Ending offset of the lock."
msgstr "Position finale du verrou."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "PATH"
msgstr "PATH"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, fuzzy
#| msgid ""
#| "Full path of the lock.  If none is found, or there are no permissions to "
#| "read the path, it will fall back to the device's mountpoint and \"...\" "
#| "is appended to the path.  The path might be truncated; use B<--"
#| "notruncate> to get the full path."
msgid ""
"Full path of the lock. If none is found, or there are no permissions to read "
"the path, it will fall back to the device\\(cqs mountpoint and \"...\" is "
"appended to the path. The path might be truncated; use B<--notruncate> to "
"get the full path."
msgstr ""
"Chemin complet du verrou. Si aucun n’est trouvé ou si la lecture du chemin "
"n’est pas permise, le point de montage du périphérique sera utilisé en "
"solution de repli et « ... » est ajouté au chemin. Le chemin pourrait être "
"tronqué ; utilisez B<--notruncate> pour obtenir le chemin complet."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "BLOCKER"
msgstr "BLOCKER"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "The PID of the process which blocks the lock."
msgstr "Le PID du processus qui bloque le verrou."

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "NOTES"
msgstr "NOTES"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, fuzzy
#| msgid ""
#| "The B<lslocks> command is meant to replace the B<lslk>(8) command,\n"
#| "originally written by Victor A. Abell E<lt>abe@purdue.eduE<gt> and "
#| "unmaintained\n"
#| "since 2001.\n"
msgid ""
"The B<lslocks> command is meant to replace the B<lslk>(8) command, "
"originally written by"
msgstr ""
"La commande B<lslocks> a pour but de remplacer la commande B<lslk>(8), "
"écrite\n"
"à l’origine par Victor A. Abell E<lt>I<abe@purdue.edu>E<gt> et non "
"maintenue\n"
"depuis 2001.\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "and unmaintained since 2001."
msgstr ""

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "AUTHORS"
msgstr "AUTEURS"

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<flock>(1), B<fcntl>(2), B<lockf>(3)"
msgstr "B<flock>(1), B<fcntl>(2), B<lockf>(3)"

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "REPORTING BUGS"
msgstr "SIGNALER DES BOGUES"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "For bug reports, use the issue tracker at"
msgstr ""
"Pour envoyer un rapport de bogue, utilisez le système de gestion des "
"problèmes à l'adresse"

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "AVAILABILITY"
msgstr "DISPONIBILITÉ"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, fuzzy
#| msgid ""
#| "The lslocks command is part of the util-linux package and is available "
#| "from https://www.kernel.org/pub/linux/utils/util-linux/."
msgid ""
"The B<lslocks> command is part of the util-linux package which can be "
"downloaded from"
msgstr ""
"La commande B<lslocks> fait partie du paquet util-linux, elle est disponible "
"sur E<lt>I<https://www.kernel.org/pub/linux/utils/util-linux/>E<gt>."

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2022-02-14"
msgstr "14 février 2022"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "util-linux 2.37.4"
msgstr "util-linux 2.37.4"

#. type: Plain text
#: opensuse-leap-15-6
msgid "Print the SIZE column in bytes rather than in a human-readable format."
msgstr ""
"Afficher la taille (colonne B<SIZE>) en octet plutôt qu'en format lisible."

#. type: Plain text
#: opensuse-leap-15-6
msgid "Display version information and exit."
msgstr "Afficher le nom et la version du logiciel et quitter."
