# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Christophe Blaess <ccb@club-internet.fr>, 1997, 2002, 2003.
# Michel Quercia <quercia AT cal DOT enst DOT fr>, 1997.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999.
# Frédéric Delanoy <delanoy_f@yahoo.com>, 2000.
# Thierry Vignaud <tvignaud@mandriva.com>, 2000.
# Christophe Sauthier <christophe@sauthier.com>, 2001.
# Sébastien Blanchet, 2002.
# Jérôme Perzyna <jperzyna@yahoo.fr>, 2004.
# Aymeric Nys <aymeric AT nnx POINT com>, 2004.
# Alain Portal <aportal@univ-montp2.fr>, 2005, 2006.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006.
# Yves Rütschlé <l10n@rutschle.net>, 2006.
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006.
# Julien Cristau <jcristau@debian.org>, 2006.
# Philippe Piette <foudre-blanche@skynet.be>, 2006.
# Jean-Baka Domelevo-Entfellner <domelevo@gmail.com>, 2006.
# Nicolas Haller <nicolas@boiteameuh.org>, 2006.
# Sylvain Archenault <sylvain.archenault@laposte.net>, 2006.
# Valéry Perrin <valery.perrin.debian@free.fr>, 2006.
# Jade Alglave <jade.alglave@ens-lyon.org>, 2006.
# Nicolas François <nicolas.francois@centraliens.net>, 2007.
# Alexandre Kuoch <alex.kuoch@gmail.com>, 2008.
# Lyes Zemmouche <iliaas@hotmail.fr>, 2008.
# Florentin Duneau <fduneau@gmail.com>, 2006, 2008, 2009, 2010.
# Alexandre Normand <aj.normand@free.fr>, 2010.
# David Prévot <david@tilapin.org>, 2010-2015.
# Jean-Paul Guillonneau <guillonneau.jeanpaul@free.fr>, 2021.
# Jean-Pierre Giraud <jean-pierregiraud@neuf.fr>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: manpages-fr-extra-util-linux\n"
"POT-Creation-Date: 2023-08-27 16:50+0200\n"
"PO-Revision-Date: 2022-05-12 10:02+0200\n"
"Last-Translator: Jean-Pierre Giraud <jean-pierregiraud@neuf.fr>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: vim\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#. type: TH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "BLKID"
msgstr "BLKID"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-05-11"
msgstr "11 mai 2022"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "util-linux 2.38.1"
msgstr "util-linux 2.38.1"

#. type: TH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "System Administration"
msgstr "Administration Système"

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "blkid - locate/print block device attributes"
msgstr "blkid – Trouver ou afficher les attributs de périphérique en mode bloc"

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<blkid> B<--label> I<label> | B<--uuid> I<uuid>"
msgstr "B<blkid> B<--label> I<étiquette> | B<--uuid> I<UUID>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"B<blkid> [B<--no-encoding> B<--garbage-collect> B<--list-one> B<--cache-"
"file> I<file>] [B<--output> I<format>] [B<--match-tag> I<tag>] [B<--match-"
"token> I<NAME=value>] [I<device>...]"
msgstr ""
"B<blkid> [B<--no-encoding> B<--garbage-collect> B<--list-one> B<--cache-"
"file> I<fichier>] [B<--output> I<format>] [B<--match-tag> I<indicateur>] "
"[B<--match-token> I<NOM>B<=>I<valeur>] [I<périphérique> ...]"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"B<blkid> B<--probe> [B<--offset> I<offset>] [B<--output> I<format>] [B<--"
"size> I<size>] [B<--match-tag> I<tag>] [B<--match-types> I<list>] [B<--"
"usages> I<list>] [B<--no-part-details>] I<device>..."
msgstr ""
"B<blkid> B<--probe> [B<--offset> I<position>] [B<--output> I<format>] [B<--"
"size> I<taille>] [B<--match-tag> I<indicateur>] [B<--match-types> I<liste>] "
"[B<--usages> I<liste>] [B<--no-part-details>] I<périphérique> ..."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"B<blkid> B<--info> [B<--output format>] [B<--match-tag> I<tag>] I<device>..."
msgstr ""
"B<blkid> B<--info> [B<--output format>] [B<--match-tag> I<indicateur>] "
"I<périphérique> ..."

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"The B<blkid> program is the command-line interface to working with the "
"B<libblkid>(3) library. It can determine the type of content (e.g., "
"filesystem or swap) that a block device holds, and also the attributes "
"(tokens, NAME=value pairs) from the content metadata (e.g., LABEL or UUID "
"fields)."
msgstr ""
"Le programme B<blkid> est une interface en ligne de commande pour la "
"bibliothèque B<libblkid>(3). Il peut déterminer le type de contenu (par "
"exemple, un système de fichiers ou une partition d'échange) associé à un "
"périphérique bloc et aussi les attributs (sous la forme de jetons "
"I<NOM>B<=>I<valeur>) des métadonnées (par exemple, l'étiquette B<LABEL> ou "
"l'B<UUID>)."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"B<It is recommended to use> B<lsblk>(8) B<command to get information about "
"block devices, or lsblk --fs to get an overview of filesystems, or> "
"B<findmnt>(8) B<to search in already mounted filesystems.>"
msgstr ""
"B<Il est recommandé d’utiliser la commande lsblk(8) pour obtenir des "
"informations à propos des périphériques bloc, ou lsblk --fs pour obtenir une "
"vue d’ensemble des systèmes de fichiers, ou findmnt(8) pour effectuer une "
"recherche dans les systèmes de fichiers déjà montés.>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"B<lsblk>(8) provides more information, better control on output formatting, "
"easy to use in scripts and it does not require root permissions to get "
"actual information. B<blkid> reads information directly from devices and for "
"non-root users it returns cached unverified information. B<blkid> is mostly "
"designed for system services and to test B<libblkid>(3) functionality."
msgstr ""
"B<lsblk>(8) fournit plus d’informations, un meilleur contrôle sur le "
"formatage de la sortie, une utilisation plus aisée dans les scripts et ne "
"nécessite pas les permissions de superutilisateur pour obtenir les "
"informations concrètes. B<blkid> lit les informations directement des "
"périphériques et pour les utilisateurs normaux, il renvoie les informations "
"mises en cache et non vérifiées. B<blkid> est principalement conçu pour les "
"services de système et pour tester les fonctionnalités de B<libblkid>(3)."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"When I<device> is specified, tokens from only this device are displayed. It "
"is possible to specify multiple I<device> arguments on the command line. If "
"none is given, all partitions or unpartitioned devices which appear in I</"
"proc/partitions> are shown, if they are recognized."
msgstr ""
"Quand un I<périphérique> est indiqué, seuls les jetons pour ce "
"I<périphérique> sont affichés. Plusieurs arguments I<périphérique> peuvent "
"être indiqués sur la ligne de commande. Si aucun n'est donné, tous les "
"partitions et périphériques non partitionnés apparaissant dans I</proc/"
"partitions> seront affichés, s'ils sont reconnus."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"B<blkid> has two main forms of operation: either searching for a device with "
"a specific NAME=value pair, or displaying NAME=value pairs for one or more "
"specified devices."
msgstr ""
"B<blkid> a deux modes de fonctionnement : soit il recherche un périphérique "
"avec un couple I<NOM>B<=>I<valeur> donné, soit il affiche les couples "
"I<NOM>B<=>I<valeur> pour un ou plusieurs I<périphérique>s indiqués."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"For security reasons B<blkid> silently ignores all devices where the probing "
"result is ambivalent (multiple colliding filesystems are detected). The low-"
"level probing mode (B<-p>) provides more information and extra exit status "
"in this case. It\\(cqs recommended to use B<wipefs>(8) to get a detailed "
"overview and to erase obsolete stuff (magic strings) from the device."
msgstr ""
"Pour des raisons de sécurité, B<blkid> ignore silencieusement tous les "
"périphériques quand le résultat des détections est ambivalent (plusieurs "
"périphériques en conflit sont détectés). Le mode bas niveau (B<-p>) fournit "
"plus d’informations et un code de retour supplémentaire. Il est recommandé "
"d’utiliser B<wipefs>(8) pour une vue d’ensemble détaillée et pour supprimer "
"toutes les choses obsolètes (chaînes magiques) sur le périphérique."

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "OPTIONS"
msgstr "OPTIONS"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"The I<size> and I<offset> arguments may be followed by the multiplicative "
"suffixes like KiB (=1024), MiB (=1024*1024), and so on for GiB, TiB, PiB, "
"EiB, ZiB and YiB (the \"iB\" is optional, e.g., \"K\" has the same meaning "
"as \"KiB\"), or the suffixes KB (=1000), MB (=1000*1000), and so on for GB, "
"TB, PB, EB, ZB and YB."
msgstr ""
"Les arguments I<taille> et I<position> peuvent être suivis de suffixes "
"multiplicatifs comme KiB=1024, MiB=1024*1024, etc., pour GiB, TiB, PiB, EiB, "
"ZiB et YiB (la partie « iB » est facultative, par exemple « K » est "
"identique à « KiB ») ou des suffixes KB=1000, MB=1000*1000, etc., pour GB, "
"TB, PB, EB, ZB et YB."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-c>, B<--cache-file> I<cachefile>"
msgstr "B<-c>, B<--cache-file> I<fichier_cache>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Read from I<cachefile> instead of reading from the default cache file (see "
"the B<CONFIGURATION FILE> section for more details). If you want to start "
"with a clean cache (i.e., don\\(cqt report devices previously scanned but "
"not necessarily available at this time), specify I</dev/null>."
msgstr ""
"Lire dans le I<fichier_cache> plutôt que dans le fichier de cache par défaut "
"(consultez la section B<FICHIER DE CONFIGURATION> pour plus de précisions). "
"Si vous souhaitez utiliser un cache vide (c'est-à-dire ne pas afficher les "
"périphériques examinés auparavant, mais qui ne seraient plus disponibles), "
"utilisez I</dev/null>."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-d>, B<--no-encoding>"
msgstr "B<-d>, B<--no-encoding>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Don\\(cqt encode non-printing characters. The non-printing characters are "
"encoded by ^ and M- notation by default. Note that the B<--output udev> "
"output format uses a different encoding which cannot be disabled."
msgstr ""
"Ne pas encoder les caractères non imprimables. Les caractères non "
"imprimables sont encodés par les notations B<^> et B<M-> par défaut. "
"Remarquez que le format de sortie B<--output\\ udev> utilise un encodage "
"différent qui ne peut pas être désactivé."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-D>, B<--no-part-details>"
msgstr "B<-D>, B<--no-part-details>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Don\\(cqt print information (PART_ENTRY_* tags) from partition table in low-"
"level probing mode."
msgstr ""
"Ne pas afficher d’information (indicateurs B<PART_ENTRY_>I<*>) de la table "
"de partitions dans le mode de détection bas niveau."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-g>, B<--garbage-collect>"
msgstr "B<-g>, B<--garbage-collect>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Perform a garbage collection pass on the blkid cache to remove devices which "
"no longer exist."
msgstr ""
"Lancer le ramasse-miettes sur le cache de B<blkid> pour supprimer les "
"périphériques qui n'existent plus."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-H>, B<--hint> I<setting>"
msgstr "B<-H>, B<--hint> I<réglage>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Set probing hint. The hints are an optional way to force probing functions "
"to check, for example, another location. The currently supported is "
"\"session_offset=I<number>\" to set session offset on multi-session UDF."
msgstr ""
"Régler les indications de détection. Les indications sont une manière "
"optionnelle pour obliger les fonctions de détection à vérifier par exemple "
"un autre emplacement. L'indication actuellement prise en charge est "
"« session_offset=I<nombre> » pour définir la position d'une session d'un "
"système de fichiers UDF multi-session."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-i>, B<--info>"
msgstr "B<-i>, B<--info>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Display information about I/O Limits (aka I/O topology). The "
"\\(aqexport\\(aq output format is automatically enabled. This option can be "
"used together with the B<--probe> option."
msgstr ""
"Afficher les renseignements de limites (topologie) d'E/S. Le format de "
"sortie B<export> est automatiquement activé. Cette option peut être utilisée "
"avec l'option B<-probe>."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-k>, B<--list-filesystems>"
msgstr "B<-k>, B<--list-filesystems>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "List all known filesystems and RAIDs and exit."
msgstr "Afficher tous les systèmes de fichiers et RAID connus et quitter."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-l>, B<--list-one>"
msgstr "B<-l>, B<--list-one>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Look up only one device that matches the search parameter specified with the "
"B<--match-token> option. If there are multiple devices that match the "
"specified search parameter, then the device with the highest priority is "
"returned, and/or the first device found at a given priority (but see below "
"note about udev). Device types in order of decreasing priority are: Device "
"Mapper, EVMS, LVM, MD, and finally regular block devices. If this option is "
"not specified, B<blkid> will print all of the devices that match the search "
"parameter."
msgstr ""
"Ne rechercher qu’un périphérique correspondant au paramètre demandé indiqué "
"avec l'option B<-match-token>. Si plusieurs périphériques correspondent, le "
"périphérique de priorité la plus haute sera affiché ou le premier "
"périphérique avec la priorité voulue (mais consultez ci-dessous la remarque "
"sur udev). Voici les types de périphériques par priorité décroissante : "
"Device Mapper, EVMS, LVM, MD, puis les périphériques bloc classiques. Si "
"cette option n'est pas utilisée, B<blkid> affichera tous les périphériques "
"qui correspondent à la recherche."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"This option forces B<blkid> to use udev when used for LABEL or UUID tokens "
"in B<--match-token>. The goal is to provide output consistent with other "
"utils (like B<mount>(8), etc.) on systems where the same tag is used for "
"multiple devices."
msgstr ""
"Cette option oblige B<blkid> à utiliser les jetons LABEL ou UUID dans B<--"
"match-token>. Le but est de fournir une sortie cohérente avec celle d’autres "
"outils (tel B<mount>(8), etc.) sur les systèmes où le même indicateur est "
"utilisé pour plusieurs périphériques."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-L>, B<--label> I<label>"
msgstr "B<-L>, B<--label> I<étiquette>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Look up the device that uses this filesystem I<label>; this is equal to B<--"
"list-one --output device --match-token LABEL=>I<label>. This lookup method "
"is able to reliably use /dev/disk/by-label udev symlinks (dependent on a "
"setting in I</etc/blkid.conf>). Avoid using the symlinks directly; it is not "
"reliable to use the symlinks without verification. The B<--label> option "
"works on systems with and without udev."
msgstr ""
"Rechercher le périphérique qui utilise cette I<étiquette> de système de "
"fichiers ; c’est équivalent à B<--list-one --output device --match-token "
"LABEL=>I<étiquette>. Cette méthode de recherche est capable d'utiliser de "
"manière fiable les liens symboliques udev du type I</dev/disk/by-label> "
"(suivant une configuration définie dans I</etc/blkid.conf>). Évitez "
"d'utiliser directement les liens symboliques, car leur utilisation n’est pas "
"sûre sans vérification. L'option B<--label> fonctionne sur des systèmes avec "
"ou sans udev."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Unfortunately, the original B<blkid>(8) from e2fsprogs uses the B<-L> option "
"as a synonym for B<-o list>. For better portability, use B<-l -o device -t "
"LABEL=>I<label> and B<-o list> in your scripts rather than the B<-L> option."
msgstr ""
"Malheureusement, le B<blkid>(8) d'origine d'e2fsprogs utilise l'option B<-L> "
"comme synonyme de B<-o list>. Pour assurer la portabilité, l'utilisation de "
"B<-l\\ -o\\ device\\ -t\\ LABEL=>I<étiquette> et B<-o\\ list> est à préférer "
"à l'option B<-L> dans les scripts."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-n>, B<--match-types> I<list>"
msgstr "B<-n>, B<--match-types> I<liste>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Restrict the probing functions to the specified (comma-separated) I<list> of "
"superblock types (names). The list items may be prefixed with \"no\" to "
"specify the types which should be ignored. For example:"
msgstr ""
"Restreindre les fonctions de détection à la I<liste>, séparée par des "
"virgules, de types (noms) de superblocs. Les types peuvent être préfixés "
"dans la liste par B<no> pour signifier qu'ils doivent être ignorés. Par "
"exemple :"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<blkid --probe --match-types vfat,ext3,ext4 /dev/sda1>"
msgstr "B<blkid --probe --match-types vfat,ext3,ext4 /dev/sda1>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "probes for vfat, ext3 and ext4 filesystems, and"
msgstr "détecte les systèmes de fichiers VFAT, ext3 et ext4, et"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<blkid --probe --match-types nominix /dev/sda1>"
msgstr "B<blkid --probe --match-types nominix /dev/sda1>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"probes for all supported formats except minix filesystems. This option is "
"only useful together with B<--probe>."
msgstr ""
"détecte tous les formats gérés sauf les systèmes de fichiers MINIX. Cette "
"option n'est utile qu'avec l'option B<--probe>."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-o>, B<--output> I<format>"
msgstr "B<-o>, B<--output> I<format>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Use the specified output format. Note that the order of variables and "
"devices is not fixed. See also option B<-s>. The I<format> parameter may be:"
msgstr ""
"Utiliser le format de sortie indiqué. Remarquez que l’ordre des variables et "
"des périphériques n’est pas fixé. Consultez également l’option B<-s>. Le "
"paramètre I<format> peut prendre une des valeurs suivantes :"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<full>"
msgstr "B<full>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "print all tags (the default)"
msgstr "Afficher tous les indicateurs (option par défaut)."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<value>"
msgstr "B<value>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "print the value of the tags"
msgstr "Afficher la valeur des indicateurs."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<list>"
msgstr "B<list>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"print the devices in a user-friendly format; this output format is "
"unsupported for low-level probing (B<--probe> or B<--info>)."
msgstr ""
"Afficher les périphériques dans un format agréable. Ce format d'affichage "
"n'est pas pris en charge pour la détection bas niveau (B<--probe> ou B<--"
"info>)."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"This output format is B<DEPRECATED> in favour of the B<lsblk>(8) command."
msgstr ""
"Ce format d’affichage est B<obsolète>, remplacé par la commande B<lsblk>(8)."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<device>"
msgstr "B<device>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"print the device name only; this output format is always enabled for the B<--"
"label> and B<--uuid> options"
msgstr ""
"Afficher seulement le nom de périphérique. Ce format d'affichage est "
"toujours activé pour les options B<--label> et B<--uuid>."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<udev>"
msgstr "B<udev>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"print key=\"value\" pairs for easy import into the udev environment; the "
"keys are prefixed by ID_FS_ or ID_PART_ prefixes. The value may be modified "
"to be safe for udev environment; allowed is plain ASCII, hex-escaping and "
"valid UTF-8, everything else (including whitespaces) is replaced with "
"\\(aq_\\(aq. The keys with I<_ENC> postfix use hex-escaping for unsafe chars."
msgstr ""
"Afficher les paires I<clé>B<=>I<valeur> pour une importation aisée dans "
"l’environnement udev. Les clés sont préfixées avec B<ID_FS_> ou B<ID_PART_>. "
"La valeur peut être modifiée pour être sûre dans l’environnement udev. Sont "
"permises l’ASCII pur, la protection par valeur hexadécimale et l’UTF-8 "
"valable. Toutes les autres valeurs (y compris les espaces blancs) sont "
"remplacées par « _ ». Les clés avec le suffixe I<_ENC> utilisent la "
"protection hexadécimale pour les caractères non sûrs."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"The udev output returns the ID_FS_AMBIVALENT tag if more superblocks are "
"detected, and ID_PART_ENTRY_* tags are always returned for all partitions "
"including empty partitions."
msgstr ""
"La sortie B<udev> renvoie l'indicateur B<ID_FS_AMBIVALENT> si plusieurs "
"superblocs sont détectés et des indicateurs B<ID_PART_ENTRY_>I<*> sont "
"toujours renvoyés pour toutes les partitions, y compris les partitions vides."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "This output format is B<DEPRECATED>."
msgstr "Ce format d’affichage est B<obsolète>."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<export>"
msgstr "B<export>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"print key=value pairs for easy import into the environment; this output "
"format is automatically enabled when I/O Limits (B<--info> option) are "
"requested."
msgstr ""
"Afficher les couples de I<clé>B<=>I<valeur> pour les importer facilement "
"dans l'environnement. Ce format de sortie est automatiquement activé quand "
"des limites d'E/S (option B<--info>) sont demandées."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"The non-printing characters are encoded by ^ and M- notation and all "
"potentially unsafe characters are escaped."
msgstr ""
"Les caractères non imprimables sont encodés par les notations B<^> et B<M-> "
"et tous les caractères potentiellement non sûrs sont protégés."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-O>, B<--offset> I<offset>"
msgstr "B<-O>, B<--offset> I<position>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Probe at the given I<offset> (only useful with B<--probe>). This option can "
"be used together with the B<--info> option."
msgstr ""
"Détecter à la I<position> donnée (seulement utile avec B<--probe>). Cette "
"option peut être utilisée avec l'option B<--info>."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-p>, B<--probe>"
msgstr "B<-p>, B<--probe>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Switch to low-level superblock probing mode (bypassing the cache)."
msgstr ""
"Passer en mode de détection bas niveau de superbloc (en contournant le "
"cache)."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Note that low-level probing also returns information about partition table "
"type (PTTYPE tag) and partitions (PART_ENTRY_* tags). The tag names produced "
"by low-level probing are based on names used internally by libblkid and it "
"may be different than when executed without B<--probe> (for example "
"PART_ENTRY_UUID= vs PARTUUID=). See also B<--no-part-details>."
msgstr ""
"Remarquez que la détection bas niveau renvoie aussi des informations sur le "
"type de table de partitions (indicateur B<PTTYPE>) et sur les partitions "
"(indicateurs B<PART_ENTRY_>I<*>). Les noms d’indicateurs produits par la "
"détection bas niveau sont basés sur les noms utilisés en interne par "
"libblkid et peuvent être différents lorsqu’elle est exécutée sans B<--probe> "
"(par exemple, B<PART_ENTRY_UUID=> vs B<PARTUUID=>). Consultez aussi B<--no-"
"part-details>."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-s>, B<--match-tag> I<tag>"
msgstr "B<-s>, B<--match-tag> I<indicateur>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"For each (specified) device, show only the tags that match I<tag>. It is "
"possible to specify multiple B<--match-tag> options. If no tag is specified, "
"then all tokens are shown for all (specified) devices. In order to just "
"refresh the cache without showing any tokens, use B<--match-tag none> with "
"no other options."
msgstr ""
"Pour chaque périphérique indiqué, n'afficher que les indicateurs qui "
"correspondent à I<indicateur>. L'option B<--match-tag> peut être indiquée "
"plusieurs fois. Si aucun indicateur n'est indiqué, tous les jetons sont "
"affichés pour tous les périphériques indiqués. Pour ne faire que rafraîchir "
"le cache sans afficher de jeton, utilisez B<--match-tag\\ none> sans aucune "
"autre option."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-S>, B<--size> I<size>"
msgstr "B<-S>, B<--size> I<taille>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Override the size of device/file (only useful with B<--probe>)."
msgstr ""
"Remplacer la taille du périphérique ou du fichier (seulement utile avec B<--"
"probe>)."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-t>, B<--match-token> I<NAME=value>"
msgstr "B<-t>, B<--match-token> I<NOM>B<=>I<valeur>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Search for block devices with tokens named I<NAME> that have the value "
"I<value>, and display any devices which are found. Common values for I<NAME> "
"include B<TYPE>, B<LABEL>, and B<UUID>. If there are no devices specified on "
"the command line, all block devices will be searched; otherwise only the "
"specified devices are searched."
msgstr ""
"Rechercher les périphériques bloc qui possèdent des jetons nommés I<NOM> et "
"qui ont pour valeur I<valeur>, puis afficher les périphériques trouvés. Les "
"valeurs usuelles de I<NOM> sont B<TYPE>, B<LABEL> et B<UUID>. S'il n'y a pas "
"de périphérique précisé sur la ligne de commande, tous les périphériques "
"bloc seront analysés ; sinon, seuls les périphériques indiqués par "
"l'utilisateur seront étudiés."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-u>, B<--usages> I<list>"
msgstr "B<-u>, B<--usages> I<liste>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Restrict the probing functions to the specified (comma-separated) I<list> of "
"\"usage\" types. Supported usage types are: filesystem, raid, crypto and "
"other. The list items may be prefixed with \"no\" to specify the usage types "
"which should be ignored. For example:"
msgstr ""
"Restreindre les fonctions de détection à la I<liste>, séparée par des "
"virgules, de types d'« utilisation ». Les types d'utilisation gérés sont : "
"B<filesystem> (système de fichiers), B<raid> (RAID), B<crypto> (chiffré) et "
"B<other> (autre). Les types d'utilisation peuvent être préfixés dans la "
"liste par B<no> pour signifier qu'ils doivent être ignorés. Par exemple :"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<blkid --probe --usages filesystem,other /dev/sda1>"
msgstr "B<blkid --probe --usages filesystem,other /dev/sda1>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "probes for all filesystem and other (e.g., swap) formats, and"
msgstr ""
"détecte tous les formats de systèmes de fichiers et autres (par exemple, les "
"espaces d'échange) et "

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<blkid --probe --usages noraid /dev/sda1>"
msgstr "B<blkid --probe --usages noraid /dev/sda1>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"probes for all supported formats except RAIDs. This option is only useful "
"together with B<--probe>."
msgstr ""
"détecte tous les formats gérés sauf les RAID. Cette option n'est utile "
"qu'avec l'option B<--probe>."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-U>, B<--uuid> I<uuid>"
msgstr "B<-U>, B<--uuid> I<UUID>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Look up the device that uses this filesystem I<uuid>. For more details see "
"the B<--label> option."
msgstr ""
"Chercher le périphérique qui utilise cet I<UUID> de système de fichiers. "
"Pour plus de précisions, consultez l'option B<--label>."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-h>, B<--help>"
msgstr "B<-h>, B<--help>"

#. type: Plain text
#: debian-bookworm
msgid "Display help text and exit."
msgstr "Afficher l’aide-mémoire puis quitter."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: debian-bookworm
msgid "Print version and exit."
msgstr "Afficher la version puis quitter."

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "EXIT STATUS"
msgstr "CODE DE RETOUR"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"If the specified device or device addressed by specified token (option B<--"
"match-token>) was found and it\\(cqs possible to gather any information "
"about the device, an exit status 0 is returned. Note the option B<--match-"
"tag> filters output tags, but it does not affect exit status."
msgstr ""
"Si le périphérique indiqué ou celui adressé par l’indicateur précisé (option "
"B<--match-token>) a été trouvé et qu’il est possible de réunir quelques "
"informations à son propos, un code de retour B<0> est renvoyé. Remarquez que "
"l’option B<--match-tag> filtre les indicateurs de sortie, mais qu’elle "
"n’affecte pas le code de retour."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"If the specified token was not found, or no (specified) devices could be "
"identified, or it is impossible to gather any information about the device "
"identifiers or device content an exit status of 2 is returned."
msgstr ""
"Si le jeton indiqué n'a pas été trouvé, ou si aucun périphérique (indiqué) "
"n'a pu être identifié, ou qu’il est impossible d’obtenir une information sur "
"les identificateurs ou le contenu de périphérique, un code de retour B<2> "
"est renvoyé."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "For usage or other errors, an exit status of 4 is returned."
msgstr ""
"Pour les erreurs d'utilisation et autres, un code de retour B<4> est renvoyé."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"If an ambivalent probing result was detected by low-level probing mode (B<-"
"p>), an exit status of 8 is returned."
msgstr ""
"Si un résultat de détection bas niveau ambivalent a été détecté (B<-p>), un "
"code de retour B<8> est renvoyé."

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "CONFIGURATION FILE"
msgstr "FICHIER DE CONFIGURATION"

#. type: Plain text
#: debian-bookworm
msgid ""
"The standard location of the I</etc/blkid.conf> config file can be "
"overridden by the environment variable B<BLKID_CONF>. The following options "
"control the libblkid library:"
msgstr ""
"L'emplacement standard du fichier de configuration I</etc/blkid.conf> peut "
"être remplacé par la variable d'environnement B<BLKID_CONF>. Les options "
"suivantes contrôlent la bibliothèque libblkid."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "I<SEND_UEVENT=E<lt>yes|notE<gt>>"
msgstr "B<SEND_UEVENT=>I<yes>|I<not>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Sends uevent when I</dev/disk/by-{label,uuid,partuuid,partlabel}/> symlink "
"does not match with LABEL, UUID, PARTUUID or PARTLABEL on the device. "
"Default is \"yes\"."
msgstr ""
"Envoyer « uevent » lorsque le lien symbolique I</dev/disk/by-{label,uuid,"
"partuuid,partlabel}/> ne correspond pas au LABEL, UUID, PARTUUID ou "
"PARTLABEL du périphérique. I<yes> par défaut."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "I<CACHE_FILE=E<lt>pathE<gt>>"
msgstr "B<CACHE_FILE=>I<chemin>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Overrides the standard location of the cache file. This setting can be "
"overridden by the environment variable B<BLKID_FILE>. Default is I</run/"
"blkid/blkid.tab>, or I</etc/blkid.tab> on systems without a I</run> "
"directory."
msgstr ""
"Remplacer l'emplacement standard du fichier de cache. Cette option peut être "
"remplacée par la variable d'environnement B<BLKID_FILE>. I</run/blkid/blkid."
"tab> par défaut ou I</etc/blkid.tab> sur les systèmes sans répertoire I</"
"run>."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "I<EVALUATE=E<lt>methodsE<gt>>"
msgstr "B<EVALUATE=>I<méthodes>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Defines LABEL and UUID evaluation method(s). Currently, the libblkid library "
"supports the \"udev\" and \"scan\" methods. More than one method may be "
"specified in a comma-separated list. Default is \"udev,scan\". The \"udev\" "
"method uses udev I</dev/disk/by-*> symlinks and the \"scan\" method scans "
"all block devices from the I</proc/partitions> file."
msgstr ""
"Définir la ou les I<méthodes> d'évaluation des LABEL et UUID. Actuellement, "
"libblkid gère les méthodes « B<udev> » et « B<scan> ». Plus d'une méthode "
"peut être indiquée dans une liste séparée par des virgules. La valeur par "
"défaut est « B<udev,scan> ». La méthode « B<udev> » utilise les liens "
"symboliques I</dev/disk/by-*> d’B<udev> et la méthode « B<scan> » analyse "
"tous les périphériques bloc du fichier I</proc/partitions>."

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "ENVIRONMENT"
msgstr "ENVIRONNEMENT"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Setting I<LIBBLKID_DEBUG=all> enables debug output."
msgstr "La configuration B<LIBBLKID_DEBUG=all> active la sortie de débogage."

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "AUTHORS"
msgstr "AUTEURS"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"B<blkid> was written by Andreas Dilger for libblkid and improved by Theodore "
"Ts\\(cqo and Karel Zak."
msgstr ""
"B<blkid> a été écrit par Andreas Dilger pour libblkid et amélioré par "
"Theodore Ts\\(cqo et Karel Zak."

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<libblkid>(3), B<findfs>(8), B<lsblk>(8), B<wipefs>(8)"
msgstr "B<libblkid>(3), B<findfs>(8), B<lsblk>(8), B<wipefs>(8)"

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "REPORTING BUGS"
msgstr "SIGNALER DES BOGUES"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "For bug reports, use the issue tracker at"
msgstr ""
"Pour envoyer un rapport de bogue, utilisez le système de gestion des "
"problèmes à l'adresse"

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "AVAILABILITY"
msgstr "DISPONIBILITÉ"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"The B<blkid> command is part of the util-linux package which can be "
"downloaded from"
msgstr ""
"La commande B<blkid> fait partie du paquet util-linux, qui peut être "
"téléchargé de"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2022-02-14"
msgstr "14 février 2022"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "util-linux 2.37.4"
msgstr "util-linux 2.37.4"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Read from I<cachefile> instead of reading from the default cache file (see "
"the CONFIGURATION FILE section for more details). If you want to start with "
"a clean cache (i.e., don\\(cqt report devices previously scanned but not "
"necessarily available at this time), specify I</dev/null>."
msgstr ""
"Lire dans le I<fichier_cache> plutôt que dans le fichier de cache par défaut "
"(consultez la section B<FICHIER DE CONFIGURATION> pour plus de précisions). "
"Si vous souhaitez utiliser un cache vide (c'est-à-dire ne pas afficher les "
"périphériques examinés auparavant, mais qui ne seraient plus disponibles), "
"utilisez I</dev/null>."

#. type: Plain text
#: opensuse-leap-15-6
msgid "Display a usage message and exit."
msgstr "Afficher l'aide sur l'utilisation, puis quitter."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Set probing hint. The hints are optional way how to force probing functions "
"to check for example another location. The currently supported is "
"\"session_offset=I<number>\" to set session offset on multi-session UDF."
msgstr ""
"Régler les indications de détection. Les indications sont une manière "
"optionnelle pour obliger les fonctions de détection à vérifier par exemple "
"un autre emplacement. L'indication actuellement prise en charge est "
"« session_offset=I<nombre> » pour définir la position d'une session d'un "
"système de fichiers UDF multi-session."

#. type: Plain text
#: opensuse-leap-15-6
msgid "Display version number and exit."
msgstr "Afficher le numéro de version, puis quitter."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"The standard location of the I</etc/blkid.conf> config file can be "
"overridden by the environment variable BLKID_CONF. The following options "
"control the libblkid library:"
msgstr ""
"L'emplacement standard du fichier de configuration I</etc/blkid.conf> peut "
"être remplacé par la variable d'environnement B<BLKID_CONF>. Les options "
"suivantes contrôlent la bibliothèque libblkid."
