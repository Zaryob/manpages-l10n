# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Julien Cristau <jcristau@debian.org>, 2006, 2007, 2008.
# Florentin Duneau <fduneau@gmail.com>, 2009.
# David Prévot <david@tilapin.org>, 2011.
# Jean-Paul Guillonneau <guillonneau.jeanpaul@free.fr>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: at 3.1.10\n"
"POT-Creation-Date: 2023-08-27 16:50+0200\n"
"PO-Revision-Date: 2021-05-17 17:02+0200\n"
"Last-Translator: Jean-Paul Guillonneau <guillonneau.jeanpaul@free.fr>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: vim\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ATD"
msgstr "ATD"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "2009-11-14"
msgstr "14 novembre 2009"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "atd - run jobs queued for later execution"
msgstr "atd – Exécuter des tâches enregistrées pour un lancement ultérieur"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"B<atd> [B<-l> I<load_avg>] [B<-b> I<batch_interval>] [B<-d>] [B<-f>] [B<-s>]"
msgstr ""
"B<atd> [B<-l> I<charge_moyenne>] [B<-b> I<intervalle_batch>] [B<-d>] [B<-f>] "
"[B<-s>]"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<atd> runs jobs queued by B<at>(1)B<.>"
msgstr "B<atd> exécute les travaux mis en file d'attente par B<at(1)>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr "OPTIONS"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-l>"
msgstr "B<-l>"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide mageia-cauldron
msgid ""
"Specifies a limiting load factor, over which batch jobs should not be run, "
"instead of the compile-time choice of 0.8.  For an SMP system with I<n> "
"CPUs, you will probably want to set this higher than I<n-1.>"
msgstr ""
"Indiquer le facteur limite de charge, au delà duquel les travaux ne "
"devraient pas être lancés, à la place du choix de B<0,8> précompilé. Pour un "
"système SMP (multiprocesseur) avec I<n> CPU, vous voudrez probablement le "
"mettre plus grand que I<n-1>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-b>"
msgstr "B<-b>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Specify the minimum interval in seconds between the start of two batch jobs "
"(60 default)."
msgstr ""
"Indiquer l'intervalle minimal en secondes entre le départ de deux "
"traitements par lots (60 par défaut)"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-d>"
msgstr "B<-d>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Debug; print error messages to standard error instead of using "
"B<syslog>(3)B<.> This option also implies B<-f>."
msgstr ""
"Débogage. Afficher les messages d'erreurs sur la sortie standard au lieu "
"d'utiliser B<syslog>(3). Cette option implique également l'option B<-f>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-f>"
msgstr "B<-f>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Run B<atd> in the foreground."
msgstr "Exécuter B<atd> en tâche de fond."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-s>"
msgstr "B<-s>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"Process the at/batch queue only once.  This is primarily of use for "
"compatibility with old versions of B<at>; B<atd -s> is equivalent to the old "
"B<atrun> command."
msgstr ""
"Traiter la file d'attente I<at/batch> une seule fois. Cette commande est "
"utile surtout pour une compatibilité avec les anciennes versions de "
"B<at>\\ ; B<atd\\ -s> est équivalent à l'ancienne commande B<atrun>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "WARNING"
msgstr "AVERTISSEMENT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<atd> won't work if its spool directory is mounted via NFS even if "
"I<no_root_squash> is set."
msgstr ""
"B<atd> ne fonctionnera pas si le répertoire de stockage est monté à travers "
"NFS, même si I<no_root_squash> est indiqué."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "FILES"
msgstr "FICHIERS"

#. type: Plain text
#: archlinux
msgid ""
"I</var/spool/atd> The directory for storing jobs; this should be mode 700, "
"owner daemon."
msgstr ""
"I</var/spool/atd>, répertoire pour stocker les travaux\\ ; les droits "
"d'accès devraient être 700, le propriétaire daemon."

#. type: Plain text
#: archlinux
msgid ""
"I</var/spool/atd> The directory for storing output; this should be mode 700, "
"owner daemon."
msgstr ""
"I</var/spool/atd>, répertoire pour stocker le résultat\\ ; les droits "
"d'accès devraient être 700, le propriétaire daemon."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"I</etc/at.allow>, I</etc/at.deny> determine who can use the B<at> system."
msgstr ""
"I</etc/at.allow>, I</etc/at.deny> déterminent qui peut utiliser le mécanisme "
"B<at>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<at>(1), B<at.deny>(5), B<at.allow>(5), B<cron>(8), B<crontab>(1), "
"B<syslog>(3)."
msgstr ""
"B<at>(1), B<at.deny>(5), B<at.allow>(5), B<cron>(8), B<crontab>(1), "
"B<syslog>(3)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "BOGUES"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The functionality of B<atd> should be merged into B<cron>(8)B<.>"
msgstr ""
"Les fonctionnalités de B<atd> devraient être regroupées dans B<cron>(8)."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"Specifies a limiting load factor, over which batch jobs should not be run, "
"instead of the compile-time choice of 1.5.  For an SMP system with I<n> "
"CPUs, you will probably want to set this higher than I<n-1.>"
msgstr ""
"Indiquer le facteur limite de charge, au delà duquel les travaux ne "
"devraient pas être lancés, à la place du choix de B<1,5> précompilé. Pour un "
"système SMP (multiprocesseur) avec I<n> CPU, vous voudrez probablement le "
"mettre plus grand que I<n-1>."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"I</var/spool/cron/atjobs> The directory for storing jobs; this should be "
"mode 700, owner daemon."
msgstr ""
"I</var/spool/cron/atjobs>, répertoire pour stocker les travaux\\ ; les "
"droits d'accès devraient être 700, le propriétaire daemon."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"I</var/spool/cron/atspool> The directory for storing output; this should be "
"mode 700, owner daemon."
msgstr ""
"I</var/spool/cron/atspool>, répertoire pour stocker le résultat\\ ; les "
"droits d'accès devraient être 700, le propriétaire daemon."

#. type: Plain text
#: fedora-39 fedora-rawhide
msgid ""
"B<atd> [B<-l> I<load_avg>] [B<-b> I<batch_interval>] [B<-d>] [B<-f>] [B<-n>] "
"[B<-s>]"
msgstr ""
"B<atd> [B<-l> I<charge_moyenne>] [B<-b> I<intervalle_batch>] [B<-d>] [B<-f>] "
"[B<-n>] [B<-s>]"

#. type: Plain text
#: fedora-39 fedora-rawhide
msgid ""
"Process the at/batch queue only once.  This is primarily of use for "
"compatibility with old versions of B<at>; B<-n> Append the hostname of the "
"system to the subject of the e-mails sent by B<atd>."
msgstr ""
"Traiter la file d'attente I<at/batch> une seule fois. Cette commande est "
"utile surtout pour une compatibilité avec les anciennes versions de "
"B<at>\\ ; B<-n>, ajout du nom d’hôte du système au sujet du courriel envoyé "
"par B<atd>."

#. type: TP
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "B<atd -s>"
msgstr "B<atd -s>"

#. type: Plain text
#: fedora-39 fedora-rawhide
msgid "is equivalent to the old B<atrun> command."
msgstr "est équivalent à l’ancienne commande B<atrun>."

#. type: Plain text
#: fedora-39 fedora-rawhide
msgid ""
"I</var/spool/at> The directory for storing jobs; this should be mode 700, "
"owner root."
msgstr ""
"I</var/spool/at>, répertoire pour stocker les travaux\\ ; les droits d'accès "
"devraient être 700, le propriétaire root."

#. type: Plain text
#: fedora-39 fedora-rawhide
msgid ""
"I</var/spool/at/spool> The directory for storing output; this should be mode "
"700, owner root."
msgstr ""
"I</var/spool/at/spool>, répertoire pour stocker le résultat\\ ; les droits "
"d'accès devraient être 700, le propriétaire root."

#. type: Plain text
#: mageia-cauldron
msgid ""
"I</var/spool/at> The directory for storing jobs; this should be mode 700, "
"owner daemon."
msgstr ""
"I</var/spool/at>, répertoire pour stocker les travaux\\ ; les droits d'accès "
"devraient être 700, le propriétaire daemon."

#. type: Plain text
#: mageia-cauldron
msgid ""
"I</var/spool/at/spool> The directory for storing output; this should be mode "
"700, owner daemon."
msgstr ""
"I</var/spool/at/spool>, répertoire pour stocker le résultat\\ ; les droits "
"d'accès devraient être 700, le propriétaire daemon."

#. type: Plain text
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Specifies a limiting load factor, over which batch jobs should not be run, "
"instead of the compile-time choice of 0.8.  This number is multiplied by the "
"amount of CPUs when comparing to /proc/loadavg, because loadavg is a sum "
"over all processors on Linux."
msgstr ""
"Indiquer le facteur limite de charge, au delà duquel les travaux ne "
"devraient pas être lancés, à la place du choix de B<0,8> précompilé. Le "
"nombre est multiplié par le nombre des processeurs lors de la comparaison "
"avec I</proc/loadavg>, car I<charge_moyenne> (I<loadavg>) est une quantité "
"concernant tous les processeurs sur Linux."

#. type: Plain text
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"I</var/spool/atjobs> The directory for storing jobs; this should be mode "
"700, owner at."
msgstr ""
"I</var/spool/atjobs>, répertoire pour stocker les travaux\\ ; les droits "
"d'accès devraient être 700, le propriétaire at."

#. type: Plain text
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"I</var/spool/atspool> The directory for storing output; this should be mode "
"700, owner at."
msgstr ""
"I</var/spool/atspool>, répertoire pour stocker le résultat\\ ; les droits "
"d'accès devraient être 700, le propriétaire at."
