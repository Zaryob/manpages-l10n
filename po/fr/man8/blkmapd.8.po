# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Valéry Perrin <valery.perrin.debian@free.fr>, 2006.
# Sylvain Cherrier <sylvain.cherrier@free.fr>, 2006, 2007, 2008, 2009.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2007.
# Dominique Simen <dominiquesimen@hotmail.com>, 2009.
# Nicolas Sauzède <nsauzede@free.fr>, 2009.
# Romain Doumenc <rd6137@gmail.com>, 2010, 2011.
# David Prévot <david@tilapin.org>, 2011, 2012, 2014.
# Denis Mugnier <myou72@orange.fr>, 2011.
# Cédric Boutillier <cedric.boutillier@gmail.com>, 2012, 2013.
msgid ""
msgstr ""
"Project-Id-Version: nfs-utils\n"
"POT-Creation-Date: 2023-08-27 16:50+0200\n"
"PO-Revision-Date: 2021-05-11 08:21+0200\n"
"Last-Translator: Jean-Paul Guillonneau <guillonneau.jeanpaul@free.fr>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: vim\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "blkmapd"
msgstr "blkmapd"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "11 August 2011"
msgstr "11 août 2011"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "blkmapd - pNFS block layout mapping daemon"
msgstr "blkmapd - Service de correspondance (« mapping ») de block pNFS"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<blkmapd [-h] [-d] [-f]>"
msgstr "B<blkmapd> [B<-h>] [B<-d>] [B<-f>]"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<blkmapd> daemon performs device discovery and mapping for the parallel "
"NFS (pNFS) block layout client [RFC5663]."
msgstr ""
"Le service B<blkmapd> permet la découverte de périphérique et la "
"correspondance pour les clients NFS parallèles (pNFS) [RFC 5663]."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The pNFS block layout protocol builds a complex storage hierarchy from a set "
"of I<simple volumes.> These simple volumes are addressed by content, using a "
"signature on the volume to uniquely name each one.  The daemon locates a "
"volume by examining each block device in the system for the given signature."
msgstr ""
"Le protocole pNFS construit une hiérarchie de stockage complexe à partir "
"d'un ensemble de I<volumes simples>. Ces volumes simples sont adressés par "
"le contenu, en utilisant une signature par volume. Le démon localise un "
"volume en examinant la signature donnée pour chaque périphérique bloc dans "
"le système."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The topology typically consists of a hierarchy of volumes built by striping, "
"slicing, and concatenating the simple volumes.  The B<blkmapd> daemon uses "
"the device-mapper driver to construct logical devices that reflect the "
"server topology, and passes these devices to the kernel for use by the pNFS "
"block layout client."
msgstr ""
"La topologie se compose généralement d'une hiérarchie de volumes construits "
"par entrelacement, tranchage et concaténation de volumes simples. Le service "
"B<blkmapd> utilise le pilote device-mapper pour construire des périphériques "
"logiques qui reflètent la topologie du serveur et passe ces dispositifs au "
"noyau pour l'utilisation par le client pNFS."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr "OPTIONS"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-h>"
msgstr "B<-h>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Display usage message."
msgstr "Afficher un message d'utilisation."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-d>"
msgstr "B<-d>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Performs device discovery only then exits."
msgstr "Effectuer seulement la découverte de périphérique puis quitter."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-f>"
msgstr "B<-f>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Runs B<blkmapd> in the foreground and sends output to stderr (as opposed to "
"syslogd)"
msgstr ""
"Lancer B<blkmapd> en tâche de premier plan et rediriger sa sortie standard "
"vers la sortie d'erreur (au lieu de syslogd)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "CONFIGURATION FILE"
msgstr "FICHIER DE CONFIGURATION"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The B<blkmapd> daemon recognizes the following value from the B<[general]> "
"section of the I</etc/nfs.conf> configuration file:"
msgstr ""
"Le service B<blkmapd> accepte la valeur suivante à partir de la section "
"B<[general]> du fichier de configuration I</etc/nfs.conf> :"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<pipefs-directory>"
msgstr "B<pipefs-directory>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Tells B<blkmapd> where to look for the rpc_pipefs filesystem.  The default "
"value is I</var/lib/nfs/rpc_pipefs>."
msgstr ""
"Indiquer à B<blkmapd> où chercher le système de fichiers rpc_pipefs. Par "
"défaut, il s'agit de I</var/lib/nfs/rpc_pipefs>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "B<nfs>(5), B<dmsetup>(8), B<nfs.conf>(5)"
msgstr "B<nfs>(5), B<dmsetup>(8), B<nfs.conf>(5)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "RFC 5661 for the NFS version 4.1 specification."
msgstr "RFC 5661 concernant la spécification de NFS version 4.1."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "RFC 5663 for the pNFS block layout specification."
msgstr "RFC 5663 pour la spécification de la disposition des blocs pNFS."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "AUTHORS"
msgstr "AUTEURS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Haiying Tang E<lt>Tang_Haiying@emc.comE<gt>"
msgstr "Haiying Tang E<lt>Tang_Haiying@emc.comE<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Jim Rees E<lt>rees@umich.eduE<gt>"
msgstr "Jim Rees E<lt>rees@umich.eduE<gt>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<nfs>(5), B<dmsetup>(8)"
msgstr "B<nfs>(5), B<dmsetup>(8)"
