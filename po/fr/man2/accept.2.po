# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Christophe Blaess <https://www.blaess.fr/christophe/>, 1996-2003.
# Stéphan Rafin <stephan.rafin@laposte.net>, 2002.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999,2002.
# François Micaux, 2002.
# Alain Portal <aportal@univ-montp2.fr>, 2003-2008.
# Jean-Philippe Guérard <fevrier@tigreraye.org>, 2005-2006.
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006-2007.
# Julien Cristau <jcristau@debian.org>, 2006-2007.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006-2008.
# Nicolas François <nicolas.francois@centraliens.net>, 2006-2008.
# Florentin Duneau <fduneau@gmail.com>, 2006-2010.
# Simon Paillard <simon.paillard@resel.enst-bretagne.fr>, 2006, 2013.
# Denis Barbier <barbier@debian.org>, 2006,2010.
# David Prévot <david@tilapin.org>, 2010, 2013, 2014.
# Cédric Boutillier <cedric.boutillier@gmail.com>, 2011, 2012, 2013.
# Frédéric Hantrais <fhantrais@gmail.com>, 2013, 2014.
# Jean-Philippe MENGUAL <jpmengual@debian.org>, 2020-2023.
msgid ""
msgstr ""
"Project-Id-Version: perkamon\n"
"POT-Creation-Date: 2023-08-27 16:48+0200\n"
"PO-Revision-Date: 2023-05-07 01:53+0200\n"
"Last-Translator: Jean-Philippe MENGUAL <jpmengual@debian.org>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 3.1.1\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "accept"
msgstr "accept"

#. type: TH
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid "2023-04-05"
msgstr "5 avril 2023"

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Pages du manuel de Linux 6.05.01"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "accept, accept4 - accept a connection on a socket"
msgstr "accept, accept4 - Accepter une connexion sur une socket"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTHÈQUE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Bibliothèque C standard (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>sys/socket.hE<gt>>\n"
msgstr "B<#include E<lt>sys/socket.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"B<int accept(int >I<sockfd>B<, struct sockaddr *_Nullable restrict >I<addr>B<,>\n"
"B<           socklen_t *_Nullable restrict >I<addrlen>B<);>\n"
msgstr ""
"B<int accept(int >I<sockfd>B<, struct sockaddr *_Nullable restrict >I<addr>B<,>\n"
"B<           socklen_t *_Nullable restrict >I<addrlen>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<#define _GNU_SOURCE>             /* See feature_test_macros(7) */\n"
"B<#include E<lt>sys/socket.hE<gt>>\n"
msgstr ""
"B<#define _GNU_SOURCE>             /* Consultez feature_test_macros(7) */\n"
"B<#include E<lt>sys/socket.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"B<int accept4(int >I<sockfd>B<, struct sockaddr *_Nullable restrict >I<addr>B<,>\n"
"B<           socklen_t *_Nullable restrict >I<addrlen>B<, int >I<flags>B<);>\n"
msgstr ""
"B<int accept4(int >I<sockfd>B<, struct sockaddr *_Nullable restrict >I<addr>B<,>\n"
"B<           socklen_t *_Nullable restrict >I<addrlen>B<, int >I<flags>B<);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<accept>()  system call is used with connection-based socket types "
"(B<SOCK_STREAM>, B<SOCK_SEQPACKET>).  It extracts the first connection "
"request on the queue of pending connections for the listening socket, "
"I<sockfd>, creates a new connected socket, and returns a new file descriptor "
"referring to that socket.  The newly created socket is not in the listening "
"state.  The original socket I<sockfd> is unaffected by this call."
msgstr ""
"L'appel système B<accept>() est employé avec les sockets utilisant un "
"protocole en mode connecté (B<SOCK_STREAM>, B<SOCK_SEQPACKET>). Il extrait "
"la première connexion de la file des connexions en attente de la socket "
"I<sockfd> à l'écoute, crée une nouvelle socket et alloue pour cette socket "
"un nouveau descripteur de fichier qu'il renvoie. La nouvelle socket n'est "
"pas en état d'écoute. La socket originale I<sockfd> n'est pas modifiée par "
"l'appel système."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The argument I<sockfd> is a socket that has been created with B<socket>(2), "
"bound to a local address with B<bind>(2), and is listening for connections "
"after a B<listen>(2)."
msgstr ""
"L'argument I<sockfd> est une socket qui a été créée avec la fonction "
"B<socket>(2), attachée à une adresse avec B<bind>(2), et attend des "
"connexions après un appel B<listen>(2)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The argument I<addr> is a pointer to a I<sockaddr> structure.  This "
"structure is filled in with the address of the peer socket, as known to the "
"communications layer.  The exact format of the address returned I<addr> is "
"determined by the socket's address family (see B<socket>(2)  and the "
"respective protocol man pages).  When I<addr> is NULL, nothing is filled in; "
"in this case, I<addrlen> is not used, and should also be NULL."
msgstr ""
"L'argument I<addr> est un pointeur sur une structure I<sockaddr>. La "
"structure sera remplie avec l'adresse du correspondant se connectant, telle "
"qu'elle est connue par la couche de communication. Le format exact du "
"paramètre I<addr> dépend du domaine dans lequel la communication s'établit "
"(consultez B<socket>(2) et la page de manuel correspondant au protocole). "
"Quand I<addr> vaut NULL, rien n'est rempli ; dans ce cas, I<addrlen> n'est "
"pas utilisé et doit aussi valoir NULL."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The I<addrlen> argument is a value-result argument: the caller must "
"initialize it to contain the size (in bytes) of the structure pointed to by "
"I<addr>; on return it will contain the actual size of the peer address."
msgstr ""
"I<addrlen> est un paramètre-résultat\\ : l'appelant doit l'initialiser de "
"telle sorte qu'il contienne la taille (en octets) de la structure pointée "
"par I<addr>, et est renseigné au retour par la longueur réelle (en octets) "
"de l'adresse remplie."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The returned address is truncated if the buffer provided is too small; in "
"this case, I<addrlen> will return a value greater than was supplied to the "
"call."
msgstr ""
"L'adresse renvoyée est tronquée si le tampon fourni est trop petit ; dans ce "
"cas, I<addrlen> renverra une valeur supérieure à celle fournie lors de "
"l'appel."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If no pending connections are present on the queue, and the socket is not "
"marked as nonblocking, B<accept>()  blocks the caller until a connection is "
"present.  If the socket is marked nonblocking and no pending connections are "
"present on the queue, B<accept>()  fails with the error B<EAGAIN> or "
"B<EWOULDBLOCK>."
msgstr ""
"S'il n'y a pas de connexion en attente dans la file, et si la socket n'est "
"pas marquée comme non bloquante, B<accept>() se met en attente d'une "
"connexion. Si la socket est non bloquante, et qu'aucune connexion n'est "
"présente dans la file, B<accept>() retourne une erreur B<EAGAIN> ou "
"B<EWOULDBLOCK>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"In order to be notified of incoming connections on a socket, you can use "
"B<select>(2), B<poll>(2), or B<epoll>(7).  A readable event will be "
"delivered when a new connection is attempted and you may then call "
"B<accept>()  to get a socket for that connection.  Alternatively, you can "
"set the socket to deliver B<SIGIO> when activity occurs on a socket; see "
"B<socket>(7)  for details."
msgstr ""
"Pour être prévenu de l'arrivée d'une connexion sur une socket, on peut "
"utiliser B<select>(2), B<poll>(2) ou B<epoll>(7). Un événement «\\ lecture\\ "
"» sera délivré lorsqu'une tentative de connexion aura lieu, et on pourra "
"alors appeler B<accept>() pour obtenir une socket pour cette connexion. "
"Autrement, on peut configurer la socket pour qu'elle envoie un signal "
"B<SIGIO> lorsqu'une activité la concernant se produit, consultez "
"B<socket>(7) pour plus de détails."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If I<flags> is 0, then B<accept4>()  is the same as B<accept>().  The "
"following values can be bitwise ORed in I<flags> to obtain different "
"behavior:"
msgstr ""
"Si I<flags> vaut B<0> alors B<accept4>() est identique à B<accept>(). Les "
"valeurs suivantes peuvent être combinées dans I<flags> par un OU binaire "
"pour obtenir un comportement différent :"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<SOCK_NONBLOCK>"
msgstr "B<SOCK_NONBLOCK>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Set the B<O_NONBLOCK> file status flag on the open file description (see "
"B<open>(2))  referred to by the new file descriptor.  Using this flag saves "
"extra calls to B<fcntl>(2)  to achieve the same result."
msgstr ""
"Placer l'attribut d'état de fichier B<O_NONBLOCK> sur la description du "
"fichier ouvert référencée par le nouveau descripteur de fichier (consulter "
"B<open>(2)). Utiliser cet attribut économise des appels supplémentaires à "
"B<fcntl>(2) pour obtenir le même résultat."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<SOCK_CLOEXEC>"
msgstr "B<SOCK_CLOEXEC>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Set the close-on-exec (B<FD_CLOEXEC>)  flag on the new file descriptor.  See "
"the description of the B<O_CLOEXEC> flag in B<open>(2)  for reasons why this "
"may be useful."
msgstr ""
"Placer l'attribut « close-on-exec » (B<FD_CLOEXEC>) sur le nouveau "
"descripteur de fichier. Consultez la description de l'attribut B<O_CLOEXEC> "
"dans B<open>(2) pour savoir pourquoi cela peut être utile."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALEUR RENVOYÉE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"On success, these system calls return a file descriptor for the accepted "
"socket (a nonnegative integer).  On error, -1 is returned, I<errno> is set "
"to indicate the error, and I<addrlen> is left unchanged."
msgstr ""
"S'ils réussissent, ces appels système renvoient un descripteur de fichier "
"pour la socket acceptée (un entier positif ou nul). En cas d'erreur, ils "
"renvoient B<-1>, I<errno> est positionné pour indiquer l'erreur et "
"I<addrlen> est laissé inchangé."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Error handling"
msgstr "Traitement des erreurs"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Linux B<accept>()  (and B<accept4>())  passes already-pending network errors "
"on the new socket as an error code from B<accept>().  This behavior differs "
"from other BSD socket implementations.  For reliable operation the "
"application should detect the network errors defined for the protocol after "
"B<accept>()  and treat them like B<EAGAIN> by retrying.  In the case of TCP/"
"IP, these are B<ENETDOWN>, B<EPROTO>, B<ENOPROTOOPT>, B<EHOSTDOWN>, "
"B<ENONET>, B<EHOSTUNREACH>, B<EOPNOTSUPP>, and B<ENETUNREACH>."
msgstr ""
"Sous Linux, B<accept>() (et B<accept4>()) renvoie les erreurs réseau déjà en "
"attente sur la socket comme une erreur de l'appel système. Ce comportement "
"diffère d'autres implémentations des sockets BSD. Pour un comportement "
"fiable, une application doit détecter les erreurs réseau définies par le "
"protocole après le B<accept>() et les traiter comme des erreurs B<EAGAIN>, "
"en réitérant le mécanisme. Dans le cas de TCP/IP, ces erreurs sont "
"B<ENETDOWN>, B<EPROTO>, B<ENOPROTOOPT>, B<EHOSTDOWN>, B<ENONET>, "
"B<EHOSTUNREACH>, B<EOPNOTSUPP>, et B<ENETUNREACH>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ERREURS"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EAGAIN> or B<EWOULDBLOCK>"
msgstr "B<EAGAIN> ou B<EWOULDBLOCK>"

#.  Actually EAGAIN on Linux
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The socket is marked nonblocking and no connections are present to be "
"accepted.  POSIX.1-2001 and POSIX.1-2008 allow either error to be returned "
"for this case, and do not require these constants to have the same value, so "
"a portable application should check for both possibilities."
msgstr ""
"La socket est marquée comme étant non bloquante et aucune connexion n'est "
"présente pour être acceptée. POSIX.1-2001 et POSIX.1-2008 permettent de "
"renvoyer l'une ou l'autre des erreurs dans ce cas et n'exige pas que ces "
"constantes aient la même valeur. Une application portable devrait donc "
"tester les deux possibilités."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EBADF>"
msgstr "B<EBADF>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<sockfd> is not an open file descriptor."
msgstr "I<sockfd> n'est pas un descripteur de fichier valable."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ECONNABORTED>"
msgstr "B<ECONNABORTED>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "A connection has been aborted."
msgstr "Une connexion a été abandonnée."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EFAULT>"
msgstr "B<EFAULT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The I<addr> argument is not in a writable part of the user address space."
msgstr "I<addr> n'est pas dans l'espace d'adressage accessible en écriture."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EINTR>"
msgstr "B<EINTR>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The system call was interrupted by a signal that was caught before a valid "
"connection arrived; see B<signal>(7)."
msgstr ""
"L'appel système a été interrompu par l'arrivée d'un signal avant qu'une "
"connexion valable ne survienne ; consultez B<signal>(7)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Socket is not listening for connections, or I<addrlen> is invalid (e.g., is "
"negative)."
msgstr ""
"La socket n'est pas en attente de connexions, ou I<addrlen> est non "
"autorisée (par exemple négatif)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "(B<accept4>())  invalid value in I<flags>."
msgstr "(B<accept4>()) I<flags> contient une valeur incorrecte."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EMFILE>"
msgstr "B<EMFILE>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The per-process limit on the number of open file descriptors has been "
"reached."
msgstr ""
"La limite du nombre de descripteurs de fichiers par processus a été atteinte."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENFILE>"
msgstr "B<ENFILE>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The system-wide limit on the total number of open files has been reached."
msgstr ""
"La limite du nombre total de fichiers ouverts pour le système entier a été "
"atteinte."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOBUFS>, B<ENOMEM>"
msgstr "B<ENOBUFS>, B<ENOMEM>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Not enough free memory.  This often means that the memory allocation is "
"limited by the socket buffer limits, not by the system memory."
msgstr ""
"Pas assez de mémoire disponible. En général, cette erreur est due à la "
"taille limitée du tampon des sockets, et non à la mémoire système proprement "
"dite."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOTSOCK>"
msgstr "B<ENOTSOCK>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The file descriptor I<sockfd> does not refer to a socket."
msgstr "Le descripteur de fichier I<sockfd> ne fait pas référence à un socket."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EOPNOTSUPP>"
msgstr "B<EOPNOTSUPP>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The referenced socket is not of type B<SOCK_STREAM>."
msgstr "La socket utilisée n'est pas de type B<SOCK_STREAM>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EPERM>"
msgstr "B<EPERM>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Firewall rules forbid connection."
msgstr "Les règles du pare-feu interdisent la connexion."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EPROTO>"
msgstr "B<EPROTO>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Protocol error."
msgstr "Erreur de protocole."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"In addition, network errors for the new socket and as defined for the "
"protocol may be returned.  Various Linux kernels can return other errors "
"such as B<ENOSR>, B<ESOCKTNOSUPPORT>, B<EPROTONOSUPPORT>, B<ETIMEDOUT>.  The "
"value B<ERESTARTSYS> may be seen during a trace."
msgstr ""
"De plus il peut se produire des erreurs réseau dépendant du protocole de la "
"socket. Certains noyaux Linux peuvent renvoyer d'autres erreurs comme "
"B<ENOSR>, B<ESOCKTNOSUPPORT>, B<EPROTONOSUPPORT>, B<ETIMEDOUT>. L'erreur "
"B<ERESTARTSYS> peut être rencontrée durant un suivi dans un débogueur."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "VERSIONS"

#.  Some testing seems to show that Tru64 5.1 and HP-UX 11 also
#.  do not inherit file status flags -- MTK Jun 05
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"On Linux, the new socket returned by B<accept>()  does I<not> inherit file "
"status flags such as B<O_NONBLOCK> and B<O_ASYNC> from the listening "
"socket.  This behavior differs from the canonical BSD sockets "
"implementation.  Portable programs should not rely on inheritance or "
"noninheritance of file status flags and always explicitly set all required "
"flags on the socket returned from B<accept>()."
msgstr ""
"Avec la version Linux de B<accept>(), la nouvelle socket n'hérite B<pas> des "
"attributs comme B<O_NONBLOCK> et B<O_ASYNC> de la socket en écoute. Ce "
"comportement est différent de l'implémentation BSD de référence. Les "
"programmes portables ne doivent pas s'appuyer sur cette particularité, et "
"doivent reconfigurer les attributs sur la socket renvoyée par B<accept>()."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDS"

#. type: TP
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "B<accept>()"
msgstr "B<accept>()"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr "POSIX.1-2008."

#. type: TP
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "B<accept4>()"
msgstr "B<accept4>()"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "Linux."
msgstr "Linux."

#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "HISTORIQUE"

#.  The BSD man page documents five possible error returns
#.  (EBADF, ENOTSOCK, EOPNOTSUPP, EWOULDBLOCK, EFAULT).
#.  POSIX.1-2001 documents errors
#.  EAGAIN, EBADF, ECONNABORTED, EINTR, EINVAL, EMFILE,
#.  ENFILE, ENOBUFS, ENOMEM, ENOTSOCK, EOPNOTSUPP, EPROTO, EWOULDBLOCK.
#.  In addition, SUSv2 documents EFAULT and ENOSR.
#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "POSIX.1-2001, SVr4, 4.4BSD (B<accept>()  first appeared in 4.2BSD)."
msgstr "POSIX.1-2001, SVr4, 4.4BSD (B<accept>() est apparu dans 4.2BSD)."

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "Linux 2.6.28, glibc 2.10."
msgstr "Linux 2.6.28, glibc 2.10."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "NOTES"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"There may not always be a connection waiting after a B<SIGIO> is delivered "
"or B<select>(2), B<poll>(2), or B<epoll>(7)  return a readability event "
"because the connection might have been removed by an asynchronous network "
"error or another thread before B<accept>()  is called.  If this happens, "
"then the call will block waiting for the next connection to arrive.  To "
"ensure that B<accept>()  never blocks, the passed socket I<sockfd> needs to "
"have the B<O_NONBLOCK> flag set (see B<socket>(7))."
msgstr ""
"Il n'y a pas nécessairement de connexion en attente après la réception de "
"B<SIGIO> ou après que B<select>(2), B<poll>(2) ou B<epoll>(7) indiquent "
"quelque chose à lire. En effet la connexion peut avoir été annulée à cause "
"d'une erreur de réseau asynchrone ou par un autre thread avant que "
"B<accept>() ne soit appelé. Si cela se produit, l'appel bloquera en "
"attendant une autre connexion. Pour s'assurer que B<accept>() ne bloquera "
"jamais, la socket I<sockfd> transmise doit avoir l'attribut B<O_NONBLOCK> "
"(consultez B<socket>(7))."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"For certain protocols which require an explicit confirmation, such as "
"DECnet, B<accept>()  can be thought of as merely dequeuing the next "
"connection request and not implying confirmation.  Confirmation can be "
"implied by a normal read or write on the new file descriptor, and rejection "
"can be implied by closing the new socket.  Currently, only DECnet has these "
"semantics on Linux."
msgstr ""
"Pour certains protocoles nécessitant une confirmation explicite, comme "
"DECNet, B<accept>() peut être considéré comme extrayant simplement la "
"connexion suivante de la file, sans demander de confirmation. On peut "
"effectuer la confirmation par une simple lecture ou écriture sur le nouveau "
"descripteur, et le rejet en fermant la nouvelle socket. Pour le moment, seul "
"DECNet se comporte ainsi sous Linux."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "The socklen_t type"
msgstr "Le type socklen_t"

#.  such as Linux libc4 and libc5, SunOS 4, SGI
#.  SunOS 5 has 'size_t *'
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"In the original BSD sockets implementation (and on other older systems)  the "
"third argument of B<accept>()  was declared as an I<int\\ *>.  A POSIX.1g "
"draft standard wanted to change it into a I<size_t\\ *>C; later POSIX "
"standards and glibc 2.x have I<socklen_t\\ * >."
msgstr ""
"Dans l'implémentation des sockets de BSD originale (et sur d'autres anciens "
"systèmes), le troisième paramètre de B<accept>() était déclaré en tant que "
"I<int\\ *>. Un brouillon du standard POSIX.1g voulait le changer pour "
"I<size_t\\ *>C ; les derniers standards POSIX et glibc\\ 2.x mentionnent "
"I<socklen_t\\ * >."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "EXEMPLES"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "See B<bind>(2)."
msgstr "Consultez B<bind>(2)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<bind>(2), B<connect>(2), B<listen>(2), B<select>(2), B<socket>(2), "
"B<socket>(7)"
msgstr ""
"B<bind>(2), B<connect>(2), B<listen>(2), B<select>(2), B<socket>(2), "
"B<socket>(7)"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "2022-12-04"
msgstr "4 décembre 2022"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Pages du manuel de Linux 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron
msgid ""
"The B<accept4>()  system call is available starting with Linux 2.6.28; "
"support in glibc is available starting with glibc 2.10."
msgstr ""
"L'appel système B<accept4>() est disponible depuis Linux 2.6.28 ; la prise "
"en charge dans la glibc est disponible depuis la glibc\\ 2.10."

#.  The BSD man page documents five possible error returns
#.  (EBADF, ENOTSOCK, EOPNOTSUPP, EWOULDBLOCK, EFAULT).
#.  POSIX.1-2001 documents errors
#.  EAGAIN, EBADF, ECONNABORTED, EINTR, EINVAL, EMFILE,
#.  ENFILE, ENOBUFS, ENOMEM, ENOTSOCK, EOPNOTSUPP, EPROTO, EWOULDBLOCK.
#.  In addition, SUSv2 documents EFAULT and ENOSR.
#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid ""
"B<accept>(): POSIX.1-2001, POSIX.1-2008, SVr4, 4.4BSD (B<accept>()  first "
"appeared in 4.2BSD)."
msgstr ""
"B<accept>() : POSIX.1-2001, POSIX.1-2008, SVr4, 4.4BSD (B<accept>() est "
"apparu dans BSD\\ 4.2)."

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid "B<accept4>()  is a nonstandard Linux extension."
msgstr "B<accept4>() est une extension non standard de Linux."

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.05"
msgstr "Pages du manuel de Linux 6.05"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "ACCEPT"
msgstr "ACCEPT"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2016-10-08"
msgstr "8 octobre 2016"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Manuel du programmeur Linux"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"B<#include E<lt>sys/types.hE<gt>>          /* See NOTES */\n"
"B<#include E<lt>sys/socket.hE<gt>>\n"
msgstr ""
"B<#include E<lt>sys/types.hE<gt>>          /* Consultez NOTES */\n"
"B<#include E<lt>sys/socket.hE<gt>>\n"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid "B<int accept(int >I<sockfd>B<, struct sockaddr *>I<addr>B<, socklen_t *>I<addrlen>B<);>\n"
msgstr "B<int accept(int >I<sockfd>B<, struct sockaddr *>I<addr>B<, socklen_t *>I<addrlen>B<);>\n"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"B<int accept4(int >I<sockfd>B<, struct sockaddr *>I<addr>B<,>\n"
"B<            socklen_t *>I<addrlen>B<, int >I<flags>B<);>\n"
msgstr ""
"B<int accept4(int >I<sockfd>B<, struct sockaddr *>I<addr>B<,>\n"
"B<            socklen_t *>I<addrlen>B<, int >I<flags>B<);>\n"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Set the B<O_NONBLOCK> file status flag on the new open file description.  "
"Using this flag saves extra calls to B<fcntl>(2)  to achieve the same result."
msgstr ""
"Placer l'attribut d'état de fichier B<O_NONBLOCK> sur la description du "
"fichier nouvellement ouvert. Utiliser cet attribut économise des appels "
"supplémentaires à B<fcntl>(2) pour obtenir le même résultat."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"On success, these system calls return a nonnegative integer that is a file "
"descriptor for the accepted socket.  On error, -1 is returned, and I<errno> "
"is set appropriately."
msgstr ""
"S'ils réussissent, ces appels système renvoient un entier positif ou nul, "
"qui est un descripteur pour la socket acceptée. En cas d'erreur, ils "
"renvoient B<-1> et I<errno> est positionné comme il faut."

#. type: Plain text
#: opensuse-leap-15-6
msgid "In addition, Linux B<accept>()  may fail if:"
msgstr "De plus, la version Linux de B<accept>() peut échouer si\\ :"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"The B<accept4>()  system call is available starting with Linux 2.6.28; "
"support in glibc is available starting with version 2.10."
msgstr ""
"L'appel système B<accept4>() est disponible depuis Linux 2.6.28 ; la prise "
"en charge dans la glibc est disponible depuis la version\\ 2.10."

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "CONFORMING TO"
msgstr "CONFORMITÉ"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"POSIX.1-2001 does not require the inclusion of I<E<lt>sys/types.hE<gt>>, and "
"this header file is not required on Linux.  However, some historical (BSD) "
"implementations required this header file, and portable applications are "
"probably wise to include it."
msgstr ""
"POSIX.1-2001 ne requiert pas l'inclusion de I<E<lt>sys/types.hE<gt>>, et cet "
"en\\-tête n'est pas nécessaire sous Linux. Cependant, il doit être inclus "
"sous certaines implémentations historiques (BSD), et les applications "
"portables devraient probablement l'utiliser."

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "EXAMPLE"
msgstr "EXEMPLE"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "COLOPHON"
msgstr "COLOPHON"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Cette page fait partie de la publication 4.16 du projet I<man-pages> Linux. "
"Une description du projet et des instructions pour signaler des anomalies et "
"la dernière version de cette page peuvent être trouvées à l'adresse \\"
"%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30 mars 2023"

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Pages du manuel de Linux 6.04"
