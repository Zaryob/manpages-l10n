# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Christophe Blaess <https://www.blaess.fr/christophe/>, 1996-2003.
# Stéphan Rafin <stephan.rafin@laposte.net>, 2002.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999, 2002.
# François Micaux, 2002.
# Alain Portal <aportal@univ-montp2.fr>, 2003-2008.
# Jean-Philippe Guérard <fevrier@tigreraye.org>, 2005-2006.
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006-2007.
# Julien Cristau <jcristau@debian.org>, 2006-2007.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006-2008.
# Nicolas François <nicolas.francois@centraliens.net>, 2006-2008.
# Florentin Duneau <fduneau@gmail.com>, 2006-2010.
# Simon Paillard <simon.paillard@resel.enst-bretagne.fr>, 2006.
# Denis Barbier <barbier@debian.org>, 2006, 2010.
# David Prévot <david@tilapin.org>, 2010-2014.
# Frédéric Hantrais <fhantrais@gmail.com>, 2013, 2014.
# Jean-Pierre Giraud <jean-pierregiraud@neuf.fr>, 2022-2023.
msgid ""
msgstr ""
"Project-Id-Version: perkamon\n"
"POT-Creation-Date: 2023-08-27 17:31+0200\n"
"PO-Revision-Date: 2023-02-22 14:24+0100\n"
"Last-Translator: Jean-Pierre Giraud <jean-pierregiraud@neuf.fr>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 3.1.1\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "unlink"
msgstr "unlinkat"

#. type: TH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30 mars 2023"

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Pages du manuel de Linux 6.05.01"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "unlink, unlinkat - delete a name and possibly the file it refers to"
msgstr ""
"unlink, unlinkat - Détruire un nom et éventuellement le fichier associé"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTHÈQUE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Bibliothèque C standard (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>unistd.hE<gt>>\n"
msgstr "B<#include E<lt>unistd.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<int unlink(const char *>I<pathname>B<);>\n"
msgstr "B<int unlink(const char *>I<pathname>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"B<#include E<lt>fcntl.hE<gt>           >/* Definition of B<AT_*> constants */\n"
"B<#include E<lt>unistd.hE<gt>>\n"
msgstr ""
"B<#include E<lt>fcntl.hE<gt>           >/* Définition des constantes B<AT_*> */\n"
"B<#include E<lt>unistd.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<int unlinkat(int >I<dirfd>B<, const char *>I<pathname>B<, int >I<flags>B<);>\n"
msgstr "B<int unlinkat(int >I<dirfd>B<, const char *>I<pathname>B<, int >I<flags>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Exigences de macros de test de fonctionnalités pour la glibc (consulter "
"B<feature_test_macros>(7)) :"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<unlinkat>():"
msgstr "B<unlinkat>():"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"    Since glibc 2.10:\n"
"        _POSIX_C_SOURCE E<gt>= 200809L\n"
"    Before glibc 2.10:\n"
"        _ATFILE_SOURCE\n"
msgstr ""
"    Depuis la glibc 2.10 :\n"
"        _POSIX_C_SOURCE E<gt>= 200809L\n"
"    Avant la glibc 2.10 :\n"
"        _ATFILE_SOURCE\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<unlink>()  deletes a name from the filesystem.  If that name was the last "
"link to a file and no processes have the file open, the file is deleted and "
"the space it was using is made available for reuse."
msgstr ""
"B<unlink>() détruit un nom dans le système de fichiers. Si ce nom était le "
"dernier lien sur un fichier, et si aucun processus n'a ouvert ce fichier, ce "
"dernier est effacé, et l'espace qu'il utilisait est rendu disponible."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If the name was the last link to a file but any processes still have the "
"file open, the file will remain in existence until the last file descriptor "
"referring to it is closed."
msgstr ""
"Si le nom était le dernier lien sur un fichier, mais qu'un processus "
"conserve encore le fichier ouvert, celui-ci continue d'exister jusqu'à ce "
"que le dernier descripteur le référençant soit fermé."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "If the name referred to a symbolic link, the link is removed."
msgstr "Si le nom correspond à un lien symbolique, le lien est supprimé."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If the name referred to a socket, FIFO, or device, the name for it is "
"removed but processes which have the object open may continue to use it."
msgstr ""
"Si le nom correspond à une socket, une FIFO, ou un périphérique, le nom est "
"supprimé mais les processus qui ont ouvert l'objet peuvent continuer à "
"l'utiliser."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "unlinkat()"
msgstr "unlinkat()"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<unlinkat>()  system call operates in exactly the same way as either "
"B<unlink>()  or B<rmdir>(2)  (depending on whether or not I<flags> includes "
"the B<AT_REMOVEDIR> flag)  except for the differences described here."
msgstr ""
"L'appel système B<unlinkat>() fonctionne exactement comme B<unlink>(2) ou "
"B<rmdir>(2) (en fonction de la présence ou non du drapeau B<AT_REMOVEDIR> "
"dans I<flags>), les seules différences étant décrites ici."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If the pathname given in I<pathname> is relative, then it is interpreted "
"relative to the directory referred to by the file descriptor I<dirfd> "
"(rather than relative to the current working directory of the calling "
"process, as is done by B<unlink>()  and B<rmdir>(2)  for a relative "
"pathname)."
msgstr ""
"Si le chemin donné dans I<pathname> est relatif, il est interprété par "
"rapport au répertoire référencé par le descripteur de fichier I<dirfd> "
"(plutôt que par rapport au répertoire de travail, comme c'est le cas pour "
"B<unlink>() et B<rmdir>(2))."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If the pathname given in I<pathname> is relative and I<dirfd> is the special "
"value B<AT_FDCWD>, then I<pathname> is interpreted relative to the current "
"working directory of the calling process (like B<unlink>()  and B<rmdir>(2))."
msgstr ""
"Si le chemin donné dans I<pathname> est relatif et si I<dirfd> a la valeur "
"spéciale B<AT_FDCWD>, alors I<pathname> est interprété par rapport au "
"répertoire de travail du processus appelant (comme pour B<unlink>() et "
"B<rmdir>(2))."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If the pathname given in I<pathname> is absolute, then I<dirfd> is ignored."
msgstr "Si le chemin donné dans I<pathname> est absolu, I<dirfd> est ignoré."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"I<flags> is a bit mask that can either be specified as 0, or by ORing "
"together flag values that control the operation of B<unlinkat>().  "
"Currently, only one such flag is defined:"
msgstr ""
"I<flags> est un masque de bits qui peut être 0 ou construit par un OU "
"binaire de valeur de drapeaux qui contrôlent le fonctionnement de "
"B<unlinkat>(). Actuellement, un seul drapeau est défini\\ :"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<AT_REMOVEDIR>"
msgstr "B<AT_REMOVEDIR>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"By default, B<unlinkat>()  performs the equivalent of B<unlink>()  on "
"I<pathname>.  If the B<AT_REMOVEDIR> flag is specified, then performs the "
"equivalent of B<rmdir>(2)  on I<pathname>."
msgstr ""
"Par défaut, B<unlinkat>() a un effet équivalent à celui de B<unlink>() sur "
"I<pathname>. Si le drapeau B<AT_REMOVEDIR> est indiqué, B<unlinkat>() "
"fonctionne comme B<rmdir>(2) sur I<pathname>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "See B<openat>(2)  for an explanation of the need for B<unlinkat>()."
msgstr ""
"Consultez B<openat>(2) pour une explication de la nécessité de B<unlinkat>()."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALEUR RENVOYÉE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"On success, zero is returned.  On error, -1 is returned, and I<errno> is set "
"to indicate the error."
msgstr ""
"En cas de succès, zéro est renvoyé. En cas d'erreur, B<-1> est renvoyé et "
"I<errno> est définie pour préciser l'erreur."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ERREURS"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EACCES>"
msgstr "B<EACCES>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Write access to the directory containing I<pathname> is not allowed for the "
"process's effective UID, or one of the directories in I<pathname> did not "
"allow search permission.  (See also B<path_resolution>(7).)"
msgstr ""
"L'accès en écriture au répertoire contenant I<pathname> n'est pas autorisé "
"pour l'UID effectif du processus, ou bien l'un des répertoires de "
"I<pathname> n'autorise pas le parcours. (Consultez aussi "
"B<path_resolution>(7).)"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EBUSY>"
msgstr "B<EBUSY>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The file I<pathname> cannot be unlinked because it is being used by the "
"system or another process; for example, it is a mount point or the NFS "
"client software created it to represent an active but otherwise nameless "
"inode (\"NFS silly renamed\")."
msgstr ""
"Le fichier I<pathname> ne peut pas être détruit avec unlink car il est "
"utilisé par le système ou par un autre processus\\ ; par exemple, il s'agit "
"d'un point de montage, ou le logiciel client NFS l'a créé pour représenter "
"un inœud actif, mais sinon sans nom («\\ I<NFS silly renamed>\\ »)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EFAULT>"
msgstr "B<EFAULT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<pathname> points outside your accessible address space."
msgstr "I<nom_chemin> pointe en dehors de l'espace d'adressage accessible."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EIO>"
msgstr "B<EIO>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "An I/O error occurred."
msgstr "Une erreur d'entrée-sortie s'est produite."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EISDIR>"
msgstr "B<EISDIR>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"I<pathname> refers to a directory.  (This is the non-POSIX value returned "
"since Linux 2.1.132.)"
msgstr ""
"I<pathname> est un répertoire. (Il s'agit d'une erreur non-POSIX renvoyée "
"depuis Linux 2.1.132)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ELOOP>"
msgstr "B<ELOOP>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Too many symbolic links were encountered in translating I<pathname>."
msgstr "Trop de liens symboliques dans le chemin d'accès I<pathname>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENAMETOOLONG>"
msgstr "B<ENAMETOOLONG>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<pathname> was too long."
msgstr "I<nom_chemin> est trop long."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOENT>"
msgstr "B<ENOENT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"A component in I<pathname> does not exist or is a dangling symbolic link, or "
"I<pathname> is empty."
msgstr ""
"Un répertoire dans le chemin d'accès I<pathname> n'existe pas ou est un lien "
"symbolique pointant nulle part, ou I<pathname> est vide"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOMEM>"
msgstr "B<ENOMEM>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Insufficient kernel memory was available."
msgstr "La mémoire disponible du noyau n'était pas suffisante."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOTDIR>"
msgstr "B<ENOTDIR>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"A component used as a directory in I<pathname> is not, in fact, a directory."
msgstr ""
"Un élément, utilisé comme répertoire, du chemin d'accès I<nom_chemin> n'est "
"pas en fait un répertoire."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EPERM>"
msgstr "B<EPERM>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The system does not allow unlinking of directories, or unlinking of "
"directories requires privileges that the calling process doesn't have.  "
"(This is the POSIX prescribed error return; as noted above, Linux returns "
"B<EISDIR> for this case.)"
msgstr ""
"Le système ne permet pas la destruction des répertoires avec unlink, ou "
"cette destruction nécessite des privilèges que le processus appelant n'a "
"pas. (Il s'agit d'une erreur conseillée par POSIX. Comme précisé plus haut, "
"Linux renvoie B<EISDIR> dans ce cas.)"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EPERM> (Linux only)"
msgstr "B<EPERM> (spécifique Linux)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The filesystem does not allow unlinking of files."
msgstr "Le système de fichiers ne permet pas la destruction avec unlink."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EPERM> or B<EACCES>"
msgstr "B<EPERM> ou B<EACCES>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The directory containing I<pathname> has the sticky bit (B<S_ISVTX>)  set "
"and the process's effective UID is neither the UID of the file to be deleted "
"nor that of the directory containing it, and the process is not privileged "
"(Linux: does not have the B<CAP_FOWNER> capability)."
msgstr ""
"Le répertoire contenant I<pathname> a son sticky bit (B<S_ISVTX>) à 1, et "
"l'UID effectif du processus n'est ni celui du fichier ni celui du répertoire "
"et le processus n'est pas privilégié (sous Linux\\ : n'a pas la capacité "
"B<CAP_FOWNER>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The file to be unlinked is marked immutable or append-only.  (See "
"B<ioctl_iflags>(2).)"
msgstr ""
"Le fichier à détruire avec unlink est marqué immuable ou comme n'acceptant "
"que des ajouts. (Voir B<ioctl_iflags>(2).)"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EROFS>"
msgstr "B<EROFS>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<pathname> refers to a file on a read-only filesystem."
msgstr "I<pathname> est placé sur un système de fichiers en lecture seule."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The same errors that occur for B<unlink>()  and B<rmdir>(2)  can also occur "
"for B<unlinkat>().  The following additional errors can occur for "
"B<unlinkat>():"
msgstr ""
"Les erreurs supplémentaires suivantes peuvent également se produire pour "
"B<unlinkat>() :"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EBADF>"
msgstr "B<EBADF>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"I<pathname> is relative but I<dirfd> is neither B<AT_FDCWD> nor a valid file "
"descriptor."
msgstr ""
"I<pathname> est relatif mais I<dirfd> n'est ni B<AT_FDWCD> ni un descripteur "
"de fichier valable."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "An invalid flag value was specified in I<flags>."
msgstr "I<flags> contient un drapeau non valable."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"I<pathname> refers to a directory, and B<AT_REMOVEDIR> was not specified in "
"I<flags>."
msgstr ""
"I<pathname> est un répertoire et B<AT_REMOVEDIR> n’était pas indiqué dans "
"I<flags>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"I<pathname> is relative and I<dirfd> is a file descriptor referring to a "
"file other than a directory."
msgstr ""
"I<pathname> est relatif et I<dirfd> est un descripteur de fichier faisant "
"référence à un fichier qui n'est pas un dossier."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDS"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr "POSIX.1-2008."

#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "HISTORIQUE"

#. type: TP
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<unlinkat>():"
msgid "B<unlink>()"
msgstr "B<unlinkat>():"

#.  SVr4 documents additional error
#.  conditions EINTR, EMULTIHOP, ETXTBSY, ENOLINK.
#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "SVr4, 4.3BSD, POSIX.1-2001."
msgstr "SVr4, 4.3BSD, POSIX.1-2001."

#. type: TP
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<unlinkat>():"
msgid "B<unlinkat>()"
msgstr "B<unlinkat>():"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "POSIX.1-2008.  Linux 2.6.16, glibc 2.4."
msgstr "POSIX.1-2008.  Linux 2.6.16, glibc 2.4."

#. type: SS
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "glibc"
msgstr "glibc"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"On older kernels where B<unlinkat>()  is unavailable, the glibc wrapper "
"function falls back to the use of B<unlink>()  or B<rmdir>(2).  When "
"I<pathname> is a relative pathname, glibc constructs a pathname based on the "
"symbolic link in I</proc/self/fd> that corresponds to the I<dirfd> argument."
msgstr ""
"Avec les anciens noyaux où B<unlinkat>() n'est pas disponible, la fonction "
"d'enveloppe de la glibc se replie sur l'utilisation de B<unlink>() ou "
"B<rmdir>(2). Qaund I<pathname> est un nom de chemin relatif, glibc construit "
"un nom de chemin basé sur le lien symbolique dans I</proc/self/fd> qui "
"correspond à l'argument I<dirfd>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "BOGUES"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Infelicities in the protocol underlying NFS can cause the unexpected "
"disappearance of files which are still being used."
msgstr ""
"Des problèmes dans le protocole sous-jacent à NFS peuvent provoquer la "
"disparition inattendue de fichiers encore utilisés."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<rm>(1), B<unlink>(1), B<chmod>(2), B<link>(2), B<mknod>(2), B<open>(2), "
"B<rename>(2), B<rmdir>(2), B<mkfifo>(3), B<remove>(3), "
"B<path_resolution>(7), B<symlink>(7)"
msgstr ""
"B<rm>(1), B<unlink>(1), B<chmod>(2), B<link>(2), B<mknod>(2), B<open>(2), "
"B<rename>(2), B<rmdir>(2), B<mkfifo>(3), B<remove>(3), "
"B<path_resolution>(7), B<symlink>(7)"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "2023-02-05"
msgstr "5 février 2023"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Pages du manuel de Linux 6.03"

#. type: SH
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "VERSIONS"
msgstr "VERSIONS"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron
msgid ""
"B<unlinkat>()  was added in Linux 2.6.16; library support was added in glibc "
"2.4."
msgstr ""
"B<unlinkat>() a été ajouté à Linux 2.6.16 ; la prise en charge de la "
"bibliothèque a été ajoutée dans la glibc 2.4."

#.  SVr4 documents additional error
#.  conditions EINTR, EMULTIHOP, ETXTBSY, ENOLINK.
#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid "B<unlink>(): SVr4, 4.3BSD, POSIX.1-2001, POSIX.1-2008."
msgstr "B<unlink>() : SVr4, 4.3BSD, POSIX.1-2001, POSIX.1-2008."

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid "B<unlinkat>(): POSIX.1-2008."
msgstr "B<unlinkat>() : POSIX.1-2008."

#. type: SH
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "NOTES"
msgstr "NOTES"

#. type: SS
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "glibc notes"
msgstr "Notes de la glibc"

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.05"
msgstr "Pages du manuel de Linux 6.05"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "UNLINK"
msgstr "UNLINK"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2017-09-15"
msgstr "15 septembre 2017"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Manuel du programmeur Linux"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"B<#include E<lt>fcntl.hE<gt>           >/* Definition of AT_* constants */\n"
"B<#include E<lt>unistd.hE<gt>>\n"
msgstr ""
"B<#include E<lt>fcntl.hE<gt>           >/* Définition des constantes AT_* */\n"
"B<#include E<lt>unistd.hE<gt>>\n"

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "Since glibc 2.10:"
msgstr "Depuis la glibc 2.10 :"

#. type: Plain text
#: opensuse-leap-15-6
msgid "_POSIX_C_SOURCE\\ E<gt>=\\ 200809L"
msgstr "_POSIX_C_SOURCE\\ E<gt>=\\ 200809L"

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "Before glibc 2.10:"
msgstr "Avant la glibc 2.10 :"

#. type: Plain text
#: opensuse-leap-15-6
msgid "_ATFILE_SOURCE"
msgstr "_ATFILE_SOURCE"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"On success, zero is returned.  On error, -1 is returned, and I<errno> is set "
"appropriately."
msgstr ""
"En cas de succès, zéro est renvoyé. En cas d'erreur, B<-1> est renvoyé et "
"I<errno> reçoit une valeur adéquate."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"I<pathname> refers to a directory.  (This is the non-POSIX value returned by "
"Linux since 2.1.132.)"
msgstr ""
"I<pathname> est un répertoire. (Il s'agit d'une erreur non-POSIX renvoyée "
"par Linux depuis le 2.1.132)."

#. type: Plain text
#: opensuse-leap-15-6
msgid "I<dirfd> is not a valid file descriptor."
msgstr "I<dirfd> n'est pas un descripteur de fichier valable."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"B<unlinkat>()  was added to Linux in kernel 2.6.16; library support was "
"added to glibc in version 2.4."
msgstr ""
"B<unlinkat>() a été ajouté au noyau Linux dans sa version 2.6.16 ; la glibc "
"le gère depuis la version 2.4."

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "CONFORMING TO"
msgstr "CONFORMITÉ"

#. type: SS
#: opensuse-leap-15-6
#, no-wrap
msgid "Glibc notes"
msgstr "Notes de la glibc"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "COLOPHON"
msgstr "COLOPHON"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Cette page fait partie de la publication 4.16 du projet I<man-pages> Linux. "
"Une description du projet et des instructions pour signaler des anomalies et "
"la dernière version de cette page peuvent être trouvées à l'adresse \\"
"%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Pages du manuel de Linux 6.04"
