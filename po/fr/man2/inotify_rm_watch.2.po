# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Christophe Blaess <https://www.blaess.fr/christophe/>, 1996-2003.
# Stéphan Rafin <stephan.rafin@laposte.net>, 2002.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999, 2002.
# François Micaux, 2002.
# Alain Portal <aportal@univ-montp2.fr>, 2003-2008.
# Jean-Philippe Guérard <fevrier@tigreraye.org>, 2005-2006.
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006-2007.
# Julien Cristau <jcristau@debian.org>, 2006-2007.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006-2008.
# Nicolas François <nicolas.francois@centraliens.net>, 2006-2008.
# Florentin Duneau <fduneau@gmail.com>, 2006-2010.
# Simon Paillard <simon.paillard@resel.enst-bretagne.fr>, 2006.
# Denis Barbier <barbier@debian.org>, 2006, 2010.
# David Prévot <david@tilapin.org>, 2010, 2012-2014.
msgid ""
msgstr ""
"Project-Id-Version: perkamon\n"
"POT-Creation-Date: 2023-08-27 17:02+0200\n"
"PO-Revision-Date: 2023-01-13 23:55+0100\n"
"Last-Translator: Thomas Vincent <tvincent@debian.org>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Poedit 1.8.11\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "inotify_rm_watch"
msgstr "inotify_rm_watch"

#. type: TH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30 mars 2023"

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Pages du manuel de Linux 6.05.01"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "inotify_rm_watch - remove an existing watch from an inotify instance"
msgstr ""
"inotify_rm_watch - Supprimer une surveillance existante d'une instance "
"inotify"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTHÈQUE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Bibliothèque C standard (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>sys/inotify.hE<gt>>\n"
msgstr "B<#include E<lt>sys/inotify.hE<gt>>\n"

#. #-#-#-#-#  archlinux: inotify_rm_watch.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  Before glibc 2.10, the second argument was types as uint32_t.
#.  https://www.sourceware.org/bugzilla/show_bug.cgi?id=7040
#. type: Plain text
#. #-#-#-#-#  debian-bookworm: inotify_rm_watch.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  Before glibc 2.10, the second argument was types as uint32_t.
#.  http://sources.redhat.com/bugzilla/show_bug.cgi?id=7040
#. type: Plain text
#. #-#-#-#-#  debian-unstable: inotify_rm_watch.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  Before glibc 2.10, the second argument was types as uint32_t.
#.  http://sources.redhat.com/bugzilla/show_bug.cgi?id=7040
#. type: Plain text
#. #-#-#-#-#  fedora-39: inotify_rm_watch.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  Before glibc 2.10, the second argument was types as uint32_t.
#.  https://www.sourceware.org/bugzilla/show_bug.cgi?id=7040
#. type: Plain text
#. #-#-#-#-#  fedora-rawhide: inotify_rm_watch.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  Before glibc 2.10, the second argument was types as uint32_t.
#.  https://www.sourceware.org/bugzilla/show_bug.cgi?id=7040
#. type: Plain text
#. #-#-#-#-#  mageia-cauldron: inotify_rm_watch.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  Before glibc 2.10, the second argument was types as uint32_t.
#.  http://sources.redhat.com/bugzilla/show_bug.cgi?id=7040
#. type: Plain text
#. #-#-#-#-#  opensuse-tumbleweed: inotify_rm_watch.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  Before glibc 2.10, the second argument was types as uint32_t.
#.  https://www.sourceware.org/bugzilla/show_bug.cgi?id=7040
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<int inotify_rm_watch(int >I<fd>B<, int >I<wd>B<);>\n"
msgstr "B<int inotify_rm_watch(int >I<fd>B<, int >I<wd>B<);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<inotify_rm_watch>()  removes the watch associated with the watch "
"descriptor I<wd> from the inotify instance associated with the file "
"descriptor I<fd>."
msgstr ""
"B<inotify_rm_watch>() supprime la surveillance associée au descripteur de "
"surveillance I<wd> de l'instance inotify associée au descripteur de fichier "
"I<fd>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Removing a watch causes an B<IN_IGNORED> event to be generated for this "
"watch descriptor.  (See B<inotify>(7).)"
msgstr ""
"La suppression d'une surveillance provoque la génération d'un événement "
"B<IN_IGNORED> pour le descripteur. Consultez B<inotify>(7)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALEUR RENVOYÉE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"On success, B<inotify_rm_watch>()  returns zero.  On error, -1 is returned "
"and I<errno> is set to indicate the error."
msgstr ""
"S'il réussit, B<inotify_rm_watch>() renvoie zéro. En cas d'erreur, il "
"renvoie B<-1> et I<errno> est positionné pour indiquer l'erreur."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ERREURS"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EBADF>"
msgstr "B<EBADF>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<fd> is not a valid file descriptor."
msgstr "I<fd> n'est pas un descripteur de fichier valable."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The watch descriptor I<wd> is not valid; or I<fd> is not an inotify file "
"descriptor."
msgstr ""
"Le descripteur de surveillance I<wd> est invalide, ou I<fd> n'est pas un "
"descripteur inotify."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDS"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "Linux."
msgstr "Linux."

#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "HISTORIQUE"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "Linux 2.6.13."
msgstr "Linux 2.6.13."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<inotify_add_watch>(2), B<inotify_init>(2), B<inotify>(7)"
msgstr "B<inotify_add_watch>(2), B<inotify_init>(2), B<inotify>(7)"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "2022-10-30"
msgstr "30 octobre 2022"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Pages du manuel de Linux 6.03"

#. type: SH
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "VERSIONS"
msgstr "VERSIONS"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid "Inotify was merged into the 2.6.13 Linux kernel."
msgstr "Inotify a été intégré dans Linux 2.6.13."

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid "This system call is Linux-specific."
msgstr "Cet appel système est spécifique à Linux."

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.05"
msgstr "Pages du manuel de Linux 6.05"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "INOTIFY_RM_WATCH"
msgstr "INOTIFY_RM_WATCH"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2017-09-15"
msgstr "15 septembre 2017"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Manuel du programmeur Linux"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<#include E<lt>sys/inotify.hE<gt>>"
msgstr "B<#include E<lt>sys/inotify.hE<gt>>"

#.  Before glibc 2.10, the second argument was types as uint32_t.
#.  http://sources.redhat.com/bugzilla/show_bug.cgi?id=7040
#. type: Plain text
#: opensuse-leap-15-6
msgid "B<int inotify_rm_watch(int >I<fd>B<, int >I<wd>B<);>"
msgstr "B<int inotify_rm_watch(int >I<fd>B<, int >I<wd>B<);>"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"On success, B<inotify_rm_watch>()  returns zero.  On error, -1 is returned "
"and I<errno> is set to indicate the cause of the error."
msgstr ""
"S'il réussit, B<inotify_rm_watch>() renvoie zéro. En cas d'erreur, il "
"renvoie -1 et I<errno> est définie pour indiquer la cause de l’erreur."

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "CONFORMING TO"
msgstr "CONFORMITÉ"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "COLOPHON"
msgstr "COLOPHON"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Cette page fait partie de la publication 4.16 du projet I<man-pages> Linux. "
"Une description du projet et des instructions pour signaler des anomalies et "
"la dernière version de cette page peuvent être trouvées à l'adresse \\"
"%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Pages du manuel de Linux 6.04"
