# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Sylvain Archenault <sylvain.archenault@laposte.net>, 2006.
# Frédéric Zulian <zulian@free.fr>, 2006.
# Grégory Colpart <reg@evolix.fr>, 2006.
# Philippe Piette <foudre-blanche@skynet.be>, 2006.
# Julien Cristau <jcristau@debian.org>, 2006.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006, 2007.
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006.
# Jean-Baka Domelevo-Entfellner <domelevo@gmail.com>, 2006.
# Florentin Duneau <fduneau@gmail.com>, 2006.
# Philippe Piette <foudre-blanche@skynet.be>, 2006, 2007.
# Florentin Duneau <fduneau@gmail.com>, 2008-2010.
# David Prévot <david@tilapin.org>, 2010-2013.
# Jean-Philippe MENGUAL <jpmengual@debian.org>, 2020-2021.
msgid ""
msgstr ""
"Project-Id-Version: manpages-fr-extra\n"
"POT-Creation-Date: 2021-08-27 16:32+0200\n"
"PO-Revision-Date: 2020-04-29 11:41+0200\n"
"Last-Translator: Jean-Philippe MENGUAL <jpmengual@debian.org>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.4\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "PGREP"
msgstr "PGREP"

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "2020-06-04"
msgstr "4 juin 2020"

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "procps-ng"
msgstr "procps-ng"

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "User Commands"
msgstr "Commandes de l'utilisateur"

#. type: SH
#: debian-bullseye
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: debian-bullseye
#, fuzzy
#| msgid ""
#| "pgrep, pkill - look up or signal processes based on name and other "
#| "attributes"
msgid ""
"pgrep, pkill, pidwait - look up, signal, or wait for processes based on name "
"and other attributes"
msgstr ""
"pgrep, pkill - Rechercher ou envoyer un signal à des processus en fonction "
"de leur nom et d'autres propriétés"

#. type: SH
#: debian-bullseye
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: debian-bullseye
msgid "B<pgrep> [options] pattern"
msgstr "B<pgrep> [B<options>] I<motif>"

#. type: Plain text
#: debian-bullseye
msgid "B<pkill> [options] pattern"
msgstr "B<pkill> [B<options>] I<motif>"

#. type: Plain text
#: debian-bullseye
#, fuzzy
#| msgid "B<pkill> [options] pattern"
msgid "B<pidwait> [options] pattern"
msgstr "B<pkill> [B<options>] I<motif>"

#. type: SH
#: debian-bullseye
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: debian-bullseye
msgid ""
"B<pgrep> looks through the currently running processes and lists the process "
"IDs which match the selection criteria to stdout.  All the criteria have to "
"match.  For example,"
msgstr ""
"B<pgrep> parcourt les processus en cours d'exécution et affiche sur la "
"sortie standard les identifiants des processus qui correspondent aux "
"critères de sélection donnés. Tous les critères doivent correspondre. Par "
"exemple\\ :"

#. type: Plain text
#: debian-bullseye
msgid "$ pgrep -u root sshd"
msgstr "$ pgrep -u root sshd"

#. type: Plain text
#: debian-bullseye
msgid ""
"will only list the processes called B<sshd> AND owned by B<root>.  On the "
"other hand,"
msgstr ""
"n'affichera que les processus appelés B<sshd> ET qui appartiennent à "
"B<root>. En revanche,"

#. type: Plain text
#: debian-bullseye
msgid "$ pgrep -u root,daemon"
msgstr "$ pgrep -u root,daemon"

#. type: Plain text
#: debian-bullseye
msgid "will list the processes owned by B<root> OR B<daemon>."
msgstr "affichera les processus appartenant à B<root> OU à B<daemon>."

#. type: Plain text
#: debian-bullseye
msgid ""
"B<pkill> will send the specified signal (by default B<SIGTERM>)  to each "
"process instead of listing them on stdout."
msgstr ""
"B<pkill> enverra le signal indiqué (B<SIGTERM> par défaut) à chaque "
"processus au lieu de les afficher sur la sortie standard."

#. type: Plain text
#: debian-bullseye
#, fuzzy
#| msgid ""
#| "B<pkill> will send the specified signal (by default B<SIGTERM>)  to each "
#| "process instead of listing them on stdout."
msgid ""
"B<pidwait> will wait for each process instead of listing them on stdout."
msgstr ""
"B<pkill> enverra le signal indiqué (B<SIGTERM> par défaut) à chaque "
"processus au lieu de les afficher sur la sortie standard."

#. type: SH
#: debian-bullseye
#, no-wrap
msgid "OPTIONS"
msgstr "OPTIONS"

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<->I<signal>"
msgstr "B<->I<signal>"

#. type: TQ
#: debian-bullseye
#, no-wrap
msgid "B<--signal> I<signal>"
msgstr "B<--signal> I<signal>"

#. type: Plain text
#: debian-bullseye
msgid ""
"Defines the signal to send to each matched process.  Either the numeric or "
"the symbolic signal name can be used.  (B<pkill> only.)"
msgstr ""
"Indiquer le signal à envoyer à chaque processus sélectionné. Le signal peut "
"être indiqué par son numéro ou par son nom symbolique (seulement pour "
"B<pkill>)."

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<-c>, B<--count>"
msgstr "B<-c>, B<--count>"

#. type: Plain text
#: debian-bullseye
#, fuzzy
#| msgid ""
#| "Suppress normal output; instead print a count of matching processes.  "
#| "When count does not match anything, e.g. returns zero, the command will "
#| "return non-zero value."
msgid ""
"Suppress normal output; instead print a count of matching processes.  When "
"count does not match anything, e.g. returns zero, the command will return "
"non-zero value. Note that for pkill and pidwait, the count is the number of "
"matching processes, not the processes that were successfully signaled or "
"waited for."
msgstr ""
"À la place de la sortie normale, afficher le nombre de processus "
"correspondant aux critères. S'il n'y a pas de correspondance, c'est-à-dire "
"si zéro est renvoyé, la commande renverra une valeur non nulle."

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<-d>, B<--delimiter> I<delimiter>"
msgstr "B<-d>, B<--delimiter> I<délimiteur>"

#. type: Plain text
#: debian-bullseye
msgid ""
"Sets the string used to delimit each process ID in the output (by default a "
"newline).  (B<pgrep> only.)"
msgstr ""
"Déterminer la chaîne utilisée pour délimiter les PID en sortie (un saut de "
"ligne par défaut) (seulement pour B<pgrep>)."

#. type: TP
#: debian-bullseye
#, fuzzy, no-wrap
#| msgid "B<-e>, B<--ed>"
msgid "B<-e>, B<--echo>"
msgstr "B<-e>, B<--ed>"

#. type: Plain text
#: debian-bullseye
#, fuzzy
#| msgid "List the process name as well as the process ID.  (B<pgrep> only.)"
msgid "Display name and PID of the process being killed.  (B<pkill> only.)"
msgstr "Afficher le nom du processus avec le PID (seulement pour B<pgrep>)."

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<-f>, B<--full>"
msgstr "B<-f>, B<--full>"

#. type: Plain text
#: debian-bullseye
msgid ""
"The I<pattern> is normally only matched against the process name.  When B<-"
"f> is set, the full command line is used."
msgstr ""
"Le I<motif> n'est normalement comparé qu'au nom du processus. Avec B<-f>, la "
"ligne de commande complète est utilisée."

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<-g>, B<--pgroup> I<pgrp>,..."
msgstr "B<-g>, B<--pgroup> I<pgrp>,..."

#. type: Plain text
#: debian-bullseye
#, fuzzy
#| msgid ""
#| "Only match processes in the process group IDs listed.  Process group 0 is "
#| "translated into B<pgrep>'s or B<pkill>'s own process group."
msgid ""
"Only match processes in the process group IDs listed.  Process group 0 is "
"translated into B<pgrep>'s, B<pkill>'s, or B<pidwait>'s own process group."
msgstr ""
"Ne rechercher que des processus dans les groupes de processus donnés. Le "
"groupe de processus B<0> se traduit par le propre groupe de processus de "
"B<pgrep> ou B<pkill>."

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<-G>, B<--group> I<gid>,..."
msgstr "B<-G>, B<--group> I<gid>,..."

#. type: Plain text
#: debian-bullseye
msgid ""
"Only match processes whose real group ID is listed.  Either the numerical or "
"symbolical value may be used."
msgstr ""
"Ne rechercher que des processus dont l'identifiant de groupe réel est donné. "
"Les valeurs utilisées peuvent être numériques ou symboliques."

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<-i>, B<--ignore-case>"
msgstr "B<-i>, B<--ignore-case>"

#. type: Plain text
#: debian-bullseye
msgid "Match processes case-insensitively."
msgstr "Rechercher les processus sans tenir compte de la casse."

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<-l>, B<--list-name>"
msgstr "B<-l>, B<--list-name>"

#. type: Plain text
#: debian-bullseye
msgid "List the process name as well as the process ID.  (B<pgrep> only.)"
msgstr "Afficher le nom du processus avec le PID (seulement pour B<pgrep>)."

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<-a>, B<--list-full>"
msgstr "B<-a>, B<--list-full>"

#. type: Plain text
#: debian-bullseye
msgid "List the full command line as well as the process ID.  (B<pgrep> only.)"
msgstr ""
"Afficher la ligne de commande complète avec le PID (seulement pour B<pgrep>)."

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<-n>, B<--newest>"
msgstr "B<-n>, B<--newest>"

#. type: Plain text
#: debian-bullseye
msgid ""
"Select only the newest (most recently started) of the matching processes."
msgstr "Ne sélectionner que le processus correspondant le plus récent."

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<-o>, B<--oldest>"
msgstr "B<-o>, B<--oldest>"

#. type: Plain text
#: debian-bullseye
msgid ""
"Select only the oldest (least recently started) of the matching processes."
msgstr "Ne sélectionner que le processus correspondant le plus ancien."

#. type: TP
#: debian-bullseye
#, fuzzy, no-wrap
#| msgid "B<-o>, B<--oldest>"
msgid "B<-O>, B<--older> I<secs>"
msgstr "B<-o>, B<--oldest>"

#. type: Plain text
#: debian-bullseye
msgid "Select processes older than secs."
msgstr ""

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<-P>, B<--parent> I<ppid>,..."
msgstr "B<-P>, B<--parent> I<ppid>,..."

#. type: Plain text
#: debian-bullseye
msgid "Only match processes whose parent process ID is listed."
msgstr "Ne sélectionner que les processus dont le PID parent est donné."

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<-s>, B<--session> I<sid>,..."
msgstr "B<-s>, B<--session> I<sid>,..."

#. type: Plain text
#: debian-bullseye
#, fuzzy
#| msgid ""
#| "Only match processes whose process session ID is listed.  Session ID 0 is "
#| "translated into B<pgrep>'s or B<pkill>'s own session ID."
msgid ""
"Only match processes whose process session ID is listed.  Session ID 0 is "
"translated into B<pgrep>'s, B<pkill>'s, or B<pidwait>'s own session ID."
msgstr ""
"Ne sélectionner que les processus dont l'identifiant de session est donné. "
"La session B<0> se traduit par le propre identifiant de session de B<pgrep> "
"ou de B<pkill>."

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<-t>, B<--terminal> I<term>,..."
msgstr "B<-t>, B<--terminal> I<term>,..."

#. type: Plain text
#: debian-bullseye
msgid ""
"Only match processes whose controlling terminal is listed.  The terminal "
"name should be specified without the \"/dev/\" prefix."
msgstr ""
"Ne sélectionner que les processus dont le terminal de contrôle est donné. Le "
"nom du terminal doit être indiqué sans le préfixe «\\ /dev/\\ »."

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<-u>, B<--euid> I<euid>,..."
msgstr "B<-u>, B<--euid> I<euid>,..."

#. type: Plain text
#: debian-bullseye
msgid ""
"Only match processes whose effective user ID is listed.  Either the "
"numerical or symbolical value may be used."
msgstr ""
"Ne sélectionner que les processus dont l'UID effectif est donné. La valeur "
"utilisée peut être numérique ou symbolique."

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<-U>, B<--uid> I<uid>,..."
msgstr "B<-U>, B<--uid> I<uid>,..."

#. type: Plain text
#: debian-bullseye
msgid ""
"Only match processes whose real user ID is listed.  Either the numerical or "
"symbolical value may be used."
msgstr ""
"Ne sélectionner que les processus dont l'UID réel est donné. La valeur "
"utilisée peut être numérique ou symbolique."

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<-v>, B<--inverse>"
msgstr "B<-v>, B<--inverse>"

#. type: Plain text
#: debian-bullseye
#, fuzzy
#| msgid ""
#| "Negates the matching.  This option is usually used in B<pgrep>'s "
#| "context.  In B<pkill>'s context the short option is disabled to avoid "
#| "accidental usage of the option."
msgid ""
"Negates the matching.  This option is usually used in B<pgrep>'s or "
"B<pidwait>'s context.  In B<pkill>'s context the short option is disabled to "
"avoid accidental usage of the option."
msgstr ""
"Inverser la sélection. Cette option est normalement utilisée dans le "
"contexte de B<pgrep>. Dans le contexte de B<pkill>, l'option courte est "
"désactivée pour éviter qu'elle soit utilisée par accident."

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<-w>, B<--lightweight>"
msgstr "B<-w>, B<--lightweight>"

#. type: Plain text
#: debian-bullseye
#, fuzzy
#| msgid ""
#| "Shows all thread ids instead of pids in B<pgrep>'s context.  In "
#| "B<pkill>'s context this option is disabled."
msgid ""
"Shows all thread ids instead of pids in B<pgrep>'s or B<pidwait>'s context.  "
"In B<pkill>'s context this option is disabled."
msgstr ""
"Montrer tous les identifiants de processus légers (« threads ») au lieu des "
"PID dans le contexte de B<pgrep>. Dans le contexte de B<pkill>, cette option "
"est désactivée."

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<-x>, B<--exact>"
msgstr "B<-x>, B<--exact>"

#. type: Plain text
#: debian-bullseye
#, fuzzy
#| msgid ""
#| "Only match processes whose names (or command line if -f is specified)  "
#| "B<exactly> match the I<pattern>."
msgid ""
"Only match processes whose names (or command lines if B<-f> is specified)  "
"B<exactly> match the I<pattern>."
msgstr ""
"Ne sélectionner que les processus dont le nom (ou la ligne de commande si B<-"
"f> est utilisée) correspond B<exactement> au I<motif>."

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<-F>, B<--pidfile> I<file>"
msgstr "B<-F>, B<--pidfile> I<fichier>"

#. type: Plain text
#: debian-bullseye
#, fuzzy
#| msgid ""
#| "Read I<PID>'s from file.  This option is perhaps more useful for B<pkill> "
#| "than B<pgrep>."
msgid ""
"Read I<PID>s from I<file>.  This option is more useful for "
"B<pkill>orB<pidwait> than B<pgrep>."
msgstr ""
"Lire les I<PID> dans le fichier. Cette option est peut-être plus utile pour "
"B<pkill> que pour B<pgrep>."

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<-L>, B<--logpidfile>"
msgstr "B<-L>, B<--logpidfile>"

#. type: Plain text
#: debian-bullseye
#, fuzzy
#| msgid "Fail if pidfile (see -F) not locked."
msgid "Fail if pidfile (see B<-F>) not locked."
msgstr ""
"Échouer si le I<fichier> de PID (consultez B<-F>) n'est pas verrouillé."

#. type: TP
#: debian-bullseye
#, fuzzy, no-wrap
#| msgid "B<-P>, B<--parent> I<ppid>,..."
msgid "B<-r>, B<--runstates> I<D,R,S,Z,>..."
msgstr "B<-P>, B<--parent> I<ppid>,..."

#. type: Plain text
#: debian-bullseye
#, fuzzy
#| msgid "Only match processes whose parent process ID is listed."
msgid "Match only processes which match the process state."
msgstr "Ne sélectionner que les processus dont le PID parent est donné."

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<--ns >I<pid>"
msgstr "B<--ns >I<pid>"

#. type: Plain text
#: debian-bullseye
#, fuzzy
#| msgid ""
#| "Match processes that belong to the same namespaces. Required to run as "
#| "root to match processes from other users. See --nslist for how to limit "
#| "which namespaces to match."
msgid ""
"Match processes that belong to the same namespaces. Required to run as root "
"to match processes from other users. See B<--nslist> for how to limit which "
"namespaces to match."
msgstr ""
"Sélectionner les processus qui appartiennent aux mêmes espaces de noms. Doit "
"être exécuté en tant que superutilisateur pour sélectionner les processus "
"d’autres utilisateurs. Consultez B<--nslist> pour une manière de limiter les "
"espaces de noms à sélectionner."

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<--nslist >I<name>B<,...>"
msgstr "B<--nslist> I<en>[B<,>I<en>]..."

#. type: Plain text
#: debian-bullseye
msgid ""
"Match only the provided namespaces. Available namespaces: ipc, mnt, net, "
"pid, user,uts."
msgstr ""
"Ne sélectionner que les espaces de noms fournis. Les espaces de noms "
"disponibles sont : ipc, mnt, net, pid, user et uts."

#. type: TP
#: debian-bullseye
#, fuzzy, no-wrap
#| msgid "B<-q>, B<--queues>"
msgid "B<-q>, B<--queue >I<value>"
msgstr "B<-q>, B<--queues>"

#. type: Plain text
#: debian-bullseye
msgid ""
"Use B<sigqueue(3)> rather than B<kill(2)> and the value argument is used to "
"specify an integer to be sent with the signal. If the receiving process has "
"installed a handler for this signal using the SA_SIGINFO flag to "
"B<sigaction(2)> , then it can obtain this data via the si_value field of the "
"siginfo_t structure."
msgstr ""

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: debian-bullseye
msgid "Display version information and exit."
msgstr "Afficher le nom et la version du logiciel et quitter."

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<-h>, B<--help>"
msgstr "B<-h>, B<--help>"

#. type: Plain text
#: debian-bullseye
msgid "Display help and exit."
msgstr "Afficher l'aide et quitter."

#. type: SH
#: debian-bullseye
#, no-wrap
msgid "OPERANDS"
msgstr "OPÉRANDES"

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "I<pattern>"
msgstr "I<motif>"

#. type: Plain text
#: debian-bullseye
msgid ""
"Specifies an Extended Regular Expression for matching against the process "
"names or command lines."
msgstr ""
"Indiquer une expression rationnelle étendue utilisée pour comparer avec les "
"noms de processus ou les lignes de commandes."

#. type: SH
#: debian-bullseye
#, no-wrap
msgid "EXAMPLES"
msgstr "EXEMPLES"

#. type: Plain text
#: debian-bullseye
msgid "Example 1: Find the process ID of the B<named> daemon:"
msgstr "Exemple 1\\ : Trouver le PID du démon B<named>\\ :"

#. type: Plain text
#: debian-bullseye
msgid "$ pgrep -u root named"
msgstr "$ pgrep -u root named"

#. type: Plain text
#: debian-bullseye
msgid "Example 2: Make B<syslog> reread its configuration file:"
msgstr ""
"Exemple 2\\ : Faire relire son fichier de configuration par B<syslog>\\ :"

#. type: Plain text
#: debian-bullseye
msgid "$ pkill -HUP syslogd"
msgstr "$ pkill -HUP syslogd"

#. type: Plain text
#: debian-bullseye
msgid "Example 3: Give detailed information on all B<xterm> processes:"
msgstr ""
"Exemple 3\\ : Donner des informations détaillées sur tous les processus "
"B<xterm>\\ :"

#. type: Plain text
#: debian-bullseye
msgid "$ ps -fp $(pgrep -d, -x xterm)"
msgstr "$ ps -fp $(pgrep -d, -x xterm)"

#. type: Plain text
#: debian-bullseye
msgid "Example 4: Make all B<chrome> processes run nicer:"
msgstr "Exemple 4\\ : Réduire la priorité de tous les processus B<chrome>\\ :"

#. type: Plain text
#: debian-bullseye
msgid "$ renice +4 $(pgrep chrome)"
msgstr "$ renice +4 $(pgrep chrome)"

#. type: SH
#: debian-bullseye
#, no-wrap
msgid "EXIT STATUS"
msgstr "CODE DE RETOUR"

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "0"
msgstr "0"

#. type: Plain text
#: debian-bullseye
#, fuzzy
#| msgid ""
#| "One or more processes matched the criteria. For pkill the process must "
#| "also have been successfully signalled."
msgid ""
"One or more processes matched the criteria. For pkill and pidwait, one or "
"more processes must also have been successfully signalled or waited for."
msgstr ""
"Un ou plusieurs processus correspondaient aux critères. Pour B<pkill>, le "
"processus doit avoir reçu un signal de réussite."

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "1"
msgstr "1"

#. type: Plain text
#: debian-bullseye
msgid "No processes matched or none of them could be signalled."
msgstr ""
"Aucun processus trouvé ou un signal n'a pas pu être envoyé à aucun d'entre "
"eux"

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "2"
msgstr "2"

#. type: Plain text
#: debian-bullseye
msgid "Syntax error in the command line."
msgstr "Erreur de syntaxe dans la ligne de commande."

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "3"
msgstr "3"

#. type: Plain text
#: debian-bullseye
msgid "Fatal error: out of memory etc."
msgstr "Erreur fatale, par exemple plus de mémoire disponible."

#. type: SH
#: debian-bullseye
#, no-wrap
msgid "NOTES"
msgstr "NOTES"

#. type: Plain text
#: debian-bullseye
#, fuzzy
#| msgid ""
#| "The process name used for matching is limited to the 15 characters "
#| "present in the output of /proc/I<pid>/stat.  Use the -f option to match "
#| "against the complete command line, /proc/I<pid>/cmdline."
msgid ""
"The process name used for matching is limited to the 15 characters present "
"in the output of /proc/I<pid>/stat.  Use the B<-f> option to match against "
"the complete command line, /proc/I<pid>/cmdline."
msgstr ""
"Le nom du processus utilisé pour la sélection est limité aux 15\\ caractères "
"présents dans /proc/I<pid>/stat. Utilisez l'option B<-f> pour sélectionner "
"en fonction de la ligne de commande complète, /proc/I<pid>/cmdline."

#. type: Plain text
#: debian-bullseye
#, fuzzy
#| msgid ""
#| "The running B<pgrep> or B<pkill> process will never report itself as a "
#| "match."
msgid ""
"The running B<pgrep>, B<pkill>, or B<pidwait> process will never report "
"itself as a match."
msgstr ""
"Le processus B<pgrep> ou B<pkill> qui s'exécute ne se considérera jamais "
"comme correspondant aux critères."

#. type: SH
#: debian-bullseye
#, no-wrap
msgid "BUGS"
msgstr "BOGUES"

#. type: Plain text
#: debian-bullseye
msgid ""
"The options B<-n> and B<-o> and B<-v> can not be combined.  Let me know if "
"you need to do this."
msgstr ""
"Les options B<-n>, B<-o> et B<-v> ne peuvent pas être utilisées en même "
"temps. Signalez-le si vous en avez besoin."

#. type: Plain text
#: debian-bullseye
msgid "Defunct processes are reported."
msgstr "Les processus zombies sont affichés."

#. type: SH
#: debian-bullseye
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: debian-bullseye
#, fuzzy
#| msgid ""
#| "B<ps>(1), B<regex>(7), B<signal>(7), B<killall>(1), B<skill>(1), "
#| "B<kill>(1), B<kill>(2)"
msgid ""
"B<ps>(1), B<regex>(7), B<signal>(7), B<sigqueue>(3), B<killall>(1), "
"B<skill>(1), B<kill>(1), B<kill>(2)"
msgstr ""
"B<ps>(1), B<regex>(7), B<signal>(7), B<killall>(1), B<skill>(1), B<kill>(1), "
"B<kill>(2)"

#. type: SH
#: debian-bullseye
#, no-wrap
msgid "AUTHOR"
msgstr "AUTEUR"

#. type: Plain text
#: debian-bullseye
msgid "E<.UR kjetilho@ifi.uio.no> Kjetil Torgrim Homme E<.UE>"
msgstr "Kjetil Torgrim Homme E<lt>I<kjetilho@ifi.uio.no>E<gt>"

#. type: SH
#: debian-bullseye
#, no-wrap
msgid "REPORTING BUGS"
msgstr "SIGNALER DES BOGUES"

#. type: Plain text
#: debian-bullseye
msgid "Please send bug reports to E<.UR procps@freelists.org> E<.UE>"
msgstr ""
"Merci d'envoyer un rapport de bogue à E<.UR procps@freelists.org> E<.UE>"
