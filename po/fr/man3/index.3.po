# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Christophe Blaess <https://www.blaess.fr/christophe/>, 1996-2003.
# Stéphan Rafin <stephan.rafin@laposte.net>, 2002.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999, 2002.
# François Micaux, 2002.
# Alain Portal <aportal@univ-montp2.fr>, 2003-2008.
# Jean-Philippe Guérard <fevrier@tigreraye.org>, 2005-2006.
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006-2007.
# Julien Cristau <jcristau@debian.org>, 2006-2007.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006-2008.
# Nicolas François <nicolas.francois@centraliens.net>, 2006-2008.
# Florentin Duneau <fduneau@gmail.com>, 2006-2010.
# Simon Paillard <simon.paillard@resel.enst-bretagne.fr>, 2006.
# Denis Barbier <barbier@debian.org>, 2006, 2010.
# David Prévot <david@tilapin.org>, 2010-2012, 2014.
# Frédéric Hantrais <fhantrais@gmail.com>, 2013, 2014.
# Jean-Pierre Giraud <jean-pierregiraud@neuf.fr>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: perkamon\n"
"POT-Creation-Date: 2023-08-27 17:02+0200\n"
"PO-Revision-Date: 2023-05-23 14:43+0200\n"
"Last-Translator: Jean-Pierre Giraud <jean-pierregiraud@neuf.fr>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Lokalize 22.12.1\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "index"
msgstr "index"

#. type: TH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30 mars 2023"

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Pages du manuel de Linux 6.05.01"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "index, rindex - locate character in string"
msgstr "index, rindex - Recherche de caractères dans une chaîne"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTHÈQUE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Bibliothèque C standard (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>strings.hE<gt>>\n"
msgstr "B<#include E<lt>strings.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"B<[[deprecated]] char *index(const char *>I<s>B<, int >I<c>B<);>\n"
"B<[[deprecated]] char *rindex(const char *>I<s>B<, int >I<c>B<);>\n"
msgstr ""
"B<[[obsolète]] char *index(const char *>I<s>B<, int >I<c>B<);>\n"
"B<[[obsolète]] char *rindex(const char *>I<s>B<, int >I<c>B<);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "B<index>()  is identical to B<strchr>(3)."
msgstr "B<index>() est identique à B<strchr>(3)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "B<rindex>()  is identical to B<strrchr>(3)."
msgstr "B<rindex>() est identique à B<strrchr>(3)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Use B<strchr>(3)  and B<strrchr>(3)  instead of these functions."
msgstr "Utiliser B<strchr>(3) et B<strrchr>(3) plutôt que ces functions."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDS"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "None."
msgstr "Aucune."

#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "HISTORIQUE"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid ""
"4.3BSD; marked as LEGACY in POSIX.1-2001.  Removed in POSIX.1-2008, "
"recommending B<strchr>(3)  and B<strrchr>(3)  instead."
msgstr ""
"4.3BSD ; marquée « LEGACY » dans POSIX.1-2001. Supprimé dans POSIX.1-2008 "
"qui recommande à la place B<strchr>(3) et B<strrchr>(3)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "B<strchr>(3), B<strrchr>(3)"
msgstr "B<strchr>(3), B<strrchr>(3)"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "2023-01-05"
msgstr "5 janvier 2023"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Pages du manuel de Linux 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid ""
"4.3BSD; marked as LEGACY in POSIX.1-2001.  POSIX.1-2008 removes the "
"specifications of B<index>()  and B<rindex>(), recommending B<strchr>(3)  "
"and B<strrchr>(3)  instead."
msgstr ""
"4.3BSD ; marquée « LEGACY » dans POSIX.1-2001. POSIX.1-2008 supprime les "
"spécifications de B<index>() et B<rindex>() et recommande à la place  "
"B<strchr>(3) et B<strrchr>(3)."

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.05"
msgstr "Pages du manuel de Linux 6.05"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "INDEX"
msgstr "INDEX"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2015-03-02"
msgstr "2 mars 2015"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "GNU"
msgstr "GNU"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Manuel du programmeur Linux"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid "B<char *index(const char *>I<s>B<, int >I<c>B<);>\n"
msgstr "B<char *index(const char *>I<s>B<, int >I<c>B<);>\n"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid "B<char *rindex(const char *>I<s>B<, int >I<c>B<);>\n"
msgstr "B<char *rindex(const char *>I<s>B<, int >I<c>B<);>\n"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"The B<index>()  function returns a pointer to the first occurrence of the "
"character I<c> in the string I<s>."
msgstr ""
"La fonction B<index>() renvoie un pointeur sur la première occurrence du "
"caractère I<c> dans la chaîne I<s>."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"The B<rindex>()  function returns a pointer to the last occurrence of the "
"character I<c> in the string I<s>."
msgstr ""
"La fonction B<rindex>() renvoie un pointeur sur la dernière occurrence du "
"caractère I<c> dans la chaîne I<s>."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"The terminating null byte (\\(aq\\e0\\(aq) is considered to be a part of the "
"strings."
msgstr ""
"Le caractère nul final (« \\e0 ») est considéré comme faisant partie de la "
"chaîne."

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALEUR RENVOYÉE"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"The B<index>()  and B<rindex>()  functions return a pointer to the matched "
"character or NULL if the character is not found."
msgstr ""
"Les fonctions B<index>() et B<rindex>() renvoient un pointeur sur le "
"caractère correspondant, ou NULL si le caractère n'est pas trouvé."

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATTRIBUTS"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Pour une explication des termes utilisés dans cette section, consulter "
"B<attributes>(7)."

#. type: tbl table
#: opensuse-leap-15-6
#, no-wrap
msgid "Interface"
msgstr "Interface"

#. type: tbl table
#: opensuse-leap-15-6
#, no-wrap
msgid "Attribute"
msgstr "Attribut"

#. type: tbl table
#: opensuse-leap-15-6
#, no-wrap
msgid "Value"
msgstr "Valeur"

#. type: tbl table
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"B<index>(),\n"
"B<rindex>()"
msgstr ""
"B<index>(),\n"
"B<rindex>()"

#. type: tbl table
#: opensuse-leap-15-6
#, no-wrap
msgid "Thread safety"
msgstr "Sécurité des threads"

#. type: tbl table
#: opensuse-leap-15-6
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "CONFORMING TO"
msgstr "CONFORMITÉ"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"B<memchr>(3), B<strchr>(3), B<string>(3), B<strpbrk>(3), B<strrchr>(3), "
"B<strsep>(3), B<strspn>(3), B<strstr>(3), B<strtok>(3)"
msgstr ""
"B<memchr>(3), B<strchr>(3), B<string>(3), B<strpbrk>(3), B<strrchr>(3), "
"B<strsep>(3), B<strspn>(3), B<strstr>(3), B<strtok>(3)"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "COLOPHON"
msgstr "COLOPHON"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Cette page fait partie de la publication 4.16 du projet I<man-pages> Linux. "
"Une description du projet et des instructions pour signaler des anomalies et "
"la dernière version de cette page peuvent être trouvées à l'adresse \\"
"%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Pages du manuel de Linux 6.04"
