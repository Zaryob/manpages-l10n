# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Christophe Blaess <https://www.blaess.fr/christophe/>, 1996-2003.
# Stéphan Rafin <stephan.rafin@laposte.net>, 2002.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999, 2002.
# François Micaux, 2002.
# Alain Portal <aportal@univ-montp2.fr>, 2003-2008.
# Jean-Philippe Guérard <fevrier@tigreraye.org>, 2005-2006.
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006-2007.
# Julien Cristau <jcristau@debian.org>, 2006-2007.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006-2008.
# Nicolas François <nicolas.francois@centraliens.net>, 2006-2008.
# Florentin Duneau <fduneau@gmail.com>, 2006-2010.
# Simon Paillard <simon.paillard@resel.enst-bretagne.fr>, 2006.
# Denis Barbier <barbier@debian.org>, 2006, 2010.
# David Prévot <david@tilapin.org>, 2010-2012, 2014.
# Frédéric Hantrais <fhantrais@gmail.com>, 2013, 2014.
msgid ""
msgstr ""
"Project-Id-Version: perkamon\n"
"POT-Creation-Date: 2023-08-27 17:24+0200\n"
"PO-Revision-Date: 2023-04-03 18:54+0200\n"
"Last-Translator: Grégoire Scano <gregoire.scano@malloc.fr>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Lokalize 22.12.1\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "strsep"
msgstr "strsep"

#. type: TH
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid "2023-07-20"
msgstr "20 juillet 2023"

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Pages du manuel de Linux 6.05.01"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "strsep - extract token from string"
msgstr "strsep - Extraction de séquence d'une chaîne"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTHÈQUE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Bibliothèque C standard (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>string.hE<gt>>\n"
msgstr "B<#include E<lt>string.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<char *strsep(char **restrict >I<stringp>B<, const char *restrict >I<delim>B<);>\n"
msgstr "B<char *strsep(char **restrict >I<stringp>B<, const char *restrict >I<delim>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Exigences de macros de test de fonctionnalités pour la glibc (consulter "
"B<feature_test_macros>(7)) :"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "B<strsep>():"
msgstr "B<strsep>() :"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"    Since glibc 2.19:\n"
"        _DEFAULT_SOURCE\n"
"    glibc 2.19 and earlier:\n"
"        _BSD_SOURCE\n"
msgstr ""
"    Depuis la glibc 2.19 :\n"
"        _DEFAULT_SOURCE\n"
"    glibc 2.19 et antérieures :\n"
"        _BSD_SOURCE\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"If I<*stringp> is NULL, the B<strsep>()  function returns NULL and does "
"nothing else.  Otherwise, this function finds the first token in the string "
"I<*stringp> that is delimited by one of the bytes in the string I<delim>.  "
"This token is terminated by overwriting the delimiter with a null byte "
"(\\[aq]\\e0\\[aq]), and I<*stringp> is updated to point past the token.  In "
"case no delimiter was found, the token is taken to be the entire string "
"I<*stringp>, and I<*stringp> is made NULL."
msgstr ""
"Si I<*stringp> est NULL, la fonction B<strsep>() renvoie NULL et ne fait "
"rien d'autre. Dans le cas contraire, cette fonction recherche la première "
"séquence (token) de la chaîne I<*stringp> qui est délimitée par l'un des "
"octets de la chaîne I<delim>. La séquence renvoyée est terminée en écrasant "
"le séparateur avec un octet NULL (« \\e0 ») et I<*stringp> est mis à jour "
"pour pointer après la séquence. Dans le cas où aucun séparateur n'est "
"trouvé, la séquence extraite est constituée de toute la chaîne I<*stringp>, "
"et I<*stringp> vaut NULL."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALEUR RENVOYÉE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<strsep>()  function returns a pointer to the token, that is, it "
"returns the original value of I<*stringp>."
msgstr ""
"La fonction B<strsep>() renvoie un pointeur sur la séquence, c'est-à-dire la "
"valeur originelle de I<*stringp>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATTRIBUTS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Pour une explication des termes utilisés dans cette section, consulter "
"B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Interface"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Attribut"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Valeur"

#. type: tbl table
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<strsep>()"
msgstr "B<strsep>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Sécurité des threads"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDS"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "None."
msgstr ""

#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "HISTORIQUE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "4.4BSD."
msgstr "4.4BSD."

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<strsep>()  function was introduced as a replacement for B<strtok>(3), "
"since the latter cannot handle empty fields.  However, B<strtok>(3)  "
"conforms to C89/C99 and hence is more portable."
msgstr ""
"La fonction B<strsep>() a été introduite en remplacement de B<strtok>(3), "
"qui ne peut pas traiter les champs vides. Néanmoins, B<strtok>(3) est "
"conforme à C89/C99 et est donc plus portable."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "BOGUES"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Be cautious when using this function.  If you do use it, note that:"
msgstr ""
"Faites attention quand vous utilisez cette fonction. Si vous l'utilisez, "
"prenez note des informations suivantes\\ :"

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "\\[bu]"
msgstr "-"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "This function modifies its first argument."
msgstr "Cette fonction modifie son premier argument."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "This function cannot be used on constant strings."
msgstr "Cette fonction ne peut pas être utilisée avec des chaînes constantes."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The identity of the delimiting character is lost."
msgstr "L’identité du caractère délimiteur est perdue."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "EXEMPLES"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The program below is a port of the one found in B<strtok>(3), which, "
"however, doesn't discard multiple delimiters or empty tokens:"
msgstr ""
"Le programme ci-dessous est un portage d'un programme trouvé dans "
"B<strtok>(3), qui néanmoins n'adandonne pas délimiteurs multiples ou de "
"séquences vides :"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"$B< ./a.out \\[aq]a/bbb///cc;xxx:yyy:\\[aq] \\[aq]:;\\[aq] \\[aq]/\\[aq]>\n"
"1: a/bbb///cc\n"
"         --E<gt> a\n"
"         --E<gt> bbb\n"
"         --E<gt>\n"
"         --E<gt>\n"
"         --E<gt> cc\n"
"2: xxx\n"
"         --E<gt> xxx\n"
"3: yyy\n"
"         --E<gt> yyy\n"
"4:\n"
"         --E<gt>\n"
msgstr ""
"$B< ./a.out \\[aq]a/bbb///cc;xxx:yyy:\\[aq] \\[aq]:;\\[aq] \\[aq]/\\[aq]>\n"
"1: a/bbb///cc\n"
"         --E<gt> a\n"
"         --E<gt> bbb\n"
"         --E<gt>\n"
"         --E<gt>\n"
"         --E<gt> cc\n"
"2: xxx\n"
"         --E<gt> xxx\n"
"3: yyy\n"
"         --E<gt> yyy\n"
"4:\n"
"         --E<gt>\n"

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "Program source"
msgstr "Source du programme"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid ""
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>string.hE<gt>\n"
"\\&\n"
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    char *token, *subtoken;\n"
"\\&\n"
"    if (argc != 4) {\n"
"        fprintf(stderr, \"Usage: %s string delim subdelim\\en\", argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    for (unsigned int j = 1; (token = strsep(&argv[1], argv[2])); j++) {\n"
"        printf(\"%u: %s\\en\", j, token);\n"
"\\&\n"
"        while ((subtoken = strsep(&token, argv[3])))\n"
"            printf(\"\\et --E<gt> %s\\en\", subtoken);\n"
"    }\n"
"\\&\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""

#. #-#-#-#-#  archlinux: strsep.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  debian-bookworm: strsep.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  debian-unstable: strsep.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  fedora-39: strsep.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  fedora-rawhide: strsep.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  mageia-cauldron: strsep.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  opensuse-leap-15-6: strsep.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#. #-#-#-#-#  opensuse-tumbleweed: strsep.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"B<memchr>(3), B<strchr>(3), B<string>(3), B<strpbrk>(3), B<strspn>(3), "
"B<strstr>(3), B<strtok>(3)"
msgstr ""
"B<memchr>(3), B<strchr>(3), B<string>(3), B<strpbrk>(3), B<strspn>(3), "
"B<strstr>(3), B<strtok>(3)"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "2023-02-05"
msgstr "5 février 2023"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Pages du manuel de Linux 6.03"

#. type: SH
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "NOTES"
msgstr "NOTES"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron
msgid ""
"The B<strsep>()  function was introduced as a replacement for B<strtok>(3), "
"since the latter cannot handle empty fields.  However, B<strtok>(3)  "
"conforms to C99 and hence is more portable."
msgstr ""
"La fonction B<strsep>() a été introduite en remplacement de B<strtok>(3), "
"qui ne peut pas traiter les champs vides. Néanmoins, B<strtok>(3) est "
"conforme à C99 et est donc plus portable."

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>string.hE<gt>\n"
msgstr ""
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>string.hE<gt>\n"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    char *token, *subtoken;\n"
msgstr ""
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    char *token, *subtoken;\n"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"    if (argc != 4) {\n"
"        fprintf(stderr, \"Usage: %s string delim subdelim\\en\", argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
msgstr ""
"    if (argc != 4) {\n"
"        fprintf(stderr, \"Usage: %s string delim subdelim\\en\", argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"    for (unsigned int j = 1; (token = strsep(&argv[1], argv[2])); j++) {\n"
"        printf(\"%u: %s\\en\", j, token);\n"
msgstr ""
"    for (unsigned int j = 1; (token = strsep(&argv[1], argv[2])); j++) {\n"
"        printf(\"%u: %s\\en\", j, token);\n"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"        while ((subtoken = strsep(&token, argv[3])))\n"
"            printf(\"\\et --E<gt> %s\\en\", subtoken);\n"
"    }\n"
msgstr ""
"        while ((subtoken = strsep(&token, argv[3])))\n"
"            printf(\"\\et --E<gt> %s\\en\", subtoken);\n"
"    }\n"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""
"    exit(EXIT_SUCCESS);\n"
"}\n"

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.05"
msgstr "Pages du manuel de Linux 6.05"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "STRSEP"
msgstr "STRSEP"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2016-03-15"
msgstr "15 mars 2016"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "GNU"
msgstr "GNU"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Manuel du programmeur Linux"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid "B<char *strsep(char **>I<stringp>B<, const char *>I<delim>B<);>\n"
msgstr "B<char *strsep(char **>I<stringp>B<, const char *>I<delim>B<);>\n"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"B<strsep>():\n"
"    Since glibc 2.19:\n"
"        _DEFAULT_SOURCE\n"
"    Glibc 2.19 and earlier:\n"
"        _BSD_SOURCE\n"
msgstr ""
"B<strsep>():\n"
"    Depuis la glibc 2.19 :\n"
"        _DEFAULT_SOURCE\n"
"    glibc 2.19 et antérieures :\n"
"        _BSD_SOURCE\n"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"If I<*stringp> is NULL, the B<strsep>()  function returns NULL and does "
"nothing else.  Otherwise, this function finds the first token in the string "
"I<*stringp>, that is delimited by one of the bytes in the string I<delim>.  "
"This token is terminated by overwriting the delimiter with a null byte "
"(\\(aq\\e0\\(aq), and I<*stringp> is updated to point past the token.  In "
"case no delimiter was found, the token is taken to be the entire string "
"I<*stringp>, and I<*stringp> is made NULL."
msgstr ""
"Si I<*stringp> est NULL, la fonction B<strsep>() renvoie NULL et ne fait "
"rien d'autre. Dans le cas contraire, cette fonction recherche la première "
"séquence (token) de la chaîne I<*stringp> qui est délimitée par l'un des "
"octets de la chaîne I<delim>. La séquence renvoyée est terminée en écrasant "
"le séparateur avec un octet NULL (« \\e0 ») et I<*stringp> est mis à jour "
"pour pointer après la séquence. Dans le cas où aucun séparateur n'est "
"trouvé, la séquence extraite est constituée de toute la chaîne I<*stringp>, "
"et I<*stringp> vaut NULL."

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "CONFORMING TO"
msgstr "CONFORMITÉ"

#. type: IP
#: opensuse-leap-15-6
#, no-wrap
msgid "*"
msgstr "*"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"B<index>(3), B<memchr>(3), B<rindex>(3), B<strchr>(3), B<string>(3), "
"B<strpbrk>(3), B<strspn>(3), B<strstr>(3), B<strtok>(3)"
msgstr ""
"B<index>(3), B<memchr>(3), B<rindex>(3), B<strchr>(3), B<string>(3), "
"B<strpbrk>(3), B<strspn>(3), B<strstr>(3), B<strtok>(3)"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "COLOPHON"
msgstr "COLOPHON"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Cette page fait partie de la publication 4.16 du projet I<man-pages> Linux. "
"Une description du projet et des instructions pour signaler des anomalies et "
"la dernière version de cette page peuvent être trouvées à l'adresse \\"
"%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30 mars 2023"

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Pages du manuel de Linux 6.04"
