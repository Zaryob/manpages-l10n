# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Christophe Blaess <https://www.blaess.fr/christophe/>, 1996-2003.
# Stéphan Rafin <stephan.rafin@laposte.net>, 2002.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999, 2002.
# François Micaux, 2002.
# Alain Portal <aportal@univ-montp2.fr>, 2003-2008.
# Jean-Philippe Guérard <fevrier@tigreraye.org>, 2005-2006.
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006-2007.
# Julien Cristau <jcristau@debian.org>, 2006-2007.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006-2008.
# Nicolas François <nicolas.francois@centraliens.net>, 2006-2008.
# Florentin Duneau <fduneau@gmail.com>, 2006-2010.
# Simon Paillard <simon.paillard@resel.enst-bretagne.fr>, 2006.
# Denis Barbier <barbier@debian.org>, 2006, 2010.
# David Prévot <david@tilapin.org>, 2010-2014.
# Lucien Gentis <lucien.gentis@waika9.com>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: perkamon\n"
"POT-Creation-Date: 2023-08-27 16:59+0200\n"
"PO-Revision-Date: 2023-05-23 15:41+0200\n"
"Last-Translator: Lucien Gentis <lucien.gentis@waika9.com>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Poedit 2.4.2\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "getmntent"
msgstr "getmntent"

#. type: TH
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid "2023-07-20"
msgstr "20 juillet 2023"

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Pages du manuel de Linux 6.05.01"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"getmntent, setmntent, addmntent, endmntent, hasmntopt, getmntent_r - get "
"filesystem descriptor file entry"
msgstr ""
"getmntent, setmntent, addmntent, endmntent, hasmntopt, getmntent_r - Obtenir "
"des descriptions d'un système de fichiers"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTHÈQUE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Bibliothèque C standard (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<#include E<lt>stdio.hE<gt>>\n"
"B<#include E<lt>mntent.hE<gt>>\n"
msgstr ""
"B<#include E<lt>stdio.hE<gt>>\n"
"B<#include E<lt>mntent.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<FILE *setmntent(const char *>I<filename>B<, const char *>I<type>B<);>\n"
msgstr "B<FILE *setmntent(const char *>I<nom_fichier>B<, const char *>I<type>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<struct mntent *getmntent(FILE *>I<stream>B<);>\n"
msgstr "B<struct mntent *getmntent(FILE *>I<flux>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"B<int addmntent(FILE *restrict >I<stream>B<,>\n"
"B<              const struct mntent *restrict >I<mnt>B<);>\n"
msgstr ""
"B<int addmntent(FILE *restrict >I<flux>B<,>\n"
"B<              const struct mntent *restrict >I<mnt>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<int endmntent(FILE *>I<streamp>B<);>\n"
msgstr "B<int endmntent(FILE *>I<streamp>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<char *hasmntopt(const struct mntent *>I<mnt>B<, const char *>I<opt>B<);>\n"
msgstr "B<char *hasmntopt(const struct mntent *>I<mnt>B<, const char *>I<opt>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"/* GNU extension */\n"
"B<#include E<lt>mntent.hE<gt>>\n"
msgstr ""
"/* GNU extension */\n"
"B<#include E<lt>mntent.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"B<struct mntent *getmntent_r(FILE *restrict >I<streamp>B<,>\n"
"B<              struct mntent *restrict >I<mntbuf>B<,>\n"
"B<              char >I<buf>B<[restrict .>I<buflen>B<], int >I<buflen>B<);>\n"
msgstr ""
"B<struct mntent *getmntent_r(FILE *restrict >I<streamp>B<,>\n"
"B<              struct mntent *restrict >I<tampon_mnt>B<,>\n"
"B<              char >I<tampon>B<[restrict .>I<taille_tampon>B<], int >I<taille_tampon>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Exigences de macros de test de fonctionnalités pour la glibc (consulter "
"B<feature_test_macros>(7)) :"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "B<getmntent_r>():"
msgstr "B<getmntent_r>() :"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"    Since glibc 2.19:\n"
"        _DEFAULT_SOURCE\n"
"    glibc 2.19 and earlier:\n"
"        _BSD_SOURCE || _SVID_SOURCE\n"
msgstr ""
"    Depuis la glibc 2.19 :\n"
"        _DEFAULT_SOURCE\n"
"    glibc 2.19 et antérieures :\n"
"        _BSD_SOURCE || _SVID_SOURCE\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"These routines are used to access the filesystem description file I</etc/"
"fstab> and the mounted filesystem description file I</etc/mtab>."
msgstr ""
"Ces routines permettent d'accéder au fichier de description des systèmes de "
"fichiers I</etc/fstab> et au fichier de description des systèmes de fichiers "
"montés I</etc/mtab>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The B<setmntent>()  function opens the filesystem description file "
"I<filename> and returns a file pointer which can be used by B<getmntent>().  "
"The argument I<type> is the type of access required and can take the same "
"values as the I<mode> argument of B<fopen>(3).  The returned stream should "
"be closed using B<endmntent>()  rather than B<fclose>(3)."
msgstr ""
"La fonction B<setmntent>() ouvre le fichier de description de systèmes de "
"fichiers I<nom_fichier> et renvoie un pointeur de fichier utilisable par "
"B<getmntent>(). L'argument I<type> est le type d'accès demandé, et peut "
"prendre les même valeurs que l'argument I<mode> de B<fopen>(3). Pour fermer "
"le flux renvoyé, il est préférable d'utiliser B<endmntent>() plutôt que "
"B<fclose>(3)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<getmntent>()  function reads the next line of the filesystem "
"description file from I<stream> and returns a pointer to a structure "
"containing the broken out fields from a line in the file.  The pointer "
"points to a static area of memory which is overwritten by subsequent calls "
"to B<getmntent>()."
msgstr ""
"La fonction B<getmntent>() lit la ligne suivante du fichier de description "
"de systèmes de fichiers sur I<flux> et renvoie un pointeur vers une "
"structure contenant une ligne de ce fichier décomposée en champs. Il s'agit "
"d'un pointeur vers une zone de données statiques, dont le contenu est "
"écrasée par des appels ultérieurs à B<getmntent>()."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<addmntent>()  function adds the I<mntent> structure I<mnt> to the end "
"of the open I<stream>."
msgstr ""
"La fonction B<addmntent>() ajoute la structure I<mntent> I<mnt> à la fin du "
"I<flux> ouvert."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<endmntent>()  function closes the I<stream> associated with the "
"filesystem description file."
msgstr ""
"La fonction B<endmntent>() ferme le I<flux> associé au fichier de "
"description de systèmes de fichiers."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<hasmntopt>()  function scans the I<mnt_opts> field (see below)  of the "
"I<mntent> structure I<mnt> for a substring that matches I<opt>.  See "
"I<E<lt>mntent.hE<gt>> and B<mount>(8)  for valid mount options."
msgstr ""
"La fonction B<hasmntopt>() parcourt le champ I<mnt_opts> (voir plus bas) de "
"la structure I<mntent> I<mnt> à la recherche d'une chaîne correspondant à "
"I<opt>. Voir I<E<lt>mntent.hE<gt>> et B<mount>(8) pour les options de "
"montage valables."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The reentrant B<getmntent_r>()  function is similar to B<getmntent>(), but "
"stores the I<mntent> structure in the provided I<*mntbuf>, and stores the "
"strings pointed to by the entries in that structure in the provided array "
"I<buf> of size I<buflen>."
msgstr ""
"La fonction réentrante B<getmntent_r>() est similaire à B<getmntent>(), mais "
"enregistre la structure I<mntent> dans le I<*tampon_mnt> fourni et "
"enregistre les chaînes pointées par les entrées de cette structure dans le "
"I<tampon> fourni de taille I<taille_tampon>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The I<mntent> structure is defined in I<E<lt>mntent.hE<gt>> as follows:"
msgstr ""
"La structure I<mntent> est définie dans I<E<lt>mntent.hE<gt>> ainsi\\ :"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"struct mntent {\n"
"    char *mnt_fsname;   /* name of mounted filesystem */\n"
"    char *mnt_dir;      /* filesystem path prefix */\n"
"    char *mnt_type;     /* mount type (see mntent.h) */\n"
"    char *mnt_opts;     /* mount options (see mntent.h) */\n"
"    int   mnt_freq;     /* dump frequency in days */\n"
"    int   mnt_passno;   /* pass number on parallel fsck */\n"
"};\n"
msgstr ""
"struct mntent {\n"
"    char *mnt_fsname;   /* Nom du système de fichiers monté */\n"
"    char *mnt_dir;      /* Chemin d'accès au système de fichiers */\n"
"    char *mnt_type;     /* Type de montage (voir mntent.h) */\n"
"    char *mnt_opts;     /* Options de montage (voir mntent.h) */\n"
"    int   mnt_freq;     /* Fréquence de sauvegarde (en jour) */\n"
"    int   mnt_passno;   /* Ordre de passage dans fsck */\n"
"};\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Since fields in the mtab and fstab files are separated by whitespace, octal "
"escapes are used to represent the characters space (\\e040), tab (\\e011), "
"newline (\\e012), and backslash (\\e\\e) in those files when they occur in "
"one of the four strings in a I<mntent> structure.  The routines "
"B<addmntent>()  and B<getmntent>()  will convert from string representation "
"to escaped representation and back.  When converting from escaped "
"representation, the sequence \\e134 is also converted to a backslash."
msgstr ""
"Comme les champs dans les fichiers mtab et fstab sont séparés par des "
"espaces, des échappements en valeurs octales sont utilisés pour représenter "
"les caractères espace (\\e040), tabulation (\\e011), nouvelle ligne (\\e012) "
"et barre oblique inverse (\\e\\e) dans ces fichiers lorsqu'ils apparaissent "
"dans l'une des quatre chaînes d'une structure I<mntent>. Les routines "
"B<addmntent>() et B<getmntent>() convertiront la représentation des chaînes "
"en représentations échappées et vice-versa. Lors de la conversion vers une "
"représentation échappée, la séquence \\e134 est aussi convertie en barre "
"oblique inverse."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALEUR RENVOYÉE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<getmntent>()  and B<getmntent_r>()  functions return a pointer to the "
"I<mntent> structure or NULL on failure."
msgstr ""
"Les fonctions B<getmntent>() et B<getmntent_r>() renvoient un pointeur sur "
"la structure de type I<mntent>, ou B<NULL> en cas d'échec."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The B<addmntent>()  function returns 0 on success and 1 on failure."
msgstr ""
"La fonction B<addmntent>() renvoie B<0> si elle réussit et B<1> si elle "
"échoue."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The B<endmntent>()  function always returns 1."
msgstr "La fonction B<endmntent>() renvoie toujours B<1>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<hasmntopt>()  function returns the address of the substring if a match "
"is found and NULL otherwise."
msgstr ""
"La fonction B<hasmntopt>() renvoie l'adresse de la sous-chaîne si une "
"correspondance est trouvée, ou B<NULL> dans le cas contraire."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "FILES"
msgstr "FICHIERS"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "I</etc/fstab>"
msgstr "I</etc/fstab>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "filesystem description file"
msgstr "fichier de description des systèmes de fichiers"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "I</etc/mtab>"
msgstr "I</etc/mtab>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "mounted filesystem description file"
msgstr "fichier de description des systèmes de fichiers montés"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATTRIBUTS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Pour une explication des termes utilisés dans cette section, consulter "
"B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Interface"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Attribut"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Valeur"

#. type: tbl table
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<setmntent>(),\n"
"B<endmntent>(),\n"
"B<hasmntopt>()"
msgstr ""
"B<setmntent>(),\n"
"B<endmntent>(),\n"
"B<hasmntopt>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Sécurité des threads"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<getmntent>()"
msgstr "B<getmntent>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "MT-Unsafe race:mntentbuf locale"
msgstr "MT-Unsafe race:mntentbuf locale"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<addmntent>()"
msgstr "B<addmntent>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe race:stream locale"
msgstr "MT-Safe race:stream locale"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<getmntent_r>()"
msgstr "B<getmntent_r>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe locale"
msgstr "MT-Safe locale"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDS"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "None."
msgstr "Aucune."

#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "HISTORIQUE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The nonreentrant functions are from SunOS 4.1.3.  A routine "
"B<getmntent_r>()  was introduced in HP-UX 10, but it returns an I<int>.  The "
"prototype shown above is glibc-only."
msgstr ""
"Les fonctions non réentrantes viennent de SunOS\\ 4.1.3. Une routine "
"B<getmntent_r>() a été introduite dans HP-UX\\ 10, mais elle renvoie un "
"entier. Le prototype ci-dessus n'est fourni que par la glibc."

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide
#, fuzzy
#| msgid ""
#| "System V also has a B<getmntent>()  function but the calling sequence "
#| "differs, and the returned structure is different.  Under System V I</etc/"
#| "mnttab> is used.  4.4BSD and Digital UNIX have a routine B<getmntinfo>(), "
#| "a wrapper around the system call B<getfsstat>()."
msgid ""
"System V also has a B<getmntent>()  function but the calling sequence "
"differs, and the returned structure is different.  Under System V I</etc/"
"mnttab> is used.  4.4BSD and Digital UNIX have a routine B<\\%getmntinfo>(), "
"a wrapper around the system call B<getfsstat>()."
msgstr ""
"System V a aussi une fonction B<getmntent>(), mais l'appel est différent, "
"ainsi que la structure renvoyée. Sous System V, le fichier I</etc/mnttab> "
"est utilisé. BSD\\ 4.4 et Digital UNIX ont une routine B<getmntinfo>() qui "
"sert à invoquer l'appel système B<getfsstat>()."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<fopen>(3), B<fstab>(5), B<mount>(8)"
msgstr "B<fopen>(3), B<fstab>(5), B<mount>(8)"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "2023-02-05"
msgstr "5 février 2023"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Pages du manuel de Linux 6.03"

#. type: SH
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "NOTES"
msgstr "NOTES"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"System V also has a B<getmntent>()  function but the calling sequence "
"differs, and the returned structure is different.  Under System V I</etc/"
"mnttab> is used.  4.4BSD and Digital UNIX have a routine B<getmntinfo>(), a "
"wrapper around the system call B<getfsstat>()."
msgstr ""
"System V a aussi une fonction B<getmntent>(), mais l'appel est différent, "
"ainsi que la structure renvoyée. Sous System V, le fichier I</etc/mnttab> "
"est utilisé. BSD\\ 4.4 et Digital UNIX ont une routine B<getmntinfo>() qui "
"sert à invoquer l'appel système B<getfsstat>()."

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.05"
msgstr "Pages du manuel de Linux 6.05"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "GETMNTENT"
msgstr "GETMNTENT"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2017-09-15"
msgstr "15 septembre 2017"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Manuel du programmeur Linux"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid "B<int addmntent(FILE *>I<stream>B<, const struct mntent *>I<mnt>B<);>\n"
msgstr "B<int addmntent(FILE *>I<flux>B<, const struct mntent *>I<mnt>B<);>\n"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"B<struct mntent *getmntent_r(FILE *>I<streamp>B<, struct mntent *>I<mntbuf>B<,>\n"
"B<                           char *>I<buf>B<, int >I<buflen>B<);>\n"
msgstr ""
"B<struct mntent *getmntent_r(FILE *>I<streamp>B<, struct mntent *>I<tampon_mnt>B<,>\n"
"B<                           char *>I<tampon>B<, int >I<taille_tampon>B<);>\n"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"B<getmntent_r>():\n"
"    Since glibc 2.19:\n"
"        _DEFAULT_SOURCE\n"
"    Glibc 2.19 and earlier:\n"
"        _BSD_SOURCE || _SVID_SOURCE\n"
msgstr ""
"B<getmntent_r>():\n"
"    Depuis la version B<2.19> de la glibc :\n"
"        _DEFAULT_SOURCE\n"
"    Versions de la Glibc antérieures à B<2.19> :\n"
"        _BSD_SOURCE || _SVID_SOURCE\n"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"The B<setmntent>()  function opens the filesystem description file "
"I<filename> and returns a file pointer which can be used by B<getmntent>().  "
"The argument I<type> is the type of access required and can take the same "
"values as the I<mode> argument of B<fopen>(3)."
msgstr ""
"La fonction B<setmntent>() ouvre le fichier de description des systèmes de "
"fichiers I<nom_fichier> et renvoie un pointeur de fichier utilisable par "
"B<getmntent>(). L'argument I<type> est le type d'accès demandé, et peut "
"prendre les même valeurs que l'argument I<mode> de B<fopen>(3)."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"The reentrant B<getmntent_r>()  function is similar to B<getmntent>(), but "
"stores the I<struct mount> in the provided I<*mntbuf> and stores the strings "
"pointed to by the entries in that struct in the provided array I<buf> of "
"size I<buflen>."
msgstr ""
"La fonction réentrante B<getmntent_r>() est similaire à B<getmntent>(), mais "
"enregistre la structure I<mntent> dans le I<tampon_mnt> fourni et enregistre "
"les chaînes pointées par les entrées de cette structure dans le I<tampon> "
"fourni de taille I<taille_tampon>."

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "CONFORMING TO"
msgstr "CONFORMITÉ"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"The nonreentrant functions are from SunOS 4.1.3.  A routine "
"B<getmntent_r>()  was introduced in HP-UX 10, but it returns an int.  The "
"prototype shown above is glibc-only."
msgstr ""
"Les fonctions non réentrantes viennent de SunOS\\ 4.1.3. Une routine "
"B<getmntent_r>() a été introduite dans HP-UX\\ 10, mais elle renvoie un "
"entier. Le prototype ci-dessus n'est fourni que par la glibc."

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "COLOPHON"
msgstr "COLOPHON"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Cette page fait partie de la publication 4.16 du projet I<man-pages> Linux. "
"Une description du projet et des instructions pour signaler des anomalies et "
"la dernière version de cette page peuvent être trouvées à l'adresse \\"
"%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30 mars 2023"

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Pages du manuel de Linux 6.04"
