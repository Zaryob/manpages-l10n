# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Guillaume Delacour <guillaume.delacour@gmail.com>
# Nicolas Haller <nicolas@boiteameuh.org>
# Jean-Marc Chaton <chaton@debian.org>
# Sylvain Archenault <sylvain.archenault@laposte.net>
# Julien Cristau <jcristau@debian.org>
# Thomas Huriaux <thomas.huriaux@gmail.com>
# Franck Bassi <fblinux@wanadoo.fr>
# Valéry Perrin <valery.perrin.debian@free.fr>
# Guilhelm Panaget <guilhelm.panaget@free.fr>
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>
# Simon Paillard <simon.paillard@resel.enst-bretagne.fr>
# Luc Froidefond <luc.froidefond@free.fr>
# Steve Petruzzello <dlist@bluewin.ch>
# Julien Rosal <albator.deb@gmail.com>
# Grégory Colpart <reg@evolix.fr>
# Cédric Lucantis <omer@no-log.org>
# Nicolas François <nicolas.francois@centraliens.net>, 2008-2010.
# Bastien Scher <bastien0705@gmail.com>, 2011-2013.
# David Prévot <david@tilapin.org>, 2012-2014.
# Jean-Pierre Giraud <jean-pierregiraud@neuf.fr>, 2020.
msgid ""
msgstr ""
"Project-Id-Version: coreutils manpages\n"
"POT-Creation-Date: 2023-10-02 11:51+0200\n"
"PO-Revision-Date: 2022-10-16 00:26+0200\n"
"Last-Translator: Jean-Pierre Giraud <jean-pierregiraud@neuf.fr>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bits\n"
"Plural-Forms: nplurals=2; plural=(n!=1);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "BASENC"
msgstr "BASENC"

#. type: TH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "September 2023"
msgstr "Septembre 2023"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "GNU coreutils 9.4"
msgstr "GNU coreutils 9.4"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr "Commandes de l'utilisateur"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "basenc - Encode/decode data and print to standard output"
msgstr ""
"basenc - Encoder ou décoder des données et les afficher sur la sortie "
"standard"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<basenc> [I<\\,OPTION\\/>]... [I<\\,FILE\\/>]"
msgstr "B<basenc> [I<\\,OPTION\\/>]... [I<\\,FICHIER\\/>]"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "basenc encode or decode FILE, or standard input, to standard output."
msgstr ""
"B<baseNC> encode ou décode le I<FICHIER> ou l'entrée standard, et les "
"affiche sur la sortie standard."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "With no FILE, or when FILE is -, read standard input."
msgstr ""
"L'entrée standard est lue quand I<FICHIER> est omis ou quand I<FICHIER> vaut "
"« - »."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Mandatory arguments to long options are mandatory for short options too."
msgstr ""
"Les paramètres obligatoires pour les options de forme longue le sont aussi "
"pour les options de forme courte."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--base64>"
msgstr "B<--base64>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "same as 'base64' program (RFC4648 section 4)"
msgstr "semblable au programme « base64 » (RFC 4648 section 4)"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--base64url>"
msgstr "B<--base64url>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "file- and url-safe base64 (RFC4648 section 5)"
msgstr ""
"base64 compatible avec les noms de fichiers et les URL (RFC 4648 section 5)"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--base32>"
msgstr "B<--base32>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "same as 'base32' program (RFC4648 section 6)"
msgstr "semblable au programme « base32 » (RFC 4648 section 6)"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--base32hex>"
msgstr "B<--base32hex>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "extended hex alphabet base32 (RFC4648 section 7)"
msgstr "base32 avec alphabet hexadécimal étendu (RFC 4648 section 7)"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--base16>"
msgstr "B<--base16>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "hex encoding (RFC4648 section 8)"
msgstr "encodage hexadécimal (RFC 4648 section 8)"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--base2msbf>"
msgstr "B<--base2msbf>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "bit string with most significant bit (msb) first"
msgstr "chaîne de bits avec le bit de poids fort (msb) en premier"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--base2lsbf>"
msgstr "B<--base2lsbf>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "bit string with least significant bit (lsb) first"
msgstr "chaîne de bits avec le bit de poids faible (lsb) en premier"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-d>, B<--decode>"
msgstr "B<-d>, B<--decode>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "decode data"
msgstr "décoder les données"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-i>, B<--ignore-garbage>"
msgstr "B<-i>, B<--ignore-garbage>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "when decoding, ignore non-alphabet characters"
msgstr "ignorer les caractères non alphabétiques lors du décodage"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-w>, B<--wrap>=I<\\,COLS\\/>"
msgstr "B<-w>, B<--wrap>=I<\\,COLONNES\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"wrap encoded lines after COLS character (default 76).  Use 0 to disable line "
"wrapping"
msgstr ""
"ajuster la longueur des lignes encodées à COLONNES caractères (76 par "
"défaut). Utilisez B<0> pour désactiver l'ajustement automatique des lignes."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--z85>"
msgstr "B<--z85>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"ascii85-like encoding (ZeroMQ spec:32/Z85); when encoding, input length must "
"be a multiple of 4; when decoding, input length must be a multiple of 5"
msgstr ""
"encodage de type ascii85 (spécification :32/Z85 de ZeroMQ) ; lors de "
"l'encodage, la longueur d'entrée doit être un multiple de 4 et lors du "
"décodage un multiple de 5"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "display this help and exit"
msgstr "afficher l'aide-mémoire et quitter."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--version>"
msgstr "B<--version>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "output version information and exit"
msgstr "afficher les informations de version et quitter."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"When decoding, the input may contain newlines in addition to the bytes of "
"the formal alphabet.  Use B<--ignore-garbage> to attempt to recover from any "
"other non-alphabet bytes in the encoded stream."
msgstr ""
"Lors du décodage, l'entrée peut contenir des changements de ligne en plus "
"des octets de l'alphabet strict. Utilisez B<--ignore-garbage> pour essayer "
"de ne pas être gêné par les autres octets non alphabétiques du flux encodé."

#. type: SH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "ENCODINGS EXAMPLES"
msgid "ENCODING EXAMPLES"
msgstr "EXEMPLES D'ENCODAGE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"$ printf '\\e376\\e117\\e202' | basenc --base64\n"
"/k+C\n"
msgstr ""
"$ printf '\\e376\\e117\\e202' | basenc --base64\n"
"/k+C\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"$ printf '\\e376\\e117\\e202' | basenc --base64url\n"
"_k-C\n"
msgstr ""
"$ printf '\\e376\\e117\\e202' | basenc --base64url\n"
"_k-C\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"$ printf '\\e376\\e117\\e202' | basenc --base32\n"
"7ZHYE===\n"
msgstr ""
"$ printf '\\e376\\e117\\e202' | basenc --base32\n"
"7ZHYE===\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"$ printf '\\e376\\e117\\e202' | basenc --base32hex\n"
"VP7O4===\n"
msgstr ""
"$ printf '\\e376\\e117\\e202' | basenc --base32hex\n"
"VP7O4===\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"$ printf '\\e376\\e117\\e202' | basenc --base16\n"
"FE4F82\n"
msgstr ""
"$ printf '\\e376\\e117\\e202' | basenc --base16\n"
"FE4F82\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"$ printf '\\e376\\e117\\e202' | basenc --base2lsbf\n"
"011111111111001001000001\n"
msgstr ""
"$ printf '\\e376\\e117\\e202' | basenc --base2lsbf\n"
"011111111111001001000001\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"$ printf '\\e376\\e117\\e202' | basenc --base2msbf\n"
"111111100100111110000010\n"
msgstr ""
"$ printf '\\e376\\e117\\e202' | basenc --base2msbf\n"
"111111100100111110000010\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"$ printf '\\e376\\e117\\e202\\e000' | basenc --z85\n"
"@.FaC\n"
msgstr ""
"$ printf '\\e376\\e117\\e202\\e000' | basenc --z85\n"
"@.FaC\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "AUTEUR"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Written by Simon Josefsson and Assaf Gordon."
msgstr "Écrit par Simon Josefsson et Assaf Gordon."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "SIGNALER DES BOGUES"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"GNU coreutils online help: E<lt>https://www.gnu.org/software/coreutils/E<gt>"
msgstr ""
"Aide en ligne de GNU coreutils : E<lt>https://www.gnu.org/software/coreutils/"
"E<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Report any translation bugs to E<lt>https://translationproject.org/team/E<gt>"
msgstr ""
"Signaler toute erreur de traduction à E<lt>https://translationproject.org/"
"team/fr.htmlE<gt>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "COPYRIGHT"
msgstr "COPYRIGHT"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid ""
"Copyright \\(co 2023 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2023 Free Software Foundation, Inc. Licence GPLv3+\\ : GNU "
"GPL version 3 ou ultérieure E<lt>https://gnu.org/licenses/gpl.htmlE<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""
"Ce programme est un logiciel libre. Vous pouvez le modifier et le "
"redistribuer. Il n'y a AUCUNE GARANTIE dans la mesure autorisée par la loi."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Full documentation E<lt>https://www.gnu.org/software/coreutils/basencE<gt>"
msgstr ""
"Documentation complète : E<lt>https://www.gnu.org/software/coreutils/"
"basencE<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "or available locally via: info \\(aq(coreutils) basenc invocation\\(aq"
msgstr ""
"aussi disponible localement à l’aide de la commande : info \\(aq(coreutils) "
"basenc invocation\\(aq"

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "September 2022"
msgstr "Septembre 2022"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "GNU coreutils 9.1"
msgstr "GNU coreutils 9.1"

#. type: SH
#: debian-bookworm debian-unstable fedora-39 mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "ENCODINGS EXAMPLES"
msgstr "EXEMPLES D'ENCODAGE"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron
msgid ""
"Copyright \\(co 2022 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2022 Free Software Foundation, Inc. Licence GPLv3+\\ : GNU "
"GPL version 3 ou ultérieure E<lt>https://gnu.org/licenses/gpl.htmlE<gt>"

#. type: TH
#: fedora-39
#, no-wrap
msgid "GNU coreutils 9.3"
msgstr "GNU coreutils 9.3"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "April 2022"
msgstr "Avril 2022"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "October 2021"
msgstr "Octobre 2021"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "GNU coreutils 8.32"
msgstr "GNU coreutils 8.32"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Copyright \\(co 2020 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2020 Free Software Foundation, Inc. Licence GPLv3+\\ : GNU "
"GPL version 3 ou ultérieure E<lt>https://gnu.org/licenses/gpl.htmlE<gt>"
