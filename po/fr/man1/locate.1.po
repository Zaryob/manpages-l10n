# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Frédéric Delanoy <delanoy_f@yahoo.com>, 2002.
# Florentin Duneau <fduneau@gmail.com>, 2006.
# bubu <bubub@no-log.org>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: manpages-fr \n"
"POT-Creation-Date: 2023-08-10 18:57+0200\n"
"PO-Revision-Date: 2022-08-01 18:21+0200\n"
"Last-Translator: bubu <bubub@no-log.org>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr_FR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Poedit 3.1\n"

#. type: TH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "locate"
msgstr "locate"

#. type: TH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "Sep 2012"
msgstr "Sep 2012"

#. type: TH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "mlocate"
msgstr "mlocate"

#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "locate - find files by name"
msgstr "locate - Trouver des fichiers par leur nom"

#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "B<locate> [I<OPTION>]... I<PATTERN>..."
msgstr "B<locate> [I<OPTION>]... I<MOTIF>..."

#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid ""
"B<locate> reads one or more databases prepared by B<updatedb>(8)  and writes "
"file names matching at least one of the I<PATTERN>s to standard output, one "
"per line."
msgstr ""
"B<locate> lit une ou plusieurs bases de données préparées par B<updatedb>(8) "
"et écrit les noms des fichiers correspondants à au moins un des I<MOTIF>s "
"sur la sortie standard, un par ligne."

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid ""
"If B<--regex> is not specified, I<PATTERN>s can contain globbing "
"characters.  If any I<PATTERN> contains no globbing characters, B<locate> "
"behaves as if the pattern were B<*>I<PATTERN>B<*>."
msgstr ""
"Si B<--regex> n'est pas spécifié, alors I<MOTIF>s peuvent contenir des "
"caractères développement de nom (jokerrs). Si aucun I<MOTIF> ne contient de "
"caractères jokers, B<locate> se comporte comme si le motif était "
"B<*>I<MOTIF>B<*>."

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid ""
"By default, B<locate> does not check whether files found in database still "
"exist (but it does require all parent directories to exist if the database "
"was built with B<--require-visibility no>).  B<locate> can never report "
"files created after the most recent update of the relevant database."
msgstr ""
"Par défaut, B<locate> ne vérifie pas si les fichiers trouvés dans la base de "
"données existent encore (mais il requiert que tous les répertoires parents "
"existent si la base de données a été construite avec B<--require-visibility "
"no>). B<locate> ne pourra pas trouver des fichiers créés après la dernière "
"mise à jour de la base de données pertinente."

#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "EXIT STATUS"
msgstr "CODE DE RETOUR"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid ""
"B<locate> exits with status 0 if any match was found or if B<locate> was "
"invoked with one of the B<--limit 0>, B<--help>, B<--statistics> or B<--"
"version> options.  If no match was found or a fatal error was encountered, "
"B<locate> exits with status 1."
msgstr ""
"B<locate> renvoie le code B<0> si une correspondance a été trouvée ou si "
"B<locate> a été passée avec l'une des options B<--limit 0>, B<--help>, B<--"
"statistics> ou B<--version>. Si aucune correspondance n'a été trouvé ou si "
"une erreur fatale est survenue, B<locate> renvoie le code B<1>."

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid ""
"Errors encountered while reading a database are not fatal, search continues "
"in other specified databases, if any."
msgstr ""
"Les erreurs rencontrées lors de la lecture de la base de données ne sont pas "
"fatales, la recherche continue dans les autres bases de données s'il y en a."

#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr "OPTIONS"

#. type: TP
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "B<-A>, B<--all>"
msgstr "B<-A>, B<--all>"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid ""
"Print only entries that match all I<PATTERN>s instead of requiring only one "
"of them to match."
msgstr ""
"Afficher seulement les entrées qui correspondent à tous les I<MOTIF>S au "
"lieu de n'en exiger qu'un pour la correspondance."

#. type: TP
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "B<-b>, B<--basename>"
msgstr "B<-b>, B<--basename>"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid ""
"Match only the base name against the specified patterns.  This is the "
"opposite of B<--wholename>."
msgstr ""
"N'établir la correspondance que dans le nom de base avec les motifs "
"indiqués. C'est le contraire de B<--wholename>."

#. type: TP
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "B<-c>, B<--count>"
msgstr "B<-c>, B<--count>"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid ""
"Instead of writing file names on standard output, write the number of "
"matching entries only."
msgstr ""
"Au lieu d'afficher les noms de fichiers sur la sortie standard, n'afficher "
"que le nombre d'entrées qui correspondent."

#. type: TP
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "B<-d, --database> I<DBPATH>"
msgstr "B<-d, --database> I<CHEMIN_B_D>"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid ""
"Replace the default database with I<DBPATH>.  I<DBPATH> is a B<:>-separated "
"list of database file names.  If more than one B<--database> option is "
"specified, the resulting path is a concatenation of the separate paths."
msgstr ""
"Remplacer la base de données par défaut par I<CHEMIN_B_D>. I<CHEMIN_B_D> est "
"une liste de noms de fichiers de base de données séparés par des deux points "
"(B<:>). Si plus d'une option B<--database> est indiquée, le chemin résultant "
"est une concaténation des différents chemins."

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid ""
"An empty database file name is replaced by the default database.  A database "
"file name B<-> refers to the standard input.  Note that a database can be "
"read from the standard input only once."
msgstr ""
"Un nom de fichier de base de données vide est remplacé par celui de la base "
"de données par défaut. Un fichier de base de données nommé B<-> fait "
"référence à l'entrée standard. Notez qu'une base de données peut être lue "
"qu'une seule foisdepuis l'entrée standard."

#. type: TP
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "B<-e>, B<--existing>"
msgstr "B<-e>, B<--existing>"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid ""
"Print only entries that refer to files existing at the time B<locate> is run."
msgstr ""
"N'afficher que les entrées qui font référence à des fichiers existants au "
"moment où B<locate> est exécuté."

#. type: TP
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "B<-L>, B<--follow>"
msgstr "B<-L>, B<--follow>"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid ""
"When checking whether files exist (if the B<--existing> option is "
"specified), follow trailing symbolic links.  This causes broken symbolic "
"links to be omitted from the output."
msgstr ""
"Lors de la vérification pour connaître l'existence des fichiers (si l'option "
"B<--existing> est spécifiée), suivre les liens symboliques finaux. Cela "
"exclut les liens symboliques cassés de la sortie."

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid ""
"This is the default behavior.  The opposite can be specified using B<--"
"nofollow>."
msgstr ""
"C'est le comportement par défaut. L'opposé peut être spécifié en utilisant "
"B<--nofollow>."

#. type: TP
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "B<-h>, B<--help>"
msgstr "B<-h>, B<--help>"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid ""
"Write a summary of the available options to standard output and exit "
"successfully."
msgstr ""
"Afficher un résumé des options disponibles sur la sortie standard et quitter."

#. type: TP
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "B<-i>, B<--ignore-case>"
msgstr "B<-i>, B<--ignore-case>"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "Ignore case distinctions when matching patterns."
msgstr ""
"Ignorer les différences entre minuscules et majuscules lors de la "
"correspondance des motifs."

#. type: TP
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "B<-l>, B<--limit>, B<-n> I<LIMIT>"
msgstr "B<-l>, B<--limit>, B<-n> I<LIMITE>"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid ""
"Exit successfully after finding I<LIMIT> entries.  If the B<--count> option "
"is specified, the resulting count is also limited to I<LIMIT>."
msgstr ""
"Quitter avec succès après avoir trouvé I<LIMITE> entrées. Si l'option B<--"
"count> est spécifiée, le décompte résultant est aussi limité à I<LIMITE>."

#. type: TP
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "B<-m>, B<--mmap>"
msgstr "B<-m>, B<--mmap>"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "Ignored, for compatibility with E<.SM BSD> and E<.SM GNU> B<locate>."
msgstr "Ignoré, pour la compatibilité avec E<.SM BSD> et E<.SM GNU> B<locate>."

#. type: TP
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "B<-P>, B<--nofollow>, B<-H>"
msgstr "B<-P>, B<--nofollow>, B<-H>"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid ""
"When checking whether files exist (if the B<--existing> option is "
"specified), do not follow trailing symbolic links.  This causes broken "
"symbolic links to be reported like other files."
msgstr ""
"Lors de la vérification de l'existence de fichiers (si l'option B<--"
"existing> est spécifiée), ne pas suivre les liens symboliques finaux. Cela "
"affichera les liens symboliques cassés comme les autres fichiers."

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "This is the opposite of B<--follow>."
msgstr "C'est le contraire de B<--follow>."

#. type: TP
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "B<-0>, B<--null>"
msgstr "B<-0>, B<--null>"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid ""
"Separate the entries on output using the E<.SM ASCII NUL> character instead "
"of writing each entry on a separate line.  This option is designed for "
"interoperability with the B<--null> option of E<.SM GNU> B<xargs>(1)."
msgstr ""
"Séparer les entrées sur la sortie en utilisant le caractère E<.SM ASCII "
"NULL> au lieu d'écrire chaque entrée sur une ligne séparée. Cette option est "
"conçue pour l'interopérabilité avec l'option B<--null> de E<.SM GNU> "
"B<xargs>(1)."

#. type: TP
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "B<-S>, B<--statistics>"
msgstr "B<-S>, B<--statistics>"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid ""
"Write statistics about each read database to standard output instead of "
"searching for files and exit successfully."
msgstr ""
"Afficher les statistiques de chaque base de données lues sur la sortie "
"standard au lieu de rechercher les fichiers, puis quitter."

#. type: TP
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "B<-q>, B<--quiet>"
msgstr "B<-q>, B<--quiet>"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid ""
"Write no messages about errors encountered while reading and processing "
"databases."
msgstr ""
"Ne pas afficher de messages sur les erreurs survenues pendant la lecture et "
"le traitement des bases de données."

#. type: TP
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "B<-r>, B<--regexp> I<REGEXP>"
msgstr "B<-r>, B<--regexp> I<REGEXP>"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid ""
"Search for a basic regexp I<REGEXP>.  No I<PATTERN>s are allowed if this "
"option is used, but this option can be specified multiple times."
msgstr ""
"Chercher une expression rationnelle simple I<REGEXP>. Aucun I<MOTIF> n'est "
"admis si cette option est utilisée, mais cette option peut être spécifiée "
"plusieurs fois."

#. type: TP
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "B<--regex>"
msgstr "B<--regex>"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "Interpret all I<PATTERN>s as extended regexps."
msgstr ""
"Interpréter tous les I<MOTIF>S comme des expressions rationnelles étendues."

#. type: TP
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "B<-s>, B<--stdio>"
msgstr "B<-s>, B<--stdio>"

#. type: TP
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid ""
"Write information about the version and license of B<locate> on standard "
"output and exit successfully."
msgstr ""
"Afficher les informations sur la version et la licence de B<locate> sur la "
"sortie standard, puis quitter avec succès."

#. type: TP
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "B<-w>, B<--wholename>"
msgstr "B<-w>, B<--wholename>"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "Match only the whole path name against the specified patterns."
msgstr ""
"Faire correspondre seulement le nom de chemin entier avec les I<MOTIF>S "
"spécifiés."

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid ""
"This is the default behavior.  The opposite can be specified using B<--"
"basename>."
msgstr ""
"C'est le comportement par défaut. Le contraire peut être spécifié par "
"l'utilisation de B<--basename>."

#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "EXEMPLES"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "To search for a file named exactly I<NAME> (not B<*>I<NAME>B<*>), use"
msgstr ""
"Pour rechercher un fichier nommé exactement I<NOM> (pas B<*>I<NOM>B<*>), "
"utilisez"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "B<locate -b> B<'\\e>I<NAME>B<'>"
msgstr "B<locate -b> B<'\\e>I<NOM>B<'>"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid ""
"Because B<\\e> is a globbing character, this disables the implicit "
"replacement of I<NAME> by B<*>I<NAME>B<*>."
msgstr ""
"Comme B<\\e> est un caractère joker, cela désactive le remplacement "
"implicite de I<NOM> par B<*>I<NOM>B<*>."

#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "FILES"
msgstr "FICHIERS"

#. type: TP
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "B</var/lib/mlocate/mlocate.db>"
msgstr "B</var/lib/mlocate/mlocate.db>"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "The database searched by default."
msgstr "La base de données recherchée par défaut."

#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "ENVIRONMENT"
msgstr "ENVIRONNEMENT"

#. type: TP
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "B<LOCATE_PATH>"
msgstr "B<LOCATE_PATH>"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid ""
"Path to additional databases, added after the default database or the "
"databases specified using the B<--database> option."
msgstr ""
"Chemin vers des bases de données supplémentaires, ajoutées après la base de "
"données par défaut ou la base de données indiquée par l'utilisation de "
"l'option B<--database>."

#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "NOTES"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid ""
"The order in which the requested databases are processed is unspecified, "
"which allows B<locate> to reorder the database path for security reasons."
msgstr ""
"L'ordre dans lequel les bases de données interrogées sont traitées n'est pas "
"spécifié, ce qui permet à B<locate> de réordonnancer le chemin de la base de "
"données pour des raisons de sécurité."

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid ""
"B<locate> attempts to be compatible to B<slocate> (without the options used "
"for creating databases) and E<.SM GNU> B<locate>, in that order.  This is "
"the reason for the impractical default B<--follow> option and for the "
"confusing set of B<--regex> and B<--regexp> options."
msgstr ""
"B<locate> se veut compatible avec B<slocate> (sans les options utilisées "
"pour créer la base de données) et E<.SM GNU> B<locate>, dans cet ordre. "
"C'est la raison de l'option B<--follow> par défaut peu pratique, et des "
"options B<--regex> et B<--regexp> qui prêtent à confusion."

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid ""
"The short spelling of the B<-r> option is incompatible to E<.SM GNU> "
"B<locate>, where it corresponds to the B<--regex> option.  Use the long "
"option names to avoid confusion."
msgstr ""
"Le raccourci B<-r> est incompatible avec E<.SM GNU> B<locate> où il "
"correspond à l'option B<--regex>. Utiliser les noms d'options de forme "
"longue pour éviter la confusion."

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid ""
"The B<LOCATE_PATH> environment variable replaces the default database in E<."
"SM BSD> and E<.SM GNU> B<locate>, but it is added to other databases in this "
"implementation and B<slocate>."
msgstr ""
"La variable d'environnement B<LOCATE_PATH> remplace la base de données par "
"défaut dans E<.SM BSD> et E<.SM GNU> B<locate>, mais est ajoutée aux autres "
"bases de données dans cette implémentation et B<slocate>."

#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "AUTEUR"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "Miloslav Trmac E<lt>mitr@redhat.comE<gt>"
msgstr "Miloslav Trmac E<lt>mitr@redhat.comE<gt>"

#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "B<updatedb>(8)"
msgstr "B<updatedb>(8)"
