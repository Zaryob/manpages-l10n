# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Sylvain Archenault <sylvain.archenault@laposte.net>, 2006.
# Frédéric Zulian <zulian@free.fr>, 2006.
# Grégory Colpart <reg@evolix.fr>, 2006.
# Philippe Piette <foudre-blanche@skynet.be>, 2006.
# Julien Cristau <jcristau@debian.org>, 2006.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006, 2007.
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006.
# Jean-Baka Domelevo-Entfellner <domelevo@gmail.com>, 2006.
# Florentin Duneau <fduneau@gmail.com>, 2006.
# Philippe Piette <foudre-blanche@skynet.be>, 2006, 2007.
# Florentin Duneau <fduneau@gmail.com>, 2008-2010.
# David Prévot <david@tilapin.org>, 2010-2013.
# Jean-Pierre Giraud <jean-pierregiraud@neuf.fr>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-fr-extra\n"
"POT-Creation-Date: 2023-08-27 17:13+0200\n"
"PO-Revision-Date: 2023-05-23 23:21-0200\n"
"Last-Translator: Jean-Pierre Giraud <jean-pierregiraud@neuf.fr>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.4\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "PMAP"
msgstr "PMAP"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "September 2012"
msgstr "Septembre 2012"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "procps-ng"
msgstr "procps-ng"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "User Commands"
msgstr "Commandes de l'utilisateur"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: opensuse-leap-15-6
msgid "pmap - report memory map of a process"
msgstr "pmap - Afficher l'empreinte mémoire d'un processus"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<pmap> [I<options>] I<pid> [...]"
msgstr "B<pmap> [I<options>] I<PID> [...]"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: opensuse-leap-15-6
msgid "The pmap command reports the memory map of a process or processes."
msgstr ""
"La commande B<pmap> affiche l'empreinte mémoire d'un ou plusieurs processus."

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "OPTIONS"
msgstr "OPTIONS"

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "B<-x>, B<--extended>"
msgstr "B<-x>, B<--extended>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "Show the extended format."
msgstr "Afficher la sortie au format étendu."

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "B<-d>, B<--device>"
msgstr "B<-d>, B<--device>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "Show the device format."
msgstr "Afficher la sortie au format périphérique."

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "B<-q>, B<--quiet>"
msgstr "B<-q>, B<--quiet>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "Do not display some header or footer lines."
msgstr "Ne pas afficher certaines lignes d'en-tête ou de pied de page."

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "B<-A>, B<--range> I<low>,I<high>"
msgstr "B<-A>, B<--range> I<bas>B<,>I<haut>"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Limit results to the given range to I<low> and I<high> address range.  "
"Notice that the low and high arguments are single string separated with "
"comma."
msgstr ""
"Restreindre les résultats à l'intervalle d'adresses comprises entre I<bas> "
"et I<haut>. Remarquez que les paramètres I<bas> et I<haut> sont de simples "
"chaînes séparées par une virgule."

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "B<-X>"
msgstr "B<-X>"

# NOTE: Missing period
#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Show even more details than the B<-x> option. WARNING: format changes "
"according to I</proc/PID/smaps>"
msgstr ""
"Afficher encore plus de précisions qu'avec l'option B<-x>. Attention, le "
"format change en fonction de I</proc/PID/smaps>."

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "B<-XX>"
msgstr "B<-XX>"

# NOTE: Missing period
#. type: Plain text
#: opensuse-leap-15-6
msgid "Show everything the kernel provides"
msgstr "Afficher tout ce que le noyau fournit."

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "B<-p>, B<--show-path>"
msgstr "B<-p>, B<--show-path>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "Show full path to files in the mapping column"
msgstr ""
"Montrer le chemin complet vers les fichiers dans la colonne de "
"correspondance."

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "B<-c>, B<--read-rc>"
msgstr "B<-c>, B<--read-rc>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "Read the default configuration"
msgstr "Lire la configuration par défaut."

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "B<-C>, B<--read-rc-from> I<file>"
msgstr "B<-C>, B<--read-rc-from> I<fichier>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "Read the configuration from I<file>"
msgstr "Lire le fichier de configuration I<fichier>."

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "B<-n>, B<--create-rc>"
msgstr "B<-n>, B<--create-rc>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "Create new default configuration"
msgstr "Créer une nouvelle configuration par défaut."

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "B<-N>, B<--create-rc-to> I<file>"
msgstr "B<-N>, B<--create-rc-to> I<fichier>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "Create new configuration to I<file>"
msgstr "Créer une nouvelle configuration dans I<fichier>."

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "B<-h>, B<--help>"
msgstr "B<-h>, B<--help>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "Display help text and exit."
msgstr "Afficher l’aide-mémoire puis quitter."

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "Display version information and exit."
msgstr "Afficher le nom et la version du logiciel et quitter."

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "EXIT STATUS"
msgstr "CODE DE RETOUR"

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "B<0>"
msgstr "B<0>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "Success."
msgstr "Réussite."

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "B<1>"
msgstr "B<1>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "Failure."
msgstr "Échec."

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "B<42>"
msgstr "B<42>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "Did not find all processes asked for."
msgstr "Les processus demandés n'ont pas tous été trouvés."

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<ps>(1), B<pgrep>(1)"
msgstr "B<ps>(1), B<pgrep>(1)"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDS"

#. type: Plain text
#: opensuse-leap-15-6
msgid "No standards apply, but pmap looks an awful lot like a SunOS command."
msgstr ""
"Aucune norme n'est respectée mais B<pmap> ressemble fortement à une commande "
"SunOS."

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "REPORTING BUGS"
msgstr "SIGNALER DES BOGUES"

#. type: Plain text
#: opensuse-leap-15-6
msgid "Please send bug reports to E<.UR procps@freelists.org> E<.UE>"
msgstr ""
"Merci d'envoyer un rapport de bogue à E<.UR procps@freelists.org> E<.UE>"
