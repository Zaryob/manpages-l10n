# Czech translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Pavel Heimlich <tropikhajma@gmail.com>, 2009.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-08-27 17:21+0200\n"
"PO-Revision-Date: 2009-02-09 20:06+0100\n"
"Last-Translator: Pavel Heimlich <tropikhajma@gmail.com>\n"
"Language-Team: Czech <translation-team-cs@lists.sourceforge.net>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 20.08.2\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "services"
msgstr "services"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "2022-10-30"
msgstr "30. října 2022"

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Linux man-pages 6.05.01"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "JMÉNO"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "services - Internet network services list"
msgstr "services - databáze služeb sítě Internet"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "POPIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<services> is a plain ASCII file providing a mapping between human-friendly "
"textual names for internet services, and their underlying assigned port "
"numbers and protocol types.  Every networking program should look into this "
"file to get the port number (and protocol) for its service.  The C library "
"routines B<getservent>(3), B<getservbyname>(3), B<getservbyport>(3), "
"B<setservent>(3), and B<endservent>(3)  support querying this file from "
"programs."
msgstr ""
"B<services> je čitelný textový soubor poskytující převod mezi názvy služeb a "
"jim příslušných čísel portů a protokolů. Každý síťový program by měl z "
"tohoto souboru získat číslo portu a typ protokolu.  Knihovna jazyka C "
"poskytuje funkce B<getservent>(3), B<getservbyname>(3), B<getservbyport>(3), "
"B<setservent>(3), a B<endservent>(3), které slouží k obsluze této databáze."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Port numbers are assigned by the IANA (Internet Assigned Numbers Authority), "
"and their current policy is to assign both TCP and UDP protocols when "
"assigning a port number.  Therefore, most entries will have two entries, "
"even for TCP-only services."
msgstr ""
"Čísla portů jsou přiřazována organizací IANA (Internet Assigned Numbers "
"Authority), jejich současná strategie je přiřazovat jak TCP tak i UDP "
"protokoly pro čísla portů. Proto má většina záznamů dva záznamy i pro služby "
"provozované pouze nad TCP protokolem."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Port numbers below 1024 (so-called \"low numbered\" ports) can be bound to "
"only by root (see B<bind>(2), B<tcp>(7), and B<udp>(7)).  This is so clients "
"connecting to low numbered ports can trust that the service running on the "
"port is the standard implementation, and not a rogue service run by a user "
"of the machine.  Well-known port numbers specified by the IANA are normally "
"located in this root-only space."
msgstr ""
"Porty s čísly menšími než 1024 mohou být přiřazeny pouze superuživatelem "
"(viz B<bind>(2), B<tcp>(7)  a B<udp>(7)).  To je proto, aby klienti, kteří "
"se připojují na tyto porty mohli důvěřovat, že zde běží standardní aplikace "
"a ne nějaký prográmek nějakého uživatele. Standardní čísla portů "
"specifikovaná IANA jsou normálně umístěna v této oblasti."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The presence of an entry for a service in the B<services> file does not "
"necessarily mean that the service is currently running on the machine.  See "
"B<inetd.conf>(5)  for the configuration of Internet services offered.  Note "
"that not all networking services are started by B<inetd>(8), and so won't "
"appear in B<inetd.conf>(5).  In particular, news (NNTP) and mail (SMTP) "
"servers are often initialized from the system boot scripts."
msgstr ""
"Výskyt dané služby v databázi služeb ještě neznamená, že služba na tomto "
"počítači momentálně běží, viz B<inetd.conf>(5), kde je uvedeno více o "
"konfiguraci internetových služeb. Mějte však na paměti, že ne všechny síťové "
"služby musí být startovány pomocí B<inetd>(8), a proto se nemusí vyskytovat "
"v B<inetd.conf>(5).  Např. news (NNTP) a mail (SMTP) servery jsou často "
"inicializovány ve startovacích skriptech."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The location of the B<services> file is defined by B<_PATH_SERVICES> in "
"I<E<lt>netdb.hE<gt>>.  This is usually set to I</etc/services>."
msgstr ""
"Umístění souboru B<services> je definováno konstantou B<_PATH_SERVICES> v "
"I<E<lt>netdb.hE<gt>>.  Obvykle je nastaveno na I</etc/services>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Each line describes one service, and is of the form:"
msgstr "Každý řádek obsahuje jeden záznam ve tvaru:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<service-name\\ \\ \\ port>B</>I<protocol\\ \\ \\ >[I<aliases ...>]"
msgstr "I<service-name\\ \\ \\ port>B</>I<protokol\\ \\ \\ >[I<aliases ...>]"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "where:"
msgstr "kde:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "I<service-name>"
msgstr "I<service-name>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"is the friendly name the service is known by and looked up under.  It is "
"case sensitive.  Often, the client program is named after the I<service-"
"name>."
msgstr ""
"je název služby pod kterým je známa a vyhledávána. Velikost písma je "
"rozhodující. Často je klient pojmenován stejně jako služba samotná."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "I<port>"
msgstr "I<port>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "is the port number (in decimal) to use for this service."
msgstr "je číslo portu v desítkové soustavě."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "I<protocol>"
msgstr "I<protokol>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"is the type of protocol to be used.  This field should match an entry in the "
"B<protocols>(5)  file.  Typical values include B<tcp> and B<udp>."
msgstr ""
"je typ použitého protokolu. Toto políčko by mělo vyhovovat záznamu ze "
"souboru B<protocols>(5)B<.> Obvyklé hodnoty jsou B<tcp> a B<udp>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "I<aliases>"
msgstr "I<aliases>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"is an optional space or tab separated list of other names for this service.  "
"Again, the names are case sensitive."
msgstr ""
"toto je volitelný seznam přezdívek oddělený mezerou nebo tabulátorem. Opět "
"zde rozhoduje velikost písmen."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Either spaces or tabs may be used to separate the fields."
msgstr "Jednotlivá pole jsou oddělena mezerami nebo tabulátorem."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Comments are started by the hash sign (#) and continue until the end of the "
"line.  Blank lines are skipped."
msgstr ""
"Komentáře začínají znakem # a pokračují až do konce řádku. Prázdné řádky "
"jsou vynechány."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The I<service-name> should begin in the first column of the file, since "
"leading spaces are not stripped.  I<service-names> can be any printable "
"characters excluding space and tab.  However, a conservative choice of "
"characters should be used to minimize compatibility problems.  For example, "
"a-z, 0-9, and hyphen (-) would seem a sensible choice."
msgstr ""
"I<service-name> by mělo začínat na začátku řádku, protože mezery na začátku "
"nejsou odstraňovány.  I<service-names> mohou být jakékoliv tisknutelné znaky "
"kromě mezery a tabulátoru, ale z důvodu kompatibility se doporučuje použít "
"pouze znaky a-z, 0-9, a pomlčka (-)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Lines not matching this format should not be present in the file.  "
"(Currently, they are silently skipped by B<getservent>(3), "
"B<getservbyname>(3), and B<getservbyport>(3).  However, this behavior should "
"not be relied on.)"
msgstr ""
"Řádky, které nevyhovují tomuto formátu, by se zde neměly vyskytovat.  (V "
"současné době jsou funkcemi B<getservent>(3), B<getservbyname>(3), a "
"B<getservbyport>(3)  tiše ignorovány, ale na to by se nemělo spoléhat.)"

#.  The following is not true as at glibc 2.8 (a line with a comma is
#.  ignored by getservent()); it's not clear if/when it was ever true.
#.    As a backward compatibility feature, the slash (/) between the
#.    .I port
#.    number and
#.    .I protocol
#.    name can in fact be either a slash or a comma (,).
#.    Use of the comma in
#.    modern installations is deprecated.
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This file might be distributed over a network using a network-wide naming "
"service like Yellow Pages/NIS or BIND/Hesiod."
msgstr ""
"Tento soubor může být distribuován po síti přes jmenné služby jako Yellow "
"Pages/NIS nebo BIND/Hesiod."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "A sample B<services> file might look like this:"
msgstr "Jednoduchý soubor B<services> může vypadat nějak takto:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"netstat         15/tcp\n"
"qotd            17/tcp          quote\n"
"msp             18/tcp          # message send protocol\n"
"msp             18/udp          # message send protocol\n"
"chargen         19/tcp          ttytst source\n"
"chargen         19/udp          ttytst source\n"
"ftp             21/tcp\n"
"# 22 - unassigned\n"
"telnet          23/tcp\n"
msgstr ""
"netstat         15/tcp\n"
"qotd            17/tcp          quote\n"
"msp             18/tcp          # message send protocol\n"
"msp             18/udp          # message send protocol\n"
"chargen         19/tcp          ttytst source\n"
"chargen         19/udp          ttytst source\n"
"ftp             21/tcp\n"
"# 22 - unassigned\n"
"telnet          23/tcp\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "FILES"
msgstr "SOUBORY"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "I</etc/services>"
msgstr "I</etc/services>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The Internet network services list"
msgstr "databáze služeb sítě Internet"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "I<E<lt>netdb.hE<gt>>"
msgstr "I<E<lt>netdb.hE<gt>>"

#.  .SH BUGS
#.  It's not clear when/if the following was ever true;
#.  it isn't true for glibc 2.8:
#.     There is a maximum of 35 aliases, due to the way the
#.     .BR getservent (3)
#.     code is written.
#.  It's not clear when/if the following was ever true;
#.  it isn't true for glibc 2.8:
#.     Lines longer than
#.     .B BUFSIZ
#.     (currently 1024) characters will be ignored by
#.     .BR getservent (3),
#.     .BR getservbyname (3),
#.     and
#.     .BR getservbyport (3).
#.     However, this will also cause the next line to be mis-parsed.
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Definition of B<_PATH_SERVICES>"
msgstr "Definice konstanty B<_PATH_SERVICES>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "DALŠÍ INFORMACE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<listen>(2), B<endservent>(3), B<getservbyname>(3), B<getservbyport>(3), "
"B<getservent>(3), B<setservent>(3), B<inetd.conf>(5), B<protocols>(5), "
"B<inetd>(8)"
msgstr ""
"B<listen>(2), B<endservent>(3), B<getservbyname>(3), B<getservbyport>(3), "
"B<getservent>(3), B<setservent>(3), B<inetd.conf>(5), B<protocols>(5), "
"B<inetd>(8)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Assigned Numbers RFC, most recently RFC\\ 1700, (AKA STD0002)."
msgstr "Assigned Numbers RFC, nejnovější RFC\\ 1700, (také známo pod STD0002)."

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.05"
msgstr "Linux man-pages 6.05"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "SERVICES"
msgstr "SERVICES"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2010-05-22"
msgstr "22. května 2010"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Linux - příručka programátora"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "COLOPHON"
msgstr "TIRÁŽ"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Tato stránka je součástí projektu Linux I<man-pages> v4.16. Popis projektu a "
"informace o hlášení chyb najdete na \\%https://www.kernel.org/doc/man\\-"
"pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"
