# Czech translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Marek Kubita <Kubitovi@mbox.lantanet.cz>, 2001.
# Pavel Heimlich <tropikhajma@gmail.com>, 2009.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-08-27 17:22+0200\n"
"PO-Revision-Date: 2009-02-09 20:06+0100\n"
"Last-Translator: Pavel Heimlich <tropikhajma@gmail.com>\n"
"Language-Team: Czech <translation-team-cs@lists.sourceforge.net>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 20.08.2\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "signal"
msgstr "signal"

#. type: TH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30. března 2023"

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Linux man-pages 6.05.01"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "JMÉNO"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "signal - ANSI C signal handling"
msgstr "signal - práce se signály v ANSI C"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "KNIHOVNA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Standardní knihovna C (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "POUŽITÍ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>signal.hE<gt>>\n"
msgstr "B<#include E<lt>signal.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<typedef void (*sighandler_t)(int);>\n"
msgstr "B<typedef void (*sighandler_t)(int);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<sighandler_t signal(int >I<signum>B<, sighandler_t >I<handler>B<);>\n"
msgstr "B<sighandler_t signal(int >I<signum>B<, sighandler_t >I<handler>B<);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "POPIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The behavior of B<signal>()  varies across UNIX versions, and has also "
#| "varied historically across different versions of Linux.  B<Avoid its "
#| "use>: use B<sigaction>(2)  instead.  See I<Portability> below."
msgid ""
"B<WARNING>: the behavior of B<signal>()  varies across UNIX versions, and "
"has also varied historically across different versions of Linux.  B<Avoid "
"its use>: use B<sigaction>(2)  instead.  See I<Portability> below."
msgstr ""
"Chování volání B<signal>() se liší napříč distribucemi UNIXu a také se liší "
"historicky v jednotlivých vydáních Linuxu. B<Vyhněte se použití této "
"funkce>. Raději použijte B<sigaction>(2). Viz I<Přenositelnost> níže."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<signal>()  sets the disposition of the signal I<signum> to I<handler>, "
"which is either B<SIG_IGN>, B<SIG_DFL>, or the address of a programmer-"
"defined function (a \"signal handler\")."
msgstr ""
"Systémové  volání  B<signal>() instaluje novou obslužnou funkci pro signál s "
"číslem I<signum>. Obsluha signálu je nastavena na  I<handler>, což může být "
"uživatelsky definovaná funkce nebo B<SIG_IGN> případně B<SIG_DFL>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If the signal I<signum> is delivered to the process, then one of the "
"following happens:"
msgstr ""
"Pokud je signál I<signum> přijat procesem, stane se jedna z následujících "
"věcí:"

#. #-#-#-#-#  archlinux: signal.2.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  debian-bookworm: signal.2.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  debian-unstable: signal.2.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  fedora-39: signal.2.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  fedora-rawhide: signal.2.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  mageia-cauldron: signal.2.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  opensuse-leap-15-6: signal.2.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: IP
#. #-#-#-#-#  opensuse-tumbleweed: signal.2.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "*"
msgstr "*"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "If the disposition is set to B<SIG_IGN>, then the signal is ignored."
msgstr "Je-li nastaveno B<SIG_IGN> signál je ignorován."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
msgid ""
"If the disposition is set to B<SIG_DFL>, then the default action associated "
"with the signal (see B<signal>(7))  occurs."
msgstr "Je-li nastaveno B<SIG_DFL> je vyvolána implicitní funkce."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If the disposition is set to a function, then first either the disposition "
"is reset to B<SIG_DFL>, or the signal is blocked (see I<Portability> below), "
"and then I<handler> is called with argument I<signum>.  If invocation of the "
"handler caused the signal to be blocked, then the signal is unblocked upon "
"return from the handler."
msgstr ""
"Pokud je dispozice nastavena na funkci, pak je nejdřív buď znovu nastavena "
"dispozice na B<SIG_DFL> nebo je signál blokován (viz I<Přenositelnost> níže) "
"a I<obslužná funkce> je volána s parametrem I<signum>. Pokud volání funkce "
"zablokovalo signál, je signál odblokován při návratu z obslužné funkce."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The signals B<SIGKILL> and B<SIGSTOP> cannot be caught or ignored."
msgstr "Signály B<SIGKILL> a B<SIGSTOP> nemohou být odchyceny nebo blokovány."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "NÁVRATOVÉ HODNOTY"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "B<signal>()  returns the previous value of the signal handler, or "
#| "B<SIG_ERR> on error."
msgid ""
"B<signal>()  returns the previous value of the signal handler.  On failure, "
"it returns B<SIG_ERR>, and I<errno> is set to indicate the error."
msgstr ""
"Funkce B<signal>() vrací předchozí hodnotu obsluhy signálu, nebo B<SIG_ERR>, "
"nastane-li chyba."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "CHYBOVÉ STAVY"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<signum> is invalid."
msgstr "I<signum> je neplatný."

#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "VERZE"

#.  libc4 and libc5 define
#.  .IR SignalHandler ;
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The use of I<sighandler_t> is a GNU extension.  Various versions of libc "
#| "predefine this type; libc4 and libc5 define I<SignalHandler>, glibc "
#| "defines I<sig_t> and, when B<_GNU_SOURCE> is defined, also "
#| "I<sighandler_t>.  Without use of such a type, the declaration of "
#| "B<signal>()  is the somewhat harder to read:"
msgid ""
"The use of I<sighandler_t> is a GNU extension, exposed if B<_GNU_SOURCE> is "
"defined; glibc also defines (the BSD-derived)  I<sig_t> if B<_BSD_SOURCE> "
"(glibc 2.19 and earlier)  or B<_DEFAULT_SOURCE> (glibc 2.19 and later)  is "
"defined.  Without use of such a type, the declaration of B<signal>()  is the "
"somewhat harder to read:"
msgstr ""
"Použití I<sighandler_t> je rozšířením GNU. Různé verze glibc předem definují "
"tento typ; libc4 a libc5 definují I<SignalHandler>, glibc definuje I<sig_t> "
"a je-li definován B<_GNU_SOURCE> pak je definován i I<sighandler_t>.  Bez "
"definice takového typu je deklarace funkce B<signal>() hůře čitelná."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<void ( *>I<signal>B<(int >I<signum>B<, void (*>I<handler>B<)(int)) ) (int);>\n"
msgstr "B<void ( *>I<signal>B<(int >I<signum>B<, void (*>I<handler>B<)(int)) ) (int);>\n"

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Portability"
msgstr "Přenositelnost"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The only portable use of B<signal>()  is to set a signal's disposition to "
"B<SIG_DFL> or B<SIG_IGN>.  The semantics when using B<signal>()  to "
"establish a signal handler vary across systems (and POSIX.1 explicitly "
"permits this variation); B<do not use it for this purpose.>"
msgstr ""
"Jediné přenositelné použití funkce B<signal>() je nastavit obsluhu signálu "
"na B<SIG_DFL> nebo B<SIG_IGN>. Sémantika použití B<signal>() na nastavení "
"obsluhy signálu se liší na různých systémech (a POSIX.1 tot explicitně "
"podporuje). B<Proto jej nepoužívejte za tímto účelem.>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"POSIX.1 solved the portability mess by specifying B<sigaction>(2), which "
"provides explicit control of the semantics when a signal handler is invoked; "
"use that interface instead of B<signal>()."
msgstr ""
"POSIX.1 vyřešil tento nesoulad v přenositelnosti zavedením B<sigaction>(2), "
"který poskytuje explicitní kontrolu sémantiky v případě vyvolání obsluhy "
"signálu. Používejte jej proto místo B<signal>()u."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDY"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "C11, POSIX.1-2008."
msgstr "C11, POSIX.1-2008."

#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "HISTORIE"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "C89, POSIX.1-2001."
msgstr "C89, POSIX.1-2001."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "In the original Unix systems, when a handler that was established using "
#| "B<signal>()  was invoked by the delivery of a signal, the disposition of "
#| "the signal would be reset to B<SIG_DFL>, and the system did not block "
#| "delivery of further instances of the signal.  System V also provides "
#| "these semantics for B<signal>().  This was bad because the signal might "
#| "be delivered again before the handler had a chance to reestablish "
#| "itself.  Furthermore, rapid deliveries of the same signal could result in "
#| "recursive invocations of the handler."
msgid ""
"In the original UNIX systems, when a handler that was established using "
"B<signal>()  was invoked by the delivery of a signal, the disposition of the "
"signal would be reset to B<SIG_DFL>, and the system did not block delivery "
"of further instances of the signal.  This is equivalent to calling "
"B<sigaction>(2)  with the following flags:"
msgstr ""
"Pokud v původních Unixových systémech byla obslužná funkce zřízená pomocí "
"B<signal>() vyvolána pomocí doručení signálu, stav byl nastaven na "
"B<SIG_DFL> a systém neblokoval další možná doručení tohoto signálu. System V "
"také poskytuje sémantiku pro B<signal>(). To bylo špatné, protože signál "
"mohl být znovu doručen před tím, než se mohla obsluha signálu obnovit. Navíc "
"doručení několika signálu stejného druhu bezprostředně za sebou vyústilo v "
"rekurzivní volání obsluhy signálu."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "sa.sa_flags = SA_RESETHAND | SA_NODEFER;\n"
msgstr "sa.sa_flags = SA_RESETHAND | SA_NODEFER;\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "In the original Unix systems, when a handler that was established using "
#| "B<signal>()  was invoked by the delivery of a signal, the disposition of "
#| "the signal would be reset to B<SIG_DFL>, and the system did not block "
#| "delivery of further instances of the signal.  System V also provides "
#| "these semantics for B<signal>().  This was bad because the signal might "
#| "be delivered again before the handler had a chance to reestablish "
#| "itself.  Furthermore, rapid deliveries of the same signal could result in "
#| "recursive invocations of the handler."
msgid ""
"System\\ V also provides these semantics for B<signal>().  This was bad "
"because the signal might be delivered again before the handler had a chance "
"to reestablish itself.  Furthermore, rapid deliveries of the same signal "
"could result in recursive invocations of the handler."
msgstr ""
"Pokud v původních Unixových systémech byla obslužná funkce zřízená pomocí "
"B<signal>() vyvolána pomocí doručení signálu, stav byl nastaven na "
"B<SIG_DFL> a systém neblokoval další možná doručení tohoto signálu. System V "
"také poskytuje sémantiku pro B<signal>(). To bylo špatné, protože signál "
"mohl být znovu doručen před tím, než se mohla obsluha signálu obnovit. Navíc "
"doručení několika signálu stejného druhu bezprostředně za sebou vyústilo v "
"rekurzivní volání obsluhy signálu."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "BSD improved on this situation by changing the semantics of signal "
#| "handling (but, unfortunately, silently changed the semantics when "
#| "establishing a handler with B<signal>()).  On BSD, when a signal handler "
#| "is invoked, the signal disposition is not reset, and further instances of "
#| "the signal are blocked from being delivered while the handler is "
#| "executing."
msgid ""
"BSD improved on this situation, but unfortunately also changed the semantics "
"of the existing B<signal>()  interface while doing so.  On BSD, when a "
"signal handler is invoked, the signal disposition is not reset, and further "
"instances of the signal are blocked from being delivered while the handler "
"is executing.  Furthermore, certain blocking system calls are automatically "
"restarted if interrupted by a signal handler (see B<signal>(7)).  The BSD "
"semantics are equivalent to calling B<sigaction>(2)  with the following "
"flags:"
msgstr ""
"BSD vylepšilo tuto situaci změnou sémantiky obsluhy signálu (ale naneštěstí "
"v tichosti také změnily sémantiku zřízení obsluhy pomocí B<signal>()). Když "
"je na BSD vyvolána obsluha signálu , tak není dispozice signálu znovu "
"nastavena a další doručení signálu je blokováno dokud se provádí obsluha."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "sa.sa_flags = SA_RESTART;\n"
msgstr "sa.sa_flags = SA_RESTART;\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The situation on Linux is as follows:"
msgstr "Situace na Linuxu je následující:"

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "\\[bu]"
msgstr "\\[bu]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The kernel's B<signal>()  system call provides System\\ V semantics."
msgstr "Systémové volání jádra B<signal>() poskytuje System V\\ sémantiku."

#.  System V semantics are also provided if one uses the separate
#.  .BR sysv_signal (3)
#.  function.
#.  .IP *
#.  The
#.  .BR signal ()
#.  function in Linux libc4 and libc5 provide System\ V semantics.
#.  If one on a libc5 system includes
#.  .I <bsd/signal.h>
#.  instead of
#.  .IR <signal.h> ,
#.  then
#.  .BR signal ()
#.  provides BSD semantics.
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "By default, in glibc 2 and later, the B<signal>()  wrapper function does "
#| "not invoke the kernel system call.  Instead, it calls B<sigaction>(2)  "
#| "using flags that supply BSD semantics.  This default behavior is provided "
#| "as long as the B<_BSD_SOURCE> feature test macro is defined.  By default, "
#| "B<_BSD_SOURCE> is defined; it is also implicitly defined if one defines "
#| "B<_GNU_SOURCE>, and can of course be explicitly defined."
msgid ""
"By default, in glibc 2 and later, the B<signal>()  wrapper function does not "
"invoke the kernel system call.  Instead, it calls B<sigaction>(2)  using "
"flags that supply BSD semantics.  This default behavior is provided as long "
"as a suitable feature test macro is defined: B<_BSD_SOURCE> on glibc 2.19 "
"and earlier or B<_DEFAULT_SOURCE> in glibc 2.19 and later.  (By default, "
"these macros are defined; see B<feature_test_macros>(7)  for details.)  If "
"such a feature test macro is not defined, then B<signal>()  provides "
"System\\ V semantics."
msgstr ""
"Implicitně v glibc 2 a pozdějších vydáních je obalen B<signal>()  funkcí, "
"která nevolá jádro systému. Místo toho volá B<sigaction>(2)  a používá "
"příznaky, které zaručí BSD sémantiku. Toto chování ja zaručeno po dobu "
"trvání definice makra B<_BSD_SOURCE>. Standartně je B<_BSD_SOURCE> "
"definováno; je také implicitně definováno, pokud je nadefinováno "
"B<_GNU_SOURCE> a také může být definováno explicitně."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "POZNÁMKY"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The effects of B<signal>()  in a multithreaded process are unspecified."
msgstr "Efekt funkce B<signal>() v procesech s vlákny nejsou specifikovány."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"According to POSIX, the behavior of a process is undefined after it ignores "
"a B<SIGFPE>, B<SIGILL>, or B<SIGSEGV> signal that was not generated by "
"B<kill>(2)  or B<raise>(3).  Integer division by zero has undefined result.  "
"On some architectures it will generate a B<SIGFPE> signal.  (Also dividing "
"the most negative integer by -1 may generate B<SIGFPE>.)  Ignoring this "
"signal might lead to an endless loop."
msgstr ""
"Dle specifikace POSIX je chování systému nespecifikováno, pokud ignoruje "
"B<SIGFPE>, B<SIGILL> nebo B<SIGSEGV> signál pokud nebyl vyvolán pomocí "
"B<kill>(2) nebo B<raise>(3). Celočíselné dělení nulou má nedefinovaný "
"výsledek. Na některých architekturách se generuje B<SIGFRE> signál. (Také "
"dělení největšího záporného celého čísla -1 generuje B<SIGFRE>).  Ignorování "
"tohoto signálu může vést k nekonečné smyčce."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "See B<sigaction>(2)  for details on what happens when B<SIGCHLD> is set "
#| "to B<SIG_IGN>."
msgid ""
"See B<sigaction>(2)  for details on what happens when the disposition "
"B<SIGCHLD> is set to B<SIG_IGN>."
msgstr ""
"Viz B<sigaction>(2) pro více informací co se stane je-li B<SIGCHLD> "
"nastaveno na B<SIG_IGN>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"See B<signal-safety>(7)  for a list of the async-signal-safe functions that "
"can be safely called from inside a signal handler."
msgstr ""
"Viz B<signal-safety>(7) pro seznam asynchronních bezpečných funkcí, které "
"mohou být bezpečně volány uvnitř funkce pro obsluhu signálu."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "DALŠÍ INFORMACE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<kill>(1), B<alarm>(2), B<kill>(2), B<pause>(2), B<sigaction>(2), "
"B<signalfd>(2), B<sigpending>(2), B<sigprocmask>(2), B<sigsuspend>(2), "
"B<bsd_signal>(3), B<killpg>(3), B<raise>(3), B<siginterrupt>(3), "
"B<sigqueue>(3), B<sigsetops>(3), B<sigvec>(3), B<sysv_signal>(3), "
"B<signal>(7)"
msgstr ""
"B<kill>(1), B<alarm>(2), B<kill>(2), B<pause>(2), B<sigaction>(2), "
"B<signalfd>(2), B<sigpending>(2), B<sigprocmask>(2), B<sigsuspend>(2), "
"B<bsd_signal>(3), B<killpg>(3), B<raise>(3), B<siginterrupt>(3), "
"B<sigqueue>(3), B<sigsetops>(3), B<sigvec>(3), B<sysv_signal>(3), "
"B<signal>(7)"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "2023-02-05"
msgstr "5. února 2023"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron
msgid "POSIX.1-2001, POSIX.1-2008, C99."
msgstr "POSIX.1-2001, POSIX.1-2008, C99."

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.05"
msgstr "Linux man-pages 6.05"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "SIGNAL"
msgstr "SIGNAL"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2017-09-15"
msgstr "15. září 2017"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Linux - příručka programátora"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<#include E<lt>signal.hE<gt>>"
msgstr "B<#include E<lt>signal.hE<gt>>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<typedef void (*sighandler_t)(int);>"
msgstr "B<typedef void (*sighandler_t)(int);>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<sighandler_t signal(int >I<signum>B<, sighandler_t >I<handler>B<);>"
msgstr "B<sighandler_t signal(int >I<signum>B<, sighandler_t >I<handler>B<);>"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"The behavior of B<signal>()  varies across UNIX versions, and has also "
"varied historically across different versions of Linux.  B<Avoid its use>: "
"use B<sigaction>(2)  instead.  See I<Portability> below."
msgstr ""
"Chování volání B<signal>() se liší napříč distribucemi UNIXu a také se liší "
"historicky v jednotlivých vydáních Linuxu. B<Vyhněte se použití této "
"funkce>. Raději použijte B<sigaction>(2). Viz I<Přenositelnost> níže."

#. type: Plain text
#: opensuse-leap-15-6
#, fuzzy
#| msgid ""
#| "B<signal>()  returns the previous value of the signal handler, or "
#| "B<SIG_ERR> on error."
msgid ""
"B<signal>()  returns the previous value of the signal handler, or B<SIG_ERR> "
"on error.  In the event of an error, I<errno> is set to indicate the cause."
msgstr ""
"Funkce B<signal>() vrací předchozí hodnotu obsluhy signálu, nebo B<SIG_ERR>, "
"nastane-li chyba."

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "CONFORMING TO"
msgstr "SPLŇUJE STANDARDY"

#. type: Plain text
#: opensuse-leap-15-6
msgid "POSIX.1-2001, POSIX.1-2008, C89, C99."
msgstr "POSIX.1-2001, POSIX.1-2008, C89, C99."

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid "    sa.sa_flags = SA_RESETHAND | SA_NODEFER;\n"
msgstr "    sa.sa_flags = SA_RESETHAND | SA_NODEFER;\n"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid "    sa.sa_flags = SA_RESTART;\n"
msgstr "    sa.sa_flags = SA_RESTART;\n"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "COLOPHON"
msgstr "TIRÁŽ"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Tato stránka je součástí projektu Linux I<man-pages> v4.16. Popis projektu a "
"informace o hlášení chyb najdete na \\%https://www.kernel.org/doc/man\\-"
"pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"
