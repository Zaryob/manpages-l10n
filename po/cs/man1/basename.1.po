# Czech translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Petr Kolář <Petr.Kolar@vslib.cz>, 2001.
# Kamil Dudka <kdudka@redhat.com>, 2009.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-10-02 11:51+0200\n"
"PO-Revision-Date: 2022-05-06 21:12+0200\n"
"Last-Translator: Kamil Dudka <kdudka@redhat.com>\n"
"Language-Team: Czech <translation-team-cs@lists.sourceforge.net>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 20.08.2\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "BASENAME"
msgstr "BASENAME"

#. type: TH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "September 2023"
msgstr "Září 2023"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "GNU coreutils 9.4"
msgstr "GNU coreutils 9.4"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr "Příručka uživatele"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "JMÉNO"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "basename - strip directory and suffix from filenames"
msgstr ""
"basename - vypíše jméno souboru bez cesty a případně bez zadané přípony"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "POUŽITÍ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<basename> I<\\,NAME \\/>[I<\\,SUFFIX\\/>]"
msgstr "B<basename> I<\\,JMÉNO \\/>[I<\\,PŘÍPONA\\/>]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<basename> I<\\,OPTION\\/>... I<\\,NAME\\/>..."
msgstr "B<basename> I<\\,VOLBA\\/>... I<\\,JMÉNO\\/>..."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "POPIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Print NAME with any leading directory components removed.  If specified, "
"also remove a trailing SUFFIX."
msgstr ""
"Oddělí ze zadaného JMÉNA všechna úvodní jména adresářů.  Jestliže je zadána "
"PŘÍPONA, bude také odstraněna."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Mandatory arguments to long options are mandatory for short options too."
msgstr ""
"Povinné argumenty dlouhých přepínačů jsou také povinné u odpovídajících "
"krátkých přepínačů."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-a>, B<--multiple>"
msgstr "B<-a>, B<--multiple>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "support multiple arguments and treat each as a NAME"
msgstr "zpracuje násobné argumenty, každý považuje za JMÉNO"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-s>, B<--suffix>=I<\\,SUFFIX\\/>"
msgstr "B<-s>, B<--suffix>=I<\\,PŘÍPONA\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "remove a trailing SUFFIX; implies B<-a>"
msgstr "odstraní koncovou PŘÍPONU; obsahuje B<-a>"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-z>, B<--zero>"
msgstr "B<-z>, B<--zero>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "end each output line with NUL, not newline"
msgstr "zakončí každý řádek znakem NUL, nikoliv nového řádku"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "display this help and exit"
msgstr "vypíše návod k použití na standardní výstup a bezchybně skončí"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--version>"
msgstr "B<--version>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "output version information and exit"
msgstr "Vypíše informaci o verzi programu a skončí"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "PŘÍKLADY"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "basename /usr/bin/sort"
msgstr "basename /usr/bin/sort"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "-E<gt> \"sort\""
msgstr "Vypíše \"sort\""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "basename include/stdio.h .h"
msgstr "basename include/stdio.h .h"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "-E<gt> \"stdio\""
msgstr "Vypíše \"stdio\""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "basename -s .h include/stdio.h"
msgstr "basename -s .h include/stdio.h"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "basename -a any/str1 any/str2"
msgstr "basename -a any/str1 any/str2"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "-E<gt> \"str1\" followed by \"str2\""
msgstr "Vypíše \"str1\" následováno \"str2\""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Written by David MacKenzie."
msgstr "Napsal David MacKenzie."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "HLÁŠENÍ CHYB"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"GNU coreutils online help: E<lt>https://www.gnu.org/software/coreutils/E<gt>"
msgstr ""
"On-line nápověda GNU coreutils: E<lt>https://www.gnu.org/software/coreutils/"
"E<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Report any translation bugs to E<lt>https://translationproject.org/team/E<gt>"
msgstr ""
"Chyby v překladu hlaste na E<lt>https://translationproject.org/team/cs."
"htmlE<gt> (česky)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "COPYRIGHT"
msgstr "COPYRIGHT"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid ""
"Copyright \\(co 2023 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2023 Free Software Foundation, Inc.  Licence GPLv3+: GNU "
"GPLverze 3 nebo novější E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""
"Toto je volné programové vybavení: můžete jej měnit a šířit. Je zcela BEZ "
"ZÁRUKY, v rozsahu povoleném zákonem."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "DALŠÍ INFORMACE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "B<dirname>(1), B<readlink>(1)"
msgstr "B<dirname>(1), B<readlink>(1)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Full documentation E<lt>https://www.gnu.org/software/coreutils/basenameE<gt>"
msgstr ""
"Úplná dokumentace je na E<lt>https://www.gnu.org/software/coreutils/"
"basenameE<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"or available locally via: info \\(aq(coreutils) basename invocation\\(aq"
msgstr ""
"nebo dostupná lokálně skrze: info \\(aq(coreutils) basename invocation\\(aq"

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "September 2022"
msgstr "Září 2022"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "GNU coreutils 9.1"
msgstr "GNU coreutils 9.1"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron
msgid ""
"Copyright \\(co 2022 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2022 Free Software Foundation, Inc.  Licence GPLv3+: GNU "
"GPLverze 3 nebo novější E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: TH
#: fedora-39
#, no-wrap
msgid "GNU coreutils 9.3"
msgstr "GNU coreutils 9.3"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "April 2022"
msgstr "Dubna 2022"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "October 2021"
msgstr "Říjen 2021"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "GNU coreutils 8.32"
msgstr "GNU coreutils 8.32"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Copyright \\(co 2020 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2020 Free Software Foundation, Inc.  Licence GPLv3+: GNU "
"GPLverze 3 nebo novější E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: Plain text
#: opensuse-leap-15-6
msgid "dirname(1), readlink(1)"
msgstr "dirname(1), readlink(1)"
