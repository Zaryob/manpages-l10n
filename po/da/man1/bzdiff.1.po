# Danish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Joe Hansen <joedalton2@yahoo.dk>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.14.0\n"
"POT-Creation-Date: 2023-08-27 16:51+0200\n"
"PO-Revision-Date: 2022-05-29 20:54+0100\n"
"Last-Translator: Joe Hansen <joedalton2@yahoo.dk>\n"
"Language-Team: Danish <debian-l10n-danish@lists.debian.org>\n"
"Language: da\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "BZDIFF"
msgstr "BZDIFF"

#. type: SH
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NAVN"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "bzcmp, bzdiff - compare bzip2 compressed files"
msgstr "bzcmp, bzdiff - sammenlign bzip2-komprimerede filer"

#. type: SH
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<bzcmp> [ cmp_options ] file1 [ file2 ]"
msgstr "B<bzcmp> [ cmp_tilvalg ] fil1 [ fil2 ]"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<bzdiff> [ diff_options ] file1 [ file2 ]"
msgstr "B<bzdiff> [ diff_tilvalg ] fil1 [ fil2 ]"

#. type: SH
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESKRIVELSE"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"I<Bzcmp> and I<bzdiff> are used to invoke the I<cmp> or the I<diff> program "
"on bzip2 compressed files.  All options specified are passed directly to "
"I<cmp> or I<diff>.  If only 1 file is specified, then the files compared are "
"I<file1> and an uncompressed I<file1>.bz2.  If two files are specified, then "
"they are uncompressed if necessary and fed to I<cmp> or I<diff>.  The exit "
"status from I<cmp> or I<diff> is preserved."
msgstr ""
"I<Bzcmp> og I<bzdiff> bruges til at igangsætte I<cmp>- eller I<diff>-"
"programmet på bzip2-komprimerede filer. Alle tilvalg angivet sendes direkte "
"til I<cmp> eller I<diff>. Hvis kun 1 fil er angivet, så er de sammenlignede "
"filer I<fil1> og en I<fil1>.bz2 uden komprimering. Hvis to filer er angivet, "
"så pakkes de ud om nødvendigt og fødes til I<cmp> eller I<diff>. "
"Afslutningsstatussen fra I<cmp> eller I<diff> bevares."

#. type: SH
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SE OGSÅ"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "cmp(1), diff(1), bzmore(1), bzless(1), bzgrep(1), bzip2(1)"
msgstr "cmp(1), diff(1), bzmore(1), bzless(1), bzgrep(1), bzip2(1)"

#. type: SH
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "FEJL"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Messages from the I<cmp> or I<diff> programs refer to temporary filenames "
"instead of those specified."
msgstr ""
"Beskeder fra I<cmp>- eller I<diff>-programmerne refererer til midlertidige "
"filnavne i stedet for dem angivet."
