# Polish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Andrzej Krzysztofowicz <ankry@green.mf.pg.gda.pl>, 2002.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-08-27 16:59+0200\n"
"PO-Revision-Date: 2022-08-07 08:55+0200\n"
"Last-Translator: Andrzej Krzysztofowicz <ankry@green.mf.pg.gda.pl>\n"
"Language-Team: Polish <manpages-pl-list@lists.sourceforge.net>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"
"X-Generator: Lokalize 19.08.3\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "getpagesize"
msgstr "getpagesize"

#. type: TH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30 marca 2023 r."

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Linux man-pages 6.05.01"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NAZWA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "getpagesize - get memory page size"
msgstr "getpagesize - pobranie rozmiaru strony pamięci"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTEKA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Standardowa biblioteka C (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SKŁADNIA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>unistd.hE<gt>>\n"
msgstr "B<#include E<lt>unistd.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<int getpagesize(void);>\n"
msgstr "B<int getpagesize(void);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Wymagane ustawienia makr biblioteki glibc (patrz B<feature_test_macros>(7)):"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<getpagesize>():"
msgstr "B<getpagesize>():"

#.         || _XOPEN_SOURCE && _XOPEN_SOURCE_EXTENDED
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "    Since glibc 2.21:\n"
#| "        _DEFAULT_SOURCE\n"
#| "    In glibc 2.19 and 2.20:\n"
#| "        _DEFAULT_SOURCE || (_XOPEN_SOURCE && _XOPEN_SOURCE\\ E<lt>\\ 500)\n"
#| "    Up to and including glibc 2.19:\n"
#| "        _BSD_SOURCE || (_XOPEN_SOURCE && _XOPEN_SOURCE\\ E<lt>\\ 500)\n"
msgid ""
"    Since glibc 2.20:\n"
"        _DEFAULT_SOURCE || ! (_POSIX_C_SOURCE E<gt>= 200112L)\n"
"    glibc 2.12 to glibc 2.19:\n"
"        _BSD_SOURCE || ! (_POSIX_C_SOURCE E<gt>= 200112L)\n"
"    Before glibc 2.12:\n"
"        _BSD_SOURCE || _XOPEN_SOURCE E<gt>= 500\n"
msgstr ""
"    Od glibc 2.21:\n"
"        _DEFAULT_SOURCE\n"
"    W glibc 2.19 i 2.20:\n"
"        _DEFAULT_SOURCE || (_XOPEN_SOURCE && _XOPEN_SOURCE\\ E<lt>\\ 500)\n"
"    Do glibc 2.19 włącznie:\n"
"        _BSD_SOURCE || (_XOPEN_SOURCE && _XOPEN_SOURCE\\ E<lt>\\ 500)\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "OPIS"

#. #-#-#-#-#  archlinux: getpagesize.2.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  debian-bookworm: getpagesize.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  .SH HISTORY
#.  This call first appeared in 4.2BSD.
#. type: Plain text
#. #-#-#-#-#  debian-unstable: getpagesize.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  .SH HISTORY
#.  This call first appeared in 4.2BSD.
#. type: Plain text
#. #-#-#-#-#  fedora-39: getpagesize.2.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  fedora-rawhide: getpagesize.2.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  mageia-cauldron: getpagesize.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  .SH HISTORY
#.  This call first appeared in 4.2BSD.
#. type: Plain text
#. #-#-#-#-#  opensuse-leap-15-6: getpagesize.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  .SH HISTORY
#.  This call first appeared in 4.2BSD.
#. type: Plain text
#. #-#-#-#-#  opensuse-tumbleweed: getpagesize.2.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The function B<getpagesize()> returns the number of bytes in a page, "
#| "where a \"page\" is the thing used where it says in the description of "
#| "B<mmap>(2)  that files are mapped in page-sized units."
msgid ""
"The function B<getpagesize>()  returns the number of bytes in a memory page, "
"where \"page\" is a fixed-length block, the unit for memory allocation and "
"file mapping performed by B<mmap>(2)."
msgstr ""
"Funkcja B<getpagesize>() zwraca ilość bajtów w stronie, przy czym \"strona\" "
"jest to pojęcie takie, jakużywane w opisie B<mmap >(2), gdzie jednostką "
"mapowanie plików jest strona pamięci."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDY"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "None."
msgstr ""

# All that section was initially translated by PB. Readded with minor corrections by MK. --MK
#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "HISTORIA"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "SVr4, 4.4BSD, SUSv2.  In SUSv2 the B<getpagesize>()  call is labeled "
#| "LEGACY, and in POSIX.1-2001 it has been dropped; HP-UX does not have this "
#| "call."
msgid ""
"This call first appeared in 4.2BSD.  SVr4, 4.4BSD, SUSv2.  In SUSv2 the "
"B<getpagesize>()  call is labeled LEGACY, and in POSIX.1-2001 it has been "
"dropped; HP-UX does not have this call."
msgstr ""
"SVr4, 4.4BSD, SUSv2. W SUSv2 funkcja B<getpagesize>() jest oznaczona jako "
"\"pozostałość\" (\"LEGACY\"), a w POSIX.1-2001 została pominięta. HP-UX nie "
"zawiera tej funkcji."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "UWAGI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Portable applications should employ I<sysconf(_SC_PAGESIZE)> instead of "
"B<getpagesize>():"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"#include E<lt>unistd.hE<gt>\n"
"long sz = sysconf(_SC_PAGESIZE);\n"
msgstr ""
"#include E<lt>unistd.hE<gt>\n"
"long sz = sysconf(_SC_PAGESIZE);\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "(where some systems also allow the synonym _SC_PAGE_SIZE for "
#| "_SC_PAGESIZE), or"
msgid "(Most systems allow the synonym B<_SC_PAGE_SIZE> for B<_SC_PAGESIZE>.)"
msgstr ""
"(gdzie niektóre systemy dopuszczają również synonim _SC_PAGE_SIZE dla "
"_SC_PAGESIZE), lub"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Whether B<getpagesize>()  is present as a Linux system call depends on the "
"architecture.  If it is, it returns the kernel symbol B<PAGE_SIZE>, whose "
"value depends on the architecture and machine model.  Generally, one uses "
"binaries that are dependent on the architecture but not on the machine "
"model, in order to have a single binary distribution per architecture.  This "
"means that a user program should not find B<PAGE_SIZE> at compile time from "
"a header file, but use an actual system call, at least for those "
"architectures (like sun4) where this dependency exists.  Here glibc 2.0 "
"fails because its B<getpagesize>()  returns a statically derived value, and "
"does not use a system call.  Things are OK in glibc 2.1."
msgstr ""
"Obecność B<getpagesize>() jako funkcji systemowej Linuksa zależy od "
"architektury. Jeśli istnieje, zwracasymbol jądra B<PAGE_SIZE>, który zależy "
"od architektury i modelu maszyny. W ogólności, aby mieć jedną dystrybucję "
"binarną dla każdej architektury, używa się binariów, które są zależne od "
"architektury, ale nie od modelu maszyny. Oznacza to, że program użytkownika "
"nie powinien określać PAGE_SIZE podczas kompilacji na podstawie plików "
"nagłówkowych, a używać bieżącej funkcji systemowej, co najmniej dla tych "
"architektur (np. sun4), dla których istnieje zależność B<PAGE_SIZE> od "
"modelu maszyny. W tym miejscu libc4, libc5 i glibc 2.0 zawodzą, gdyż ich "
"B<getpagesize>() zwraca wartość wyliczoną statycznie, a nie korzysta funkcji "
"systemowej. Wszystko jest OK w glibc 2.1."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "ZOBACZ TAKŻE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<mmap>(2), B<sysconf>(3)"
msgstr "B<mmap>(2), B<sysconf>(3)"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "2023-02-05"
msgstr "5 lutego 2023 r."

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid ""
"SVr4, 4.4BSD, SUSv2.  In SUSv2 the B<getpagesize>()  call is labeled LEGACY, "
"and in POSIX.1-2001 it has been dropped; HP-UX does not have this call."
msgstr ""
"SVr4, 4.4BSD, SUSv2. W SUSv2 funkcja B<getpagesize>() jest oznaczona jako "
"\"pozostałość\" (\"LEGACY\"), a w POSIX.1-2001 została pominięta. HP-UX nie "
"zawiera tej funkcji."

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.05"
msgstr "Linux man-pages 6.05"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "GETPAGESIZE"
msgstr "GETPAGESIZE"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2017-09-15"
msgstr "15 września 2017 r."

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Podręcznik programisty Linuksa"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<#include E<lt>unistd.hE<gt>>"
msgstr "B<#include E<lt>unistd.hE<gt>>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<int getpagesize(void);>"
msgstr "B<int getpagesize(void);>"

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "Since glibc 2.19:"
msgstr "Od glibc 2.19:"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid "_DEFAULT_SOURCE || ! (_POSIX_C_SOURCE\\ E<gt>=\\ 200112L)\n"
msgstr "_DEFAULT_SOURCE || ! (_POSIX_C_SOURCE\\ E<gt>=\\ 200112L)\n"

#. type: Plain text
#: opensuse-leap-15-6
msgid "From glibc 2.12 to 2.19:"
msgstr "glibc 2.12 do 2.19:"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid "_BSD_SOURCE || ! (_POSIX_C_SOURCE\\ E<gt>=\\ 200112L)\n"
msgstr "_BSD_SOURCE || ! (_POSIX_C_SOURCE\\ E<gt>=\\ 200112L)\n"

#.     || _XOPEN_SOURCE\ &&\ _XOPEN_SOURCE_EXTENDED
#. type: Plain text
#: opensuse-leap-15-6
msgid "Before glibc 2.12: _BSD_SOURCE || _XOPEN_SOURCE\\ E<gt>=\\ 500"
msgstr "Przed glibc 2.12: _BSD_SOURCE || _XOPEN_SOURCE\\ E<gt>=\\ 500"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "CONFORMING TO"
msgstr "ZGODNE Z"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "COLOPHON"
msgstr "O STRONIE"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Angielska wersja tej strony pochodzi z wydania 4.16 projektu Linux I<man-"
"pages>. Opis projektu, informacje dotyczące zgłaszania błędów oraz najnowszą "
"wersję oryginału można znaleźć pod adresem \\%https://www.kernel.org/doc/man-"
"pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"
