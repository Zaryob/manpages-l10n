# Polish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Przemek Borys <pborys@dione.ids.pl>, 1999.
# Andrzej Krzysztofowicz <ankry@green.mf.pg.gda.pl>, 2002.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-08-27 17:13+0200\n"
"PO-Revision-Date: 2002-12-12 10:30+0100\n"
"Last-Translator: Andrzej Krzysztofowicz <ankry@green.mf.pg.gda.pl>\n"
"Language-Team: Polish <manpages-pl-list@lists.sourceforge.net>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"
"X-Generator: Lokalize 19.08.3\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "pipe"
msgstr ""

#. type: TH
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid "2023-07-30"
msgstr "30 lipca 2023 r."

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Linux man-pages 6.05.01"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NAZWA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "pipe, pipe2 - create pipe"
msgstr "pipe, pipe2 - utworzenie potoku"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTEKA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Standardowa biblioteka C (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SKŁADNIA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>unistd.hE<gt>>\n"
msgstr "B<#include E<lt>unistd.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<int pipe2(int >I<pipefd>B<[2], int >I<flags>B<);>\n"
msgid "B<int pipe(int >I<pipefd>B<[2]);>\n"
msgstr "B<int pipe2(int >I<pipefd>B<[2], int >I<flags>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "B<#include E<lt>fcntl.hE<gt>           >/* Definition of AT_* constants */\n"
#| "B<#include E<lt>unistd.hE<gt>>\n"
msgid ""
"B<#define _GNU_SOURCE>             /* See feature_test_macros(7) */\n"
"B<#include E<lt>fcntl.hE<gt>>              /* Definition of B<O_*> constants */\n"
"B<#include E<lt>unistd.hE<gt>>\n"
msgstr ""
"B<#include E<lt>fcntl.hE<gt>           >/* Definicja stałych AT_* */\n"
"B<#include E<lt>unistd.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<int pipe2(int >I<pipefd>B<[2], int >I<flags>B<);>\n"
msgstr "B<int pipe2(int >I<pipefd>B<[2], int >I<flags>B<);>\n"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid ""
"/* On Alpha, IA-64, MIPS, SuperH, and SPARC/SPARC64, pipe() has the\n"
"   following prototype; see VERSIONS */\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"B<struct fd_pair {>\n"
"B<long fd[2];>\n"
"B<};>\n"
"B<struct fd_pair pipe(void);>\n"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "OPIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<pipe>()  creates a pipe, a unidirectional data channel that can be used "
"for interprocess communication.  The array I<pipefd> is used to return two "
"file descriptors referring to the ends of the pipe.  I<pipefd[0]> refers to "
"the read end of the pipe.  I<pipefd[1]> refers to the write end of the "
"pipe.  Data written to the write end of the pipe is buffered by the kernel "
"until it is read from the read end of the pipe.  For further details, see "
"B<pipe>(7)."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If I<flags> is 0, then B<pipe2>()  is the same as B<pipe>().  The following "
"values can be bitwise ORed in I<flags> to obtain different behavior:"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<O_CLOEXEC>"
msgstr "B<O_CLOEXEC>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Set the close-on-exec (B<FD_CLOEXEC>)  flag on the two new file "
"descriptors.  See the description of the same flag in B<open>(2)  for "
"reasons why this may be useful."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<O_DIRECT> (since Linux 3.4)"
msgstr ""

#.  commit 9883035ae7edef3ec62ad215611cb8e17d6a1a5d
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Create a pipe that performs I/O in \"packet\" mode.  Each B<write>(2)  to "
"the pipe is dealt with as a separate packet, and B<read>(2)s from the pipe "
"will read one packet at a time.  Note the following points:"
msgstr ""

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "\\[bu]"
msgstr "\\[bu]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Writes of greater than B<PIPE_BUF> bytes (see B<pipe>(7))  will be split "
"into multiple packets.  The constant B<PIPE_BUF> is defined in I<E<lt>limits."
"hE<gt>>."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If a B<read>(2)  specifies a buffer size that is smaller than the next "
"packet, then the requested number of bytes are read, and the excess bytes in "
"the packet are discarded.  Specifying a buffer size of B<PIPE_BUF> will be "
"sufficient to read the largest possible packets (see the previous point)."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Zero-length packets are not supported.  (A B<read>(2)  that specifies a "
"buffer size of zero is a no-op, and returns 0.)"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Older kernels that do not support this flag will indicate this via an "
"B<EINVAL> error."
msgstr ""

#.  commit 0dbf5f20652108106cb822ad7662c786baaa03ff
#.  FIXME . But, it is not possible to specify O_DIRECT when opening a FIFO
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Since Linux 4.5, it is possible to change the B<O_DIRECT> setting of a pipe "
"file descriptor using B<fcntl>(2)."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<O_NONBLOCK>"
msgstr "B<O_NONBLOCK>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Set the B<O_NONBLOCK> file status flag on the open file descriptions "
"referred to by the new file descriptors.  Using this flag saves extra calls "
"to B<fcntl>(2)  to achieve the same result."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<O_NOTIFICATION_PIPE>"
msgstr ""

#.  commit c73be61cede5882f9605a852414db559c0ebedfd
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Since Linux 5.8, general notification mechanism is built on the top of the "
"pipe where kernel splices notification messages into pipes opened by user "
"space.  The owner of the pipe has to tell the kernel which sources of events "
"to watch and filters can also be applied to select which subevents should be "
"placed into the pipe."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "WARTOŚĆ ZWRACANA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "On success, zero is returned.  On error, -1 is returned, and I<errno> is "
#| "set appropriately."
msgid ""
"On success, zero is returned.  On error, -1 is returned, I<errno> is set to "
"indicate the error, and I<pipefd> is left unchanged."
msgstr ""
"Po pomyślnym zakończeniu zwracane jest zero. W wypadku błędu zwracane jest "
"-1 i odpowiednio ustawiane I<errno>."

#.  http://austingroupbugs.net/view.php?id=467
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"On Linux (and other systems), B<pipe>()  does not modify I<pipefd> on "
"failure.  A requirement standardizing this behavior was added in "
"POSIX.1-2008 TC2.  The Linux-specific B<pipe2>()  system call likewise does "
"not modify I<pipefd> on failure."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "BŁĘDY"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EFAULT>"
msgstr "B<EFAULT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<pipefd> is not valid."
msgstr "I<pipefd> jest nieprawidłowy."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "(B<pipe2>())  Invalid value in I<flags>."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EMFILE>"
msgstr "B<EMFILE>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The per-process limit on the number of open file descriptors has been "
"reached."
msgstr ""
"Zostało osiągnięte ograniczenie na liczbę otwartych deskryptorów plików dla "
"procesu."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENFILE>"
msgstr "B<ENFILE>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The system-wide limit on the total number of open files has been reached."
msgstr ""
"Zostało osiągnięte systemowe ograniczenie na całkowitą liczbę otwartych "
"plików."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The user hard limit on memory that can be allocated for pipes has been "
"reached and the caller is not privileged; see B<pipe>(7)."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<ENOPKG>"
msgstr "B<ENOPKG>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"(B<pipe2>())  B<O_NOTIFICATION_PIPE> was passed in I<flags> and support for "
"notifications (B<CONFIG_WATCH_QUEUE>)  is not compiled into the kernel."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "WERSJE"

#.  See http://math-atlas.sourceforge.net/devel/assembly/64.psabi.1.33.ps.Z
#.  for example, section 3.2.1 "Registers and the Stack Frame".
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The System V ABI on some architectures allows the use of more than one "
"register for returning multiple values; several architectures (namely, "
"Alpha, IA-64, MIPS, SuperH, and SPARC/SPARC64)  (ab)use this feature in "
"order to implement the B<pipe>()  system call in a functional manner: the "
"call doesn't take any arguments and returns a pair of file descriptors as "
"the return value on success.  The glibc B<pipe>()  wrapper function "
"transparently deals with this.  See B<syscall>(2)  for information regarding "
"registers used for storing second file descriptor."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDY"

#. type: TP
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "B<pipe>()"
msgstr ""

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr "POSIX.1-2008."

#. type: TP
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "B<pipe2>()"
msgstr ""

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "Linux."
msgstr "Linux."

# All that section was initially translated by PB. Readded with minor corrections by MK. --MK
#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "HISTORIA"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "POSIX.1-2001."
msgstr "POSIX.1-2001."

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "Linux 2.6.27, glibc 2.9."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "PRZYKŁADY"

#.  fork.2 refers to this example program.
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The following program creates a pipe, and then B<fork>(2)s to create a child "
"process; the child inherits a duplicate set of file descriptors that refer "
"to the same pipe.  After the B<fork>(2), each process closes the file "
"descriptors that it doesn't need for the pipe (see B<pipe>(7)).  The parent "
"then writes the string contained in the program's command-line argument to "
"the pipe, and the child reads this string a byte at a time from the pipe and "
"echoes it on standard output."
msgstr ""

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Program source"
msgstr "Kod źródłowy programu"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid ""
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>string.hE<gt>\n"
"#include E<lt>sys/wait.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"
"\\&\n"
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    int    pipefd[2];\n"
"    char   buf;\n"
"    pid_t  cpid;\n"
"\\&\n"
"    if (argc != 2) {\n"
"        fprintf(stderr, \"Usage: %s E<lt>stringE<gt>\\en\", argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    if (pipe(pipefd) == -1) {\n"
"        perror(\"pipe\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    cpid = fork();\n"
"    if (cpid == -1) {\n"
"        perror(\"fork\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    if (cpid == 0) {    /* Child reads from pipe */\n"
"        close(pipefd[1]);          /* Close unused write end */\n"
"\\&\n"
"        while (read(pipefd[0], &buf, 1) E<gt> 0)\n"
"            write(STDOUT_FILENO, &buf, 1);\n"
"\\&\n"
"        write(STDOUT_FILENO, \"\\en\", 1);\n"
"        close(pipefd[0]);\n"
"        _exit(EXIT_SUCCESS);\n"
"\\&\n"
"    } else {            /* Parent writes argv[1] to pipe */\n"
"        close(pipefd[0]);          /* Close unused read end */\n"
"        write(pipefd[1], argv[1], strlen(argv[1]));\n"
"        close(pipefd[1]);          /* Reader will see EOF */\n"
"        wait(NULL);                /* Wait for child */\n"
"        exit(EXIT_SUCCESS);\n"
"    }\n"
"}\n"
msgstr ""

#. #-#-#-#-#  archlinux: pipe.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  debian-bookworm: pipe.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  debian-unstable: pipe.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  fedora-39: pipe.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  fedora-rawhide: pipe.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  mageia-cauldron: pipe.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  opensuse-leap-15-6: pipe.2.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#. #-#-#-#-#  opensuse-tumbleweed: pipe.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "ZOBACZ TAKŻE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<fork>(2), B<read>(2), B<socketpair>(2), B<splice>(2), B<tee>(2), "
"B<vmsplice>(2), B<write>(2), B<popen>(3), B<pipe>(7)"
msgstr ""
"B<fork>(2), B<read>(2), B<socketpair>(2), B<splice>(2), B<tee>(2), "
"B<vmsplice>(2), B<write>(2), B<popen>(3), B<pipe>(7)"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "2023-02-05"
msgstr "5 lutego 2023 r."

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"/* On Alpha, IA-64, MIPS, SuperH, and SPARC/SPARC64, pipe() has the\n"
"   following prototype; see NOTES */\n"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron
msgid ""
"B<pipe2>()  was added in Linux 2.6.27; glibc support is available starting "
"with glibc 2.9."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid "B<pipe>(): POSIX.1-2001, POSIX.1-2008."
msgstr "B<pipe>(): POSIX.1-2001, POSIX.1-2008."

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid "B<pipe2>()  is Linux-specific."
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "NOTES"
msgstr "UWAGI"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "#include E<lt>complex.hE<gt>\n"
#| "#include E<lt>stdlib.hE<gt>\n"
#| "#include E<lt>unistd.hE<gt>\n"
#| "#include E<lt>stdio.hE<gt>\n"
msgid ""
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>string.hE<gt>\n"
"#include E<lt>sys/wait.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"
msgstr ""
"#include E<lt>complex.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "int\n"
#| "main(int argc, char *argv[])\n"
#| "{\n"
#| "    int pipefd[2];\n"
#| "    pid_t cpid;\n"
#| "    char buf;\n"
msgid ""
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    int    pipefd[2];\n"
"    char   buf;\n"
"    pid_t  cpid;\n"
msgstr ""
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    int pipefd[2];\n"
"    pid_t cpid;\n"
"    char buf;\n"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"    if (argc != 2) {\n"
"        fprintf(stderr, \"Usage: %s E<lt>stringE<gt>\\en\", argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
msgstr ""
"    if (argc != 2) {\n"
"        fprintf(stderr, \"Usage: %s E<lt>stringE<gt>\\en\", argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"    if (pipe(pipefd) == -1) {\n"
"        perror(\"pipe\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
msgstr ""
"    if (pipe(pipefd) == -1) {\n"
"        perror(\"pipe\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"    cpid = fork();\n"
"    if (cpid == -1) {\n"
"        perror(\"fork\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
msgstr ""
"    cpid = fork();\n"
"    if (cpid == -1) {\n"
"        perror(\"fork\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"    if (cpid == 0) {    /* Child reads from pipe */\n"
"        close(pipefd[1]);          /* Close unused write end */\n"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"        while (read(pipefd[0], &buf, 1) E<gt> 0)\n"
"            write(STDOUT_FILENO, &buf, 1);\n"
msgstr ""
"        while (read(pipefd[0], &buf, 1) E<gt> 0)\n"
"            write(STDOUT_FILENO, &buf, 1);\n"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"        write(STDOUT_FILENO, \"\\en\", 1);\n"
"        close(pipefd[0]);\n"
"        _exit(EXIT_SUCCESS);\n"
msgstr ""
"        write(STDOUT_FILENO, \"\\en\", 1);\n"
"        close(pipefd[0]);\n"
"        _exit(EXIT_SUCCESS);\n"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"    } else {            /* Parent writes argv[1] to pipe */\n"
"        close(pipefd[0]);          /* Close unused read end */\n"
"        write(pipefd[1], argv[1], strlen(argv[1]));\n"
"        close(pipefd[1]);          /* Reader will see EOF */\n"
"        wait(NULL);                /* Wait for child */\n"
"        exit(EXIT_SUCCESS);\n"
"    }\n"
"}\n"
msgstr ""

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.05"
msgstr "Linux man-pages 6.05"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "PIPE"
msgstr "PIPE"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2017-11-26"
msgstr "26 listopada 2017 r."

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Podręcznik programisty Linuksa"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"B<#define _GNU_SOURCE>             /* See feature_test_macros(7) */\n"
"B<#include E<lt>fcntl.hE<gt>>              /* Obtain O_* constant definitions */\n"
"B<#include E<lt>unistd.hE<gt>>\n"
msgstr ""

#. type: IP
#: opensuse-leap-15-6
#, no-wrap
msgid "*"
msgstr "*"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Set the B<O_NONBLOCK> file status flag on the two new open file "
"descriptions.  Using this flag saves extra calls to B<fcntl>(2)  to achieve "
"the same result."
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"On success, zero is returned.  On error, -1 is returned, and I<errno> is set "
"appropriately."
msgstr ""
"Po pomyślnym zakończeniu zwracane jest zero. Po błędzie zwracane jest -1 i "
"odpowiednio ustawiane jest I<errno>."

#.  http://austingroupbugs.net/view.php?id=467
#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"On Linux (and other systems), B<pipe>()  does not modify I<pipefd> on "
"failure.  A requirement standardizing this behavior was added in "
"POSIX.1-2016.  The Linux-specific B<pipe2>()  system call likewise does not "
"modify I<pipefd> on failure."
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"B<pipe2>()  was added to Linux in version 2.6.27; glibc support is available "
"starting with version 2.9."
msgstr ""

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "CONFORMING TO"
msgstr "ZGODNE Z"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "EXAMPLE"
msgstr "PRZYKŁAD"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"#include E<lt>sys/types.hE<gt>\n"
"#include E<lt>sys/wait.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"
"#include E<lt>string.hE<gt>\n"
msgstr ""
"#include E<lt>sys/types.hE<gt>\n"
"#include E<lt>sys/wait.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"
"#include E<lt>string.hE<gt>\n"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    int pipefd[2];\n"
"    pid_t cpid;\n"
"    char buf;\n"
msgstr ""
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    int pipefd[2];\n"
"    pid_t cpid;\n"
"    char buf;\n"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "COLOPHON"
msgstr "O STRONIE"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Angielska wersja tej strony pochodzi z wydania 4.16 projektu Linux I<man-"
"pages>. Opis projektu, informacje dotyczące zgłaszania błędów oraz najnowszą "
"wersję oryginału można znaleźć pod adresem \\%https://www.kernel.org/doc/man-"
"pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30 marca 2023 r."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"
