# Dutch translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Jos Boersema <joshb@xs4all.nl>, 2001.
# Mario Blättermann <mario.blaettermann@gmail.com>, 2019.
# Luc Castermans <luc.castermans@gmail.com>, 2021, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 2.15\n"
"POT-Creation-Date: 2023-08-27 17:21+0200\n"
"PO-Revision-Date: 2023-05-21 21:50+0200\n"
"Last-Translator: Luc Castermans <luc.castermans@gmail.com>\n"
"Language-Team: Dutch <kde-i18n-nl@kde.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 22.12.3\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "setreuid"
msgstr "setreuid"

#. type: TH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30 maart 2023"

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Linux man-pagina's 6.05.01"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NAAM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "setreuid, setregid - set real and/or effective user or group ID"
msgstr "setreuid, setregid - zet echte en/of geldende gebruiker of groep ID"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTHEEK"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Standard C bibliotheek  (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SAMENVATTING"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>unistd.hE<gt>>\n"
msgstr "B<#include E<lt>unistd.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"B<int setreuid(uid_t >I<ruid>B<, uid_t >I<euid>B<);>\n"
"B<int setregid(gid_t >I<rgid>B<, gid_t >I<egid>B<);>\n"
msgstr ""
"B<int setreuid(uid_t >I<ruid>B<, uid_t >I<euid>B<);>\n"
"B<int setregid(gid_t >I<rgid>B<, gid_t >I<egid>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr "Feature Test Macro´s eisen in  glibc (zie B<feature_test_macros>(7)):"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<setreuid>(), B<setregid>():"
msgstr "B<setreuid>(), B<setregid>():"

#.     || _XOPEN_SOURCE && _XOPEN_SOURCE_EXTENDED
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"    _XOPEN_SOURCE E<gt>= 500\n"
"        || /* Since glibc 2.19: */ _DEFAULT_SOURCE\n"
"        || /* glibc E<lt>= 2.19: */ _BSD_SOURCE\n"
msgstr ""
"    _XOPEN_SOURCE E<gt>= 500\n"
"        || /* Since glibc 2.19: */ _DEFAULT_SOURCE\n"
"        || /* Glibc E<lt>= 2.19: */ _BSD_SOURCE\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHRIJVING"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<setreuid>()  sets real and effective user IDs of the calling process."
msgstr ""
"B<setreuid>() zet echte en geldende gebruiker ID's van het huidige proces."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Supplying a value of -1 for either the real or effective user ID forces the "
"system to leave that ID unchanged."
msgstr ""
"Opgeven van een waarde van -1 voor of het echte of het geldende ID dwingt "
"het systeem dat ID onveranderd te laten."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Unprivileged processes may only set the effective user ID to the real user "
"ID, the effective user ID, or the saved set-user-ID."
msgstr ""
"Niet-geprivilegieerde processen mogen alleen het effectieve gebruiker ID "
"naar het geldende gebruiker ID, het effectieve gebruiker ID of het "
"opgeslagen set-user-ID  zetten."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Unprivileged users may only set the real user ID to the real user ID or the "
"effective user ID."
msgstr ""
"On-geprivilegieerde gebruikers mogen het echte gebruiker ID naar het "
"geldende gebruiker ID zetten en andersom."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If the real user ID is set (i.e., I<ruid> is not -1) or the effective user "
"ID is set to a value not equal to the previous real user ID, the saved set-"
"user-ID will be set to the new effective user ID."
msgstr ""
"Als het echte gebruiker ID veranderd wordt (i.e., I<ruid> is niet -1) of het "
"geldende gebruiker ID wordt naar een waarde gezet niet gelijk aan het vorige "
"echte gebruiker ID, dan zal het bewaarde ID gezet worden naar het nieuwe "
"geldende gebruiker ID."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Completely analogously, B<setregid>()  sets real and effective group ID's of "
"the calling process, and all of the above holds with \"group\" instead of "
"\"user\"."
msgstr ""
"Volledig analoog zet B<setregid>() echte en effectieve groep ID´s van het "
"aanroepende proces, en alles hierboven is geldig met \"groep\" in plaats van "
"\"gebruiker\"."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "EIND WAARDE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"On success, zero is returned.  On error, -1 is returned, and I<errno> is set "
"to indicate the error."
msgstr ""
"Bij succes wordt nul teruggegeven. Bij falen wordt -1 teruggegeven en wordt "
"I<errno> overeenkomstig gezet."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"I<Note>: there are cases where B<setreuid>()  can fail even when the caller "
"is UID 0; it is a grave security error to omit checking for a failure return "
"from B<setreuid>()."
msgstr ""
"I<Opmerking>: er zijn gevallen waarbij B<setreuid>() kan falen, zelfs "
"wanneer de aanroeper UID 0 is; het is een grove veiligheidsfout om de "
"terugkeer waarde  van B<setreuid>() niet te controleren. "

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "FOUTEN"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EAGAIN>"
msgstr "B<EAGAIN>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The call would change the caller's real UID (i.e., I<ruid> does not match "
"the caller's real UID), but there was a temporary failure allocating the "
"necessary kernel data structures."
msgstr ""
"De aanroep zou de echte UID van de aanroeper veranderen (m.a.w., I<ruid> "
"komt niet overeen met de echte UID van de aanroeper), maar er trad een "
"tijdelijke fout op bij het toekennen van de benodigde data structuren in de "
"kernel."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"I<ruid> does not match the caller's real UID and this call would bring the "
"number of processes belonging to the real user ID I<ruid> over the caller's "
"B<RLIMIT_NPROC> resource limit.  Since Linux 3.1, this error case no longer "
"occurs (but robust applications should check for this error); see the "
"description of B<EAGAIN> in B<execve>(2)."
msgstr ""
"I<ruid> komt niet overeen met de echte gebruiker ID van de aanroeper en deze "
"aanroep zou het aantal processen, behorende bij de echte gebruikers ID "
"I<ruid>, boven de resource limiet B<RLIMIT_NPROC> van de aanroeper brengen. "
"Vanaf Linux 3.1 komt deze fout niet meer voor (nog steeds zouden robuuste "
"applicaties op deze fout moeten controleren); zie de beschrijving van "
"B<EAGAIN> in B<execvd>(2)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"One or more of the target user or group IDs is not valid in this user "
"namespace."
msgstr ""
"Een of meer van de doel gebruiker of groep ID´s is niet geldig in de "
"gebruiker naamruimte."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EPERM>"
msgstr "B<EPERM>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The calling process is not privileged (on Linux, does not have the necessary "
"capability in its user namespace: B<CAP_SETUID> in the case of "
"B<setreuid>(), or B<CAP_SETGID> in the case of B<setregid>())  and a change "
"other than (i)  swapping the effective user (group) ID with the real user "
"(group) ID, or (ii) setting one to the value of the other or (iii) setting "
"the effective user (group) ID to the value of the saved set-user-ID (saved "
"set-group-ID) was specified."
msgstr ""
"De aanroepende proces is niet-geprivilegieerd (op Linux: heeft niet de "
"noodzakelijke capaciteit in zijn gebruiker naamruimte: B<CAP_SETUID> in het "
"geval van B<setreuid>(), of B<CAP_SETGID> in het geval van B<setregid>()) en "
"een verandering anders dan (i) ruilen van het effectieve gebruiker (groep) "
"ID met de echte gebruiker (groep) ID, of (ii) eentje zetten op de waarde van "
"de ander of (iii) zetten van de effectieve gebruiker (groep) ID op de waarde "
"van het opgeslagen set-user-ID (opgeslagen set-group-ID) werd "
"gespecificeerd. "

#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "VERSIES"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"POSIX.1 does not specify all of the UID changes that Linux permits for an "
"unprivileged process.  For B<setreuid>(), the effective user ID can be made "
"the same as the real user ID or the saved set-user-ID, and it is unspecified "
"whether unprivileged processes may set the real user ID to the real user ID, "
"the effective user ID, or the saved set-user-ID.  For B<setregid>(), the "
"real group ID can be changed to the value of the saved set-group-ID, and the "
"effective group ID can be changed to the value of the real group ID or the "
"saved set-group-ID.  The precise details of what ID changes are permitted "
"vary across implementations."
msgstr ""
"POSIX.1 specificeert niet alle UID veranderingen die Linux toestaat voor een "
"niet-geprivilegieerd proces. Met B<setreuid>() kan het effectieve gebruiker "
"ID identiek gemaakt worden aan het echte gebruiker ID of het opgeslagen set-"
"user-ID, en het is niet gespecificeerd of een niet-geprivilegieerd proces "
"het echte gebruiker ID op het echte gebruiker ID, het echte groep ID of het "
"opgeslagen set-user-ID mag zetten. Met B<setreuid>() kan het echte groep ID "
"verandert worden naar de waarde van het echte groep ID of het opgeslagen set-"
"group-ID. De precieze details omtrent welke ID veranderingen toegestaan "
"worden variëren per implementatie. "

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"POSIX.1 makes no specification about the effect of these calls on the saved "
"set-user-ID and saved set-group-ID."
msgstr ""
"POSIX.1  specificeert het effect van deze aanroepen op de opgeslagen set-"
"user-ID en opgeslagen set-group-ID niet."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "VOLDOET AAN"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr "POSIX.1-2008."

#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "GESCHIEDENIS"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "POSIX.1-2001, 4.3BSD (first appeared in 4.2BSD)."
msgstr "POSIX.1-2001, 4.3BSD (verschenen voor het eerst in 4.2BSD)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Setting the effective user (group) ID to the saved set-user-ID (saved set-"
"group-ID) is possible since Linux 1.1.37 (1.1.38)."
msgstr ""
"Zetten van het effectieve gebruiker (groep) ID op het opgeslagen set-user-ID "
"(opgeslagen set-group-ID) is mogelijk vanaf Linux 1.1.37 (1.1.38)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The original Linux B<setreuid>()  and B<setregid>()  system calls supported "
"only 16-bit user and group IDs.  Subsequently, Linux 2.4 added "
"B<setreuid32>()  and B<setregid32>(), supporting 32-bit IDs.  The glibc "
"B<setreuid>()  and B<setregid>()  wrapper functions transparently deal with "
"the variations across kernel versions."
msgstr ""
"De originele Linux B<setreuid>()  en B<setregid>()  systeem aanroepen "
"ondersteunen alleen 16-bit gebruiker en groep IDs.  Vervolgens, voegde Linux "
"2.4 B<setreuid32>()  en B<setregid32>() toe voor 32-bit IDs ondersteuning.  "
"De glibc B<setreuid>()  en  B<setregid>()  omwikkel functies handelen de "
"variaties in kernel versies transparant af."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "C library/kernel differences"
msgstr "C library/kernel verschillen"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"At the kernel level, user IDs and group IDs are a per-thread attribute.  "
"However, POSIX requires that all threads in a process share the same "
"credentials.  The NPTL threading implementation handles the POSIX "
"requirements by providing wrapper functions for the various system calls "
"that change process UIDs and GIDs.  These wrapper functions (including those "
"for B<setreuid>()  and B<setregid>())  employ a signal-based technique to "
"ensure that when one thread changes credentials, all of the other threads in "
"the process also change their credentials.  For details, see B<nptl>(7)."
msgstr ""
"Op kernel niveau zijn gebruiker ID en groep ID een per-thread attribute. "
"Hoewel POSIX vereist dat alle threads van een proces dezelfde identiteit "
"delen. De NPTL implementatie van threads implementeert de POSIX eis door in  "
"omwikkel functies te voorzien voor de diverse systeem aanroepen die de UID´s "
"en GID´s veranderen. Deze omwikkel functies (inclusief die ene voor "
"B<setreuid>() en B<setregid>())  gebruiken een op signalen gebaseerde "
"techniek om er van zeker te zijn dat als een thread zijn identiteit "
"verandert, alle andere threads van dat proces ook hun identiteit "
"veranderen.  Zie voor details B<nptl>(7)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "ZIE OOK"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<getgid>(2), B<getuid>(2), B<seteuid>(2), B<setgid>(2), B<setresuid>(2), "
"B<setuid>(2), B<capabilities>(7), B<credentials>(7), B<user_namespaces>(7)"
msgstr ""
"B<getgid>(2), B<getuid>(2), B<seteuid>(2), B<setgid>(2), B<setresuid>(2), "
"B<setuid>(2), B<capabilities>(7), B<credentials>(7), B<user_namespaces>(7)"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "2023-02-05"
msgstr "5 februari 2023"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pagina's 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid ""
"POSIX.1-2001, POSIX.1-2008, 4.3BSD (B<setreuid>()  and B<setregid>()  first "
"appeared in 4.2BSD)."
msgstr ""
"POSIX.1-2001, POSIX.1-2008, 4.3BSD (B<setreuid>()  en B<setregid>()  "
"verschenen voor het eerst in 4.2BSD)."

#. type: SH
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "NOTES"
msgstr "OPMERKINGEN"

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.05"
msgstr "Linux man-pagina's 6.05"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "SETREUID"
msgstr "SETREUID"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2017-09-15"
msgstr "15 september 2017"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Linux Programmeurs Handleiding"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<#include E<lt>sys/types.hE<gt>>"
msgstr "B<#include E<lt>sys/types.hE<gt>>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<#include E<lt>unistd.hE<gt>>"
msgstr "B<#include E<lt>unistd.hE<gt>>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<int setreuid(uid_t >I<ruid>B<, uid_t >I<euid>B<);>"
msgstr "B<int setreuid(uid_t >I<ruid>B<, uid_t >I<euid>B<);>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<int setregid(gid_t >I<rgid>B<, gid_t >I<egid>B<);>"
msgstr "B<int setregid(gid_t >I<rgid>B<, gid_t >I<egid>B<);>"

#.     || _XOPEN_SOURCE\ &&\ _XOPEN_SOURCE_EXTENDED
#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"_XOPEN_SOURCE\\ E<gt>=\\ 500\n"
"    || /* Since glibc 2.19: */ _DEFAULT_SOURCE\n"
"    || /* Glibc versions E<lt>= 2.19: */ _BSD_SOURCE\n"
msgstr ""
"_XOPEN_SOURCE\\ E<gt>=\\ 500\n"
"    || /* Sinds glibc 2.19: */ _DEFAULT_SOURCE\n"
"    || /* Glibc versies E<lt>= 2.19: */ _BSD_SOURCE\n"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"On success, zero is returned.  On error, -1 is returned, and I<errno> is set "
"appropriately."
msgstr ""
"Bij succes wordt nul teruggegeven. Bij falen wordt -1 teruggegeven en wordt "
"I<errno> overeenkomstig gezet."

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "CONFORMING TO"
msgstr "VOLDOET AAN"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "COLOPHON"
msgstr "COLOFON"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Deze pagina is onderdeel van release 4.16 van het Linux I<man-pages>-"
"project. Een beschrijving van het project, informatie over het melden van "
"bugs en de nieuwste versie van deze pagina zijn op \\%https://www.kernel.org/"
"doc/man-pages/ te vinden."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"
