# Dutch translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
#
# Mario Blättermann <mario.blaettermann@gmail.com>, 2020, 2021.
# Luc Castermans <luc.castermans@gmail.com>, 2021-2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.2.0\n"
"POT-Creation-Date: 2023-10-02 12:02+0200\n"
"PO-Revision-Date: 2023-10-23 12:39+0200\n"
"Last-Translator: Luc Castermans <luc.castermans@gmail.com>\n"
"Language-Team: Dutch <>\n"
"Language: nl_NL\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1)\n"
"X-Generator: Lokalize 22.12.3\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "GRUB-FSTEST"
msgstr "GRUB-FSTEST"

#. type: TH
#: archlinux
#, no-wrap
msgid "July 2023"
msgstr "Juli 2023"

#. type: TH
#: archlinux
#, no-wrap
msgid "GRUB 2:2.12rc1-1"
msgstr "GRUB 2:2.12rc1-1"

#. type: TH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "User Commands"
msgstr "Opdrachten voor gebruikers"

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "NAME"
msgstr "NAAM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "grub-fstest - debug tool for GRUB filesystem drivers"
msgstr ""
"grub-fstest - Debug-gereedschap voor GRUB bestandssysteem-stuurprogrammas"

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "SYNOPSIS"
msgstr "SAMENVATTING"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "B<grub-fstest> [I<\\,OPTION\\/>...] I<\\,IMAGE_PATH COMMANDS\\/>"
msgstr "B<grub-fstest> [I<\\,OPTIE\\/>...] I<\\,IMAGE-PAD OPDRACHTEN\\/>"

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHRIJVING"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Debug tool for filesystem driver."
msgstr "Debug-gereedschap voor bestandssysteem-stuurprogramma."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Commands:"
msgstr "Commandos:"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "blocklist FILE"
msgstr "blocklist BESTAND"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Display blocklist of FILE."
msgstr "De blokkenlijst van BESTAND tonen."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "cat FILE"
msgstr "cat BESTAND"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Copy FILE to standard output."
msgstr "Dit BESTAND naar standaarduitvoer kopiëren."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "cmp FILE LOCAL"
msgstr "cmp BESTAND DEZE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Compare FILE with local file LOCAL."
msgstr "BESTAND met lokaal bestand DEZE vergelijken."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "cp FILE LOCAL"
msgstr "cp BESTAND DEZE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Copy FILE to local file LOCAL."
msgstr "BESTAND naar lokaal bestand DEZE kopiëren."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "crc FILE"
msgstr "crc BESTAND"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Get crc32 checksum of FILE."
msgstr "De CRC32-controlesom van BESTAND berekenen."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "hex FILE"
msgstr "hex BESTAND"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Show contents of FILE in hex."
msgstr "De inhoud van BESTAND in hexadecimaal tonen."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "ls PATH"
msgstr "ls PAD"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "List files in PATH."
msgstr "De bestanden in de map PAD tonen."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "xnu_uuid DEVICE"
msgstr "xnu_uuid APPARAAT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Compute XNU UUID of the device."
msgstr "De XNU-UUID van APPARAAT berekenen."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-c>, B<--diskcount>=I<\\,NUM\\/>"
msgstr "B<-c>, B<--diskcount>=I<\\,NUM\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Specify the number of input files."
msgstr "Te gebruiken aantal invoerbestanden."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-C>, B<--crypto>"
msgstr "B<-C>, B<--crypto>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Mount crypto devices."
msgstr "Versleutelde apparaten aankoppelen."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-d>, B<--debug>=I<\\,STRING\\/>"
msgstr "B<-d>, B<--debug>=I<\\,TEKENREEKS\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Set debug environment variable."
msgstr "Een debug-omgevingsvariabele instellen."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-K>, B<--zfs-key>=I<\\,FILE\\/>|prompt"
msgstr "B<-K>, B<--zfs-key>=I<\\,BESTAND\\/>|prompt"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Load zfs crypto key."
msgstr "De ZFS-versleutelingsleutel laden."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-n>, B<--length>=I<\\,NUM\\/>"
msgstr "B<-n>, B<--length>=I<\\,NUM\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Handle N bytes in output file."
msgstr "N bytes in uitvoerbestand verwerken."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-r>, B<--root>=I<\\,DEVICE_NAME\\/>"
msgstr "B<-r>, B<--root>=I<\\,APPARAATNAAM\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Set root device."
msgstr "root-apparaat instellen."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-s>, B<--skip>=I<\\,NUM\\/>"
msgstr "B<-s>, B<--skip>=I<\\,NUM\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Skip N bytes from output file."
msgstr "N bytes in uitvoerbestand overslaan."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-u>, B<--uncompress>"
msgstr "B<-u>, B<--uncompress>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Uncompress data."
msgstr "Gegevens decomprimeren."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-v>, B<--verbose>"
msgstr "B<-v>, B<--verbose>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "print verbose messages."
msgstr "uitgebreide meldingen tonen."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "-?, B<--help>"
msgstr "-?, B<--help>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "give this help list"
msgstr "een hulptekst tonen"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<--usage>"
msgstr "B<--usage>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "give a short usage message"
msgstr "een korte gebruikssamenvatting tonen"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "print program version"
msgstr "programmaversie tonen"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid ""
"Mandatory or optional arguments to long options are also mandatory or "
"optional for any corresponding short options."
msgstr ""
"Een verplicht argument bij een lange optie geldt ook voor de korte vorm."

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "SEE ALSO"
msgstr "ZIE OOK"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "B<grub-probe>(8)"
msgstr "B<grub-probe>(8)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid ""
"The full documentation for B<grub-fstest> is maintained as a Texinfo "
"manual.  If the B<info> and B<grub-fstest> programs are properly installed "
"at your site, the command"
msgstr ""
"De volledige documentatie voor B<grub-fstest> wordt bijgehouden als een "
"Texinfo-handleiding. Als de programma´s B<info> en B<grub-fstest> correct op "
"uw systeem zijn geïnstalleerd, geeft de opdracht"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "B<info grub-fstest>"
msgstr "B<info grub-fstest>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "should give you access to the complete manual."
msgstr "toegang tot de volledige handleiding."

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "April 2023"
msgstr "April 2023"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "GRUB 2.06-13"
msgstr "GRUB 2.06-13"

#. type: TH
#: debian-unstable
#, no-wrap
msgid "September 2023"
msgstr "September 2023"

#. type: TH
#: debian-unstable
#, no-wrap
#| msgid "GRUB 2:2.12rc1-1"
msgid "GRUB 2.12~rc1-10"
msgstr "GRUB 2.12~rc1-10"
