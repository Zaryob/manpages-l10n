# Vietnamese translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-08-27 17:20+0200\n"
"PO-Revision-Date: 2022-01-18 19:49+0100\n"
"Last-Translator: Automatically generated\n"
"Language-Team: Vietnamese <>\n"
"Language: vi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SDIFF"
msgstr "SDIFF"

#. type: TH
#: archlinux fedora-39 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "May 2023"
msgstr "Tháng 5 năm 2023"

#. type: TH
#: archlinux fedora-39 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "diffutils 3.10"
msgstr "diffutils 3.10"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr "Các câu lệnh"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "TÊN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid "Side-by-side merge of differences between FILE1 and FILE2."
msgid "sdiff - side-by-side merge of file differences"
msgstr "Hòa trộn những khác biệt TẬP_TIN1 VÀ TẬP_TIN2 cạnh nhau."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "TÓM TẮT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<sdiff> [I<OPTION>]... I<FILE1 FILE2>"
msgstr "B<sdiff> [I<TÙY_CHỌN>]… I<TẬP_TIN1 TẬP_TIN2>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "MÔ TẢ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Side-by-side merge of differences between FILE1 and FILE2."
msgstr "Hòa trộn những khác biệt TẬP_TIN1 VÀ TẬP_TIN2 cạnh nhau."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Mandatory arguments to long options are mandatory for short options too."
msgstr "Tùy chọn dài yêu cầu đối số thì tùy chọn ngắn cũng vậy."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-o>, B<--output>=I<FILE>"
msgstr "B<-o>, B<--output>=I<TẬP_TIN>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "operate interactively, sending output to FILE"
msgstr "Thực hiện một cách tương tác, gửi kết xuất ra TẬP-TIN"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-i>, B<--ignore-case>"
msgstr "B<-i>, B<--ignore-case>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "consider upper- and lower-case to be the same"
msgstr "coi chữ HOA và thường là như nhau"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-E>, B<--ignore-tab-expansion>"
msgstr "B<-E>, B<--ignore-tab-expansion>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "ignore changes due to tab expansion"
msgstr "bỏ qua mọi thay đổi do mở rộng tab"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-Z>, B<--ignore-trailing-space>"
msgstr "B<-Z>, B<--ignore-trailing-space>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "ignore white space at line end"
msgstr "bỏ qua tất cả dấu cách ở cuối dòng"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-b>, B<--ignore-space-change>"
msgstr "B<-b>, B<--ignore-space-change>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "ignore changes in the amount of white space"
msgstr "bỏ qua thay đổi gây ra bởi nhóm dấu cách"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-W>, B<--ignore-all-space>"
msgstr "B<-W>, B<--ignore-all-space>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "ignore all white space"
msgstr "bỏ qua tất cả dấu cách"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-B>, B<--ignore-blank-lines>"
msgstr "B<-B>, B<--ignore-blank-lines>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "ignore changes whose lines are all blank"
msgstr "bỏ qua thay đổi do đó là dòng trống"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-I>, B<--ignore-matching-lines>=I<RE>"
msgstr "B<-I>, B<--ignore-matching-lines>=I<RE>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "ignore changes all whose lines match RE"
msgstr "bỏ qua thay đổi do mà mọi dòng khớp với RE"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--strip-trailing-cr>"
msgstr "B<--strip-trailing-cr>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "strip trailing carriage return on input"
msgstr "cắt bỏ ký tự về đầu dòng (cr) theo sau khi nhập"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-a>, B<--text>"
msgstr "B<-a>, B<--text>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "treat all files as text"
msgstr "coi mọi tập tin là văn bản thường"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-w>, B<--width>=I<NUM>"
msgstr "B<-w>, B<--width>=I<SỐ>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "output at most NUM (default 130) print columns"
msgstr "kết xuất nhiểu nhất là SỐ cột (mặc định là 130)"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-l>, B<--left-column>"
msgstr "B<-l>, B<--left-column>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "output only the left column of common lines"
msgstr "kết xuất chỉ cột bên trái của các dòng chung nhau"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-s>, B<--suppress-common-lines>"
msgstr "B<-s>, B<--suppress-common-lines>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "do not output common lines"
msgstr "không kết xuất các dòng chung nhau"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-t>, B<--expand-tabs>"
msgstr "B<-t>, B<--expand-tabs>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "expand tabs to spaces in output"
msgstr "khai triển ký tự tab thành các khoảng trắng khi kết xuất"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--tabsize>=I<NUM>"
msgstr "B<--tabsize>=I<SỐ>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "tab stops at every NUM (default 8) print columns"
msgstr "điểm đừng tab sau mỗi SỐ cột đã in ra (mặc định là 8)"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-d>, B<--minimal>"
msgstr "B<-d>, B<--minimal>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "try hard to find a smaller set of changes"
msgstr "cố tìm một thay đổi nhỏ"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-H>, B<--speed-large-files>"
msgstr "B<-H>, B<--speed-large-files>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "assume large files, many scattered small changes"
msgstr "Giả định _tập tin lớn_ và nhiều thay đổi nhỏ rải rác (_tốc độ_)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--diff-program>=I<PROGRAM>"
msgstr "B<--diff-program>=I<CHƯƠNG_TRÌNH>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "use PROGRAM to compare files"
msgstr "dùng CHƯƠNG_TRÌNH để so sánh các tập tin"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "display this help and exit"
msgstr "hiển thị trợ giúp này rồi thoát"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-v>, B<--version>"
msgstr "B<-v>, B<--version>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "output version information and exit"
msgstr "đưa ra thông tin phiên bản rồi thoát"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If a FILE is '-', read standard input.  Exit status is 0 if inputs are the "
"same, 1 if different, 2 if trouble."
msgstr ""
"Nếu TẬP-TIN là “-”, thì đọc từ đầu vào tiêu chuẩn. Trạng thái thoát là số 0 "
"nếu hai đầu vào bằng nhau, số 1 nếu là khác nhau và số 2 nếu gặp lỗi."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "TÁC GIẢ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Written by Thomas Lord."
msgstr "Tác giả: Thomas Lord."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "THÔNG BÁO LỖI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Report bugs to: bug-diffutils@gnu.org"
msgstr ""
"Thông báo lỗi nào cho: E<lt>bug-diffutils@gnu.orgE<gt>; Thông báo lỗi dịch "
"nào cho: E<lt>http://translationproject.org/team/vi.htmlE<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"GNU diffutils home page: E<lt>https://www.gnu.org/software/diffutils/E<gt>"
msgstr ""
"Trang chủ GNU diffutils: E<lt>https://www.gnu.org/software/diffutils/E<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "General help using GNU software: E<lt>https://www.gnu.org/gethelp/E<gt>"
msgstr ""
"Trợ giúp chung về cách sử dụng phần mềm GNU: E<lt>https://www.gnu.org/"
"gethelp/E<gt>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "COPYRIGHT"
msgstr "BẢN QUYỀN"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"Copyright \\(co 2023 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2023 Free Software Foundation, Inc.  Giấy phép GPL pb3+ : "
"Giấy phép Công cộng GNU phiên bản 3 hay sau E<lt>https://gnu.org/licenses/"
"gpl.htmlE<gt>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""
"Đây là phần mềm tự do: bạn có quyền sửa đổi và phát hành lại nó. KHÔNG CÓ "
"BẢO HÀNH GÌ CẢ, với điều khiển được pháp luật cho phép."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "XEM THÊM"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid "cmp(1), diff(1), diff3(1)"
msgid "B<cmp>(1), B<diff>(1), B<diff3>(1)"
msgstr "B<cmp>(1), B<diff>(1), B<diff3>(1)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The full documentation for B<sdiff> is maintained as a Texinfo manual.  If "
"the B<info> and B<sdiff> programs are properly installed at your site, the "
"command"
msgstr ""
"Tài liệu hướng dẫn đầy đủ về B<sdiff> được bảo trì dưới dạng một sổ tay "
"Texinfo.  Nếu chương trình B<info> và B<sdiff> được cài đặt đúng ở địa chỉ "
"của bạn thì câu lệnh"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<info sdiff>"
msgstr "B<info sdiff>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "should give you access to the complete manual."
msgstr "nên cho phép bạn truy cập đến toàn bộ sổ tay."

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "August 2021"
msgstr "Tháng 8 năm 2021"

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "diffutils 3.8"
msgstr "diffutils 3.8"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"Copyright \\(co 2021 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2021 Free Software Foundation, Inc.  Giấy phép GPL pb3+ : "
"Giấy phép Công cộng GNU phiên bản 3 hay sau E<lt>https://gnu.org/licenses/"
"gpl.htmlE<gt>."

#. type: Plain text
#: debian-bookworm debian-unstable opensuse-leap-15-6
msgid "cmp(1), diff(1), diff3(1)"
msgstr "B<cmp>(1), B<diff>(1), B<diff3>(1)"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "May 2017"
msgstr "Tháng 5 năm 2017"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "diffutils 3.6"
msgstr "diffutils 3.6"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"GNU diffutils home page: E<lt>http://www.gnu.org/software/diffutils/E<gt>"
msgstr ""
"Trang chủ GNU diffutils: E<lt>https://www.gnu.org/software/diffutils/E<gt>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "General help using GNU software: E<lt>http://www.gnu.org/gethelp/E<gt>"
msgstr ""
"Trợ giúp chung về sử dụng phần mềm GNU: E<lt>https://www.gnu.org/gethelp/"
"E<gt>"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Copyright \\(co 2017 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>http://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2017 Free Software Foundation, Inc.  Giấy phép GPL pb3+ : "
"Giấy phép Công cộng GNU phiên bản 3 hay sau E<lt>http://gnu.org/licenses/gpl."
"htmlE<gt>."
