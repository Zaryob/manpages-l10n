# Vietnamese translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-07-25 19:36+0200\n"
"PO-Revision-Date: 2022-01-18 19:49+0100\n"
"Last-Translator: Automatically generated\n"
"Language-Team: Vietnamese <>\n"
"Language: vi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "GRUB-REBOOT"
msgstr "GRUB-REBOOT"

#. type: TH
#: archlinux
#, no-wrap
msgid "July 2023"
msgstr "Tháng 7 năm 2023"

#. type: TH
#: archlinux
#, no-wrap
msgid "GRUB 2:2.12rc1-1"
msgstr "GRUB 2:2.12rc1-1"

#. type: TH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "System Administration Utilities"
msgstr "Tiện ích quản trị hệ thống"

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "NAME"
msgstr "TÊN"

#. type: Plain text
#: archlinux
msgid ""
"grub-reboot - set the default boot entry for GRUB, for the next boot only"
msgstr ""
"grub-reboot - Đặt mục menu mặc định sẽ khởi động cho GRUB, chú ý là chỉ áp "
"dụng cho lần khởi động sau."

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "SYNOPSIS"
msgstr "TÓM TẮT"

#. type: Plain text
#: archlinux
msgid "B<grub-reboot> [I<\\,OPTION\\/>] I<\\,MENU_ENTRY\\/>"
msgstr "B<grub-reboot> [I<\\,CÁC-TÙY_CHỌN\\/>] I<\\,ĐIỂM-VÀO-MENU\\/>"

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "DESCRIPTION"
msgstr "MÔ TẢ"

#. type: Plain text
#: archlinux
msgid "Set the default boot menu entry for GRUB, for the next boot only."
msgstr ""
"Đặt mục menu mặc định sẽ khởi động cho GRUB, chú ý là chỉ áp dụng cho lần "
"khởi động sau."

#. type: TP
#: archlinux
#, no-wrap
msgid "B<-h>, B<--help>"
msgstr "B<-h>, B<--help>"

#. type: Plain text
#: archlinux
msgid "print this message and exit"
msgstr "hiển thị trợ giúp này rồi thoát"

#. type: TP
#: archlinux
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: archlinux
msgid "print the version information and exit"
msgstr "đưa ra thông tin phiên bản rồi thoát"

#. type: TP
#: archlinux
#, no-wrap
msgid "B<--boot-directory>=I<\\,DIR\\/>"
msgstr "B<--boot-directory>=I<\\,TMỤC\\/>"

#. type: Plain text
#: archlinux
msgid ""
"expect GRUB images under the directory DIR/grub instead of the I<\\,/boot/"
"grub\\/> directory"
msgstr ""
"cần ảnh GRUB nằm dưới thư mục T-MỤC/grub thay vì tại thư mục I<\\,/boot/"
"grub\\/>"

#. type: Plain text
#: archlinux
msgid ""
"MENU_ENTRY is a number, a menu item title or a menu item identifier. Please "
"note that menu items in submenus or sub-submenus require specifying the "
"submenu components and then the menu item component. The titles should be "
"separated using the greater-than character (E<gt>) with no extra spaces. "
"Depending on your shell some characters including E<gt> may need escaping. "
"More information about this is available in the GRUB Manual in the section "
"about the 'default' command."
msgstr ""
"ĐIỂM-VÀO-MENU là một con số, một tiêu đề hay định danh của một trình đơn. "
"Hãy chú ý là các mục trình đơn trong trình đơn con hoặc là trình đơn cháu "
"yêu cầu phải chỉ ra thành phần của trình đơn con và sau đó là thành phần của "
"mục trình đơn. Tiêu đề phải được ngăn cách sử dụng ký tự lớn hơn (E<gt>) mà "
"không có thêm các khoảng trắng thêm. Phụ thuộc vào hệ vỏ của bạn mà một số "
"ký tự bao gồm E<gt> cần được thoát chuỗi. Để có thêm thông tin hãy xem trong "
"sổ tay hướng dẫn GRUB tại phần lệnh “default”."

#. type: Plain text
#: archlinux
msgid ""
"NOTE: In cases where GRUB cannot write to the environment block, such as "
"when it is stored on an MDRAID or LVM device, the chosen boot menu entry "
"will remain the default even after reboot."
msgstr ""

#. type: SH
#: archlinux
#, no-wrap
msgid "REPORTING BUGS"
msgstr "THÔNG BÁO LỖI"

#. type: Plain text
#: archlinux
msgid "Report bugs to E<lt>bug-grub@gnu.orgE<gt>."
msgstr ""
"Hãy thông báo lỗi cho  E<lt>bug-grub@gnu.orgE<gt>. Thông báo lỗi dịch cho: "
"E<lt>http://translationproject.org/team/vi.htmlE<gt>."

#. type: SH
#: archlinux
#, no-wrap
msgid "SEE ALSO"
msgstr "XEM THÊM"

#. type: Plain text
#: archlinux
msgid "B<grub-set-default>(8), B<grub-editenv>(1)"
msgstr "B<grub-set-default>(8), B<grub-editenv>(1)"

#. type: Plain text
#: archlinux
msgid ""
"The full documentation for B<grub-reboot> is maintained as a Texinfo "
"manual.  If the B<info> and B<grub-reboot> programs are properly installed "
"at your site, the command"
msgstr ""
"Tài liệu hướng dẫn đầy đủ về B<grub-reboot> được bảo trì dưới dạng một sổ "
"tay Texinfo.  Nếu chương trình B<info> và B<grub-reboot> được cài đặt đúng ở "
"địa chỉ của bạn thì câu lệnh"

#. type: Plain text
#: archlinux
msgid "B<info grub-reboot>"
msgstr "B<info grub-reboot>"

#. type: Plain text
#: archlinux
msgid "should give you access to the complete manual."
msgstr "nên cho phép bạn truy cập đến toàn bộ sổ tay."

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "November 2003"
msgstr "Tháng 11 năm 2003"

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "grub-reboot 0.01"
msgstr "grub-reboot 0.01"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "grub-reboot - manual page for grub-reboot 0.01"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "B<grub-reboot> I<entry >[I<options to grub>]"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "grub-reboot"
msgstr "grub-reboot"

#. type: Plain text
#: debian-bookworm debian-unstable
#, fuzzy
#| msgid "Reboot into firmware setup menu."
msgid "Reboots into the specified OS entry in menu.lst"
msgstr "Khởi động lại vào trình đơn cài đặt firmware"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "(where \"entry\" is the entry number in menu.lst)"
msgstr ""
