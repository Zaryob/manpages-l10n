# Ukrainian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Yuri Chornoivan <yurchor@ukr.net>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-08-27 16:52+0200\n"
"PO-Revision-Date: 2022-07-21 23:18+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <trans-uk@lists.fedoraproject.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"
"X-Generator: Lokalize 20.12.0\n"

#. type: TH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "CHCPU"
msgstr "CHCPU"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-05-11"
msgstr "11 травня 2022 року"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "util-linux 2.38.1"
msgstr "util-linux 2.38.1"

#. type: TH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "System Administration"
msgstr "Керування системою"

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "NAME"
msgstr "НАЗВА"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "chcpu - configure CPUs"
msgstr "chcpu — налаштовування процесорів"

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "SYNOPSIS"
msgstr "КОРОТКИЙ ОПИС"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<chcpu> B<-c>|B<-d>|B<-e>|B<-g> I<cpu-list>"
msgstr "B<chcpu> B<-c>|B<-d>|B<-e>|B<-g> I<список-процесорів>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<chcpu> B<-p> I<mode>"
msgstr "B<chcpu> B<-p> I<режим>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<chcpu> B<-r>|B<-h>|B<-V>"
msgstr "B<chcpu> B<-r>|B<-h>|B<-V>"

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИС"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"B<chcpu> can modify the state of CPUs. It can enable or disable CPUs, scan "
"for new CPUs, change the CPU dispatching I<mode> of the underlying "
"hypervisor, and request CPUs from the hypervisor (configure) or return CPUs "
"to the hypervisor (deconfigure)."
msgstr ""
"B<chcpu> може вносити зміни до стану процесорів. Програма може вмикати або "
"вимикати процесори, шукати нові процесори, змінювати I<режим> розподілу у "
"базовому гіпервізорі і забирати процесори у гіпервізора (налаштовувати) або "
"повертати процесори гіпервізору (скасовувати налаштовування)."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Some options have a I<cpu-list> argument. Use this argument to specify a "
"comma-separated list of CPUs. The list can contain individual CPU addresses "
"or ranges of addresses. For example, B<0,5,7,9-11> makes the command "
"applicable to the CPUs with the addresses 0, 5, 7, 9, 10, and 11."
msgstr ""
"У деяких параметрів є аргумент I<список-процесорів>. Скористайтеся цим "
"аргументом для визначення списку відокремлених комами процесорів. Список "
"може містити адреси окремих процесорів або діапазони адрес. Наприклад, "
"B<0,5,7,9-11> застосує команду до процесорів із адресами 0, 5, 7, 9, 10 і 11."

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "OPTIONS"
msgstr "ПАРАМЕТРИ"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-c>, B<--configure> I<cpu-list>"
msgstr "B<-c>, B<--configure> I<список-процесорів>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Configure the specified CPUs. Configuring a CPU means that the hypervisor "
"takes a CPU from the CPU pool and assigns it to the virtual hardware on "
"which your kernel runs."
msgstr ""
"Налаштувати вказані процесори. Налаштовування процесора означає, що "
"гіпервізор бере процесор з буфера процесорів і призначає його до "
"віртуального апаратного забезпечення, на якому працює ядро."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-d>, B<--disable> I<cpu-list>"
msgstr "B<-d>, B<--disable> I<список-процесорів>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Disable the specified CPUs. Disabling a CPU means that the kernel sets it "
"offline."
msgstr ""
"Вимкнути вказані процесори. Вимикання процесора означає, що ядро переводить "
"його до стану недоступності."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-e>, B<--enable> I<cpu-list>"
msgstr "B<-e>, B<--enable> I<список-процесорів>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Enable the specified CPUs. Enabling a CPU means that the kernel sets it "
"online. A CPU must be configured, see B<-c>, before it can be enabled."
msgstr ""
"Увімкнути вказані процесори. Вмикання процесора означає, що ядро встановлює "
"для нього стан доступності. Процесор має бути налаштовано — див. B<-c> — "
"перш ніж його можна буде увімкнути."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-g>, B<--deconfigure> I<cpu-list>"
msgstr "B<-g>, B<--deconfigure> I<список-процесорів>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Deconfigure the specified CPUs. Deconfiguring a CPU means that the "
"hypervisor removes the CPU from the virtual hardware on which the Linux "
"instance runs and returns it to the CPU pool. A CPU must be offline, see B<-"
"d>, before it can be deconfigured."
msgstr ""
"Скасувати налаштовування вказаних процесорів. Скасовування налаштовування "
"процесорів означає, що гіпервізор вилучає процесор з віртуального апаратного "
"забезпечення, на якому запущено Linux і повертає процесор до буфера "
"процесорів. Процесор має бути недоступним, див. B<-d>, перш ніж "
"налаштовування процесора може бути скасовано."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-p>, B<--dispatch> I<mode>"
msgstr "B<-p>, B<--dispatch> I<режим>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Set the CPU dispatching I<mode> (polarization). This option has an effect "
"only if your hardware architecture and hypervisor support CPU polarization. "
"Available I<modes> are:"
msgstr ""
"Встановити I<режим> розподілу процесорів (поляризацію). Цей параметр працює, "
"лише якщо в архітектурі апаратного забезпечення і у гіпервізорі передбачено "
"поляризацію процесорів. Доступними є такі I<режими>:"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<horizontal>"
msgstr "B<horizontal>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "The workload is spread across all available CPUs."
msgstr "Навантаження розподіляється між усіма доступними процесорами."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<vertical>"
msgstr "B<vertical>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "The workload is concentrated on few CPUs."
msgstr "Навантаження концентрується на декількох процесорах."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-r>, B<--rescan>"
msgstr "B<-r>, B<--rescan>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Trigger a rescan of CPUs. After a rescan, the Linux kernel recognizes the "
"new CPUs. Use this option on systems that do not automatically detect newly "
"attached CPUs."
msgstr ""
"Увімкнути повторне сканування процесорів. Після повторного сканування ядро "
"Linux розпізнаватиме нові процесори. Скористайтеся цим параметром у "
"системах, де не передбачено автоматичного виявлення нових долучених "
"процесорів."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-h>, B<--help>"
msgstr "B<-h>, B<--help>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Display help text and exit."
msgstr "Вивести текст довідки і завершити роботу."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: debian-bookworm
msgid "Print version and exit."
msgstr "Вивести дані щодо версії і завершити роботу."

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "EXIT STATUS"
msgstr "СТАН ВИХОДУ"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<chcpu> has the following exit status values:"
msgstr "У B<chcpu> передбачено такі значення стану виходу:"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<0>"
msgstr "B<0>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "success"
msgstr "успіх"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<1>"
msgstr "B<1>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "failure"
msgstr "невдача"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<64>"
msgstr "B<64>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "partial success"
msgstr "частковий успіх"

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "AUTHORS"
msgstr "АВТОРИ"

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "COPYRIGHT"
msgstr "АВТОРСЬКІ ПРАВА"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Copyright IBM Corp. 2011"
msgstr "Авторські права належать корпорації IBM, 2011"

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "SEE ALSO"
msgstr "ДИВ. ТАКОЖ"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<lscpu>(1)"
msgstr "B<lscpu>(1)"

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "REPORTING BUGS"
msgstr "ЗВІТИ ПРО ВАДИ"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "For bug reports, use the issue tracker at"
msgstr "Для звітування про вади використовуйте систему стеження помилками на"

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "AVAILABILITY"
msgstr "ДОСТУПНІСТЬ"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"The B<chcpu> command is part of the util-linux package which can be "
"downloaded from"
msgstr "B<chcpu> є частиною пакунка util-linux, який можна отримати з"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2022-02-14"
msgstr "14 лютого 2022 року"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "util-linux 2.37.4"
msgstr "util-linux 2.37.4"

#. type: Plain text
#: opensuse-leap-15-6
msgid "Display version information and exit."
msgstr "Вивести дані щодо версії і завершити роботу."
