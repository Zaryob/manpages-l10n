# Ukrainian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Yuri Chornoivan <yurchor@ukr.net>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-08-27 17:33+0200\n"
"PO-Revision-Date: 2022-12-27 18:48+0200\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <trans-uk@lists.fedoraproject.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"
"X-Generator: Lokalize 20.12.0\n"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "XZLESS"
msgstr "XZLESS"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2010-09-27"
msgstr "27 вересня 2010 року"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Tukaani"
msgstr "Tukaani"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "XZ Utils"
msgstr "XZ Utils"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "NAME"
msgstr "НАЗВА"

#. type: Plain text
#: opensuse-leap-15-6
msgid "xzless, lzless - view xz or lzma compressed (text) files"
msgstr "xzless, lzless — перегляд стиснених xz або lzma (текстових) файлів"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "SYNOPSIS"
msgstr "КОРОТКИЙ ОПИС"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<xzless> [I<file>...]"
msgstr "B<xzless> [I<файл>...]"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<lzless> [I<file>...]"
msgstr "B<lzless> [I<файл>...]"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИС"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"B<xzless> is a filter that displays text from compressed files to a "
"terminal.  It works on files compressed with B<xz>(1)  or B<lzma>(1).  If no "
"I<files> are given, B<xzless> reads from standard input."
msgstr ""
"B<xzless> є фільтром, який показує текст зі стиснених файлів у терміналі. "
"Працює для файлів, які стиснуто за допомогою B<xz>(1) або B<lzma>(1). Якщо "
"не вказано жодного I<файла>, B<xzless> читатиме дані зі стандартного джерела "
"вхідних даних."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"B<xzless> uses B<less>(1)  to present its output.  Unlike B<xzmore>, its "
"choice of pager cannot be altered by setting an environment variable.  "
"Commands are based on both B<more>(1)  and B<vi>(1)  and allow back and "
"forth movement and searching.  See the B<less>(1)  manual for more "
"information."
msgstr ""
"Для показу виведених даних B<xzless> використовує B<less>(1). На відміну від "
"B<xzmore>, вибір програми для поділу на сторінки не можна змінити за "
"допомогою змінної середовища. Команди засновано на B<more>(1) і B<vi>(1). За "
"допомогою команд можна просуватися назад і вперед даними та шукати дані. Щоб "
"дізнатися більше, ознайомтеся із підручником з B<less>(1)."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"The command named B<lzless> is provided for backward compatibility with LZMA "
"Utils."
msgstr ""
"Команду B<lzless> реалізовано для забезпечення зворотної сумісності з LZMA "
"Utils."

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "ENVIRONMENT"
msgstr "СЕРЕДОВИЩЕ"

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "B<LESSMETACHARS>"
msgstr "B<LESSMETACHARS>"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"A list of characters special to the shell.  Set by B<xzless> unless it is "
"already set in the environment."
msgstr ""
"Список символів, які є особливими символами командної оболонки. "
"Встановлюється B<xzless>, якщо його ще не встановлено у середовищі."

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "B<LESSOPEN>"
msgstr "B<LESSOPEN>"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Set to a command line to invoke the B<xz>(1)  decompressor for preprocessing "
"the input files to B<less>(1)."
msgstr ""
"Має значення рядка команди для виклику засобу розпаковування B<xz>(1) для "
"обробки вхідних файлів B<less>(1)."

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "SEE ALSO"
msgstr "ДИВ. ТАКОЖ"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<less>(1), B<xz>(1), B<xzmore>(1), B<zless>(1)"
msgstr "B<less>(1), B<xz>(1), B<xzmore>(1), B<zless>(1)"
