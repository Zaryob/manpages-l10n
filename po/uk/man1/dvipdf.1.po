# Ukrainian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Andrij Mizyk <andm1zyk@proton.me>, 2022.
# Yuri Chornoivan <yurchor@ukr.net>, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-10-02 11:56+0200\n"
"PO-Revision-Date: 2023-07-06 20:30+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"
"X-Generator: Lokalize 20.12.0\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "DVIPDF"
msgstr "DVIPDF"

#. type: TH
#: archlinux debian-unstable mageia-cauldron
#, no-wrap
msgid "13 September 2023"
msgstr "13 вересня 2023 року"

#. type: TH
#: archlinux debian-unstable mageia-cauldron
#, no-wrap
msgid "10.02.0"
msgstr "10.02.0"

#. type: TH
#: archlinux debian-bookworm debian-unstable mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "Ghostscript"
msgstr "Ghostscript"

#. type: SH
#: archlinux debian-bookworm debian-unstable mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "НАЗВА"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron
#: opensuse-tumbleweed
msgid "dvipdf - Convert TeX DVI file to PDF using ghostscript and dvips"
msgstr ""
"dvipdf - конвертує файл TeX DVI у PDF за допомогою ghostscript та dvips"

#. type: SH
#: archlinux debian-bookworm debian-unstable mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "КОРОТКИЙ ОПИС"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron
#: opensuse-tumbleweed
msgid "B<dvipdf> [ I<options> ] I<input.dvi> [ I<output.pdf> ] ..."
msgstr "B<dvipdf> [ I<параметри> ] I<input.dvi> [ I<output.pdf> ] ..."

#. type: SH
#: archlinux debian-bookworm debian-unstable mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИС"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"This script invokes B<dvips>(1)  with the B<-q> option, and pipes its output "
"into B<gs>(1)  with the following options:"
msgstr ""
"Цей скрипт виконує B<dvips>(1) з параметром B<-q>, і передає виведення в "
"B<gs>(1) із наступними параметрами:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron
#: opensuse-tumbleweed
msgid "B<-q -dNOPAUSE -dBATCH -sDEVICE=pdfwrite>"
msgstr "B<-q -dNOPAUSE -dBATCH -sDEVICE=pdfwrite>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron
#: opensuse-tumbleweed
msgid "as well as B<-sOutputFile> and any options from the command-line."
msgstr "а також B<-sВихіднийФайл> та будь-які парамтери командного рядка."

#. type: SH
#: archlinux debian-bookworm debian-unstable mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "ДИВ. ТАКОЖ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron
#: opensuse-tumbleweed
msgid "gs(1), dvips(1)"
msgstr "gs(1), dvips(1)"

#. type: SH
#: archlinux debian-bookworm debian-unstable mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "VERSION"
msgstr "ВЕРСІЯ"

#. type: Plain text
#: archlinux debian-unstable mageia-cauldron
msgid "This document was last revised for Ghostscript version 10.02.0."
msgstr "Востаннє цей документ переглядався у Ghostscript версії 10.02.0."

#. type: SH
#: archlinux debian-bookworm debian-unstable mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "АВТОР"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"Artifex Software, Inc. are the primary maintainers of Ghostscript.  This "
"manpage by George Ferguson."
msgstr ""
"Основними супровідниками Ghostscript є Artifex Software, Inc.  Дана man-"
"сторінка від George Ferguson."

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "21 September 2022"
msgstr "21 вересня 2022 року"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "10.00.0"
msgstr "10.00.0"

#. type: Plain text
#: debian-bookworm
msgid "This document was last revised for Ghostscript version 10.00.0."
msgstr "Востаннє цей документ переглядався у Ghostscript версії 10.00.0."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "4 April 2022"
msgstr "4 квітня 2022"

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "9.56.1"
msgstr "9.56.1"

#. type: Plain text
#: opensuse-tumbleweed
msgid "This document was last revised for Ghostscript version 9.56.1."
msgstr "Востаннє цей документ переглядався Ghostscript версії 9.56.1."
